% Copyright (C) 2014-2019 by Thomas Auzinger <thomas@auzinger.name>

\documentclass[draft,final]{vutinfth} % Remove option 'final' to obtain debug information.

% Load packages to allow in- and output of non-ASCII characters.
\usepackage{lmodern}        % Use an extension of the original Computer Modern font to minimize the use of bitmapped letters.
\usepackage[T1]{fontenc}    % Determines font encoding of the output. Font packages have to be included before this line.
\usepackage[utf8]{inputenc} % Determines encoding of the input. All input files have to use UTF8 encoding.

% Extended LaTeX functionality is enables by including packages with \usepackage{...}.
\usepackage{amsmath}    % Extended typesetting of mathematical expression.
\usepackage{amssymb}    % Provides a multitude of mathematical symbols.
\usepackage{mathtools}  % Further extensions of mathematical typesetting.
\usepackage{microtype}  % Small-scale typographic enhancements.
\usepackage[inline]{enumitem} % User control over the layout of lists (itemize, enumerate, description).
\usepackage{multirow}   % Allows table elements to span several rows.
\usepackage{booktabs}   % Improves the typesettings of tables.
\usepackage{subcaption} % Allows the use of subfigures and enables their referencing.
\usepackage[ruled,linesnumbered,algochapter]{algorithm2e} % Enables the writing of pseudo code.
\usepackage[usenames,dvipsnames,table]{xcolor} % Allows the definition and use of colors. This package has to be included before tikz.
\usepackage{nag}       % Issues warnings when best practices in writing LaTeX documents are violated.
\usepackage[disable]{todonotes} % Provides tooltip-like todo notes.
%\usepackage{refcheck}%\nocite{*}%check if any reference is unused
\usepackage{hyperref}  % Enables cross linking in the electronic document version. This package has to be included second to last.
\usepackage[acronym,toc]{glossaries} % Enables the generation of glossaries and lists fo acronyms. This package has to be included last.
% Define convenience functions to use the author name and the thesis title in the PDF document properties.
\newcommand{\authorname}{Raphael Ecker} % The author name without titles.
\newcommand{\thesistitle}{Latency-Aware Stream Operator Placement Optimization in Fog Computing Environments} % Optimizing the Stream Operator Placement for Low Latency in Fog Computing Environments

% Set PDF document properties
\hypersetup{
    pdfpagelayout   = TwoPageRight,           % How the document is shown in PDF viewers (optional).
    linkbordercolor = {Melon},                % The color of the borders of boxes around crosslinks (optional).
    pdfauthor       = {\authorname},          % The author's name in the document properties (optional).
    pdftitle        = {\thesistitle},         % The document's title in the document properties (optional).
    pdfsubject      = {},              % The document's subject in the document properties (optional).
    pdfkeywords     = {} % The document's keywords in the document properties (optional).
}

\setpnumwidth{2.5em}        % Avoid overfull hboxes in the table of contents (see memoir manual).
\setsecnumdepth{subsection} % Enumerate subsections.

\nonzeroparskip             % Create space between paragraphs (optional).
\setlength{\parindent}{0pt} % Remove paragraph identation (optional).

\makeindex      % Use an optional index.
\makeglossaries % Use an optional glossary.
%\glstocfalse   % Remove the glossaries from the table of contents.

% Set persons with 4 arguments:
%  {title before name}{name}{title after name}{gender}
%  where both titles are optional (i.e. can be given as empty brackets {}).
\setauthor{}{\authorname}{}{male}
\setadvisor{Associate Prof. Dr.-Ing.}{Stefan Schulte}{}{male}

% For bachelor and master theses:
\setfirstassistant{}{Vasileios Karagiannis}{MSc.}{male}
%\setsecondassistant{Pretitle}{Forename Surname}{Posttitle}{male}
%\setthirdassistant{Pretitle}{Forename Surname}{Posttitle}{male}

% For dissertations:
%\setfirstreviewer{Pretitle}{Forename Surname}{Posttitle}{male}
%\setsecondreviewer{Pretitle}{Forename Surname}{Posttitle}{male}

% For dissertations at the PhD School and optionally for dissertations:
%\setsecondadvisor{Pretitle}{Forename Surname}{Posttitle}{male} % Comment to remove.

% Required data.
\setregnumber{01426241}
\setdate{\day}{\month}{\year} % Set date with 3 arguments: {day}{month}{year}.
\settitle{\thesistitle}{Titel der Arbeit} % Sets English and German version of the title (both can be English or German). If your title contains commas, enclose it with additional curvy brackets (i.e., {{your title}}) or define it as a macro as done with \thesistitle.
\setsubtitle{PROPOSAL}{PROPOSAL} % Sets English and German version of the subtitle (both can be English or German).

% Select the thesis type: bachelor / master / doctor / phd-school.
% Bachelor:
%\setthesis{bachelor}
%
% Master:
\setthesis{master}
\setmasterdegree{dipl.} % dipl. / rer.nat. / rer.soc.oec. / master
%
% Doctor:
%\setthesis{doctor}
%\setdoctordegree{rer.soc.oec.}% rer.nat. / techn. / rer.soc.oec.
%
% Doctor at the PhD School
%\setthesis{phd-school} % Deactivate non-English title pages (see below)

% For bachelor and master:
\setcurriculum{Software Engineering \& Internet Computing}{Software Engineering \& Internet Computing} % Sets the English and German name of the curriculum.

\begin{document}

\frontmatter % Switches to roman numbering.
% The structure of the thesis has to conform to the guidelines at
%  https://informatics.tuwien.ac.at/study-services
\addtitlepage{english} % English title page.
%\addtitlepage{naustrian} % German title page (not for dissertations at the PhD School).

% Select the language of the thesis, e.g., english or naustrian.
\selectlanguage{english}

% Switch to arabic numbering and start the enumeration of chapters in the table of content.
\mainmatter

\def\thesection{\arabic{section}}
\section{Problem Description}

%old section intro
%Stream Processing is a widely used software engineering pattern designed to handle and operate on large amounts of data with low latency, allowing business to react to changes and create value quickly. Stream Processing applications are commonly designed for and deployed in Cloud Computing environments. The adaption of the Internet of Things and concepts such as Smart Cities highlight limitations in this approach, which Fog Computing aims to alleviate~\cite{DBLP:journals/jnca/AssuncaoVB18,DBLP:reference/bdt/Simmhan19, DBLP:journals/jbd/NasiriNG19}. 

Stream Processing is a widely-used software engineering pattern designed to handle and operate on large amounts of data with low latency, allowing businesses to react to changes and create value quickly. It refers to a pattern of splitting a complex computation into smaller operations, which form a directed acyclic graph. In this graph, the nodes represent stream processing operators, data sources and data sinks, and the directed edges symbolize the flow of the data. These graphs model Stream Processing applications~\cite{DBLP:journals/jnca/AssuncaoVB18}.
Events are emitted from data sources, such as sensors or applications, and are then processed on the fly by continuously streaming new data items through the graph~\cite{DBLP:conf/icsoc/VeithAL18}.
Operators execute simple functions to transform the data or provide functionalities such as filtering or the joining of streams. They can be placed and replicated on different machines to handle more expensive functions or larger amounts of data. This allows the Stream Processing application to scale with the workload~\cite{DBLP:journals/jnca/AssuncaoVB18}. 
The data sinks of the application located at the sinks of the graph collect the results and transfer them to other programs, services or databases~\cite{DBLP:conf/icsoc/VeithAL18}.
Stream Processing allows low latency computations by immediately processing data instead of performing batch operations or queries like in more traditional approaches~\cite{DBLP:journals/jnca/AssuncaoVB18,DBLP:journals/computers/Shahrivari14}.



The search for the best assignment of operators to available computational resources is called the Stream Operator Placement Problem~\cite{doi:10.1002/spe.2699}. Many heuristics have been developed, which prioritize different optimization targets such as minimizing the makespan or cost of execution~\cite{DBLP:journals/jnca/AssuncaoVB18}. Stream Processing Applications are often deployed in the cloud to benefit from its scalability by leasing and releasing virtualised computational resources~\cite{DBLP:reference/bdt/ZobaedS19}.

The interest in connecting more devices to the internet to enable smart capabilities or big data analysis highlights limitations in the cloud-centric approach usually applied for Stream Processing Applications~\cite{DBLP:conf/icsoc/VeithAL18}. The Internet of Things has use cases such as Mobile e-Health or Smart Homes and Smart Factories~\cite{DBLP:journals/jbd/NasiriNG19,DBLP:journals/comsur/JavedASK18}. For such localized applications, transmitting large amounts of data to distant data centers introduces costs in the form of additional networking infrastructure. Additionally, this causes latency, which limits the support of applications requiring near real-time processing, such as virtual reality~\cite{DBLP:reference/bdt/Simmhan19}. Similar concerns exist for Smart Cities with infrastructure implementations such as Smart Energy Grids or traffic management~\cite{DBLP:journals/jbd/NasiriNG19}. 
Relying only on cloud-based data centers therefore limits the cost-efficient support of near real-time applications with low latency requirements for the Internet of Things and Smart Cities~\cite{DBLP:conf/icsoc/VeithAL18}.

Fog Computing is a new computing paradigm, which aims to alleviate these issues by exploiting computational resources between the edge of the network and the cloud. The resources form an additional intermediary architectural layer\todo{This actually depends on the definition of fog computing; we usually say that the fog is from the edge to the cloud, i.e., the resources are not only in this intermediary architectural layer.}. Fog Computing reduces the latency and networking infrastructure needs by providing computational resources located closer to the data sources, while still providing more processing power than Internet of Things (edge) devices~\cite{DBLP:reference/bdt/Simmhan19}. An example are cloudlets, which are small and powerful clusters of computational nodes distributed in proximity to the edge of the network with high bandwidth and low latency connections~\cite{DBLP:journals/pervasive/SatyanarayananBCD09}.
Similarly to the cloud, services and applications can be deployed on these fog resources using virtualization technologies~\cite{DBLP:reference/bdt/Simmhan19}.

Today, most existing Stream Operator Placement approaches are not designed for Fog Computing environments, because they do not consider latency or differences in the capabilities of available resources in their placement assignment~\cite{DBLP:reference/bdt/Simmhan19}. Furthermore, many proposed placement heuristics for the fog were merely tested in simulations and not evaluated in real-world deployments~\cite{doi:10.1002/spe.2699, DBLP:journals/jnca/AssuncaoVB18}. This thesis aims to design a Stream Operator Placement strategy, specifically designed for the requirements of the Fog Computing environments and the use of near real-time applications.

\section{Expected Results}

The thesis will research the placement of stream operators in Fog Computing environments to support near-real time applications. For this, we will conceptualize and implement a heuristic evaluated on real workloads. The thesis will primarily contribute the following three results:

\textbf{Heuristic Design}
The plan is to develop a new heuristic for the Stream Operator Placement Problem. The algorithm will be based on the existing concept of cloudlets and attempts to find locally optimal solutions~\cite{DBLP:journals/pervasive/SatyanarayananBCD09}. This is because the problem is NP-hard and therefore it is unknown if an instance can be solved in polynomial time~\cite{doi:10.1002/spe.2699}\todo{This reference does not really back up the statement made}. This can become a significant issue for instances with many available resources or operators to place.
In case resources in the search space are not sufficient, the search space will be expanded iteratively by including additional computational resources or geographical clusters such as cloudlets. This can be accomplished with different strategies, such as preferring to add resources closer to the more powerful cloud layer. Each iteration of the heuristic will be based on a constrained optimization problem, which is a modeling technique commonly used in the domain~\cite{doi:10.1002/spe.2699}.

\textbf{Implementation}
A prototype for the designed heuristic will be implemented and integrated into a modern Stream Processing framework such as Apache Spark or Flink~\cite{DBLP:conf/hotcloud/ZahariaCFSS10,DBLP:journals/debu/CarboneKEMHT15}.
A summary of software components and the overall architecture of the system will be created. Similarly, the used process model of requesting and assigning resources will be documented. 

\textbf{Evaluation}
The evaluation will answer the questions of how this heuristic compares to existing approaches and how differences in the heuristic design or implementation affect the heuristic and its performance.
Metrics collected in the evaluation will primarily consist of the makespan of a processing job, the latency of the stream application, the scheduling delay, and performance cost. Additionally, the performance of the different constrained optimization problem solvers supported by MiniZinc will be compared for this specific use case~\cite{DBLP:conf/cp/NethercoteSBBDT07}.

\newpage

\section{Methodological Approach}
To achieve the aims of this thesis and ensure the validity of the expected results, a methodology consisting of the following stages will be followed:

\textbf{Survey of the state of the art} The first step will be a survey about the fog environment and existing heuristics within it or related ones, such as the cloud. The survey will aim to answer which heuristic methods exist, which optimization goals are being researched and how the heuristics are organized, such as decentralized approaches. 

\textbf{Design}
Based on the survey, the properties of the fog environment for the heuristic to operate in will be specified and a new constrained optimization problem will be defined. This is done by adapting definitions of previous works or including new formulations. Similarly, the high-level design of the heuristic will be inspired by existing related work.

\textbf{Implementation}
The placement heuristic will be integrated into a modern Stream Processing framework. MiniZinc will be used to model and efficiently solve the constrained optimization problem. MiniZinc provides a unified constraint modeling language, which is supported by many state of the art solvers and therefore allows for their comparison~\cite{DBLP:conf/cp/NethercoteSBBDT07}. It ensures independence of specific implementations and provides the possibility of compatibility and usage with improved solvers released in the future.

\textbf{Evaluation} The evaluation of this implementation will consist of deploying a cluster and benchmarking the heuristic using existing framework specific tools or workloads such as SparkBench~\cite{DBLP:journals/cluster/LiTWZS17}. The existing implementations will be used as a baseline for performance comparisons. Repetition of experiments will account for small variations in the results caused by external influences, such as changing CPU frequencies or task scheduling of the operating system. Different workloads will be used to thoroughly collect information about the performance and limitations of the approach. The dataset will consist of taxi trajectories, which were used for the development of T-drive~\cite{DBLP:conf/kdd/YuanZXS11,DBLP:conf/gis/YuanZZXXSH10}. This data is representative of intended use cases of the fog such as Smart Cars or traffic management~\cite{DBLP:journals/jbd/NasiriNG19}. A similar dataset has previously been used to benchmark stream processing applications~\cite{DBLP:conf/debs/JerzakZ15}. Synthetic data may be used to test edge cases or additional datasets might be considered for a more diverse evaluation.

\section{State of the Art}
This section first introduces related scheduling and placement problems, before summarizing the state of the art in solvers for the Stream Operator Placement Problem by using examples of generally observable trends and specific approaches. 

The Stream Operator Placement Problem is a variant of the Job Shop Scheduling Problem and therefore NP-hard~\cite{doi:10.1002/spe.2699}. Solving such assignment problems optimally can become a significant issue for large instances, such as with many resources or operators, because it is unknown if they can be solved in polynomial time. 
Similar approaches exist in the field of Cloud Computing for the placement of containers or tasks on virtual machines. The assignment of virtual network functions, such as DNS, caches or firewalls, to networking hardware is also a related domain~\cite{DBLP:journals/comsur/LaghrissiT19}.
 
There are various strategies that can be applied to efficiently perform these calculations. At the cost of guaranteed optimality, the requirements can be relaxed in the following ways for the Stream Operator Placement Problem in the fog~\cite{doi:10.1002/spe.2699}.
The placement can be decomposed into partial problems for which the optimal solution can be efficiently computed and then merged~\cite{DBLP:journals/iotj/DengLLLL16}.
Similarly, each placement heuristic might manage a different section of the search space such as clusters of machines and calculate optimal solutions within it. Overloaded cloudlets or colonies can forward resource requests to neighbouring ones or the cloud~\cite{DBLP:conf/icfec/Skarlat00D17}. Another approach is building a hierarchy out of computational resources. Operators are assigned by calculating a placement on each level in the hierarchy until a resource at the bottom is reached~\cite{DBLP:journals/tpds/NardelliCGP19}.
Instead of calculating the optimal assignment within the search space, greedy approaches or iterative improvement strategies on existing solutions can also be used to explore it efficiently. Common examples of these are Tabu Search, Genetic Algorithms or Swarm-based Meta-heuristics, such as Ant Colony Optimization~\cite{doi:10.1002/spe.2699}.

The problem with most of these Stream Operator Placement heuristics for the fog is that their results primarily rely on simulations and are not integrated into commonly used frameworks. Furthermore, simulations only approximate real-world scenarios and insights gained may not be representative for reasons such as systematic errors in the used model.
In contrast, the results of this thesis will be based on an implementation deployed on computational resources to attempt to collect more accurate data on the performance impact.
The heuristic will use the concept of splitting the search space by relying on the concept of cloudlets. It matches the goal of supporting near real-time applications by eliminating distant resources for the search of the placement~\cite{DBLP:journals/pervasive/SatyanarayananBCD09}.
%The following paragraphs introduce stream operator placement strategies which have been tested with Apache Spark and solve different fog related problems, but are not directly designed to support the environment.


%different optimization goals? geospatial/Quadtree usage? Machine Learning models?

Accurately estimating the resource usage of a task for scheduling is difficult. Symbiosis simplifies this and instead attempts to identify if a task will be limited by the executor's network or CPU performance. It then attempts to balance both categories on executors to fully utilize available resources~\cite{DBLP:conf/infocom/JiangM0L16}. RUPAM uses a similar concept, but adds categories for memory, GPU and disk I/O usage with independent queues for each category. Additionally, the resource usage of tasks are monitored and stored to better estimate the requirements for future executions~\cite{DBLP:conf/cluster/XuBLK18}. While these approaches support heterogeneous capabilities of resources, which is relevant for the different devices expected in a fog environment, the lack of latency awareness seems to limit the approach to cloud environments~\cite{DBLP:reference/bdt/Simmhan19}. This represents one point the thesis aims to improve upon. With the placement in the fog, the same question is relevant of how extensively metrics should be collected for scheduling. Requiring additional information can limit backward compatibility with existing software~\cite{DBLP:conf/infocom/JiangM0L16}. Furthermore, additional metrics potentially only complicate the task without improving the heuristic's performance.


Sparrow is a decentralized placement heuristic to avoid the performance bottleneck of a central manager. This distributed architecture matches closely to the fog. To place a task, probes are sent to random executors to query for their resource usage. On the least-used ones, a reservation for the task is placed. Once an executor is ready, it requests the task from the heuristic. It also removes the reservations in queues of other executors if no more instances of the same task are left to be scheduled. This methodology allows to quickly place new tasks on very large clusters, because of the decentralized approach. The simple queuing strategy does not further consider heterogeneous tasks or resources and therefore causes inefficiencies with them~\cite{DBLP:conf/sosp/OusterhoutWZS13}. The support of these properties are vital for fog environments and the work at hand will therefore consider them in the model of the constrained optimization problem~\cite{DBLP:reference/bdt/Simmhan19}.

Other heuristics such as Hopper have been built upon this strategy. Hopper aims to reduce the impact of stragglers, i.e., tasks which are unexpectedly executed much slower than planned. It accomplishes this by speculatively executing an additional instance of a task and aborting the slower execution once one finishes~\cite{DBLP:journals/ccr/RenAWY15}. The probing technique of these placement heuristics has also been improved by allowing other tasks to reuse existing reservations made by the same heuristic. This technique is called probe sharing and also improves the ordering of task executions by being more resilient to the random resource usage sampling of executors~\cite{DBLP:conf/iscc/LiLZM17}. While these decentralized approaches manage to place operators very fast, the simple strategy at their core is not aware of the latency the network might introduce. 
This thesis aims to support near real-time streaming applications and therefore must also consider this latency during the placement of operators.
To accomplish this, a region- or cloudlet-based search space division will be attempted to improve on the locality of operator placements~\cite{DBLP:journals/pervasive/SatyanarayananBCD09}. Additionally, latency can be explicitly modeled in the constrained optimization problem.

\section{Relevance to the Curriculum}
This thesis contains the design and development of a software component to be used in a distributed system. It requires formal understanding and critical thinking for the definition of the optimization problem and the design of the scheduling heuristic. Various topics such as distributed systems, Stream Processing, the design of algorithms or heuristics and constraint satisfaction problems are related to this thesis and have been covered by lectures of the Software \& Information Engineering master curriculum. The following list contains some of these lectures presenting relevant knowledge or concepts:
\enlargethispage{1cm}

\begin{itemize}
	\item 184.269 Advanced Internet Computing  
	\item 180.456 Advanced Software Engineering   
	\item 184.153 Distributed Systems Engineering
	\item 184.260 Distributed Systems Technologies   
	\item 185.291 Formale Methoden der Informatik  
	%\item 185.A75 Peer-to-Peer Systems   
	\item 181.190 Problem Solving and Search in Artificial Intelligence   
	\item 194.038 Swarm-based Metaheuristics   
\end{itemize}
\backmatter
% Add a bibliography.
\bibliographystyle{alpha}
\begingroup
%\renewcommand\chaptermark[1]{\markboth{\thechapter\ #1}{}}
\renewcommand{\chapter}[2]{\section{#2}}%
\bibliography{db}
\endgroup
\end{document}