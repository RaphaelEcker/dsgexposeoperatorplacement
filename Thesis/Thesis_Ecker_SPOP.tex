% Copyright (C) 2014-2020 by Thomas Auzinger <thomas@auzinger.name>

\documentclass[draft,final]{vutinfth} % Remove option 'final' to obtain debug information.

% Load packages to allow in- and output of non-ASCII characters.
\usepackage{lmodern}        % Use an extension of the original Computer Modern font to minimize the use of bitmapped letters.
\usepackage[T1]{fontenc}    % Determines font encoding of the output. Font packages have to be included before this line.
\usepackage[utf8]{inputenc} % Determines encoding of the input. All input files have to use UTF8 encoding.

% Extended LaTeX functionality is enables by including packages with \usepackage{...}.
\usepackage{amsmath}    % Extended typesetting of mathematical expression.
\usepackage{amssymb}    % Provides a multitude of mathematical symbols.
\usepackage{pifont}%for cross/checkmark
\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

\usepackage{mathtools}  % Further extensions of mathematical typesetting.
\usepackage{microtype}  % Small-scale typographic enhancements.
\usepackage[inline]{enumitem} % User control over the layout of lists (itemize, enumerate, description).
\usepackage{multirow}   % Allows table elements to span several rows.
\usepackage{booktabs}   % Improves the typesettings of tables.
\usepackage{subcaption} % Allows the use of subfigures and enables their referencing.
\usepackage[ruled,linesnumbered,algochapter]{algorithm2e} % Enables the writing of pseudo code.
\usepackage[usenames,dvipsnames,table]{xcolor} % Allows the definition and use of colors. This package has to be included before tikz.
\usepackage{nag}       % Issues warnings when best practices in writing LaTeX documents are violated.
\usepackage{todonotes} % Provides tooltip-like todo notes.
%\usepackage{refcheck}%\nocite{*}%check if any reference is unused
\usepackage{hyperref}  % Enables cross linking in the electronic document version. This package has to be included second to last.
\usepackage[noabbrev, capitalise]{cleveref}%add nameinlink to limit to include the name as part of the reference links
\usepackage[acronym,toc,automake]{glossaries} % Enables the generation of glossaries and lists fo acronyms. This package has to be included last. %TODO: remove automake option if it causes problems
\usepackage{tikz}%for eval plots
\usepackage{pgfplots}%for eval plots
\usepackage{pgfplotstable}%for eval plots
\usepackage{lscape}%for state of the art large landscape table

\newcommand*\rot{\rotatebox{90}}


% Define convenience functions to use the author name and the thesis title in the PDF document properties.
\newcommand{\authorname}{Raphael Ecker} % The author name without titles.
\newcommand{\thesistitle}{Latency-Aware Stream Operator Placement Optimisation in Fog Computing Environments} % The title of the thesis. The English version should be used, if it exists.

% Set PDF document properties
\hypersetup{
    pdfpagelayout   = TwoPageRight,           % How the document is shown in PDF viewers (optional).
    linkbordercolor = {Melon},                % The color of the borders of boxes around crosslinks (optional).
    pdfauthor       = {\authorname},          % The author's name in the document properties (optional).
    pdftitle        = {\thesistitle},         % The document's title in the document properties (optional).
    pdfsubject      = {},              % The document's subject in the document properties (optional).
    pdfkeywords     = {} % The document's keywords in the document properties (optional).
}

\setpnumwidth{2.5em}        % Avoid overfull hboxes in the table of contents (see memoir manual).
\setsecnumdepth{subsection} % Enumerate subsections.

\nonzeroparskip             % Create space between paragraphs (optional).
\setlength{\parindent}{0pt} % Remove paragraph identation (optional).

\makeindex      % Use an optional index.
\makeglossaries % Use an optional glossary.
%\glstocfalse   % Remove the glossaries from the table of contents.

% Set persons with 4 arguments:
%  {title before name}{name}{title after name}{gender}
%  where both titles are optional (i.e. can be given as empty brackets {}).
\setauthor{}{\authorname}{BSc}{male}
\setadvisor{Prof. Dr.-Ing.}{Stefan Schulte}{}{male}

% For bachelor and master theses:
\setfirstassistant{}{Vasileios Karagiannis}{PhD}{male}
%\setsecondassistant{Pretitle}{Forename Surname}{Posttitle}{male}
%\setthirdassistant{Pretitle}{Forename Surname}{Posttitle}{male}

% For dissertations:
%\setfirstreviewer{Pretitle}{Forename Surname}{Posttitle}{male}
%\setsecondreviewer{Pretitle}{Forename Surname}{Posttitle}{male}

% For dissertations at the PhD School and optionally for dissertations:
%\setsecondadvisor{Pretitle}{Forename Surname}{Posttitle}{male} % Comment to remove.

% Required data.
\setregnumber{01426241}
\setdate{\day}{\month}{\year} % Set date with 3 arguments: {day}{month}{year}.
\settitle{\thesistitle}{\thesistitle}%Latenz berücksichtigende Stream Operatoren Platzierungs Optimierung in Fog Computing Umgebungen % Sets English and German version of the title (both can be English or German). If your title contains commas, enclose it with additional curvy brackets (i.e., {{your title}}) or define it as a macro as done with \thesistitle.
\setsubtitle{}{} % Sets English and German version of the subtitle (both can be English or German).

% Select the thesis type: bachelor / master / doctor / phd-school.
% Bachelor:
%\setthesis{bachelor}
%
% Master:
\setthesis{master}
\setmasterdegree{dipl.} % dipl. / rer.nat. / rer.soc.oec. / master
%
% Doctor:
%\setthesis{doctor}
%\setdoctordegree{rer.soc.oec.}% rer.nat. / techn. / rer.soc.oec.
%
% Doctor at the PhD School
%\setthesis{phd-school} % Deactivate non-English title pages (see below)

% For bachelor and master:
\setcurriculum{Software Engineering \& Internet Computing}{Software Engineering \& Internet Computing} % Sets the English and German name of the curriculum.
% For dissertations at the PhD School:
%\setfirstreviewerdata{Affiliation, Country}
%\setsecondreviewerdata{Affiliation, Country}
\newacronym{nist}{NIST}{National Institute of Standards and Technology}
\newacronym{iot}{IoT}{Internet of Things}
\newacronym{faas}{FaaS}{Functions as a Service}
\newacronym{saas}{SaaS}{Software as a Service}
\newacronym{paas}{PaaS}{Platform as a Service}
\newacronym{caas}{CaaS}{Containers as a Service}
\newacronym{iaas}{IaaS}{Infrastructure as a Service}
\newacronym{vnf}{VNF}{Virtualised Network Function}
\newacronym{api}{API}{Application Programming Interface}
\newacronym{mec}{MEC}{Mobile Edge Computing}
\newacronym{mcc}{MCC}{Mobile Cloud Computing}
\newacronym{sdn}{SDN}{Software-Defined Networking}
\newacronym{dag}{DAG}{directed acyclic graph}
\newacronym{csp}{CSP}{constraint satisfaction problem}
\newacronym{cop}{COP}{constrained optimisation problem}
\newacronym{qos}{QoS}{Quality of Service}
\newacronym{cdn}{CDN}{content delivery network}
\newacronym{mace}{MaCE}{Maximum Cumulative Excess}
\newacronym{sbon}{SBON}{Stream-based Overlay Network}
\newacronym{rest}{REST}{Representational State Transfer}
\newacronym{mopa}{MOPA}{Multi-operator Placement Algorithm}
\newacronym{ilp}{ILP}{Integer Linear Programming}

\creflabelformat{equation}{#2\textup{#1}#3}

%\pretocmd{\chapter}{\glsresetall}{}{}%reset glossary at chapter begin-> first occurence in chapter is always expanded
\SetKwRepeat{Do}{do}{while}

\begin{document}
\frontmatter % Switches to roman numbering.
% The structure of the thesis has to conform to the guidelines at
%  https://informatics.tuwien.ac.at/study-services

\addtitlepage{naustrian} % German title page (not for dissertations at the PhD School).
\addtitlepage{english} % English title page.
\addstatementpage

\pgfkeys{/pgf/number format/.cd,1000 sep={\,}}%pgfplots space seperator for thousands instead of "," to avoid confusion

\begin{danksagung*}
Zunächst möchte ich meinen Mitstudenten für unsere Jahre der gemeinsamen Herausforderungen danken.

Ich möchte auch meinen Betreuern Professor Stefan Schulte und Vasileios Karagiannis für ihre kontinuierliche Unterstützung bei der Erstellung dieser Arbeit danken.
Insbesondere ihre freundliche und entgegenkommende Bereitschaft zum Wissensaustausch und zur Durchführung von Reviews mit konstruktiver Kritik waren immens hilfreich.

Natürlich muss ich auch meiner Familie danken, die nicht nur diese Arbeit, sondern auch meine früheren Studien unterstützt und ertragen hat.
Letztlich ist es ihre Leistung, die es mir ermöglicht hat, diesen Lebensweg überhaupt einzuschlagen.

\end{danksagung*}

\begin{acknowledgements*}
To begin with, I want to thank my peers for our years of shared challenges.

I would also like to thank my advisors, professor Stefan Schulte and Vasileios Karagiannis, for their continued support during the writing of this thesis. In particular, their welcoming and forthcoming willingness to share knowledge and perform reviews with constructive criticisms have been immensely helpful.

Of course, I also have to thank my family, which has supported and endured not only this thesis but also my previous studies. Ultimately, it is their accomplishments that have enabled me to even take this path in life.

\end{acknowledgements*}

\begin{kurzfassung}
\input{chapters/0_abstract_de}
\end{kurzfassung}
\glsresetall

\begin{abstract}
\input{chapters/0_abstract_en}
\end{abstract}
\glsresetall
% Select the language of the thesis, e.g., english or naustrian.
\selectlanguage{english}

% Add a table of contents (toc).
\tableofcontents % Starred version, i.e., \tableofcontents*, removes the self-entry.

% Switch to arabic numbering and start the enumeration of chapters in the table of content.
\mainmatter

\chapter{Introduction}
\label{chap:introduction}
\input{chapters/1_introduction}

\chapter{Background}
\label{chap:background}
\input{chapters/2_background}

\chapter{State of the Art}
\label{chap:stateOfTheArt}
\input{chapters/3_stateOfTheArt}

\chapter{Heuristic Design}
\label{chap:heuristicDesign}
\input{chapters/4_heuristicDesign}

\chapter{Heuristic Implementation}
\label{chap:heuristicimplementation}
\input{chapters/5_heuristicImplementation}

\chapter{Evaluation}
\label{chap:evaluation}
\input{chapters/6_evaluation}

\chapter{Conclusion and Future Work}
\label{chap:conclusion}
\input{chapters/7_conclusion}


\begin{appendices}
\chapter{Configurations of the Evaluation Topologies}
\label{chap:topologies}
\input{chapters/appendix_topologies}
\end{appendices}
\backmatter



% Use an optional list of figures.
\listoffigures % Starred version, i.e., \listoffigures*, removes the toc entry.

% Use an optional list of tables.
\cleardoublepage % Start list of tables on the next empty right hand page.
\listoftables % Starred version, i.e., \listoftables*, removes the toc entry.

% Use an optional list of algorithms.
\listofalgorithms
\addcontentsline{toc}{chapter}{List of Algorithms}

% Add an index.
\printindex

% Add a glossary.
\printglossaries


% Add a bibliography.
\bibliographystyle{alpha}
\bibliography{db}
\end{document}