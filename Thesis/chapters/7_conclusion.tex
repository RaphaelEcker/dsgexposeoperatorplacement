This section summarises the work of this thesis in \cref{sec:conc_conc} and presents potential future work and research questions based on this work in \cref{sec:conc_futureWork}.

\section{Conclusion}
\label{sec:conc_conc}
Stream processing is a software engineering paradigm that offers low latency and high scalability and reliability by splitting an application into smaller operators that can be distributed across computational resources.
With the advent of the \gls{iot}, the need to process data continues to grow.
Fog computing is a new paradigm that aims to move computations closer to the sources of data or the devices relying on it and thereby offer lower latency and reduce the demand on the network caused by long-distance transmissions. 
As such, stream processing is highly applicable to the processing of \gls{iot} data and can be deployed in fog computing environments.
Ensuring the best performance with stream processing is therefore of interest, of which a significant part is how the operators are distributed: the stream processing operator placement problem.

The first steps were a discussion of the necessary background of this thesis, which also contains the collection of the different ideas, concepts and understandings of fog computing.
Additionally, an extensive survey of the state of the art in the placement of stream processing operators and related domains was made.
This information defined requirements for a placement heuristic and the assumptions on fog computing this thesis relies on.
Particularly the heterogeneous capabilities of computational resources and awareness of the capabilities of the network grow in importance with fog computing.
Additionally, the aim to realise an online scheduler to adapt to changing conditions was defined.
Apache Storm was then selected as a suitable modern framework for this thesis, in part for its easy accessibility of the scheduling \gls{api} and its usage by previous researchers.
A latency estimation component was designed, Apache Storm's operator placement model was simplified and a \gls{cop} to solve was formulated.
The \gls{cop} is generally applicable to stream processing, except for the Apache Storm-specific constraint of only a limited amount of topologies being allowed to execute on individual computational resources.
In addition to the constraints to avoid exceeding a resource's capacities, it contains a scoring function to rate potential placements.
The scoring function considers the number of co-located operators, as this allows for additional optimisations, the number of exchanged events over the network, to minimise them, the overall usage of computational resources, to consolidate the usage and allow unused resources to be shut down, and finally the latency.

For the implementation, a hill-climbing heuristic and an ant system meta-heuristic were implemented and integrated into Apache Storm, as well as a hybrid approach combining both solutions.
For the edge case of cyclic topologies, a new heuristic to select edges in cyclic graphs to remove and thereby turn them into a \gls{dag} was designed and implemented.
In contrast to the previous heuristics it was based on, it additionally considers the paths within the topology to minimise the impact of the removed edge on them.
It thereby preserves a more accurate cycle-free representation of the cyclic topology for the use case, but at the cost of increased computational complexity.
It is used to estimate the latency at the data sinks of a stream processing topology in Apache Storm for cyclic topologies, even though support for cyclic topologies is not necessarily intended or maintained in Apache Storm.
Both the architecture of the solution and the process of the implementation, as well as the difficulties with the placement heuristics and the other components, have been documented.

Additionally, it has been discussed why the gains made in research rarely progress into production use, but instead, methods such as round-robin or random assignments are still commonly used, leading to an ever-growing split between research and actual use.
Particularly, a lack of documentation, not easily accessible \glspl{api} and the actual difficulty in implementing a solution have been identified as causes that significantly raise the complexity more than one would expect for such a task.
This forms both a large barrier of entry and difficulty in verifying the correctness and maintaining implementations in the long term.

Finally, the implementation was evaluated with benchmarks of manually defined and randomly generated stream processing topologies on an emulated network.
The implementation was compared against Apache Storm's default scheduler and the Resource Aware Scheduler.
The Resource Aware Scheduler represents the state of the art with Apache Storm and is already fully integrated.
The default scheduler was significantly outperformed in any metric, except for the increased computational cost of scheduling.
In comparison to the Resource Aware Scheduler, a much more comparable performance was achieved with a trade-off that often reduces the minimum latency at the cost of the maximum latency.
Additionally, on larger topologies, a higher average throughput was measured, but on smaller topologies, the Resource Aware Scheduler generally outperforms the placements of the proposed solution at similar scales.
In general, across the various topologies and configurations, good performance was achieved more consistently with the proposed solutions. 
The capability to adapt placements to the currently observed conditions was verified and can reduce resource consumption or increase processing capabilities when needed.
Unfortunately, the solution provides little use in production, in part due to it being a prototype and because Apache Storm creates significant service interruptions when a placement changes.
This is a known problem that researchers have previously been able to solve but has not yet been integrated.

Co-location of operators has also been shown to be highly beneficial to optimise in Apache Storm, and likely other stream processing engines, but may have limited applicability to topologies where a single computational resource may not be capable of hosting multiple operators.
Co-location is very significant to the optimisation of throughput and can be used as a cheaper heuristic to minimise latency indirectly in smaller networks.
Between hill-climbing and the ant system, the placement problem, as defined and implemented, is better suited for hill-climbing.
It allowed for cheaper computations in the evaluation and is more effective at optimising latency and co-location, resulting in improved throughput.
An ant system may be more effective if the order of the solution construction of ant only considers operators with already placed neighbours, but this would also limit the exploration of the search space and potentially result in a greater difficulty in escaping local optima. 
As such, while the results have been shown to provide various improvements and a variety of observations, there is still more work left to be done, which the following section discusses.

\section{Future Work}
\label{sec:conc_futureWork}

The placement problem could be expanded to account for other properties that may become more relevant with fog computing, such as the mobility of devices, their battery availability or the availability of the device and reliability of the stream processing application.
Additionally, the network usage could be better modelled as Apache Storm does allow to provide custom definitions to which successor operators the data should be sent, which is an edge case that was not considered.
In general, new metrics or constraints can be easily added, because of the definition of the problem as a \gls{cop}.
For example, enactment costs or the operating costs of the computational resources can be easily added by extending the scoring function if such metrics are actually available.

Another limitation is that the current placement is a centralised process, while fog computing is largely decentralised.
This mismatch could be resolved by adjusting the heuristic, but it is less of a problem in smaller deployments, which could be private ones.
For example, the current centralised placement could be used to manage individual regions, such as cloudlets or geographic areas, rather than the entire cluster.
To fully decentralise the placement, large modifications would be necessary and especially the scoring function would be problematic as it requires essentially global knowledge of the topology.
Alternatively, a model could be attempted where with each topology, a scheduler dedicated to it is created on any of the resources, thereby providing the centralised knowledge, but avoiding a single resource being the bottleneck for all topologies.

A problem closely related to operator placement is deciding the replication or, in other words, the number of instances of an operator.
In Apache Storm, this is static and, as such, was not a relevant problem, but is available in other stream processing engines and, therefore, is a capability that could be added to further improve elasticity.

The largest improvement in the field of operator placement would likely not come from developing yet another scheduler but instead from improving the environment of this domain.
Most schedulers use relatively similar information, resource request, network usage rates, selectivity, network link latencies and so on, while also working with \glspl{dag} that form the basis for stream processing.
This means that there is the potential to attempt to create a more standardised \gls{api} that could then be either integrated directly into stream processing engines or as an adapter to existing \glspl{api}.
While this would mean losing some specificity by creating placements independent of specific stream processing engines and likely miss some specific information, because of new limitations,  it would externalise the placement of operators from the actual stream processing engines.
This would mean that solutions would not have to be specific to an engine and, as such, could be far easier to reproduce results or compare them among researchers.
Additionally, an external component is easier to release, especially when the \gls{api} is standardised, intended for this usage and documented.
This would not only allow for a standardisation of emulators or simulators to reduce the need for costly benchmarks, but also reduce the barrier of entry and difficulty of developing and maintaining a scheduler. 
The largest problem with the operator placement problem at this time seems not to be developing better schedulers anymore, but having any sort of improvement moving into production use, because there the state of the art is still often random scheduling or round-robin assignments.
As much as such a design and integration into various projects would be a monumental effort, so would the gains likely be in the long term.






