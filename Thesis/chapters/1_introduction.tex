\section{Motivation}
In computing, there has always been a drive to perform calculations quicker or more efficiently.
As such, many applications across industries rely on near real-time computations.
These can be split into two types: 
Applications, which accomplish their aims better by shortening the delay incurred by computations, resulting in benefits such as financial gains or better usability.
In many scenarios, such as monitoring or trading on the stock market, any data and the potential to act on it is worth the most when it is first collected and loses in value as it ages~\cite{CappellariRC16_RealtimeAnalyticsProductMonitoringStockTradingFraudDetection}.
The second type of applications requires near real-time results to function correctly.
This could be factory automations or autonomous driving with safety concerns or fraud detection systems for financial transfers and payments~\cite{CollinSIRTW20_AutonomousDrivingLatency,VicentiniGT14_FactoryAutomationRobotLatencySafety,AxenieTBHSB19_streamingFraudDetection}.
Fog computing and stream processing are technologies that have been established with the aim of supporting near real-time computations~\cite{IEEE_FogStandard,AssuncaoVB18_StreamProcessingEdgeSurvey}.

Fog computing is a computing paradigm of an infrastructure of computational resources available for rent, which, among other benefits, explicitly aims to support real-time computations.
It is the logical continuation of the widely successful cloud computing with the aim of solving some shortcomings that have been found.
Fog computing aims to provide geographically distributed computational resources in comparison to the centralised data centres of cloud computing.
This geographic distribution allows it to provide computational resources, which are located closer to data sources or users.
The reduced distances thereby also reduce the network latency involved when transferring data, providing new potential for real-time computations~\cite{IEEE_FogStandard}.

Stream processing is a software engineering pattern for performing highly scalable computations with low latency.
In stream processing, calculations are made by performing transformations on input data.
A stream processing application is built out of a network of such transforming operations, which can all be executed concurrently.
Any data to be processed is then simply streamed through this network of operations, with a stream of computational results exiting it.
As such, this pattern allows for software that is highly scalable due to the inherent concurrency and provides near real-time computations, because any input has only to be routed through and transformed by the network.
Furthermore, the operations can be distributed by executing them on separate computational resources~\cite{AssuncaoVB18_StreamProcessingEdgeSurvey}.

This process creates the obvious question of how the operators should best be distributed on available computational resources: the stream operator placement problem~\cite{VarshneyS20_CharacterizingSchedulingEdgeFogCloud}.
To utilise the full potential fog computing and stream processing provide in supporting near real-time computations, this point where both technologies intersect must similarly be considered.
As such, it is necessary to optimise the placement of stream operators with the same goal in mind. 
With the geographical distribution of fog computing, the distance and, therefore also, network latency between computational resources gets larger.
For this reason, the impact of network latency is more significant when an application is distributed among resources, in comparison to centralised cloud data-centres.
Therefore, this thesis aims to develop a latency-aware stream operator placement optimisation for fog computing environments, to better support the applications desired by the industry with near real-time computational needs.

\section{Aims of the Thesis}
The consideration of the network and latency is not a new concept for the placement of stream processing operators, but one which is gaining interest because of fog computing.
The primary aim is to create a new heuristic to solve the stream operator placement problem in fog computing environments.
The following problems and research questions are answered or solved and represent the results of the thesis:

\subsubsection{Requirements Analysis} 
To select a stream processing framework for the implementation of a new placement heuristic as well as the design of it, is necessary to understand the requirements of this task and the fog computing environment. This can be formulated as the following research question:
``What technical requirements are necessary to be fulfilled for the placement of operators in a fog computing environment?''

\subsubsection{Design}
Before implementing a placement heuristic, the design has to be defined first. 
The design has to consider both the previously specified requirements and how to achieve the best results in a fog computing environment.
The research questions to be answered with this result are the following:
``What design choices can be made or are suited for fog computing environments? How should operators be placed on computational resources?''

\subsubsection{Implementation}
The primary aim and artefact created during the work on this thesis is the implementation of the placement heuristic.
The implementation follows the design and has to fulfil the specified requirements.
This step answers these research questions:
``What stream processing framework is best suited for the implementation? How can the designed heuristic be implemented and what is its architecture?''

\subsubsection{Evaluation and Comparison of Implementation} 
To ascertain the quality of the proposed placement heuristic, it has to be evaluated in detail.
For this, a quantitative evaluation is performed by deploying the placement heuristic and benchmarking it with representative workloads. 
The research questions to be answered by this approach are the following:
``What is the quality of the found operator placements? How scalable is the heuristic with an increase in the problem size? How efficient is the heuristic in finding placements? How does this approach compare to previous works?''



\section{Methodology}

The methodology to answer the research questions and achieve the aims of this thesis, while ensuring the validity of results consists of the following steps:

\subsubsection{Literature Survey}

The literature survey aims first to establish a baseline understanding and collection of definitions for various concepts which form the necessary background for this thesis.
As such, stream processing, the \gls{iot}, as well as cloud and fog computing are researched and explained.
In particular, the relations and interactions of these paradigms as well as differences in definitions or common understanding are of interest.

Following this, the optimisation of stream processing applications and the stream operator placement problem are discussed.
This includes the varying definitions used for the stream operator placement problem.
Similarly, differences in placement algorithms and heuristics are summarised and broadly categorised into characteristics before concrete implementations are introduced.
For these implementations, there is both the aim to showcase a wide spectrum of different ideas as well as focusing on the ones most relevant to fog computing and network awareness.
To finish the survey, related placement problems in other areas of research are presented and compared to the stream operator placement problem.


\subsubsection{Design}
For the design of the placement heuristic, a requirements analysis is performed.
Similarly, the intended use case and specific assumptions about the fog environment and resulting limitations are clarified.
The design includes the solution architecture, such as a component to measure network metrics and the placement heuristic, as well as their general interaction.
Furthermore, the constraints and optimisation function used by the placement heuristic are formalised and details of the strategy are defined. 
The design, in particular, aims to improve on the state of the art of network-aware placement techniques.

\subsubsection{Implementation}
Based on the design, a Java implementation of the placement heuristic is integrated into the popular stream processing framework Apache Storm~\cite{ToshniwalTSRPKJGFDBMR14_StormAtTwitter}.
The selection of this framework and a broad overview of alternatives and their strengths, weaknesses and practical relevancy to this thesis are presented.
The placement heuristic, in combination with components to measure network and operator metrics, allows the optimisation of the placement of stream processing operators without requiring changes to internal interfaces to ensure the general compatibility of the approach.


\subsubsection{Evaluation}
The proposed implementation is benchmarked in an emulated fog computing network with various workloads and compared against existing solutions.
The resulting placements of operators are analysed using metrics such as throughput, resource consumption and latency.
Individual experiments are repeated to allow for the aggregation of the results and to understand the consistency with which they can be achieved.

\section{Thesis Structure}
This thesis is structured as follows.
\Cref{chap:background} introduces the concepts of cloud computing, fog computing and stream processing with the \acrfull{iot} as a use case driving these developments.
Additionally, the background on optimising stream processing applications is included in the chapter.
The definition of the stream operator placement problem and a survey of algorithms and heuristics to solve it are provided in \cref{chap:stateOfTheArt}.
\Cref{chap:heuristicDesign} contains the requirements analysis, fog environment definition, framework selection and design of the heuristic used to create the novel implementation of this thesis.
\Cref{chap:heuristicimplementation} presents the details of the implementation itself.
In \cref{chap:evaluation}, the test procedure and environment are specified and the measured performance in addition to further analysis are imparted.
\Cref{chap:conclusion} contributes a summary of the achieved results and a discussion on alternative approaches or open and follow-up research questions.