#!/bin/bash
echo "$USER"
if [ -f "already_ran" ]; then
    echo "Already ran the Entrypoint once. Holding indefinitely for debugging."
    cat
fi
touch already_ran
set -e

# Allow the container to be started with `--user`
if [ "$1" = 'storm' -a "$(id -u)" = '0' ]; then
    chown -R storm:storm "$0" "$STORM_CONF_DIR" "$STORM_DATA_DIR" "$STORM_LOG_DIR" /latencyEstimation.jar
	rm -f already_ran
    exec gosu storm "$0" "$@"
fi

# Generate the config only if it doesn't exist
CONFIG="$STORM_CONF_DIR/storm.yaml"
if [ ! -f "$CONFIG" ]; then
    cat << EOF > "$CONFIG"
storm.zookeeper.servers: [zookeeper]
nimbus.seeds: [nimbus]
storm.log.dir: "$STORM_LOG_DIR"
storm.local.dir: "$STORM_DATA_DIR"
EOF
fi
java -jar /latencyEstimation.jar &>../logs/latenyEstimationLog.txt &

exec "$@"&>../logs/executionLog.txt