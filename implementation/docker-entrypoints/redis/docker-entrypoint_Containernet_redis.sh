#!/bin/bash
echo "$USER"
if [ -f "already_ran" ]; then
    echo "Already ran the Entrypoint once. Holding indefinitely for debugging."
    cat
fi
touch already_ran
set -e
echo "script executing">>/logRedis.txt
# first arg is `-f` or `--some-option`
# or first arg is `something.conf`
if [ "${1#-}" != "$1" ] || [ "${1%.conf}" != "$1" ]; then
	set -- redis-server "$@"
fi

# allow the container to be started with `--user`
if [ "$1" = 'redis-server' -a "$(id -u)" = '0' ]; then
	find . \! -user redis -exec chown redis '{}' +
	chown -R redis "$0" /logRedis.txt /usr/local/etc/redis/redis.conf
	rm -f already_ran
	exec gosu redis "$0" "$@" 
	#>>logRedis.txt 2>&1
fi

exec "$@" 
#>>/logRedis.txt 2>&1