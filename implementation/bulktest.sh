#!/bin/bash
declare -a staticSchedulers=("DefaultScheduler" "ResourceAwareScheduler")
declare -a onlineSchedulers=("LocalSearch" "AntSystem" "HybridSearch")

if [ -d "ExperimentalResults" ]; then
	mkdir ExperimentalResults
fi
./fix_containernet_messes.sh
mn -c
(cd output && ./cleanup.sh)
for j in {1..5}
do
	for i in {1..10}
	do
		for topology in {-3,-2,-1,1,2,3,4,5,6,7,8,9}
		do
			for scheduler in "${staticSchedulers[@]}"
			do
				for workers in {3,7,11}
				do
					path="$topology/$scheduler/workers$workers/$i"
					echo "$path"
					if [ -d "ExperimentalResults/$path" ]; then
						continue
					fi
					now="$(date +"%T")"
					echo $now
					now="${now//:/_}"
					timeout --foreground --preserve-status 7m python3 containernet_storm.py $scheduler $topology $workers>> output/containernet.txt 2>&1 
					exit_status=$?
					./fix_containernet_messes.sh >> output/containernet.txt 2>&1 
					mn -c >> output/containernet.txt 2>&1 
					if [[ $exit_status -ne 0 ]]||[ -f "output/testdriver/failedRun.txt" ]; then
						mkdir -p "ExperimentalResults/Errors/$path/$now"
						cp -r output/* "ExperimentalResults/Errors/$path/$now"
						rm "ExperimentalResults/Errors/$path/$now/cleanup.sh"
					else
						mkdir -p "ExperimentalResults/$path"
						cp -r output/* "ExperimentalResults/$path/"
						rm "ExperimentalResults/$path/cleanup.sh"
					fi
					(cd output && ./cleanup.sh)
				done
			done
			for scheduler in "${onlineSchedulers[@]}"
			do
				path="$topology/$scheduler/$i"
				echo "$path"
				if [ -d "ExperimentalResults/$path" ]; then
					continue
				fi
				now="$(date +"%T")"
				echo $now
				now="${now//:/_}"
				timeout --foreground --preserve-status 20m python3 containernet_storm.py $scheduler $topology >> output/containernet.txt 2>&1 
				exit_status=$?
				./fix_containernet_messes.sh >> output/containernet.txt 2>&1 
				mn -c >> output/containernet.txt 2>&1 
				if [[ $exit_status -ne 0 ]]||[ -f "output/testdriver/failedRun.txt" ]; then
					echo "detected error"
					mkdir -p "ExperimentalResults/Errors/$path/$now"
					cp -r output/* "ExperimentalResults/Errors/$path/$now"
					rm "ExperimentalResults/Errors/$path/$now/cleanup.sh"
				else
					mkdir -p "ExperimentalResults/$path"
					cp -r output/* "ExperimentalResults/$path/"
					rm "ExperimentalResults/$path/cleanup.sh"
				fi
				(cd output && ./cleanup.sh)
			done
		done
	done
done
