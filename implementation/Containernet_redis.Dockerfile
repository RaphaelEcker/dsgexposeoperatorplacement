FROM redis
RUN apt-get update && apt-get install -y \
        bash \
        iproute2 \
		iputils-ping \
		net-tools \
		netcat \
		dos2unix && \
		apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/*
		
COPY docker-entrypoints/redis/docker-entrypoint_Containernet_redis.sh /usr/local/bin/docker-entrypoint.sh
RUN dos2unix /usr/local/bin/docker-entrypoint.sh
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
COPY redis.conf /usr/local/etc/redis/redis.conf
EXPOSE 6379