package util.CsvMerger.phases;

import util.CsvMerger.Columns;

import java.util.*;

public class MeasureMedianMaxThroughputPhase extends Phase{
    public MeasureMedianMaxThroughputPhase(int index, List<List<String[]>> phasesCSV){
        super.index=index;
        super.phasesCSV=phasesCSV;
    }

    @Override
    public String parse() {
        List<String[]> lines=phasesCSV.get(super.index);
        List<Integer> throughputs=new ArrayList<>();
        for (String[] columns:lines){
            int currentThroughput=Integer.parseInt(columns[Columns.THROUGHPUT.ordinal()]);
            if (currentThroughput>0) {
                int minLatency = Integer.parseInt(columns[Columns.MIN.ordinal()]);
                if (minLatency >= 300) {
                    throughputs.add(currentThroughput);
                }
            }
        }

        if (!throughputs.isEmpty()){
            return ""+median(throughputs);
        }
        return "";
    }

    private long median(List<Integer> values) {
        Collections.sort(values);
        if (values.size() % 2 == 0) {
            return values.get(values.size() / 2) + values.get(values.size() / 2 - 1) / 2;
        } else {
            return values.get(values.size() / 2);
        }
    }

    @Override
    public String getHeader() {
        return "medianMaxThroughput";
    }
}
