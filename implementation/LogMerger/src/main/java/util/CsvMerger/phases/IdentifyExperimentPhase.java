package util.CsvMerger.phases;

import util.ExperimentFinder.ExperimentIdentifier;

import java.nio.file.Path;

public class IdentifyExperimentPhase extends Phase {
    private String path;
    public IdentifyExperimentPhase(ExperimentIdentifier e, Path experimentsRootFolder) {
        super();
        path=experimentsRootFolder.relativize(e.getPath()).toString();
    }

    @Override
    public String parse() {
        return path;
    }

    @Override
    public String getHeader() {
        return "experiment";
    }
}
