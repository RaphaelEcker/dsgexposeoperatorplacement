package util.CsvMerger.phases;

import util.CsvMerger.Columns;
import util.ExperimentFinder.ExperimentIdentifier;

import java.util.List;
import java.util.ListIterator;

public class MeasureLatencyPhase extends Phase{
    ExperimentIdentifier e;
    public MeasureLatencyPhase(int index, List<List<String[]>> phasesCSV, ExperimentIdentifier e){
        super.index=index;
        super.phasesCSV=phasesCSV;
        this.e=e;
    }

    @Override
    public String parse() {
        List<String[]> lines=phasesCSV.get(super.index);
        ListIterator<String[]> it=lines.listIterator(lines.size());
        while (it.hasPrevious()) {
            String[] columns=it.previous();
            if (columns[Columns.MIN.ordinal()].equals(columns[Columns.MESSAGE.ordinal()])){
                if (Integer.parseInt(columns[Columns.MIN.ordinal()])>1000) System.err.println("Warning: high min latency:"+columns[Columns.MIN.ordinal()]+" in "+e.getPath().toString());
                return columns[Columns.MIN.ordinal()]+";"+columns[Columns.MAX.ordinal()]+";"+columns[Columns.AVG.ordinal()]+";"+columns[Columns.NINETYPERCENTILE.ordinal()]+";"+columns[Columns.NINETYFIVEPERCENTILE.ordinal()]+";"+columns[Columns.NINETYNINEPERCENTILE.ordinal()]+";"+columns[Columns.WORKERS.ordinal()]+";"+columns[Columns.HOSTS.ordinal()]+";"+columns[Columns.SMALLHOSTS.ordinal()]+";"+columns[Columns.MIDHOSTS.ordinal()]+";"+columns[Columns.LARGEHOSTS.ordinal()];
            }
        }
        return ";;;;;;;;;;";
    }

    @Override
    public String getHeader() {
        return "min;max;avg;90percentile;95percentile;99percentile;workers;hosts;smallHosts;midHosts;largeHosts";
    }
}
