package util.CsvMerger.phases;

import util.CsvMerger.Columns;

import java.util.Iterator;
import java.util.List;

public class MaxDowntimePhase extends Phase{
    public MaxDowntimePhase(List<List<String[]>> phasesCSV){
        super.phasesCSV=phasesCSV;
    }

    @Override
    public String parse() {
        int currentDownTime=0;
        int maxDownTime=0;
        int totalDownTime=0;
        for (List<String[]>lines:phasesCSV) {
            Iterator<String[]> it = lines.iterator();
            while (it.hasNext()) {
                String[] columns = it.next();
                if (columns[Columns.MIN.ordinal()].equals("9223372036854775807")&&!columns[Columns.SENDRATE.ordinal()].equals("0")) {
                    currentDownTime+=Integer.parseInt(columns[Columns.DELTA.ordinal()]);
                    maxDownTime=Math.max(maxDownTime,currentDownTime);
                    totalDownTime+=Integer.parseInt(columns[Columns.DELTA.ordinal()]);
                }else{
                    currentDownTime=0;
                }
            }
        }
        return ""+maxDownTime+";"+totalDownTime;

    }

    @Override
    public String getHeader() {
        return "maxDowntime;totalDownTime";
    }
}
