package util.CsvMerger.phases;

import util.CsvMerger.Columns;

import java.util.List;
import java.util.ListIterator;

public class MeasureThroughputPhase extends Phase{
    public MeasureThroughputPhase(int index, List<List<String[]>> phasesCSV){
        super.index=index;
        super.phasesCSV=phasesCSV;
    }

    @Override
    public String parse() {
        List<String[]> lines=phasesCSV.get(super.index);
        int sustainableThroughput=1;
        int lastSendRate=0;
        int minAvgLatency=Integer.MAX_VALUE;
        for (String[] columns:lines){
            int currentSendRate=Integer.parseInt(columns[Columns.SENDRATE.ordinal()]);
            int avgLatency=Integer.parseInt(columns[Columns.AVG.ordinal()]);

            minAvgLatency=Math.min(minAvgLatency,avgLatency);
            if (lastSendRate<currentSendRate&&avgLatency<=minAvgLatency+100){
                sustainableThroughput=currentSendRate;
            }else{
                break;
            }
            lastSendRate=currentSendRate;
        }
        ListIterator<String[]> it=lines.listIterator(lines.size());
        while (it.hasPrevious()) {
            String[] columns=it.previous();
            if (columns[Columns.THROUGHPUT.ordinal()].equals(columns[Columns.MESSAGE.ordinal()])){
                return columns[Columns.THROUGHPUT.ordinal()]+";"+sustainableThroughput;
            }
        }
        return ";";
    }

    @Override
    public String getHeader() {
        return "throughput;sustainableInput";
    }
}
