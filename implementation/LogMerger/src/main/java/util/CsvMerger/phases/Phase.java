package util.CsvMerger.phases;

import java.util.List;

public abstract class Phase {
    protected int index;
    protected List<List<String[]>> phasesCSV;
    public abstract String parse();

    public abstract String getHeader();
}
