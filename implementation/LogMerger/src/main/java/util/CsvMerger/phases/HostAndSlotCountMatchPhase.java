package util.CsvMerger.phases;

import util.CsvMerger.Columns;
import util.ExperimentFinder.ExperimentIdentifier;

import java.util.Iterator;
import java.util.List;

public class HostAndSlotCountMatchPhase extends Phase{
    private ExperimentIdentifier e;
    public HostAndSlotCountMatchPhase(ExperimentIdentifier e,List<List<String[]>> phasesCSV){
        super.phasesCSV=phasesCSV;
        this.e=e;
    }

    @Override
    public String parse() {
        for (List<String[]>lines:phasesCSV) {
            Iterator<String[]> it = lines.iterator();
            while (it.hasNext()) {
                String[] columns = it.next();
                if (!columns[Columns.HOSTS.ordinal()].equals(columns[Columns.WORKERS.ordinal()])) {
                    System.out.println("host and slotcount mismatch:"+e.getSolver()+":"+e.getTopologyId());
                    return "0";
                }
            }
        }
        return "1";

    }

    @Override
    public String getHeader() {
        return "hostsAndWorkersMatches";
    }
}
