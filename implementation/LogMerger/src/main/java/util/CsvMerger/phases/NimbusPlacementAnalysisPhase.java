package util.CsvMerger.phases;

import util.CsvMerger.Columns;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class NimbusPlacementAnalysisPhase extends Phase{
    public NimbusPlacementAnalysisPhase(List<List<String[]>> phasesCSV){
        super.phasesCSV=phasesCSV;
    }

    @Override
    public String parse() {
        int totalPlacements=0;
        int totalIterations=0;
        int totalTimeMS=0;
        int lastTime=-1;
        int calculatedPlacementCount=0;
        for (List<String[]>lines:phasesCSV) {
            Iterator<String[]> it = lines.iterator();
            while (it.hasNext()) {
                String[] columns = it.next();
                if (columns[Columns.CHANGEDASSIGNMENT.ordinal()].equals("1")) totalPlacements++;
                if (!columns[Columns.NIMBUSTIMEMS.ordinal()].equals("null")&&!columns[Columns.NIMBUSTIMEMS.ordinal()].equals(lastTime+"")) {
                    lastTime= Integer.parseInt(columns[Columns.NIMBUSTIMEMS.ordinal()]);
                    totalTimeMS+=lastTime;
                    totalIterations+=Integer.parseInt(columns[Columns.NIMBUSITERATIONS.ordinal()]);
                    calculatedPlacementCount++;
                }
            }
        }
        if (lastTime==-1){
            return ";;;;;";
        }else{
            return totalPlacements+";"+calculatedPlacementCount+";"+totalIterations/(double)calculatedPlacementCount+";"+totalTimeMS/(double)calculatedPlacementCount+";"+totalIterations+";"+totalTimeMS;
        }

    }

    @Override
    public String getHeader() {
        return "totalPlacements;totalPlacementComputations;avgIterations;avgTimeMS;totalIterations;totalTimeMS";
    }
}
