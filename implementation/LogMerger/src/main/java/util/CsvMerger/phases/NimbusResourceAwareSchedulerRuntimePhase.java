package util.CsvMerger.phases;

import util.ExperimentFinder.ExperimentIdentifier;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NimbusResourceAwareSchedulerRuntimePhase extends Phase {
    private ExperimentIdentifier e;
    public NimbusResourceAwareSchedulerRuntimePhase(ExperimentIdentifier e) {
        super();
        this.e=e;
    }

    @Override
    public String parse() {
        int ms=-1;

        try {
            List<String> lines= Files.readAllLines(e.getPath().resolve("nimbus_logs\\nimbus.log"), Charset.defaultCharset());
            Pattern runtimeRegex=Pattern.compile("state\\.elapsedtime=(\\d+),");
            for (String line:lines){
                Matcher matcher=runtimeRegex.matcher((line));

                if (matcher.find()) {
                    String time = matcher.group(1);
                    ms=Integer.parseInt(time);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "1;1;1;"+ms+";1;"+ms;
    }

    @Override
    public String getHeader() {
        return "totalPlacements;totalPlacementComputations;avgIterations;avgTimeMS;totalIterations;totalTimeMS";
    }
}
