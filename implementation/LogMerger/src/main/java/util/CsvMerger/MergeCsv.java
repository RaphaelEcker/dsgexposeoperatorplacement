package util.CsvMerger;

import util.CsvMerger.phases.*;
import util.ExperimentFinder.ExperimentIdentifier;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class MergeCsv {
    private Set<String> processedPaths=new HashSet<>();
    public void merge(List<ExperimentIdentifier> experiments, Path experimentsRootFolder){
        for (ExperimentIdentifier e:experiments){
            if (e.getError()) continue;
            Path parent=e.getPath().getParent();
            String pFolder=parent.toString();
            if (!processedPaths.contains(pFolder)){
                processedPaths.add(pFolder);
                createCSV(parent,e, experimentsRootFolder);
            }
        }
    }
    private void createCSV(Path parent, ExperimentIdentifier e,Path experimentsRootFolder){
        List<List<Phase>> summaries=new ArrayList<>();
        for (File f : parent.toFile().listFiles()) {
            Path p=parent.resolve(f.getName());
            ExperimentIdentifier adaptedExIdentifier=new ExperimentIdentifier(p,e.getTopologyId(),e.getSolver(),f.getName(),e.getWorkers(),e.getError());
            if (f.isDirectory()) summaries.add(summariseRun(p,adaptedExIdentifier, experimentsRootFolder));
        }
        writeCSV(parent,formatSummaries(summaries,e));
    }

    private String formatSummaries(List<List<Phase>> summaries,ExperimentIdentifier e) {
        StringBuilder output= new StringBuilder();
        StringBuilder header= new StringBuilder();
        if (summaries.size()>0){
            for (Iterator<Phase> it=summaries.get(0).iterator();it.hasNext();){
                Phase p=it.next();
                header.append(p.getHeader());
                if (it.hasNext()) header.append(";");
            }
        }
        for (List<Phase> l:summaries){
            for (Iterator<Phase> it=l.iterator();it.hasNext();){
                Phase p=it.next();
                output.append(p.parse());
                if (it.hasNext()) output.append(";");
            }
            output.append("\n");
        }
        return header.toString()+"\n"+output.toString();
    }

    private List<Phase> summariseRun(Path runFolder, ExperimentIdentifier e, Path experimentsRootFolder) {
        Path csvPath=runFolder.resolve("testdriver/defaultExperimentName.csv");
        String content = "";
        List<String> lines;
        try {
            //content = Files.readString(csvPath, Charset.defaultCharset());
            lines=Files.readAllLines(csvPath,Charset.defaultCharset());
        } catch (IOException ex) {
            ex.printStackTrace();
            return new ArrayList<>();
        }
        List<List<String[]>> phasesCSV=new ArrayList<>();
        //String[] splitString=content.split("\n");
        //List<String> lines= new ArrayList<>(List.of(splitString));
        lines.remove(0);

        for (String line:lines){
            String[] columns=line.split(";");
            int index=Integer.parseInt(columns[Columns.TESTPHASE.ordinal()]);
            List<String[]> phaseLines;
            if (index>=phasesCSV.size()){
                phaseLines = new ArrayList<>();
                phasesCSV.add(phaseLines);
            }else {
                phaseLines = phasesCSV.get(index);
            }
            phaseLines.add(columns);
        }
        List<Phase> phases=initialisePhaseParsing(e, phasesCSV, experimentsRootFolder);
        return phases;
    }

    private List<Phase> initialisePhaseParsing(ExperimentIdentifier e, List<List<String[]>> phasesCSV, Path experimentsRootFolder) {
        ArrayList<Phase> phases = new ArrayList<>();
        int phaseCount = phasesCSV.size();
        phases.add(new IdentifyExperimentPhase(e, experimentsRootFolder));
        phases.add(new MaxDowntimePhase(phasesCSV));
        phases.add(new HostAndSlotCountMatchPhase(e,phasesCSV));
        if (phaseCount==8){
            //cyclic topologies
            phases.add(new MeasureLatencyPhase(7,phasesCSV,e));
            String solver=e.getSolver();
            if (solver.equals("AntSystem")||solver.equals("LocalSearch")||solver.equals("HybridSearch")) {
                phases.add(new NimbusPlacementAnalysisPhase(phasesCSV));
            }
        } else if (phaseCount==9) {
            //static placement
            phases.add(new MeasureLatencyPhase(7,phasesCSV,e));
            phases.add(new MeasureThroughputPhase(8,phasesCSV));
            phases.add(new MeasureMedianMaxThroughputPhase(8,phasesCSV));
        }else if (phaseCount==31){
            phases.add(new MeasureLatencyPhase(7,phasesCSV,e));
            phases.add(new MeasureThroughputPhase(8,phasesCSV));
            phases.add(new MeasureMedianMaxThroughputPhase(8,phasesCSV));
            phases.add(new MeasureLatencyPhase(18,phasesCSV,e));
            phases.add(new MeasureThroughputPhase(19,phasesCSV));
            phases.add(new MeasureMedianMaxThroughputPhase(19,phasesCSV));
            phases.add(new MeasureLatencyPhase(29,phasesCSV,e));
            phases.add(new MeasureThroughputPhase(30,phasesCSV));
            phases.add(new MeasureMedianMaxThroughputPhase(30,phasesCSV));
            phases.add(new NimbusPlacementAnalysisPhase(phasesCSV));
        }else{
            System.err.println("incorrect phase Count:"+phaseCount+" in "+e.getPath().toString());
        }
        if (e.getSolver().equals("ResourceAwareScheduler")){
            phases.add(new NimbusResourceAwareSchedulerRuntimePhase(e));
        }
        return phases;
    }

    private void writeCSV(Path parent, String content){
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(parent.resolve("summary.csv").toFile(), false));
            bw.write(content);
            bw.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
