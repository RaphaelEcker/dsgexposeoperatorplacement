package util.ExperimentFinder;

import java.nio.file.Path;

public class ExperimentIdentifier {
    private Path path;
    private String topologyId;
    private String solver;
    private String run;
    private String workers;
    private Boolean error;

    public ExperimentIdentifier(Path path,
                                String topologyId,
                                String solver,
                                String run,
                                String workers,
                                Boolean error) {
        this.path = path;
        this.topologyId = topologyId;
        this.solver = solver;
        this.run = run;
        this.workers = workers;
        this.error = error;
    }

    public Path getPath() {
        return path;
    }

    public String getTopologyId() {
        return topologyId;
    }

    public String getSolver() {
        return solver;
    }

    public String getRun() {
        return run;
    }

    public String getWorkers() {
        return workers;
    }

    public Boolean getError() {
        return error;
    }

    @Override
    public String toString() {
        return "ExperimentIdentifier{" +
                "path=" + path +
                ", topologyId='" + topologyId + '\'' +
                ", solver='" + solver + '\'' +
                ", run='" + run + '\'' +
                ", workers='" + workers + '\'' +
                ", error=" + error +
                '}';
    }
}
