package util.ExperimentFinder;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class ExperimentFinder {
    public List<ExperimentIdentifier> search(Path root){
        List<ExperimentIdentifier> ret=new ArrayList<>();
        for (File f:root.toFile().listFiles()){
            if (f.getName().endsWith(".dat")||f.getName().endsWith(".dat#")) continue;
            if (!f.getName().equals("Errors")){
                ret.addAll(searchTopology(root.resolve(f.getName()),false, f.getName()));
            }else{
                ret.addAll(searchErrors(root.resolve(f.getName())));
            }
        }
        return ret;
    }
    private List<ExperimentIdentifier> searchTopology(Path root, boolean error, String topId){
        List<ExperimentIdentifier> ret=new ArrayList<>();
        for (File f:root.toFile().listFiles()){
            if (f.getName().endsWith(".dat")||f.getName().endsWith(".dat#")) continue;
            ret.addAll(searchSolver(root.resolve(f.getName()),error,topId,f.getName()));
        }
        return ret;
    }

    private List<ExperimentIdentifier> searchSolver(Path root, boolean error, String topId,String solverName) {
        List<ExperimentIdentifier> ret=new ArrayList<>();
        for (File f:root.toFile().listFiles()){
            if (f.getName().startsWith("workers")){
                ret.addAll(searchRuns(root.resolve(f.getName()),error,topId,solverName,f.getName()));
            }else{
                ret.addAll(searchRuns(root,error,topId,solverName,""));
            }
        }
        return ret;
    }
    private List<ExperimentIdentifier> searchRuns(Path root, boolean error, String topId,String solverName,String workers) {
        List<ExperimentIdentifier> ret=new ArrayList<>();
        if (error) {
            for (File f : root.toFile().listFiles()) {
                ret.addAll(searchErrorRuns(root.resolve(f.getName()),topId,solverName,workers));
            }
        }else{
            for (File f : root.toFile().listFiles()) {
                if (f.isDirectory()) {
                    ExperimentIdentifier e = new ExperimentIdentifier(root.resolve(f.getName()), topId, solverName, f.getName(), workers, false);
                    ret.add(e);
                }
            }
        }
        return ret;
    }

    private List<ExperimentIdentifier> searchErrorRuns(Path root, String topId,String solverName,String workers) {
        List<ExperimentIdentifier> ret=new ArrayList<>();
        for (File f : root.toFile().listFiles()) {
            if (f.isDirectory()) {
                ExperimentIdentifier e = new ExperimentIdentifier(root.resolve(f.getName()), topId, solverName, f.getName(), workers, true);
                ret.add(e);
            }
        }
        return ret;
    }


    private List<ExperimentIdentifier> searchErrors(Path root){
        List<ExperimentIdentifier> ret=new ArrayList<>();
        for (File f:root.toFile().listFiles()){
            ret.addAll(searchTopology(root.resolve(f.getName()),true,f.getName()));
        }
        return ret;
    }
}
