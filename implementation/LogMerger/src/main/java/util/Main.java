package util;


import util.CsvMerger.MergeCsv;
import util.ExperimentFinder.ExperimentFinder;
import util.ExperimentFinder.ExperimentIdentifier;
import util.LogMerger.MergeLogs;
import util.MergeSummaries.MergeSummaries;
import util.TopologySubmissionStatsMerger.TopologySubmissionStatsMerger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class Main {
    private static final boolean MERGELOGS=false;
    public static void main(String[] args){
        Path currentRelativePath = Paths.get("path\\to\\ExperimentalResults");
        List<ExperimentIdentifier> experiments=new ExperimentFinder().search(currentRelativePath);
        if (MERGELOGS) {
            for (ExperimentIdentifier e : experiments) {
                System.out.println(e.getPath().toString() + "=" + e);
                MergeLogs.merge(e);
            }
        }
        new MergeCsv().merge(experiments, currentRelativePath);
        try {
            BufferedWriter bw1 = new BufferedWriter(new FileWriter(currentRelativePath.resolve("throughputFactor_average.dat").toFile(), false));
            BufferedWriter bw2 = new BufferedWriter(new FileWriter(currentRelativePath.resolve("minLatencyFactor.dat").toFile(), false));
            BufferedWriter bw3 = new BufferedWriter(new FileWriter(currentRelativePath.resolve("maxLatencyFactor.dat").toFile(), false));
            BufferedWriter bw4 = new BufferedWriter(new FileWriter(currentRelativePath.resolve("avgLatencyFactor.dat").toFile(), false));
            BufferedWriter bw5 = new BufferedWriter(new FileWriter(currentRelativePath.resolve("sustainableInputFactor.dat").toFile(), false));
            BufferedWriter bw6 = new BufferedWriter(new FileWriter(currentRelativePath.resolve("runtimeMS_average.dat").toFile(), false));
            List<BufferedWriter> writers=new ArrayList<>();
            writers.add(bw1);
            writers.add(bw2);
            writers.add(bw3);
            writers.add(bw4);
            writers.add(bw5);
            writers.add(bw6);
            List<String> solverNames=new ArrayList<>();

            solverNames.add("AntSystem");
            solverNames.add("HybridSearch");
            solverNames.add("LocalSearch");
            solverNames.add("ResourceAwareScheduler");
            Collections.sort(solverNames);
            for (BufferedWriter bw:writers) {
                bw.write("topology");
                for (String s : solverNames) {
                    bw.write(";" + s+";std");
                }
                bw.write("\n");
                bw.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            BufferedWriter bw1 = new BufferedWriter(new FileWriter(currentRelativePath.resolve("throughputFactor_min.dat").toFile(), false));
            BufferedWriter bw2 = new BufferedWriter(new FileWriter(currentRelativePath.resolve("throughputFactor_max.dat").toFile(), false));
            List<BufferedWriter> writers=new ArrayList<>();
            writers.add(bw1);
            writers.add(bw2);
            List<String> solverNames=new ArrayList<>();

            solverNames.add("AntSystem");
            solverNames.add("HybridSearch");
            solverNames.add("LocalSearch");
            solverNames.add("ResourceAwareScheduler");
            Collections.sort(solverNames);
            for (BufferedWriter bw:writers) {
                bw.write("topology");
                for (String s : solverNames) {
                    bw.write(";" + s);
                }
                bw.write("\n");
                bw.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            BufferedWriter bw1 = new BufferedWriter(new FileWriter(currentRelativePath.resolve("throughputFactor_averageToResAware.dat").toFile(), false));
            BufferedWriter bw2 = new BufferedWriter(new FileWriter(currentRelativePath.resolve("avgLatencyFactor_averageToResAware.dat").toFile(), false));
            BufferedWriter bw3 = new BufferedWriter(new FileWriter(currentRelativePath.resolve("maxLatencyFactor_averageToResAware.dat").toFile(), false));
            BufferedWriter bw4 = new BufferedWriter(new FileWriter(currentRelativePath.resolve("minLatencyFactor_averageToResAware.dat").toFile(), false));
            BufferedWriter bw5 = new BufferedWriter(new FileWriter(currentRelativePath.resolve("sustainableInputFactor_averageToResAware.dat").toFile(), false));
            List<BufferedWriter> writers=new ArrayList<>();
            writers.add(bw1);
            writers.add(bw2);
            writers.add(bw3);
            writers.add(bw4);
            writers.add(bw5);
            List<String> topologyNames=new ArrayList<>();

            topologyNames.add("AntSystem");
            topologyNames.add("HybridSearch");
            topologyNames.add("LocalSearch");
            topologyNames.add("DefaultScheduler");
            Collections.sort(topologyNames);
            for (BufferedWriter bw:writers) {
                bw.write("topology");
                for (String s : topologyNames) {
                    bw.write(";" + s+";std");
                }
                bw.write("\n");
                bw.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        new MergeSummaries().merge(currentRelativePath);
        new TopologySubmissionStatsMerger().merge(currentRelativePath);
    }
}
