package util.TopologySubmissionStatsMerger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class TopologySubmissionStatsMerger {
    public void merge(Path path){
        Path topologyStats=path.resolve("../TopologyStats");
        if (!topologyStats.toFile().exists())topologyStats.toFile().mkdir();
        for (File f:path.toFile().listFiles()){
            if (!f.getName().equals("Errors")&&!f.getName().endsWith(".dat")){
                summarizeTopology(topologyStats,path.resolve(f.getName()), f.getName());
            }
        }
    }

    private void summarizeTopology(Path topologyStats,Path topology, String topId) {
        Path solver=topology.resolve("ResourceAwareScheduler");
        if (solver.toFile().exists()){
            traverseWorkerCounts(topologyStats,solver,topId);
        }
    }

    private void traverseWorkerCounts(Path topologyStats, Path solver, String topId) {
        Map<Integer,WorkerStats> workerStatsMap=new TreeMap<>();
        for (File f:solver.toFile().listFiles()){
            if (!f.getName().equals("Errors")&&!f.getName().endsWith(".dat")) {
                String workers=f.getName().substring(7);
                int wCount=Integer.parseInt(workers);
                Path tStats=solver.resolve(f.getName()+"/1/topologySubmitter_logs/topologyStats.csv");
                if (tStats.toFile().exists()){
                    workerStatsMap.put(wCount,parse(tStats));
                }
            }
        }
        String cpuRequestedHeader="cpuRequested for ";
        cpuRequestedHeader+=workerStatsMap.keySet().stream().map(Object::toString).collect(Collectors.joining("/"));
        List<String> lines;
        try {
            lines=Files.readAllLines(solver.resolve("workers"+workerStatsMap.keySet().iterator().next()+"/1/topologySubmitter_logs/topologyStats.csv"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(topologyStats.resolve(topId+".csv").toFile(), false));
            String header=lines.get(0);
            String[] splitHeader=header.split(";");
            int cpuIndex=Arrays.asList(splitHeader).indexOf("cpuRequested");
            int idIndex=Arrays.asList(splitHeader).indexOf("id");
            splitHeader[cpuIndex]=cpuRequestedHeader;
            bw.write(String.join(";", splitHeader)+"\n");
            lines.remove(0);
            for (String line:lines){
                String[] fields=line.split(";");
                String cpuUsages=workerStatsMap.values().stream().map(e->e.operatorCpuRequested.get(fields[idIndex])).collect(Collectors.joining("/"));
                fields[cpuIndex]=cpuUsages;
                bw.write(String.join(";", fields)+"\n");
            }
            bw.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private WorkerStats parse(Path tStats) {
        List<String> lines;
        try {
            lines=Files.readAllLines(tStats);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        WorkerStats workerStats=new WorkerStats();
        String header=lines.get(0);
        int cpuIndex=Arrays.asList(header.split(";")).indexOf("cpuRequested");
        int idIndex=Arrays.asList(header.split(";")).indexOf("id");
        lines.remove(0);
        for (String s:lines){
            String[] line=s.split(";");
            workerStats.operatorCpuRequested.put(line[idIndex],line[cpuIndex]);
        }
        return workerStats;
    }
}
