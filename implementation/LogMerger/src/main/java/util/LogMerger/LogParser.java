package util.LogMerger;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogParser {

    public static List<LogEntry> parse(Path path, String identifier, LogEntry.LogProducer logProducer){
        Pattern dateRegex=Pattern.compile("(\\d+-\\d+-\\d+\\s\\d+:\\d+:\\d+\\.\\d+)");
        List<LogEntry> ret;
        identifier= String.format("%-15s", identifier+":");
        try {
            final BufferedReader in = new BufferedReader(
                    new InputStreamReader(new FileInputStream(path.toAbsolutePath().toString()), Charset.defaultCharset()));
            String read;
            List<String> lines=new LinkedList<>();
            String lastLine=null;
            while ((read = in.readLine()) != null) {
                if (lastLine==null){
                    lastLine=read;
                }else if (read.startsWith("2022")) {
                    lines.add(lastLine);
                    lastLine=read;
                }else {
                    lastLine += read;
                }
            }
            lines.add(lastLine);
            in.close();
            ret= new ArrayList<>(lines.size());
            for (String line:lines){
                Matcher matcher=dateRegex.matcher((line).substring(0,24));
                line=identifier+"\t"+line;

                if (matcher.find()) {
                    String date = matcher.group(1);
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                    try {
                        Date parsedDate = format.parse(date);
                        boolean interesting = line.contains("exception") || line.contains("Exception")||line.contains("has finished loading")||line.contains("Create Netty Server")||line.contains("All connections are ready for");
                        ret.add(new LogEntry(line, parsedDate, logProducer, interesting));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }else{
                    System.out.println("line without date?: "+line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            ret= new ArrayList<>();
        }
        return ret;
    }
}
