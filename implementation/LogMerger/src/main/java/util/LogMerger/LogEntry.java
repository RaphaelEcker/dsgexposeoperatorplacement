package util.LogMerger;

import java.util.Date;

public class LogEntry implements Comparable<LogEntry> {
    private String line;
    private Date date;
    private LogProducer logProducer;
    private boolean interesting;

    public LogEntry(String line, Date date, LogProducer logProducer, boolean interesting) {
        this.line = line;
        this.date = date;
        this.logProducer = logProducer;
        this.interesting=interesting;
    }

    public String getLine() {
        return line;
    }

    public Date getDate() {
        return date;
    }

    public boolean isInteresting() {
        return interesting;
    }

    public LogProducer getLogProducer() {
        return logProducer;
    }

    @Override
    public int compareTo(LogEntry o) {
        return this.getDate().compareTo(o.getDate());
    }

    public enum LogProducer{
        Nimbus,Supervisor,Worker
    }
}
