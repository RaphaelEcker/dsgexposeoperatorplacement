package util.LogMerger;

import util.ExperimentFinder.ExperimentIdentifier;
import util.LogMerger.LogEntry;
import util.LogMerger.LogParser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class MergeLogs {
    public static void merge(ExperimentIdentifier e){
        List<LogEntry> logEntryList= LogParser.parse(e.getPath().resolve("nimbus_logs/nimbus.log"), "nimbus", LogEntry.LogProducer.Nimbus);
        //logEntryList.addAll(parseSupervisors(e.getPath().resolve("worker_logs")));
        //Collections.sort(logEntryList);

        Set<List<LogEntry>> logSet=parseSupervisors(e.getPath().resolve("worker_logs"));
        logSet.add(logEntryList);
        List<LogEntry> resultList=merge(logSet);

        try {
            String ret=resultList.stream().map(l->l.getLine()).collect(Collectors.joining("\n"));
            BufferedWriter bw = new BufferedWriter(new FileWriter(e.getPath().resolve("joinedLogs.txt").toFile(), false));
            bw.write(ret);
            bw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            String ret=resultList.stream().filter(l->l.isInteresting()).map(l->l.getLine()).collect(Collectors.joining("\n"));
            BufferedWriter bw = new BufferedWriter(new FileWriter(e.getPath().resolve("joinedLogsI.txt").toFile(), false));
            bw.write(ret);
            bw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    private static Set<List<LogEntry>> parseSupervisors(Path worker_logs) {
        Set<List<LogEntry>> set=new HashSet<>();
        for (File f:worker_logs.toFile().listFiles()){
            set.add(LogParser.parse(worker_logs.resolve(f.getName()+"/supervisor.log"), f.getName(), LogEntry.LogProducer.Supervisor));
            parseWorkerLogs(set,worker_logs.resolve(f.getName()),f.getName());
        }
        return set;
    }

    private static void parseWorkerLogs(Set<List<LogEntry>> set,Path workerLogs, String worker) {
        for (File f:workerLogs.toFile().listFiles()){
            if (f.getName().equals("workers-artifacts")){
                parseTopologies(set,workerLogs.resolve(f.getName()),worker);
            }
        }
    }

    private static void parseTopologies(Set<List<LogEntry>> set,Path topolgies, String worker) {
        for (File f:topolgies.toFile().listFiles()){
            parsePorts(set,topolgies.resolve(f.getName()),worker);
        }
    }
    private static void parsePorts(Set<List<LogEntry>> set,Path ports,String worker) {
        for (File f:ports.toFile().listFiles()){
            set.add(LogParser.parse(ports.resolve(f.getName()+"/worker.log"), worker+":"+f.getName(), LogEntry.LogProducer.Worker));
        }
    }

    public static <T extends Comparable<? super T>> List<T> merge(Set<List<T>> lists) {
        int totalSize = 0; // every element in the set
        for (List<T> l : lists) {
            totalSize += l.size();
        }

        List<T> result = new ArrayList<T>(totalSize);

        List<T> lowest;

        while (result.size() < totalSize) { // while we still have something to add
            lowest = null;

            for (List<T> l : lists) {
                if (! l.isEmpty()) {
                    if (lowest == null) {
                        lowest = l;
                    } else if (l.get(0).compareTo(lowest.get(0)) <= 0) {
                        lowest = l;
                    }
                }
            }

            result.add(lowest.get(0));
            lowest.remove(0);
        }

        return result;
    }
}
