package util.MergeSummaries;

import java.util.*;

public class SchedulerRelativePerformance {
    Map<String, Map<Integer, List<Integer>>> solverDatapointMap=new TreeMap<>();
    private Map<String, Map<Integer, Double>> meanPerformanceMap;
    private String schedulerName;
    private boolean allowInterpolation;
    private Metric metric;
    public SchedulerRelativePerformance(List<SummaryDatapoint> datapoints, String schedulerName, boolean allowInterpolation,Metric metric) {
        this.schedulerName=schedulerName;
        this.allowInterpolation=allowInterpolation;
        this.metric=metric;
        for (SummaryDatapoint d:datapoints){
            Map<Integer, List<Integer>> solverPerformaceMap = solverDatapointMap.computeIfAbsent(d.getSolver(), k -> new TreeMap<>());
            List<Integer> throughputs = solverPerformaceMap.computeIfAbsent(d.getCpu(), k -> new ArrayList<>());
            switch (metric){
                case MIN_LATENCY:
                    if (d.getMinLatency() != -1) {
                        if (d.getMinLatency() <= 1000) {
                            throughputs.add(d.getMinLatency() + 1);//avoid 0, essentially counting ms that have started rather than passed, or rounding up instead of down
                        }
                    }
                    break;
                case AVG_LATENCY:
                    if (d.getAvgLatency() != -1) {
                        if (d.getAvgLatency() <= 1000) {
                            throughputs.add(d.getAvgLatency());
                        }
                    }
                    break;
                case MAX_LATENCY:
                    if (d.getMaxLatency() != -1) {
                        if (d.getMaxLatency() <= 1000) {
                            throughputs.add(d.getMaxLatency());
                        }
                    }
                    break;
                case MEDIAN_MAX_THROUGHPUT:
                    if (d.getMedianMaxThroughput()!=-1)throughputs.add(d.getMedianMaxThroughput());
                    break;
                case SUSTAINABLE_INPUT:
                    if (d.getSustainableInput()!=-1)throughputs.add(d.getSustainableInput());
                    break;
            }

        }
        meanPerformanceMap = calculateMeanPerformanceMap();
    }
    public Map<String,ValueStdTuple> output(RelativePerformance mode, String topId){
        if (meanPerformanceMap == null) return null;
        Map<String,ValueStdTuple> throughputFactorMap=new TreeMap<>();
        Map<Integer, Double> defaultSchedulerPerformanceMap=meanPerformanceMap.get(schedulerName);
        for (Map.Entry<String,Map<Integer, Double>> entry:meanPerformanceMap.entrySet()) {
            if (entry.getKey().equals(schedulerName)) continue;
            List<Double> speedups = convertPerformanceToSpeedups(defaultSchedulerPerformanceMap, entry, topId);
            if (speedups.isEmpty()){
                throughputFactorMap.put(entry.getKey(), new ValueStdTuple(-1,-1));
            }else {
                switch (mode){
                    case AVERAGE:
                        double average = 0;
                        for (Double d : speedups) {
                            average += d;
                        }
                        average /= speedups.size();
                        double std=0;
                        if (speedups.size()-1!=0) {
                            for (Double d : speedups) {
                                std += Math.pow(d - average, 2);
                            }
                            std /= (speedups.size() - 1);
                            std = Math.sqrt(std);
                        }
                        throughputFactorMap.put(entry.getKey(), new ValueStdTuple(average,std));
                        break;
                    case MIN:
                        double min = Double.MAX_VALUE;
                        for (Double d : speedups) {
                            min=Math.min(d,min);
                        }
                        throughputFactorMap.put(entry.getKey(), new ValueStdTuple(min,-1));
                        break;
                    case MAX:
                        double max = Double.MIN_VALUE;
                        for (Double d : speedups) {
                            max=Math.max(d,max);
                        }
                        throughputFactorMap.put(entry.getKey(), new ValueStdTuple(max,-1));
                }

            }
        }
        return throughputFactorMap;
    }



    private Map<String, Map<Integer, Double>> calculateMeanPerformanceMap() {
        Map<String, Map<Integer, Double>> meanPerformanceMap=new TreeMap<>();
        for (Map.Entry<String,Map<Integer, List<Integer>>> entry:solverDatapointMap.entrySet()) {
            Map<Integer, Double> meanInnerPerformanceMap=new TreeMap<>();
            meanPerformanceMap.put(entry.getKey(),meanInnerPerformanceMap);
            for (Map.Entry<Integer, List<Integer>> innerEntry : entry.getValue().entrySet()){
                if (!innerEntry.getValue().isEmpty()) {
                    Double average = 0d;
                    for (Integer i : innerEntry.getValue()) {
                        average += i;
                    }
                    average /= innerEntry.getValue().size();
                    meanInnerPerformanceMap.put(innerEntry.getKey(), average);
                }
            }
        }
        return meanPerformanceMap;
    }



    private List<Double> convertPerformanceToSpeedups(Map<Integer, Double> defaultSchedulerPerformanceMap,
                                    Map.Entry<String, Map<Integer, Double>> entry, String topId) {
        boolean printedInterpolationMessage=false;
        List<Double> speedups=new ArrayList<>();
        for (Map.Entry<Integer,Double> innerEntry: entry.getValue().entrySet()){
            Double defSchedulerThroughput= defaultSchedulerPerformanceMap.get(innerEntry.getKey());
            if (defSchedulerThroughput==null) {
                if (allowInterpolation) {
                    if (!printedInterpolationMessage){
                        //System.out.println(topId+":missing direct match(es) from "+entry.getKey()+" to " + this.schedulerName + " "+metric+" performance-> interpolating: "+defaultSchedulerPerformanceMap.size()+ " values available");
                        printedInterpolationMessage=true;
                    }
                    Map.Entry<Integer, Double> largerEntry = null;
                    Map.Entry<Integer, Double> smallerEntry = null;
                    for (Map.Entry<Integer, Double> e : defaultSchedulerPerformanceMap.entrySet()) {
                        if (e.getKey() < innerEntry.getKey()) smallerEntry = e;
                        if (e.getKey() > innerEntry.getKey()) {
                            largerEntry = e;
                            break;
                        }
                    }
                    if (largerEntry != null && smallerEntry != null) {
                        defSchedulerThroughput = (smallerEntry.getValue() * (largerEntry.getKey() - innerEntry.getKey()) + largerEntry.getValue() * (innerEntry.getKey() - smallerEntry.getKey())) / (largerEntry.getKey() - smallerEntry.getKey());

                    } else if (largerEntry != null) {
                        defSchedulerThroughput = largerEntry.getValue();
                    } else {
                        defSchedulerThroughput = smallerEntry.getValue();
                    }
                    speedups.add(innerEntry.getValue() / defSchedulerThroughput);
                }
            }else{
                speedups.add(innerEntry.getValue() / defSchedulerThroughput);
            }
        }
        if (speedups.isEmpty()) System.out.println(topId+": failed to calculate factor increase from "+entry.getKey()+" to " + this.schedulerName + " for "+metric+", interpolation is "+allowInterpolation);
        return speedups;
    }
    public enum RelativePerformance{
        MIN,MAX,AVERAGE
    }
    public enum Metric{
        MIN_LATENCY,AVG_LATENCY,MAX_LATENCY,MEDIAN_MAX_THROUGHPUT,SUSTAINABLE_INPUT
    }
}
