package util.MergeSummaries;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class MergeSummaries {
    public void merge(Path path){
        for (File f:path.toFile().listFiles()){
            if (!f.getName().equals("Errors")&&!f.getName().endsWith(".dat")&&!f.getName().endsWith(".dat#")){
                summarizeTopology(path.resolve(f.getName()), f.getName());
            }
        }
    }

    private void summarizeTopology(Path path, String topId) {
        List<SummaryDatapoint> datapoints=new LinkedList<>();
        for (File f:path.toFile().listFiles()){
            if (f.getName().endsWith(".dat")||f.getName().endsWith(".dat#")) continue;
            findAndReadSolverDatapoints(datapoints, path.resolve(f.getName()), topId, f.getName());
        }
        {
            {
                Map<String, ValueStdTuple> averageRunTimes=new SchedulerAverageRunTime(datapoints).output();
                writeFactorMap(path, "runtimeMS", "","average", topId, averageRunTimes, SchedulerRelativePerformance.RelativePerformance.AVERAGE);
            }
            {//interpolated throughout to default
                SchedulerRelativePerformance relPerformance = new SchedulerRelativePerformance(datapoints, "DefaultScheduler", true, SchedulerRelativePerformance.Metric.MEDIAN_MAX_THROUGHPUT);
                Map<String, ValueStdTuple> throughputAvgFactorMap = relPerformance.output(SchedulerRelativePerformance.RelativePerformance.AVERAGE,topId);
                writeFactorMap(path, "throughput", "Factor","average", topId, throughputAvgFactorMap, SchedulerRelativePerformance.RelativePerformance.AVERAGE);
                Map<String, ValueStdTuple> throughputMinFactorMap = relPerformance.output(SchedulerRelativePerformance.RelativePerformance.MIN,topId);
                writeFactorMap(path, "throughput", "Factor","min", topId, throughputMinFactorMap, SchedulerRelativePerformance.RelativePerformance.MIN);
                Map<String, ValueStdTuple> throughputMaxFactorMap = relPerformance.output(SchedulerRelativePerformance.RelativePerformance.MAX,topId);
                writeFactorMap(path, "throughput", "Factor","max", topId, throughputMaxFactorMap, SchedulerRelativePerformance.RelativePerformance.MAX);
            }
            {//throughput to res aware
                SchedulerRelativePerformance relToResAwarePerformance = new SchedulerRelativePerformance(datapoints, "ResourceAwareScheduler", false, SchedulerRelativePerformance.Metric.MEDIAN_MAX_THROUGHPUT);
                Map<String, ValueStdTuple> throughputAvgFactorRelToResAwareMap = relToResAwarePerformance.output(SchedulerRelativePerformance.RelativePerformance.AVERAGE,topId);
                writeFactorMap(path, "throughput","Factor", "averageToResAware", topId, throughputAvgFactorRelToResAwareMap, SchedulerRelativePerformance.RelativePerformance.AVERAGE);
            }
            {//sustainableInput
                SchedulerRelativePerformance relPerformance = new SchedulerRelativePerformance(datapoints, "DefaultScheduler", true, SchedulerRelativePerformance.Metric.SUSTAINABLE_INPUT);
                Map<String, ValueStdTuple> sustainableInputAvgFactorMap = relPerformance.output(SchedulerRelativePerformance.RelativePerformance.AVERAGE,topId);
                writeFactorMap(path, "sustainableInput", "Factor","", topId, sustainableInputAvgFactorMap, SchedulerRelativePerformance.RelativePerformance.AVERAGE);
                SchedulerRelativePerformance relToResAwarePerformance = new SchedulerRelativePerformance(datapoints, "ResourceAwareScheduler", false, SchedulerRelativePerformance.Metric.SUSTAINABLE_INPUT);
                Map<String, ValueStdTuple> sustainableInputFactorRelToResAwareMap = relToResAwarePerformance.output(SchedulerRelativePerformance.RelativePerformance.AVERAGE,topId);
                writeFactorMap(path, "sustainableInput","Factor", "averageToResAware", topId, sustainableInputFactorRelToResAwareMap, SchedulerRelativePerformance.RelativePerformance.AVERAGE);
            }
            {//interpolated latency to default
                SchedulerRelativePerformance relToDefLatencyPerformance = new SchedulerRelativePerformance(datapoints, "DefaultScheduler", true, SchedulerRelativePerformance.Metric.AVG_LATENCY);
                Map<String, ValueStdTuple> latencyAvgFactorRelToDefMap = relToDefLatencyPerformance.output(SchedulerRelativePerformance.RelativePerformance.AVERAGE,topId);
                writeFactorMap(path, "avgLatency", "Factor","", topId, latencyAvgFactorRelToDefMap, SchedulerRelativePerformance.RelativePerformance.AVERAGE);
                SchedulerRelativePerformance relToDefMaxLatencyPerformance = new SchedulerRelativePerformance(datapoints, "DefaultScheduler", true, SchedulerRelativePerformance.Metric.MAX_LATENCY);
                Map<String, ValueStdTuple> maxLatencyAvgFactorRelToDefMap = relToDefMaxLatencyPerformance.output(SchedulerRelativePerformance.RelativePerformance.AVERAGE,topId);
                writeFactorMap(path, "maxLatency", "Factor","", topId, maxLatencyAvgFactorRelToDefMap, SchedulerRelativePerformance.RelativePerformance.AVERAGE);
                SchedulerRelativePerformance relToDefMinLatencyPerformance = new SchedulerRelativePerformance(datapoints, "DefaultScheduler", true, SchedulerRelativePerformance.Metric.MIN_LATENCY);
                Map<String, ValueStdTuple> minLatencyAvgFactorRelToDefMap = relToDefMinLatencyPerformance.output(SchedulerRelativePerformance.RelativePerformance.AVERAGE,topId);
                writeFactorMap(path, "minLatency", "Factor","", topId, minLatencyAvgFactorRelToDefMap, SchedulerRelativePerformance.RelativePerformance.AVERAGE);
            }
            {//latency to res aware
                SchedulerRelativePerformance relToResAwareLatencyPerformance = new SchedulerRelativePerformance(datapoints, "ResourceAwareScheduler", false, SchedulerRelativePerformance.Metric.AVG_LATENCY);
                Map<String, ValueStdTuple> latencyAvgFactorRelToResAwareMap = relToResAwareLatencyPerformance.output(SchedulerRelativePerformance.RelativePerformance.AVERAGE,topId);
                writeFactorMap(path, "avgLatency", "Factor","averageToResAware", topId, latencyAvgFactorRelToResAwareMap, SchedulerRelativePerformance.RelativePerformance.AVERAGE);
                SchedulerRelativePerformance relToResAwareMaxLatencyPerformance = new SchedulerRelativePerformance(datapoints, "ResourceAwareScheduler", false, SchedulerRelativePerformance.Metric.MAX_LATENCY);
                Map<String, ValueStdTuple> maxLatencyAvgFactorRelToResAwareMap = relToResAwareMaxLatencyPerformance.output(SchedulerRelativePerformance.RelativePerformance.AVERAGE,topId);
                writeFactorMap(path, "maxLatency", "Factor","averageToResAware", topId, maxLatencyAvgFactorRelToResAwareMap, SchedulerRelativePerformance.RelativePerformance.AVERAGE);
                SchedulerRelativePerformance relToResAwareMinLatencyPerformance = new SchedulerRelativePerformance(datapoints, "ResourceAwareScheduler", false, SchedulerRelativePerformance.Metric.MIN_LATENCY);
                Map<String, ValueStdTuple> minLatencyAvgFactorRelToResAwareMap = relToResAwareMinLatencyPerformance.output(SchedulerRelativePerformance.RelativePerformance.AVERAGE,topId);
                writeFactorMap(path, "minLatency", "Factor","averageToResAware", topId, minLatencyAvgFactorRelToResAwareMap, SchedulerRelativePerformance.RelativePerformance.AVERAGE);
            }
        }

        Set<String> solverNames= datapoints.stream().map(d->d.getSolver()).collect(Collectors.toSet());
        for (String solver:solverNames) {
            try {
                BufferedWriter bw1 = new BufferedWriter(new FileWriter(path.getParent().resolve(topId + "_"+solver+"_throughput.dat").toFile(), false));
                BufferedWriter bw2 = new BufferedWriter(new FileWriter(path.getParent().resolve(topId + "_"+solver+ "_latency.dat").toFile(), false));
                BufferedWriter bw3 = new BufferedWriter(new FileWriter(path.getParent().resolve(topId + "_"+solver+"_medianMaxThroughput.dat").toFile(), false));
                BufferedWriter bw4 = new BufferedWriter(new FileWriter(path.getParent().resolve(topId + "_"+solver+ "_avgLatency.dat").toFile(), false));
                BufferedWriter bw5 = new BufferedWriter(new FileWriter(path.getParent().resolve(topId + "_"+solver+ "_maxLatency.dat").toFile(), false));
                BufferedWriter bw6 = new BufferedWriter(new FileWriter(path.getParent().resolve(topId + "_"+solver+ "_sustainableInput.dat").toFile(), false));
                bw1.write("x y label\n");
                bw2.write("x y label\n");
                bw3.write("x y label\n");
                bw4.write("x y label\n");
                bw5.write("x y label\n");
                bw6.write("x y label\n");
                for (SummaryDatapoint d : datapoints) {
                    if (d.getSolver().equals(solver)) {
                        if (d.getThroughput() != -1) bw1.write(d.getCpu() + " " + d.getThroughput() + " " + d.getSolver() + "\n");
                        if (d.getMinLatency()<1000) bw2.write(d.getCpu() + " " + d.getMinLatency() + " " + d.getSolver() + "\n");
                        if (d.getMedianMaxThroughput() != -1) bw3.write(d.getCpu() + " " + d.getMedianMaxThroughput() + " " + d.getSolver() + "\n");
                        if (d.getAvgLatency()<1000)bw4.write(d.getCpu() + " " + d.getAvgLatency() + " " + d.getSolver() + "\n");
                        if (d.getMaxLatency()<1000)bw5.write(d.getCpu() + " " + d.getMaxLatency() + " " + d.getSolver() + "\n");
                        if (d.getSustainableInput()!=-1)bw6.write(d.getCpu() + " " + d.getSustainableInput() + " " + d.getSolver() + "\n");
                    }
                }
                bw1.close();
                bw2.close();
                bw3.close();
                bw4.close();
                bw5.close();
                bw6.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static void writeFactorMap(Path path, String prefix, String name, String fileAppendix, String topId, Map<String, ValueStdTuple> factorMap, SchedulerRelativePerformance.RelativePerformance mode) {
        if (factorMap !=null) {
            try {

                BufferedWriter bw1 = new BufferedWriter(new FileWriter(path.getParent().resolve(prefix+name+(fileAppendix.equals("")?"":"_"+fileAppendix)+".dat").toFile(), true));
                bw1.write(topId);
                List<String> topologyNames = new ArrayList<>(factorMap.keySet());
                Collections.sort(topologyNames);
                if (mode.equals(SchedulerRelativePerformance.RelativePerformance.AVERAGE)){
                    for (String t : topologyNames) {
                        ValueStdTuple entry=factorMap.get(t);
                        if (entry.value==-1){
                            bw1.write(";;");
                        }else {
                            bw1.write(";" + entry.value + ";" + entry.std);
                        }
                    }
                }else {
                    for (String t : topologyNames) {
                        if (factorMap.get(t).value==-1){
                            bw1.write(";");
                        }else{
                            bw1.write(";" + factorMap.get(t).value);
                        }
                    }
                }
                bw1.write("\n");
                bw1.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }else{
            System.out.println(topId+": factorMap is null");
        }
    }

    private void findAndReadSolverDatapoints(List<SummaryDatapoint> datapoints, Path path, String topId, String solverName) {
        Path summaryPath=path.resolve("summary.csv");
        if (Files.exists(summaryPath)){
            readSolverDatapoints(datapoints, summaryPath, topId, solverName);
        }else{
            for (File f:path.toFile().listFiles()){
                findAndReadSolverDatapoints(datapoints, path.resolve(f.getName()), topId, solverName);
            }
        }
    }

    private void readSolverDatapoints(List<SummaryDatapoint> datapoints, Path summaryPath, String topId, String solverName) {
        try {
            List<String> lines=Files.readAllLines(summaryPath, Charset.defaultCharset());
            if (lines.size()<1) return;
            String headerString=lines.get(0);
            String[] header=headerString.split(";");
            boolean headerContainsThroughput=headerString.contains("throughput");
            boolean headerContainsMedianMaxThroughput=headerString.contains("medianMaxThroughput");
            boolean headerContainsSustainableInput=headerString.contains("sustainableInput");
            boolean headerContainsAvgTimeMS=headerString.contains("avgTimeMS");
            lines.remove(0);
            for (String line:lines){
                parseCsvLine(datapoints,header,line,topId,solverName,headerContainsThroughput,headerContainsMedianMaxThroughput,headerContainsSustainableInput,headerContainsAvgTimeMS);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void parseCsvLine(List<SummaryDatapoint> datapoints, String[] header, String line, String topId, String solverName, boolean headerContainsThroughput, boolean headerContainsMedianMaxThroughput, boolean headerContainsSustainableInput, boolean headerContainsavgTimeMS) {
        String[] columns=line.split(";");
        Integer latency=null;
        Integer throughput=null;
        Integer smallHosts=null;
        Integer midHosts=null;
        Integer largeHosts=null;
        Integer medianMaxThroughput=null;
        Integer avgLatency=null;
        Integer maxLatency=null;
        Integer sustainableInput=null;
        Double averageTimeMS=null;
        for (int i=0;i< columns.length;i++){
            String fieldName=header[i];
            String fieldValue=columns[i];
            switch (fieldName){
                case "avgTimeMS":
                    averageTimeMS=Double.parseDouble(fieldValue);
                    break;
            }
        }
        for (int i=0;i< columns.length;i++){
            String fieldName=header[i];
            String fieldValue=columns[i];

            switch (fieldName){
                case "min":
                    latency=Integer.parseInt(fieldValue);
                    break;
                case "avg":
                    avgLatency=Integer.parseInt(fieldValue);
                    break;
                case "max":
                    maxLatency=Integer.parseInt(fieldValue);
                    break;
                case "throughput":
                    throughput=Integer.parseInt(fieldValue);
                    break;
                case "smallHosts":
                    smallHosts=Integer.parseInt(fieldValue);
                    break;
                case "midHosts":
                    midHosts=Integer.parseInt(fieldValue);
                    break;
                case "largeHosts":
                    largeHosts=Integer.parseInt(fieldValue);
                    break;
                case "medianMaxThroughput":
                    medianMaxThroughput=Integer.parseInt(fieldValue);
                    break;
                case "sustainableInput":
                    sustainableInput=Integer.parseInt(fieldValue);
                    break;
                case "avgTimeMS":
                    averageTimeMS=Double.parseDouble(fieldValue);
                    break;
            }

            if (latency!=null&&avgLatency!=null&&maxLatency!=null&&(throughput!=null||!headerContainsThroughput)&&(medianMaxThroughput!=null||!headerContainsMedianMaxThroughput)&&(sustainableInput!=null||!headerContainsSustainableInput)&&(averageTimeMS!=null||!headerContainsavgTimeMS)&&smallHosts!=null&&midHosts!=null&&largeHosts!=null){
                int cpu=smallHosts*30+midHosts*100+largeHosts*150;
                if (throughput==null){
                    throughput=-1;
                }
                if (medianMaxThroughput==null){
                    medianMaxThroughput=-1;
                }
                if (sustainableInput==null){
                    sustainableInput=-1;
                }
                datapoints.add(new SummaryDatapoint(solverName, cpu, latency, avgLatency,maxLatency,throughput,medianMaxThroughput,sustainableInput, averageTimeMS));
                latency=null;
                avgLatency=null;
                maxLatency=null;
                throughput=null;
                smallHosts=null;
                midHosts=null;
                largeHosts=null;
                medianMaxThroughput=null;
                sustainableInput=null;
            }
        }
    }

}
