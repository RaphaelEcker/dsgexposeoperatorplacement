package util.MergeSummaries;

import java.util.*;

public class DefaultSchedulerRelativePerformance {
    Map<String, Map<Integer, List<Integer>>> solverDatapointMap=new TreeMap<>();
    private Map<String, Map<Integer, Double>> meanPerformanceMap;

    public DefaultSchedulerRelativePerformance(List<SummaryDatapoint> datapoints) {
        for (SummaryDatapoint d:datapoints){
            Map<Integer, List<Integer>> solverPerformaceMap = solverDatapointMap.computeIfAbsent(d.getSolver(), k -> new TreeMap<>());
            List<Integer> throughputs = solverPerformaceMap.computeIfAbsent(d.getCpu(), k -> new ArrayList<>());
            if (d.getMedianMaxThroughput()!=-1)throughputs.add(d.getMedianMaxThroughput());
        }
        meanPerformanceMap = calculateMeanPerformanceMap();
    }
    public Map<String,ValueStdTuple> output(RelativePerformance mode){
        if (meanPerformanceMap == null) return null;
        Map<String,ValueStdTuple> throughputFactorMap=new TreeMap<>();
        Map<Integer, Double> defaultSchedulerPerformanceMap=meanPerformanceMap.get("DefaultScheduler");
        for (Map.Entry<String,Map<Integer, Double>> entry:meanPerformanceMap.entrySet()) {
            if (entry.getKey().equals("DefaultScheduler")) continue;
            List<Double> speedups = convertPerformanceToSpeedups(defaultSchedulerPerformanceMap, entry);
            if (speedups.isEmpty()){
                throughputFactorMap.put(entry.getKey(), new ValueStdTuple(-1,-1));
            }else {
                switch (mode){
                    case AVERAGE:
                        double average = 0;
                        for (Double d : speedups) {
                            average += d;
                        }
                        average /= speedups.size();
                        double std=0;
                        for (Double d : speedups) {
                            std+=Math.pow(d-average,2);
                        }
                        std/=(speedups.size()-1);
                        std=Math.sqrt(std);
                        if (speedups.size()-1==0) std=0;
                        throughputFactorMap.put(entry.getKey(), new ValueStdTuple(average,std));
                        break;
                    case MIN:
                        double min = Double.MAX_VALUE;
                        for (Double d : speedups) {
                            min=Math.min(d,min);
                        }
                        throughputFactorMap.put(entry.getKey(), new ValueStdTuple(min,-1));
                        break;
                    case MAX:
                        double max = Double.MIN_VALUE;
                        for (Double d : speedups) {
                            max=Math.max(d,max);
                        }
                        throughputFactorMap.put(entry.getKey(), new ValueStdTuple(max,-1));
                }

            }
        }
        return throughputFactorMap;
    }



    private Map<String, Map<Integer, Double>> calculateMeanPerformanceMap() {
        Map<String, Map<Integer, Double>> meanPerformanceMap=new TreeMap<>();
        for (Map.Entry<String,Map<Integer, List<Integer>>> entry:solverDatapointMap.entrySet()) {
            Map<Integer, Double> meanInnerPerformanceMap=new TreeMap<>();
            meanPerformanceMap.put(entry.getKey(),meanInnerPerformanceMap);
            for (Map.Entry<Integer, List<Integer>> innerEntry : entry.getValue().entrySet()){
                if (innerEntry.getValue().isEmpty()) return null;
                Double average=0d;
                for (Integer i:innerEntry.getValue()){
                    average+=i;
                }
                average/=innerEntry.getValue().size();
                meanInnerPerformanceMap.put(innerEntry.getKey(),average);
            }
        }
        return meanPerformanceMap;
    }



    private List<Double> convertPerformanceToSpeedups(Map<Integer, Double> defaultSchedulerPerformanceMap,
                                    Map.Entry<String, Map<Integer, Double>> entry) {
        List<Double> speedups=new ArrayList<>();
        for (Map.Entry<Integer,Double> innerEntry: entry.getValue().entrySet()){
            Double defSchedulerThroughput= defaultSchedulerPerformanceMap.get(innerEntry.getKey());
            if (defSchedulerThroughput==null) {
                System.out.println("missing direct match def scheduler throughput performance");
                Map.Entry<Integer, Double> largerEntry=null;
                Map.Entry<Integer, Double> smallerEntry=null;
                for (Map.Entry<Integer, Double> e: defaultSchedulerPerformanceMap.entrySet()){
                    if (e.getKey()<innerEntry.getKey()) smallerEntry=e;
                    if (e.getKey()>innerEntry.getKey()){
                        largerEntry=e;
                        break;
                    }
                }
                if (largerEntry!=null&smallerEntry!=null){
                    defSchedulerThroughput=(smallerEntry.getValue()*(largerEntry.getKey()-innerEntry.getKey())+largerEntry.getValue()*(innerEntry.getKey()-smallerEntry.getKey()))/(largerEntry.getKey()-smallerEntry.getKey());
                }else if (largerEntry!=null){
                    defSchedulerThroughput=largerEntry.getValue();
                }else{
                    defSchedulerThroughput=smallerEntry.getValue();
                }
                speedups.add(innerEntry.getValue() / defSchedulerThroughput);
            }else{
                speedups.add(innerEntry.getValue() / defSchedulerThroughput);
            }
        }
        return speedups;
    }
    public enum RelativePerformance{
        MIN,MAX,AVERAGE
    }
}
