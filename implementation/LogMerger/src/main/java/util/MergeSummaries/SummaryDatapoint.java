package util.MergeSummaries;

public class SummaryDatapoint {
    private final String solver;
    private final int cpu;
    private final int minLatency;
    private final int throughput;
    private final int medianMaxThroughput;
    private final int avgLatency;
    private final int maxLatency;
    private final int sustainableInput;
    private final Double averageTimeMS;
    public SummaryDatapoint(String solver, int cpu, int minLatency, int avgLatency, int maxLatency, int throughput, int medianMaxThroughput, int sustainableInput, Double averageTimeMS) {
        this.solver = solver;
        this.cpu = cpu;
        this.minLatency = minLatency;
        this.avgLatency=avgLatency;
        this.maxLatency=maxLatency;
        this.throughput = throughput;
        this.medianMaxThroughput=medianMaxThroughput;
        this.sustainableInput=sustainableInput;
        this.averageTimeMS=averageTimeMS;
    }

    public String getSolver() {
        return solver;
    }

    public int getCpu() {
        return cpu;
    }

    public int getMinLatency() {
        return minLatency;
    }

    public int getThroughput() {
        return throughput;
    }

    public int getMedianMaxThroughput() {
        return medianMaxThroughput;
    }

    public int getAvgLatency() {
        return avgLatency;
    }

    public int getMaxLatency() {
        return maxLatency;
    }

    public int getSustainableInput() {
        return sustainableInput;
    }

    public Double getAverageTimeMS() {
        return averageTimeMS;
    }
}
