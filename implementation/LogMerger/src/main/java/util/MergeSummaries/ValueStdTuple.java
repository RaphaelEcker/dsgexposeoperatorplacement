package util.MergeSummaries;

public class ValueStdTuple {
    public double value;
    public double std;

    public ValueStdTuple(double value, double std) {
        this.value = value;
        this.std = std;
    }
}
