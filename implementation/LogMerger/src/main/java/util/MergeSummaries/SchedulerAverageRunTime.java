package util.MergeSummaries;

import java.util.*;

public class SchedulerAverageRunTime {
    Map<String, List<Double>> solverDatapointMap=new TreeMap<>();
    private String schedulerName;
    public SchedulerAverageRunTime(List<SummaryDatapoint> datapoints) {
        this.schedulerName=schedulerName;
        for (SummaryDatapoint d:datapoints){
            List<Double> avgTimes = solverDatapointMap.computeIfAbsent(d.getSolver(), k -> new LinkedList<>());
            if (d.getAverageTimeMS()!=null)avgTimes.add(d.getAverageTimeMS());
        }
        List<String> keysToRemove=new LinkedList<>();
        for (Map.Entry<String,List<Double>> entry:solverDatapointMap.entrySet()) {
            if (entry.getValue().isEmpty())keysToRemove.add(entry.getKey());
        }
        for (String s:keysToRemove){
            solverDatapointMap.remove(s);
        }
    }
    public Map<String,ValueStdTuple> output(){
        Map<String,ValueStdTuple> averagesMap=new TreeMap<>();
        for (Map.Entry<String,List<Double>> entry:solverDatapointMap.entrySet()) {
            List<Double> avgTimes = entry.getValue();
            if (avgTimes.isEmpty()){
                averagesMap.put(entry.getKey(), new ValueStdTuple(-1,-1));
            }else {
                double average = 0;
                for (Double d : avgTimes) {
                    average += d;
                }
                average /= avgTimes.size();
                double std=0;
                if (avgTimes.size()-1!=0) {
                    for (Double d : avgTimes) {
                        std += Math.pow(d - average, 2);
                    }
                    std /= (avgTimes.size() - 1);
                    std = Math.sqrt(std);
                }
                averagesMap.put(entry.getKey(), new ValueStdTuple(average,std));
            }
        }
        return averagesMap;
    }
}
