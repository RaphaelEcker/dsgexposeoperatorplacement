#!/bin/bash
docker run --network stormevaluation_default --link stormevaluation_nimbus_1:nimbus -it --rm storm storm kill benchmark_topology -w 0
docker run --network stormevaluation_default --link stormevaluation_nimbus_1:nimbus -it --rm -v $(pwd)/target/StormBenchmarkTopology-1.0-SNAPSHOT-jar-with-dependencies.jar:/topology.jar storm /bin/bash -c "storm jar /topology.jar topologies.BenchmarkTopology benchmark_topology 123"

