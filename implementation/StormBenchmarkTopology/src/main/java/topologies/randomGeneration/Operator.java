package topologies.randomGeneration;

import java.util.HashSet;
import java.util.Set;

public class Operator {
    private String id;
    private Set<Operator> successors = new HashSet<>();
    private Set<Operator> predecessors = new HashSet<>();
    private int layer;

    public Operator(String id, int layer) {
        this.id = id;
        this.layer = layer;
    }

    public Set<Operator> getSuccessors() {
        return successors;
    }

    public Set<Operator> getPredecessors() {
        return predecessors;
    }

    public String getId() {
        return id;
    }

    public int getLayer() {
        return layer;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Operator)) return false;

        final Operator operator = (Operator) o;

        return getId() != null ? getId().equals(operator.getId()) : operator.getId() == null;
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }
}
