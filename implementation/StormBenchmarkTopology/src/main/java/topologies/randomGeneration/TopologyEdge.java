package topologies.randomGeneration;

public class TopologyEdge {
    private final Operator source;
    private final Operator target;

    public TopologyEdge(Operator source, Operator target) {
        this.source = source;
        this.target = target;
    }

    public Operator getSource() {
        return source;
    }

    public Operator getTarget() {
        return target;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TopologyEdge)) return false;

        final TopologyEdge that = (TopologyEdge) o;

        if (getSource() != null ? !getSource().equals(that.getSource()) : that.getSource() != null) return false;
        return getTarget() != null ? getTarget().equals(that.getTarget()) : that.getTarget() == null;
    }

    @Override
    public int hashCode() {
        int result = getSource() != null ? getSource().hashCode() : 0;
        result = 31 * result + (getTarget() != null ? getTarget().hashCode() : 0);
        return result;
    }
}
