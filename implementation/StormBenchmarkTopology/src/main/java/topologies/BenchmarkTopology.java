package topologies;

import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.ConfigurableTopology;
import org.apache.storm.topology.SpoutDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import topologies.randomGeneration.Operator;
import topologies.randomGeneration.TopologyEdge;
import topologies.topologyComponents.BenchmarkSink;
import topologies.topologyComponents.cyclic.bolt.CyclicBusyBolt;
import topologies.topologyComponents.cyclic.spout.CyclicTCPSpout;
import topologies.topologyComponents.dag.bolt.BusyBolt;
import topologies.topologyComponents.dag.spout.TCPSpout;

import java.io.*;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This is a basic example of a Storm topology.
 */
public class BenchmarkTopology extends ConfigurableTopology {
    private static final int MIN_CPU_WOKRER=30;
    private static final int ASSUMED_AVG_WORKER_CPU =25;
    private static final int CONSTANT_OPERATOR_CPU=75;
    private static final int SPOUT_SINK_CPU=10;
    private static final int SPOUT_MEMORY=1024*1024*10;
    private static final int SINK_MEMORY=1024*1024*10;
    public final static String TestHost = "10.1.0.6";
    private static final int OPERATOR_INSTANCE_COUNT_EACH = 2;
    public final static int DataGeneratorPort = 10000;
    public final static int ThroughputMeasurementPort = DataGeneratorPort + 1;
    public static final boolean useSeriesParallelGraphTopologyGeneration = true;
    public static final Map<String, Double> operatorFlow = new HashMap<>();
    public static final Map<String, Double> operatorSelectivity = new HashMap<>();
    private int workers = 3;
    private double cpuWorkLoad = 0d;
    private Map<String, Double> mapCpuRequested = new HashMap<>();
    private Map<String, Double> mapMemoryRequested = new HashMap<>();

    public static void main(String[] args) throws Exception {
        ConfigurableTopology.start(new BenchmarkTopology(), args);
    }

    protected int run(String[] args) {
        String topologyName = "benchmark";
        String s = "1234567890";

        if (args != null) {
            if (args.length > 2) {
                topologyName = args[0];
                s = args[1];
                workers = Integer.parseInt(args[2]);
            } else if (args.length > 1) {
                topologyName = args[0];
                s = args[1];
            } else {
                s = args[0];
            }
        }


        long topolgyId = Long.parseLong(s);
        PrintWriter pw = null;
        String filepath = "/logs/topologyStats.csv";
        File f = Paths.get("/logs").toFile();
        System.out.println(Paths.get(filepath).toFile().getAbsolutePath());
        if (!f.exists()) System.out.println("creating output dir:" + f.mkdir());
        try {
            pw = new PrintWriter(new BufferedWriter(new FileWriter(Paths.get(filepath).toFile().getAbsolutePath(), false)), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (pw != null) pw.println("id;selectivity;cpuLoad;cpuRequested;memory;bandwidth;instances");

        //conf.setTopologyStrategy("org.apache.storm.scheduler.resource.strategies.scheduling.MyScheduler");
        TopologyBuilder builder = new TopologyBuilder();
        if (topolgyId == -3) {
            configureSpout(builder.setSpout("spout", new TCPSpout(), 1), "spout", SPOUT_SINK_CPU, SPOUT_MEMORY, 0, 1, true, pw, 1);
            BoltDeclarer busy11 = builder.setBolt("busy1-1", new BusyBolt(), 1).shuffleGrouping("spout");
            configureOperator(busy11, "busy1-1", (CONSTANT_OPERATOR_CPU*12)/10, 1024, 100, 1, false, pw, 1);
            BoltDeclarer temp = builder.setBolt("sink", new BenchmarkSink(), 1).shuffleGrouping("busy1-1");
            configureOperator(temp, "sink", SPOUT_SINK_CPU, SINK_MEMORY, 0, 0, true, pw, 1);
            scaleCpuUsage("busy1-1",busy11,pw,1);
        } else if (topolgyId == -2) {
            configureSpout(builder.setSpout("spout", new TCPSpout(), 1), "spout", SPOUT_SINK_CPU, SPOUT_MEMORY, 0, 1, true, pw, 1);
            BoltDeclarer busy11 = builder.setBolt("busy1-1", new BusyBolt(), 10).shuffleGrouping("spout");
            operatorFlow.put("busy1-1",1d);
            configureOperator(busy11, "busy1-1", (CONSTANT_OPERATOR_CPU*12)/10, 1024, 100, 1, false, pw, 10);
            BoltDeclarer temp = builder.setBolt("sink", new BenchmarkSink(), 1).shuffleGrouping("busy1-1");
            configureOperator(temp, "sink", SPOUT_SINK_CPU, SINK_MEMORY, 0, 0, true, pw, 1);
            scaleCpuUsage("busy1-1", busy11, pw, 10);
        } else if (topolgyId == -1) {
            operatorFlow.put("sink",5d);
            configureSpout(builder.setSpout("spout", new TCPSpout(), 1), "spout", SPOUT_SINK_CPU, SPOUT_MEMORY, 0, 1, true, pw, 1);
            BoltDeclarer busy11 = builder.setBolt("busy1-1", new BusyBolt(), 1).shuffleGrouping("spout");
            configureOperator(busy11, "busy1-1", (CONSTANT_OPERATOR_CPU*12)/10, 10, 100, 1, false, pw, 1);
            BoltDeclarer busy21 = builder.setBolt("busy2-1", new BusyBolt(), 1).shuffleGrouping("busy1-1");
            configureOperator(busy21, "busy2-1", (CONSTANT_OPERATOR_CPU*12)/10, 10, 100, 1, false, pw, 1);
            BoltDeclarer busy31 = builder.setBolt("busy3-1", new BusyBolt(), 1).shuffleGrouping("busy2-1");
            configureOperator(busy31, "busy3-1", (CONSTANT_OPERATOR_CPU*12)/10, 10, 100, 1, false, pw, 1);
            BoltDeclarer busy41 = builder.setBolt("busy4-1", new BusyBolt(), 1).shuffleGrouping("busy3-1");
            configureOperator(busy41, "busy4-1", (CONSTANT_OPERATOR_CPU*12)/10, 10, 100, 1, false, pw, 1);
            BoltDeclarer sink = builder.setBolt("sink", new BenchmarkSink(), 1).shuffleGrouping("busy4-1");
            configureOperator(sink, "sink", SPOUT_SINK_CPU, SINK_MEMORY, 0, 0, true, pw, 1);
            scaleCpuUsage("busy1-1", busy11, pw, 1);
            scaleCpuUsage("busy2-1", busy21, pw, 1);
            scaleCpuUsage("busy3-1", busy31, pw, 1);
            scaleCpuUsage("busy4-1", busy41, pw, 1);
        } else if (topolgyId > 0) {
            if (useSeriesParallelGraphTopologyGeneration) {
                generateSeriesParallelRandomTopology(builder, topolgyId, pw);
            } else {
                generateRandomTopology(builder, topolgyId, pw);
            }
        } else {
            if (pw != null) pw.close();
            System.err.println("wrong topology ID");
            System.out.println("wrong topology ID");
            System.exit(-1);
        }
        if (pw != null) pw.close();
        conf.setDebug(false);


        conf.setNumWorkers(workers);


        return submit(topologyName, conf, builder);
    }


    private void configureOperator(BoltDeclarer bolt,
                                   String componentId,
                                   int cpu,
                                   int memory,
                                   int bandwidth,
                                   double selectivity,
                                   boolean logStats,
                                   PrintWriter pw,
                                   int count) {
        double cpuRequested = cpu * operatorFlow.getOrDefault(componentId, 1d)/count;
        cpuRequested = ((double) Math.round(cpuRequested * 10)) / 10d;
        cpuRequested=Math.max(20,cpuRequested);
        double memoryRequested = Math.ceil(2*2*memory / (1024d * 1024)) + 10;
        bolt.setMemoryLoad(memoryRequested);
        if (!componentId.equals("sink")) {
            System.out.println(componentId + ": workload is:" + cpuWorkLoad + " adding:" + (cpuRequested * count) + " results in workload:" + (cpuWorkLoad + (cpuRequested * count)));
            cpuWorkLoad += cpuRequested * count;
        }
        if (!logStats)mapCpuRequested.put(componentId, cpuRequested);
        mapMemoryRequested.put(componentId, memoryRequested);
        cpuRequested = Math.min(cpuRequested, 1.5*MIN_CPU_WOKRER);

        bolt.setCPULoad(cpuRequested);
        if (logStats)mapCpuRequested.put(componentId, cpuRequested);
        configureComponent(componentId, cpu, memory, bandwidth, selectivity);
        if (logStats) {
            outputOperatorStats(pw, componentId, selectivity,cpu, cpuRequested, memoryRequested, bandwidth, count);
        }
    }

    private void configureComponent(String componentId, int cpu, int memory, int bandwidth, double selectivity) {
        conf.put(componentId + "selectivity", selectivity);
        conf.put(componentId + "CPUload", cpu);
        conf.put(componentId + "memory", memory);
        conf.put(componentId + "bandwidth", bandwidth);
    }

    private void attemptToDisableBackpressure(){
        conf.put("topology.backpressure.check.millis",300*60*1000);
    }

    private void outputOperatorStats(PrintWriter pw,
                                     String componentId,
                                     double selectivity,
                                     int cpu,
                                     double cpuRequested,
                                     double memoryRequested,
                                     int bandwidth,
                                     int count) {
        selectivity = Math.round(selectivity * 100) / 100d;
        if (pw != null) {
            pw.println(componentId + ";" + selectivity + ";" +cpu+";"+ cpuRequested + ";" + memoryRequested + ";" + bandwidth + ";" + count);
        }
        System.out.println(componentId + ": cpu:"+cpu+" cpuRequested:" + cpuRequested + " mem:" + memoryRequested + " band:" + bandwidth + " selec:" + selectivity + " instances:" + count+ " operatorFlow:"+operatorFlow.getOrDefault(componentId,-1d));

    }

    private void configureSpout(SpoutDeclarer bolt,
                                String componentId,
                                int cpu,
                                int memory,
                                int bandwidth,
                                double selectivity,
                                boolean logStats,
                                PrintWriter pw,
                                int count) {
        double cpuRequested = cpu * operatorFlow.getOrDefault(componentId, 1d)/count;
        cpuRequested = ((double) Math.round(cpuRequested * 10)) / 10d;

        double memoryRequested = Math.ceil(2*2*memory / (1024d * 1024)) + 10;
        bolt.setMemoryLoad(memoryRequested);

        //cpuWorkLoad += cpuRequested * count;
        mapMemoryRequested.put(componentId, memoryRequested);
        cpuRequested = Math.min(cpuRequested, MIN_CPU_WOKRER);
        bolt.setCPULoad(cpuRequested);
        //mapCpuRequested.put(componentId, cpuRequested);
        configureComponent(componentId, cpu, memory, bandwidth, selectivity);
        if (logStats) {
            outputOperatorStats(pw, componentId, selectivity, cpu,cpuRequested, memoryRequested, bandwidth, count);
        }
    }

    private void logOperatorStats(String componentId, PrintWriter pw, int count) {
        double selectivity = (Double) conf.get(componentId + "selectivity");

        int bandwidth = (Integer) conf.get(componentId + "bandwidth");
        int cpu = (Integer) conf.get(componentId + "CPUload");
        double cpuRequested = mapCpuRequested.get(componentId);
        double memoryRequested = mapMemoryRequested.get(componentId);
        selectivity = Math.round(selectivity * 100) / 100d;
        outputOperatorStats(pw, componentId, selectivity, cpu,cpuRequested, memoryRequested, bandwidth, count);
    }

    private void scaleCpuUsage(String componentId, BoltDeclarer bolt, PrintWriter pw, int count) {
        double availableCPU;
        if (workers>=4){
            availableCPU=400 + (workers - 4) * ASSUMED_AVG_WORKER_CPU - 2 * SPOUT_SINK_CPU;
        }else {
            availableCPU=300 + (workers - 3) * ASSUMED_AVG_WORKER_CPU - 2 * SPOUT_SINK_CPU;
        }
        double workloadToScale=cpuWorkLoad;
        System.out.println("available CPU:"+availableCPU+" workload:"+workloadToScale);
        double scaleFactor=availableCPU/workloadToScale;
        System.out.println(componentId+": scaleFactor: "+scaleFactor);
        double cpuLoad =  scaleFactor* mapCpuRequested.get(componentId);
        cpuLoad = ((double) Math.round(cpuLoad * 10)) / 10d;
        cpuLoad = Math.max(1,Math.min(cpuLoad, MIN_CPU_WOKRER));
        bolt.setCPULoad(cpuLoad);
        mapCpuRequested.put(componentId, cpuLoad);
        logOperatorStats(componentId, pw, count);
    }

    private void generateRandomTopology(TopologyBuilder builder, long topologyId, PrintWriter pw) {
        Random random = new Random(3141592653589793238L);
        for (int i = 0; i < topologyId - 1; i++) {
            for (int j = 0; j < 1000; j++) {
                random.nextInt();
            }
        }
        boolean cyclic = topologyId > 7;
        //if (cyclic) attemptToDisableBackpressure();

        int operatorCount = generateGaussianOperatorCount(random);
        int layerCount = Math.round(random.nextInt(operatorCount - 1)) + 1;//mean=8 std=5
        Map<String, Operator> operatorMap = new HashMap<>();
        Map<Integer, Set<Operator>> layerMap = new HashMap<>();
        for (int i = 0; i < operatorCount; i++) {
            int layer = Math.round(random.nextInt(layerCount));
            Operator op = new Operator(i + "", layer);
            operatorMap.put(i + "", op);
            if (!layerMap.containsKey(layer)) {
                layerMap.put(layer, new HashSet<>());
            }
            layerMap.get(layer).add(op);
        }

        for (int i = 0; i < operatorCount; i++) {
            int successorCount = (int) Math.round(Math.abs(random.nextGaussian() / 4)) + 1;
            Operator op = operatorMap.get(i + "");
            int layer = op.getLayer();
            List<Operator> potentialSuccessors = new ArrayList<>();
            for (int j = layer + 1; j < layerCount; j++) {
                potentialSuccessors.addAll(layerMap.getOrDefault(j, new HashSet<>()));
            }
            Collections.shuffle(potentialSuccessors);
            for (int j = 0; j < successorCount && j < potentialSuccessors.size(); j++) {
                op.getSuccessors().add(potentialSuccessors.get(j));
                potentialSuccessors.get(j).getPredecessors().add(op);
            }
        }
        Map<String, BoltDeclarer> boltDeclarerMap = new HashMap<>();
        if (cyclic) {
            configureSpout(builder.setSpout("spout", new CyclicTCPSpout(), 1), "spout", SPOUT_SINK_CPU, SPOUT_MEMORY, 0, 1, true, pw, 1);
        } else {
            configureSpout(builder.setSpout("spout", new TCPSpout(), 1), "spout", SPOUT_SINK_CPU, SPOUT_MEMORY, 0, 1, true, pw, 1);
        }
        boltDeclarerMap.put("sink", builder.setBolt("sink", new BenchmarkSink(), 1));
        configureOperator(boltDeclarerMap.get("sink"), "sink", SPOUT_SINK_CPU, SINK_MEMORY, 0, 0, true, pw, 1);
        for (int i = 0; i < operatorCount; i++) {
            if (cyclic) {
                boltDeclarerMap.put(i + "", builder.setBolt(i + "", new CyclicBusyBolt(), OPERATOR_INSTANCE_COUNT_EACH));
            } else {
                boltDeclarerMap.put(i + "", builder.setBolt(i + "", new BusyBolt(), OPERATOR_INSTANCE_COUNT_EACH));
            }
        }
        for (int i = 0; i < operatorCount; i++) {
            Operator op = operatorMap.get(i + "");
            BoltDeclarer boltDeclarer = boltDeclarerMap.get(i + "");
            for (Operator o : op.getPredecessors()) {
                boltDeclarer.shuffleGrouping(o.getId());
            }
            if (op.getPredecessors().isEmpty()) {
                boltDeclarer.shuffleGrouping("spout");
            }
            if (op.getSuccessors().isEmpty()) {
                boltDeclarerMap.get("sink").shuffleGrouping(op.getId());
            }
        }
        for (int i = 0; i < operatorCount; i++) {
            BoltDeclarer boltDeclarer = boltDeclarerMap.get(i + "");
            generateRandomOperatorStats(random, i + "", boltDeclarer, pw, OPERATOR_INSTANCE_COUNT_EACH);
        }
        for (int i = 0; i < operatorCount; i++) {
            BoltDeclarer boltDeclarer = boltDeclarerMap.get(i + "");
            scaleCpuUsage(i + "", boltDeclarer, pw, OPERATOR_INSTANCE_COUNT_EACH);
        }

        for (Map.Entry<Integer, Set<Operator>> e : layerMap.entrySet()) {
            if (!e.getValue().isEmpty()) {
                System.out.println("layer:" + e.getKey());
                for (Operator o : e.getValue()) {
                    System.out.println(o.getId() + "->[" + o.getSuccessors().stream().map(z -> z.getId()).collect(Collectors.joining(", ")) + "]");
                }
            }
        }

        if (cyclic) {
            generateCyclicEdge(operatorCount, operatorMap, boltDeclarerMap);
        }

    }

    private void generateCyclicEdge(int operatorCount,
                                    Map<String, Operator> operatorMap,
                                    Map<String, BoltDeclarer> boltDeclarerMap) {
        List<Operator> potentialCycles = new ArrayList<>();
        for (int i = 0; i < operatorCount; i++) {
            Operator op = operatorMap.get(i + "");
            if (!op.getPredecessors().isEmpty() && !op.getPredecessors().contains(operatorMap.get("spout")) && !op.getId().equals("sink")) {
                potentialCycles.add(op);
            }
        }
        if (!potentialCycles.isEmpty()) {
            Collections.shuffle(potentialCycles);
            Operator op = potentialCycles.get(0);
            Set<Operator> potentialPredecessors = new HashSet<>(op.getPredecessors());
            int lastSetSize;
            do {//find all direct and indirect predecessors from operator
                lastSetSize = potentialPredecessors.size();
                for (Operator o : new HashSet<>(potentialPredecessors)) {
                    for (Operator pre : o.getPredecessors()) {
                        if (!pre.getId().equals("spout")) {
                            potentialPredecessors.add(pre);
                        }
                    }
                }
            } while (lastSetSize != potentialPredecessors.size());
            List<Operator> predecessors = new ArrayList<>(potentialPredecessors);
            Collections.shuffle(predecessors);
            Operator cycleTarget = predecessors.get(0);
            boltDeclarerMap.get(cycleTarget.getId()).shuffleGrouping(op.getId());
            conf.put(op.getId() + "cyclic", true);
            System.out.println("cycle:" + op.getId() + "->" + cycleTarget.getId());
        }
    }

    private void generateSeriesParallelRandomTopology(TopologyBuilder builder, long topologyId, PrintWriter pw) {
        final boolean cyclic = topologyId > 7;
        //if (cyclic) attemptToDisableBackpressure();
        Random random = new Random(3141592653589793238L);
        for (int i = 0; i < topologyId - 1; i++) {
            for (int j = 0; j < 1000; j++) {
                random.nextInt();
            }
        }
        int operatorCount = generateLinearOperatorCount(random,cyclic);
        List<TopologyEdge> edges = new ArrayList<>();
        Map<String, Operator> operatorMap = new HashMap<>();
        {//generate spout and sink
            Operator source = new Operator("spout", -1);
            operatorMap.put("spout", source);
            Operator sink = new Operator("sink", -1);
            operatorMap.put("sink", sink);
            edges.add(addEdge(source, sink));
        }
        operatorCount -= 2;
        int operatorName = 0;
        while (operatorCount > 0) {//replace random edges and insert one operator or a diamond pattern
            TopologyEdge e = edges.get(random.nextInt(edges.size()));
            edges.remove(e);
            removeEdge(e);
            if (random.nextDouble() > 0.6 && operatorCount > 1) {
                Operator one = new Operator((operatorName++) + "", -1);
                operatorMap.put(one.getId(), one);
                Operator two = new Operator((operatorName++) + "", -1);
                operatorMap.put(two.getId(), two);

                edges.add(addEdge(e.getSource(), one));
                edges.add(addEdge(e.getSource(), two));
                edges.add(addEdge(one, e.getTarget()));
                edges.add(addEdge(two, e.getTarget()));

                operatorCount -= 2;
            } else {
                Operator one = new Operator((operatorName++) + "", -1);
                operatorMap.put(one.getId(), one);
                edges.add(addEdge(e.getSource(), one));
                edges.add(addEdge(one, e.getTarget()));
                operatorCount--;
            }
        }

        operatorCount = operatorMap.size() - 2;

        {//rename operators in topological sort order
            int operatorId = 0;
            Queue<Operator> layerQueue = new LinkedList<>();
            layerQueue.add(operatorMap.get("spout"));
            operatorMap.get("spout").setLayer(0);
            Map<String, Integer> predecessorMap = new HashMap<>();
            for (Operator o : operatorMap.values()) {
                predecessorMap.put(o.getId(), o.getPredecessors().size());
            }
            int layer = -1;
            while (!layerQueue.isEmpty()) {//format output in topological sort order and layers
                Operator o = layerQueue.poll();
                int count = predecessorMap.get(o.getId());
                if (count <= 1) {
                    int newLayer = 0;
                    for (Operator p : o.getPredecessors()) {
                        newLayer = Math.max(p.getLayer() + 1, newLayer);
                    }
                    o.setLayer(newLayer);
                    if (o.getLayer() > layer) {
                        layer = o.getLayer();
                    }
                    if (!o.getId().equals("sink") && !o.getId().equals("spout")) {
                        o.setId((operatorId++) + "");
                        operatorMap.put(o.getId(), o);
                    }
                    layerQueue.addAll(o.getSuccessors());
                } else {
                    predecessorMap.put(o.getId(), count - 1);
                }
            }
        }

        Map<String, BoltDeclarer> boltDeclarerMap = new HashMap<>();
        {//translate spout and sink to storm topology
            if (cyclic) {
                configureSpout(builder.setSpout("spout", new CyclicTCPSpout(), 1), "spout", SPOUT_SINK_CPU, SPOUT_MEMORY, 0, 1, true, pw, 1);
            } else {
                configureSpout(builder.setSpout("spout", new TCPSpout(), 1), "spout", SPOUT_SINK_CPU, SPOUT_MEMORY, 0, 1, true, pw, 1);
            }
            boltDeclarerMap.put("sink", builder.setBolt("sink", new BenchmarkSink(), 1));
            configureOperator(boltDeclarerMap.get("sink"), "sink", SPOUT_SINK_CPU, SINK_MEMORY, 0, 0, true, pw, 1);
        }
        operatorFlow.put("spout", 1d);
        operatorSelectivity.put("spout", 1d);
        for (int i = 0; i < operatorCount; i++) {//add operators to storm topology
            if (cyclic) {
                boltDeclarerMap.put(i + "", builder.setBolt(i + "", new CyclicBusyBolt(), OPERATOR_INSTANCE_COUNT_EACH));
            } else {
                boltDeclarerMap.put(i + "", builder.setBolt(i + "", new BusyBolt(), OPERATOR_INSTANCE_COUNT_EACH));
            }
            double flow = 0;
            for (Operator pre : operatorMap.get(i + "").getPredecessors()) {
                flow += operatorFlow.get(pre.getId()) * operatorSelectivity.get(pre.getId());
            }
            operatorFlow.put(i + "", flow);
            generateRandomOperatorStats(random, i + "", boltDeclarerMap.get(i + ""), pw, OPERATOR_INSTANCE_COUNT_EACH);
        }
        for (int i = 0; i < operatorCount; i++) {
            BoltDeclarer boltDeclarer = boltDeclarerMap.get(i + "");
            scaleCpuUsage(i + "", boltDeclarer, pw, OPERATOR_INSTANCE_COUNT_EACH);
        }

        for (TopologyEdge e : edges) {//add edges to storm topology
            BoltDeclarer target = boltDeclarerMap.get(e.getTarget().getId());
            target.shuffleGrouping(e.getSource().getId());
        }


        {
            Queue<Operator> layerQueue = new LinkedList<>();
            layerQueue.add(operatorMap.get("spout"));
            operatorMap.get("spout").setLayer(0);
            Map<String, Integer> predecessorMap = new HashMap<>();
            for (Operator o : operatorMap.values()) {
                predecessorMap.put(o.getId(), o.getPredecessors().size());
            }
            int layer = -1;
            while (!layerQueue.isEmpty()) {//format output in topological sort order and layers
                Operator o = layerQueue.poll();
                int count = predecessorMap.get(o.getId());
                if (count <= 1) {
                    if (o.getLayer() > layer) {
                        layer = o.getLayer();
                        System.out.println("layer:" + layer);
                    }
                    System.out.println(o.getId() + "->[" + o.getSuccessors().stream().map(z -> z.getId() + "(L" + z.getLayer() + ")").collect(Collectors.joining(", ")) + "]");
                    layerQueue.addAll(o.getSuccessors());
                } else {
                    predecessorMap.put(o.getId(), count - 1);
                }
            }
        }


        if (cyclic) {
            generateCyclicEdge(operatorCount, operatorMap, boltDeclarerMap);
        }

    }

    private TopologyEdge addEdge(Operator source, Operator target) {
        source.getSuccessors().add(target);
        target.getPredecessors().add(source);
        return new TopologyEdge(source, target);
    }

    private void removeEdge(Operator source, Operator target) {
        source.getSuccessors().remove(target);
        target.getPredecessors().remove(source);
    }

    private void removeEdge(TopologyEdge edge) {
        removeEdge(edge.getSource(), edge.getTarget());
    }

    private void generateRandomOperatorStats(Random random,
                                             String name,
                                             BoltDeclarer boltDeclarer,
                                             PrintWriter pw,
                                             int count) {
        int cpuLoad = Math.max(1, (int) Math.round(random.nextGaussian() * CONSTANT_OPERATOR_CPU + CONSTANT_OPERATOR_CPU*1.2d));
        int memory = (int) Math.round(Math.abs((random.nextGaussian()*25/OPERATOR_INSTANCE_COUNT_EACH+10) * 1024 * 1024));
        int bandwidth = (int) Math.round(Math.abs(random.nextGaussian() * 100)) + 20;
        double selectivity = Math.max(0.3, random.nextGaussian() * 0.30 + 0.9);
        operatorSelectivity.put(name, selectivity);
        configureOperator(boltDeclarer, name, cpuLoad, memory, bandwidth, selectivity, false, pw, count);
    }

    private int generateGaussianOperatorCount(Random random) {
        return Math.max(2, (int) Math.round(random.nextGaussian() * 5 + 8));//mean=8 std=5
    }

    private int generateLinearOperatorCount(Random random, boolean cyclic) {
        if (cyclic){
            return random.nextInt(15) + 5;
        }else {
            return random.nextInt(20) + 5;
        }
    }

}