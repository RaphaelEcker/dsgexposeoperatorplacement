package topologies.topologyComponents.cyclic.spout;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import topologies.BenchmarkTopology;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public class CyclicTCPSpout extends BaseRichSpout {

    SpoutOutputCollector collector;
    private Socket clientSocket;
    private DataInputStream in;
    private Random r = new Random();

    @Override
    public void open(Map<String, Object> conf, TopologyContext context, SpoutOutputCollector collector) {
        this.collector = collector;
        try {
            clientSocket = new Socket(BenchmarkTopology.TestHost, BenchmarkTopology.DataGeneratorPort);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            in = new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        try {
            in.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.close();
    }

    @Override
    public void nextTuple() {
        if (!clientSocket.isConnected()) {
            try {
                clientSocket.close();
                clientSocket = new Socket(BenchmarkTopology.TestHost, BenchmarkTopology.DataGeneratorPort);
                in = new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }

        long timestamp;
        try {
            if (in.available() == 0) return;
            timestamp = in.readLong();
            UUID msgID = new UUID(r.nextLong(), r.nextLong());//secure random not necessary-> use normal&faster random UUID.randomUUID();
            collector.emit(new Values(timestamp, 0L), msgID);
        } catch (IOException e) {
            e.printStackTrace();
            try {//socket likely closed
                in.close();
                clientSocket.close();
                clientSocket = new Socket(BenchmarkTopology.TestHost, BenchmarkTopology.DataGeneratorPort);
                in = new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }


    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("timestamp", "cycle"));
    }
}