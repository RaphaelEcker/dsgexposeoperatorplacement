package topologies.topologyComponents.cyclic.bolt;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import topologies.topologyComponents.workload.RandomNumberGenerationTask;
import topologies.topologyComponents.workload.Workload;

import java.util.Map;
import java.util.Random;

public class CyclicBusyBolt extends BaseRichBolt {
    private Workload workload;
    private OutputCollector collector;
    private double selectivity;
    private int CPUload;
    private String wasteBandwidth;
    private Random random;
    private char[] memoryWaste;
    private double count = 0;
    private boolean cyclic = false;
    private int extraBandwidth;
    private int memory;


    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("result", "timestamp", "cycle"));
    }

    @Override
    public void prepare(Map<String, Object> map, TopologyContext topologyContext, OutputCollector outputCollector) {
        String componentId = topologyContext.getThisComponentId();
        this.collector = outputCollector;
        this.selectivity = (double) map.getOrDefault(componentId + "selectivity", 1);
        this.CPUload = ((Long) map.getOrDefault(componentId + "CPUload", 1)).intValue();
        this.cyclic = (Boolean) map.getOrDefault(componentId + "cyclic", false);
        memory = ((Long) map.getOrDefault(componentId + "memory", 1)).intValue();
        extraBandwidth = ((Long) map.getOrDefault(componentId + "bandwidth", 1)).intValue();
        this.workload = null;
        this.wasteBandwidth=null;
        this.memoryWaste=null;
        this.random=null;
        initialize();
    }
    public void initialize(){
        //late initialize to reduce memory consumption while potentially two workers are running this operator
        this.workload = new RandomNumberGenerationTask();
        this.random = new Random();
        if (extraBandwidth > 0) {
            StringBuilder builder=new StringBuilder();
            for (int i = 0; i < extraBandwidth; i++) {
                builder.append((char) (random.nextInt(26) + 97));
            }
            this.wasteBandwidth=builder.toString();
        }else{
            this.wasteBandwidth = "";
        }
        this.memoryWaste = new char[memory];
        for (int i = 0; i < this.memoryWaste.length; i++) {
            this.memoryWaste[i] = (char) (random.nextInt(26) + 97);
        }
    }

    @Override
    public void execute(Tuple tuple) {
        long cycle = tuple.getLongByField("cycle");
        if (cyclic) {
            if (cycle > 2) {
                collector.ack(tuple);
                return;
            } else {
                cycle++;
            }
        }

        long timestamp = tuple.getLongByField("timestamp");
        int nth = workload.work(this.CPUload*25);


        String result = nth + "" + memoryWaste[random.nextInt(memoryWaste.length)]+wasteBandwidth;
        count += selectivity;
        int tuplesToEmit = (int) count;//(int)Math.round(random.nextDouble()*selectivity*2d);
        count -= tuplesToEmit;
        for (int i = 0; i < tuplesToEmit; i++) {
            collector.emit(new Values(result, timestamp, cycle));
        }
        collector.ack(tuple);
    }

    @Override
    public void cleanup() {
        //try to free up memory up quickly, because storm bug starts new worker assignment before last closes completely, thereby potentially using twice of the intended memory consumption
        this.wasteBandwidth=null;
        this.memoryWaste=null;
        this.random=null;
        System.gc();
    }
}
