package topologies.topologyComponents;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import topologies.BenchmarkTopology;
import topologies.TimeKeeper;
import topologies.topologyComponents.dag.spout.TCPSpout;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.Map;

public class BenchmarkSink extends BaseRichBolt {
    private static Logger LOG = LoggerFactory.getLogger(TCPSpout.class);
    private OutputCollector collector;
    private Socket clientSocket;
    private DataOutputStream out;
    private TimeKeeper timeKeeper;
    private Thread timeKeeperThread;


    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
    }

    @Override
    public void prepare(Map<String, Object> map, TopologyContext topologyContext, OutputCollector outputCollector) {
        this.collector = outputCollector;
        try {
            clientSocket = new Socket(BenchmarkTopology.TestHost, BenchmarkTopology.ThroughputMeasurementPort);
        } catch (IOException e) {
            LOG.error("error connecting", e);
            e.printStackTrace();
        }
        try {

            out = new DataOutputStream(new BufferedOutputStream(clientSocket.getOutputStream()));
        } catch (IOException e) {
            LOG.error("error creating dataoutputstream", e);
            e.printStackTrace();
        }
        //this.timeKeeper=new TimeKeeper();
        //this.timeKeeperThread=new Thread(timeKeeper);
        //timeKeeperThread.start();
    }

    @Override
    public void execute(Tuple tuple) {

        long tupletime = tuple.getLongByField("timestamp");
        long latency = System.currentTimeMillis() - tupletime;
        //long latency=this.timeKeeper.getTimestamp()-tupletime;
        if (!clientSocket.isConnected()) {
            try {
                clientSocket.close();
                clientSocket = new Socket(BenchmarkTopology.TestHost, BenchmarkTopology.ThroughputMeasurementPort);
                out = new DataOutputStream(new BufferedOutputStream(clientSocket.getOutputStream()));
            } catch (IOException e) {
                e.printStackTrace();
                LOG.error("error reconnecting when not connected", e);
                return;
            }
        }
        try {
            out.writeLong(latency);
            out.flush();
            collector.ack(tuple);
        } catch (SocketException e) {
            LOG.error("socket error writing to output", e);
            e.printStackTrace();
            try {
                clientSocket.close();
                clientSocket = new Socket(BenchmarkTopology.TestHost, BenchmarkTopology.ThroughputMeasurementPort);
                out = new DataOutputStream(new BufferedOutputStream(clientSocket.getOutputStream()));
            } catch (IOException ex) {
                LOG.error("error recreating socket", e);
                ex.printStackTrace();
            }

        } catch (IOException e) {
            LOG.error("io error writing to output", e);
            e.printStackTrace();
        }
    }

    @Override
    public void cleanup() {
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
            LOG.error("error closing output stream during cleanup", e);
        }
        try {
            clientSocket.close();
        } catch (IOException e) {
            LOG.error("error closing socket during cleanup", e);
            e.printStackTrace();
        }
        if (timeKeeperThread != null) timeKeeperThread.interrupt();
    }
}
