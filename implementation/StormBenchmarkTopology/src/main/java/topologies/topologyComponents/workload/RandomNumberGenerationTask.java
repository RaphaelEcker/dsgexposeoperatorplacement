package topologies.topologyComponents.workload;

import java.util.Random;

public class RandomNumberGenerationTask implements Workload {
    private Random random = new Random();

    @Override
    public int work(int n) {
        for (int i = 0; i < n - 1; i++) {
            random.nextInt();
        }
        return random.nextInt();
    }
}
