package topologies.topologyComponents.workload;

public interface Workload {
    int work(int n);
}
