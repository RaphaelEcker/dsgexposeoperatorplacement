package topologies.topologyComponents.dag.spout;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import topologies.BenchmarkTopology;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public class TCPSpout extends BaseRichSpout {
    private static Logger LOG = LoggerFactory.getLogger(TCPSpout.class);
    private SpoutOutputCollector collector;
    private Socket clientSocket;
    private DataInputStream in;
    private Random r = new Random();

    @Override
    public void open(Map<String, Object> conf, TopologyContext context, SpoutOutputCollector collector) {
        LOG.info(this.getClass() + " opening");
        this.collector = collector;
        try {
            clientSocket = new Socket(BenchmarkTopology.TestHost, BenchmarkTopology.DataGeneratorPort);
        } catch (IOException e) {
            LOG.error("failed socket creation", e);
            e.printStackTrace();
        }
        try {
            in = new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));
        } catch (IOException e) {
            LOG.error("failed datainputstream creation", e);
            e.printStackTrace();
        }

    }

    @Override
    public void close() {
        LOG.info(this.getClass() + " closing");
        try {
            in.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
            LOG.error("failed inputstream/socket closing during exiting", e);
        }
        super.close();
    }

    @Override
    public void nextTuple() {
        if (!clientSocket.isConnected()) {
            try {
                clientSocket.close();
                clientSocket = new Socket(BenchmarkTopology.TestHost, BenchmarkTopology.DataGeneratorPort);
                in = new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));
            } catch (IOException e) {
                LOG.error("wasn't connected and failed reconnect attempt", e);
                e.printStackTrace();
                return;
            }
        }

        long timestamp;
        try {
            if (in.available() == 0) return;
            timestamp = in.readLong();
            UUID msgID = new UUID(r.nextLong(), r.nextLong());//secure random not necessary-> use normal&faster random UUID.randomUUID();
            collector.emit(new Values(timestamp), msgID);
        } catch (IOException e) {
            LOG.error("failed reading data", e);
            e.printStackTrace();
            try {//socket likely closed
                in.close();
                clientSocket.close();
                clientSocket = new Socket(BenchmarkTopology.TestHost, BenchmarkTopology.DataGeneratorPort);
                in = new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));
            } catch (IOException ex) {
                LOG.error("failed reconnect after failing to read data", e);
                ex.printStackTrace();
            }
        }

    }


    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("timestamp"));
    }
}