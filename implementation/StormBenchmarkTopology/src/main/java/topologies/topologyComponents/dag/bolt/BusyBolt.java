package topologies.topologyComponents.dag.bolt;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import topologies.topologyComponents.dag.spout.TCPSpout;
import topologies.topologyComponents.workload.RandomNumberGenerationTask;
import topologies.topologyComponents.workload.Workload;

import java.lang.instrument.Instrumentation;
import java.util.Map;
import java.util.Random;

public class BusyBolt extends BaseRichBolt {
    private static Logger LOG = LoggerFactory.getLogger(BusyBolt.class);
    private Workload workload;
    private OutputCollector collector;
    private double selectivity;
    private int CPUload;
    private String wasteBandwidth;
    private Random random;
    private char[] memoryWaste;
    private double count = 0;
    private int extraBandwidth;
    private int memory;


    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("result", "timestamp"));
    }

    @Override
    public void prepare(Map<String, Object> map, TopologyContext topologyContext, OutputCollector outputCollector) {
        String componentId = topologyContext.getThisComponentId();
        this.collector = outputCollector;
        this.selectivity = (double) map.getOrDefault(componentId + "selectivity", 1);
        this.CPUload = ((Long) map.getOrDefault(componentId + "CPUload", 1)).intValue();
        memory = ((Long) map.getOrDefault(componentId + "memory", 1)).intValue();
        extraBandwidth = ((Long) map.getOrDefault(componentId + "bandwidth", 1)).intValue();
        this.workload=null;
        this.wasteBandwidth=null;
        this.memoryWaste=null;
        this.random=null;
        initialize();
    }
    public void initialize(){
        //late initialize to reduce memory consumption while potentially two workers are running this operator
        this.workload = new RandomNumberGenerationTask();
        this.random = new Random();
        if (extraBandwidth > 0) {
            StringBuilder builder=new StringBuilder();
            for (int i = 0; i < extraBandwidth; i++) {
                builder.append((char) (random.nextInt(26) + 97));
            }
            this.wasteBandwidth=builder.toString();
        }else{
            this.wasteBandwidth = "";
        }
        LOG.info("free memory, exception?:"+Runtime.getRuntime().freeMemory()+" attempting to reserve: "+memory*2+" total memory: "+Runtime.getRuntime().totalMemory()+" max memory: "+Runtime.getRuntime().maxMemory());
        this.memoryWaste = new char[memory];
        for (int i = 0; i < this.memoryWaste.length; i++) {
            this.memoryWaste[i] = (char) (random.nextInt(26) + 97);
        }
    }

    @Override
    public void execute(Tuple tuple) {
        //if (random==null||memoryWaste==null) initialize();
        long timestamp = tuple.getLongByField("timestamp");
        int nth = workload.work(this.CPUload*25);


        String result = nth + memoryWaste[random.nextInt(memoryWaste.length)]+wasteBandwidth;
        count += selectivity;
        int tuplesToEmit = (int) count;//(int)Math.round(random.nextDouble()*selectivity*2d);
        count -= tuplesToEmit;
        for (int i = 0; i < tuplesToEmit; i++) {
            collector.emit(new Values(result, timestamp));
        }
        collector.ack(tuple);
    }

    @Override
    public void cleanup() {
        //try to free up memory up quickly, because storm bug starts new worker assignment before last closes completely, thereby potentially using twice of the intended memory consumption
        this.wasteBandwidth=null;
        this.memoryWaste=null;
        this.random=null;
        System.gc();
    }
}
