package Main;

import Datagen.DataGenerator;
import Datagen.GenerationTuple;
import Datagen.SendingSocket;
import TestPhases.*;
import ThroughputAnalysis.ReceivingSocket;
import ThroughputAnalysis.ThroughputAnalysis;
import TimeKeeper.TimeKeeper;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
    public final static int DataGeneratorPort = 10000;
    public final static int ThroughputMeasurementPort = DataGeneratorPort + 1;
    public static int MinInput = 50;
    public static final String uiHost = "http://10.1.0.4:8080";
    public static String experimentName;
    private static BigInteger sentTuples = BigInteger.valueOf(0);
    private static BigInteger receivedTuples = BigInteger.valueOf(0);

    public static void main(String[] args) {
        int lowTupleRate = 5000;
        int highTupleRate = 10000;
        int resTupleRate = 75000;
        if (args.length > 0) {
            experimentName = args[0];
        } else {
            experimentName = "defaultExperimentName";
        }
        boolean staticPlacement = true;
        if (args.length > 1) {
            staticPlacement = Boolean.parseBoolean(args[1]);
        }
        int seed=-1;
        if (args.length > 2) {
            seed = Integer.parseInt(args[2]);
            Map<String, Integer> tupleRateLookup = getTupleRateLookup();
            lowTupleRate = tupleRateLookup.getOrDefault(seed + "low", lowTupleRate);
            highTupleRate = tupleRateLookup.getOrDefault(seed + "high", highTupleRate);
            resTupleRate = tupleRateLookup.getOrDefault(seed + "res", resTupleRate);
        }
        //MinInput=Math.min(10000,lowTupleRate);
        TimeKeeper timeKeeper = new TimeKeeper();
        Thread timeKeeperThread = new Thread(timeKeeper);
        timeKeeperThread.start();

        BlockingQueue<GenerationTuple> sendQueue = new LinkedBlockingQueue<>();

        SendingSocket sendingSocket = new SendingSocket(sendQueue, timeKeeper);
        Thread sendingThread = new Thread(sendingSocket);
        sendingThread.start();

        DataGenerator datagen = new DataGenerator(sendQueue, timeKeeper);
        Thread datagenThread = new Thread(datagen);
        datagenThread.start();


        BlockingQueue<Long> receivingQueue = new LinkedBlockingQueue<Long>();

        ReceivingSocket receivingSocket = new ReceivingSocket(receivingQueue, timeKeeper, datagen);
        Thread receivingThread = new Thread(receivingSocket);
        receivingThread.start();

        TestPhase[] phases;
        if (seed>7) {
            if (staticPlacement) {
                phases = new TestPhase[]{new WaitForZeroThroughput(true), new ConstantThroughput(30000, lowTupleRate), new ChangeSchedulingPermitted(false), new WaitForSomeThroughput(), new WaitForZeroThroughput(), new WaitForSomeThroughput(), new WaitForZeroThroughput(),new MeasureLatency(15000, lowTupleRate)};
            } else {
                phases = new TestPhase[]{new WaitForZeroThroughput(true), new ConstantThroughput(120000, lowTupleRate), new ChangeSchedulingPermitted(false), new WaitForSomeThroughput(), new WaitForZeroThroughput(), new WaitForSomeThroughput(), new WaitForZeroThroughput(), new MeasureLatency(15000, lowTupleRate)};
            }
        }else{
            if (staticPlacement) {
                phases = new TestPhase[]{new WaitForZeroThroughput(true), new ConstantThroughput(30000, lowTupleRate), new ChangeSchedulingPermitted(false), new WaitForSomeThroughput(), new WaitForZeroThroughput(), new WaitForSomeThroughput(), new WaitForZeroThroughput(), new MeasureLatency(15000, lowTupleRate), new MeasureThroughput()};
            } else {
                phases = new TestPhase[]{new WaitForZeroThroughput(true), new ConstantThroughput(120000, lowTupleRate), new ChangeSchedulingPermitted(false) , new WaitForSomeThroughput(), new WaitForZeroThroughput(), new WaitForSomeThroughput(), new WaitForZeroThroughput(), new MeasureLatency(15000, lowTupleRate), new MeasureThroughput(), new WaitForZeroThroughput(),
                        new ConstantThroughput(60000, highTupleRate),new ChangeSchedulingPermitted(true),
                        new ConstantThroughput(9*60000, highTupleRate), new ChangeSchedulingPermitted(false), new WaitForSomeThroughput(), new WaitForZeroThroughput(), new WaitForSomeThroughput(), new WaitForZeroThroughput(), new MeasureLatency(15000, lowTupleRate), new MeasureThroughput(), new WaitForZeroThroughput(),
                        //new ConstantThroughput(10000, resTupleRate),new ChangeSchedulingPermitted(true),
                        //new ConstantThroughput(120000, resTupleRate), new ChangeSchedulingPermitted(false), new WaitForSomeThroughput(), new WaitForZeroThroughput(), new WaitForSomeThroughput(), new WaitForZeroThroughput(), new MeasureLatency(15000, lowTupleRate), new MeasureThroughput(), new WaitForZeroThroughput(),
                        new ConstantThroughput(60000, lowTupleRate),new ChangeSchedulingPermitted(true),
                        new ConstantThroughput(60000, lowTupleRate), new ChangeSchedulingPermitted(false), new WaitForSomeThroughput(), new WaitForZeroThroughput(), new WaitForSomeThroughput(), new WaitForZeroThroughput(), new MeasureLatency(15000, lowTupleRate), new MeasureThroughput()};
            }
        }
        new ThroughputAnalysis(receivingQueue, timeKeeper, datagen, phases).run();


        receivingThread.interrupt();
        datagenThread.interrupt();
        sendingThread.interrupt();
        timeKeeperThread.interrupt();
        if (phases[phases.length - 1].isCompleted()) {
            System.exit(0);
        } else {
            File f = Paths.get("output/failedRun.txt").toFile();
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.exit(-1);
        }
    }

    public static void addSentTuples(long deltaMessageAddition) {
        synchronized (Main.class) {
            sentTuples = sentTuples.add(BigInteger.valueOf(deltaMessageAddition));
        }
    }

    public static void addReceivedTuples(long deltaMessageAddition) {
        synchronized (Main.class) {
            receivedTuples = receivedTuples.add(BigInteger.valueOf(deltaMessageAddition));
        }
    }

    public static double getApplicationSelectivity() {
        synchronized (Main.class) {
            return (receivedTuples.doubleValue() + 1) / (sentTuples.doubleValue() + 1);
        }
    }


    private static Map<String, Integer> getTupleRateLookup() {
        Map<String, Integer> map = new HashMap<>();
        map.put("-3low", 5000);
        map.put("-3high", 30000);
        map.put("-3res", 40000);
        map.put("-2low", 5000);
        map.put("-2high", 20000);
        map.put("-2res", 40000);
        map.put("-1low", 5000);
        map.put("-1high", 10000);
        map.put("-1res", 11000);
        map.put("1low", 1000);
        map.put("1high", 2500);
        map.put("1res", 10000);
        map.put("2low", 5000);
        map.put("2high", 15000);
        map.put("2res", 18000);
        map.put("3low", 1000);
        map.put("3high", 3500);
        map.put("3res", 4000);
        map.put("4low", 1000);
        map.put("4high", 6000);
        map.put("4res", 18000);
        map.put("5low", 5000);
        map.put("5high", 11000);
        map.put("5res", 17000);
        map.put("6low", 6000);
        map.put("6high", 12000);
        map.put("6res", 12000);
        map.put("7low", 1000);
        map.put("7high", 6000);
        map.put("7res", 6000);
/*
        for (int i = 8; i < 8; i++) {
            map.put(i + "low", 5000);
            map.put(i + "high", 10000);
        }
        */

        map.put("8low", 500);
        map.put("8high", 1000);
        map.put("8res", 1000);
        map.put("9low", 500);
        map.put("9high", 1000);
        map.put("9res", 1000);
        return map;
    }
}
