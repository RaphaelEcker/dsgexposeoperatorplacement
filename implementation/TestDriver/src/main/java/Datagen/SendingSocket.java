package Datagen;

import Main.Main;
import TimeKeeper.TimeKeeper;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

public class SendingSocket implements Runnable {
    private boolean running = true;
    private final BlockingQueue<GenerationTuple> queue;
    private final TimeKeeper timeKeeper;

    public SendingSocket(BlockingQueue<GenerationTuple> queue, TimeKeeper timeKeeper) {
        this.timeKeeper = timeKeeper;
        this.queue = queue;
    }

    public void run() {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(Main.DataGeneratorPort);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (serverSocket == null) return;
        while (running) {
            Socket s = null;
            DataOutputStream out = null;
            try {
                System.out.println("Datagen.SendingSocket: Waiting for connection attempts");
                s = serverSocket.accept();
                System.out.println("Datagen.SendingSocket: connected");
                out = new DataOutputStream(new BufferedOutputStream(s.getOutputStream()));
                sendData(s, out);


            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) out.close();
                    if (s != null) s.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (Thread.interrupted()) running = false;
        }
        System.out.println(this.getClass().getSimpleName() + " stopping");
    }

    private void sendData(Socket s, DataOutputStream out) throws IOException {
        long currentLatency;
        long lastLatency = 0;
        while (running && s.isConnected()) {
            try {
                GenerationTuple t = queue.take();
                long timestamp = t.getTimestamp();
                currentLatency = timestamp - timeKeeper.getTimestamp();
                if (currentLatency > 30) {
                    if (currentLatency > lastLatency + 5) {
                        System.out.println("Sending socket can't keep up: current latency:" + currentLatency + "ms");
                        lastLatency = currentLatency;
                    }
                } else {
                    lastLatency = currentLatency;
                }
                for (int i = 0; i < t.getCount(); i++) {
                    out.writeLong(timestamp);
                }
                out.flush();
            } catch (InterruptedException e) {
                e.printStackTrace();
                this.running = false;
            }
        }
    }
}
