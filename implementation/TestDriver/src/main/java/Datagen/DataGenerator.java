package Datagen;

import TimeKeeper.TimeKeeper;

import java.util.concurrent.BlockingQueue;

import static Main.Main.addSentTuples;

public class DataGenerator implements Runnable {
    private volatile int sendRate = 0;
    private boolean running = true;
    private final TimeKeeper timeKeeper;
    private final BlockingQueue<GenerationTuple> queue;

    public DataGenerator(BlockingQueue<GenerationTuple> queue, TimeKeeper timeKeeper) {
        this.queue = queue;
        this.timeKeeper = timeKeeper;
    }

    public void run() {
        double delta = 0;
        long lastTimeStamp = timeKeeper.getTimestamp();
        long currentTimeStamp;
        while (running) {
            currentTimeStamp = timeKeeper.getTimestamp();
            delta = (currentTimeStamp - lastTimeStamp) / 1000d;
            int deltaMessageAddition = (int) (sendRate * delta);
            if (deltaMessageAddition > 0) {
                lastTimeStamp = currentTimeStamp;
                try {
                    queue.put(new GenerationTuple(currentTimeStamp, deltaMessageAddition));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    running = false;
                }
                /*
                for (int i = 0; i < deltaMessageAddition; i++) {
                    try {
                        queue.put(timeKeeper.getTimestamp());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        running = false;
                    }
                }*/
                addSentTuples(deltaMessageAddition);
            } else if (sendRate == 0) {
                lastTimeStamp = currentTimeStamp;
            }
            if (delta > 0.030 && sendRate > 100) {
                System.out.println("Error can't keep up with data generation:" + delta + "s sendRate:" + sendRate);
                System.err.println("Error can't keep up with data generation:" + delta + "s sendRate:" + sendRate);
            }

            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                running = false;
            }

            if (Thread.interrupted()) running = false;
        }
        System.out.println(this.getClass().getSimpleName() + " stopping");
    }

    public int getSendRate() {
        return sendRate;
    }

    public void setSendRate(int sendRate) {
        System.out.println("updating sendRate:" + sendRate);
        this.sendRate = sendRate;
    }
}
