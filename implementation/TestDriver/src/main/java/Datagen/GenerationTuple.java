package Datagen;

public class GenerationTuple {
    private long timestamp;
    private int count;

    public GenerationTuple(long timestamp, int count) {
        this.timestamp = timestamp;
        this.count = count;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getCount() {
        return count;
    }
}
