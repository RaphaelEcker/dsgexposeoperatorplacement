package ThroughputAnalysis;

import Datagen.DataGenerator;
import Main.Main;
import TestPhases.TestPhase;
import ThroughputAnalysis.dto.ClusterSummaryDTO;
import ThroughputAnalysis.dto.SupervisorDTO;
import TimeKeeper.TimeKeeper;
import com.google.gson.Gson;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisConnectionException;

import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.PriorityQueue;
import java.util.concurrent.BlockingQueue;

public class ThroughputAnalysis implements Runnable {
    private PriorityQueue<Integer> topSafeInput = new PriorityQueue<>();
    private static final int MS_BATCH_MEASURE_INTERVAL = 1000;
    private static boolean CONTAINERNET = true;
    private final DataGenerator datagen;
    private final TimeKeeper timeKeeper;
    private final BlockingQueue<Long> queue;
    private boolean running = true;
    private boolean started = false;
    private PrintWriter pw;
    private Jedis jedis;
    private long startTime = 0;
    private final TestPhase[] testPhases;
    private int testPhaseIndex = 0;
    private Gson gson = new Gson();
    private long lastPlacementUpdate = 0;
    private long lastTimestampWithReceivedTuples = System.currentTimeMillis();

    public ThroughputAnalysis(BlockingQueue<Long> queue,
                              TimeKeeper timeKeeper,
                              DataGenerator datagen,
                              TestPhase[] testPhases) {
        this.testPhases = testPhases;
        this.queue = queue;
        this.timeKeeper = timeKeeper;
        this.datagen = datagen;
        while (jedis == null) {
            try {
                try {
                    HostAndPort redis = new HostAndPort(CONTAINERNET ? "10.0.0.5" : "redis", 6379);
                    jedis = new Jedis(redis);
                    jedis.connect();
                } catch (Exception e) {
                    CONTAINERNET = !CONTAINERNET;
                    HostAndPort redis = new HostAndPort(CONTAINERNET ? "10.0.0.5" : "redis", 6379);
                    jedis = new Jedis(redis);
                    jedis.connect();
                }
                System.out.println("connected to " + (CONTAINERNET ? "10.0.0.5" : "redis") + ":6379[redis]");
            } catch (JedisConnectionException e) {
                jedis = null;
            }
        }


    }

    public void run() {
        try {
            File f = Paths.get("output").toFile();
            System.out.println(Paths.get("output/" + Main.experimentName + ".csv").toFile().getAbsolutePath());
            if (!f.exists()) System.out.println("creating output dir:" + f.mkdir());
            pw = new PrintWriter(new BufferedWriter(new FileWriter(Paths.get("output/" + Main.experimentName + ".csv").toFile().getAbsolutePath(), false)), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (pw == null) {
            System.out.println("no printwriter, exiting...");
            return;
        }
        System.out.println("ready for experiment: " + Main.experimentName);
        output("time;runtime;delta;throughput;min;max;avg;90percentile;95percentile;99percentile;sendRate;message;testPhase;changedAssignment;workers;hosts;smallHost;midHost;largeHost;NimbusLatencyEstimation;NimbusIterations;NimbusTimeMS");
        while (running) {
            if (!started) {
                try {
                    Long value = queue.take();
                    this.started = true;
                    this.startTime = timeKeeper.getTimestamp();
                    datagen.setSendRate(Main.MinInput);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                startReadingThroughput();
            }
            if (Thread.interrupted()) {
                System.out.println(this.getClass().getSimpleName() + " interrupted while reading queue");
                running = false;
            }
        }
        pw.close();
        jedis.del("schedulingEnabled");
        jedis.del("!Nimbus-enactAssignment");
        jedis.del("!Nimbus-iterations");
        jedis.del("!Nimbus-timeMS");
        jedis.del("!latencyEstimation");
        jedis.close();
        System.out.println(this.getClass().getSimpleName() + " stopping");
    }

    void startReadingThroughput() {
        long delta;
        long lastTimeStamp = timeKeeper.getTimestamp();
        long currentTimeStamp;
        ArrayList<Long> throughputBatch = new ArrayList<>();
        while (running) {
            queue.drainTo(throughputBatch);
            currentTimeStamp = timeKeeper.getTimestamp();

            delta = currentTimeStamp - lastTimeStamp;
            if (delta > MS_BATCH_MEASURE_INTERVAL) {
                analyse(throughputBatch, currentTimeStamp, delta);
                throughputBatch.clear();
                lastTimeStamp = currentTimeStamp;
            }
            if (delta > (long) (MS_BATCH_MEASURE_INTERVAL * 1.2d)) {
                System.out.println("Error can't keep up with throughput:" + delta + "ms");
                System.err.println("Error can't keep up with throughput:" + delta + "ms");
            }

            if (delta < 1000) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    System.out.println(this.getClass().getSimpleName() + " interrupted while sleeping");
                    running = false;
                }
            }
            if (Thread.interrupted()) running = false;
        }
    }

    public long getLastPlacementUpdate() {
        return lastPlacementUpdate;
    }

    void analyse(ArrayList<Long> throughputBatch, long timestamp, long delta) {
        long runtime = timestamp - this.startTime;
        //if (throughputBatch.size() == 0) return;
        if (throughputBatch.size() != 0) lastTimestampWithReceivedTuples = System.currentTimeMillis();
        if (System.currentTimeMillis() - lastTimestampWithReceivedTuples > 90000) {
            System.out.println("testrun failed, long time without receiving any tuples");
            datagen.setSendRate(0);
            this.running = false;
            return;
        }
        //Main.addSentTuples(Math.round(datagen.getSendRate()*(delta/1000d)));
        Main.addReceivedTuples(throughputBatch.size());
        String updatedAssignment = jedis.get("!Nimbus-enactAssignment");
        if (updatedAssignment != null && updatedAssignment.equals("1")) {
            lastPlacementUpdate = System.currentTimeMillis();
        }else{
            updatedAssignment="0";
        }
        String nimbusTimeMS = jedis.get("!Nimbus-timeMS");
        String nimbusIterations = jedis.get("!Nimbus-iterations");
        //jedis.set("!Nimbus-iterations","-1");
        //jedis.set("!Nimbus-timeMS","-1");
        jedis.set("!Nimbus-enactAssignment", "0");
        String latencyEstimation = jedis.get("!latencyEstimation");
        if (latencyEstimation == null || latencyEstimation.equals("")) latencyEstimation = -1 + "";
        String hosts = "-1";
        String workers = "-1";
        String smallHost="-1";
        String midHost="-1";
        String largeHost="-1";
        try {
            int hostCount = 0;
            int slotCount = 0;
            int smallCount=0;
            int midCount=0;
            int largeCount=0;
            String json = getUrl(Main.uiHost + "/api/v1/supervisor/summary");
            ClusterSummaryDTO summary = gson.fromJson(json, ClusterSummaryDTO.class);
            for (SupervisorDTO s : summary.supervisors) {
                if (s.slotsUsed != 0) {
                    slotCount += s.slotsUsed;
                    hostCount += 1;
                    if (s.totalMem<600){
                        smallCount++;
                    }else if (s.totalMem<1000){
                        midCount++;
                    }else{
                        largeCount++;
                    }
                }
            }
            if (slotCount != hostCount) {
                System.out.println("WARNING slot and host count mismatch");
            }
            //count of low, mid, high hosts
            hosts = hostCount + "";
            workers = slotCount + "";
            smallHost=smallCount+"";
            midHost=midCount+"";
            largeHost=largeCount+"";
        } catch (Exception e) {
            if (runtime > 20000) {
                System.out.println("testrun failed, UI API query failure, exiting");
                datagen.setSendRate(0);
                this.running = false;
                return;
            }
            e.printStackTrace();
        }

        long min = Long.MAX_VALUE, max = Long.MIN_VALUE, ninetyPercentile = Long.MAX_VALUE, ninetfivePercentile = Long.MAX_VALUE, ninetyninePercentile = Long.MAX_VALUE;
        long avg = Long.MAX_VALUE;
        BigInteger count;
        if (!throughputBatch.isEmpty()) {
            count = BigInteger.valueOf(throughputBatch.size());
            BigInteger sum = BigInteger.valueOf(0);
            for (long v : throughputBatch) {
                if (v < min) min = v;
                if (v > max) max = v;
                sum = sum.add(BigInteger.valueOf(v));
            }
            avg = (long) new BigDecimal(sum).divide(new BigDecimal(count), RoundingMode.HALF_UP).doubleValue();
            Collections.sort(throughputBatch);
            ninetyPercentile = throughputBatch.get((int) Math.round((throughputBatch.size() - 1) * 0.9));
            ninetfivePercentile = throughputBatch.get((int) Math.round((throughputBatch.size() - 1) * 0.95));
            ninetyninePercentile = throughputBatch.get((int) Math.round((throughputBatch.size() - 1) * 0.99));
        }
        System.out.println("Application selectivity:" + Main.getApplicationSelectivity() + " current selectivity:" + ((double) throughputBatch.size() / ((double) datagen.getSendRate() + 1)));
        //alternatively implement quick select with repeated values (->left&right pivot index) to calculate percentile;
        double perSecondTP = throughputBatch.size() / (delta / 1000d);

        int newSendrate = 0;
        while (true) {
		    if (lastPlacementUpdate+5000>=System.currentTimeMillis()){
		    	this.topSafeInput.clear();
		    }
            newSendrate = this.testPhases[testPhaseIndex].execute(runtime, delta, Math.round(perSecondTP), min, max, avg, ninetyPercentile, +ninetfivePercentile, ninetyninePercentile, datagen.getSendRate(), this);
            String time=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
            output(time+";"+runtime + ";" + delta + ";" + Math.round(perSecondTP) + ";" + min + ";" + max + ";" + avg + ";" + ninetyPercentile + ";" + ninetfivePercentile + ";" + ninetyninePercentile + ";" + datagen.getSendRate() + ";" + this.testPhases[testPhaseIndex].getMessage() + ";" + testPhaseIndex + ";" + updatedAssignment + ";" + workers + ";" + hosts + ";" +smallHost+";"+midHost+";"+largeHost+";"+ latencyEstimation + ";" + nimbusIterations + ";" + nimbusTimeMS);
            if (this.testPhases[testPhaseIndex].isCompleted()) {
                if (testPhaseIndex + 1 < this.testPhases.length) {
                    testPhaseIndex++;
                } else {
                    System.out.println("testrun complete");
                    this.running = false;
                    break;
                }
            } else {
                break;
            }
        }
        if (datagen.getSendRate() != newSendrate) {
            datagen.setSendRate(newSendrate);
        }
		if (lastPlacementUpdate+5000>=System.currentTimeMillis()){
        	this.topSafeInput.clear();
        }
    }

    public void setSchedulingEnabled(boolean enabled) {
        if (enabled) {
            jedis.set("schedulingEnabled", "true");
        } else {
            jedis.set("schedulingEnabled", "false");
        }
    }

    public static String getUrl(String urlToRead) throws Exception {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(conn.getInputStream()))) {
            for (String line; (line = reader.readLine()) != null; ) {
                result.append(line);
            }
        }
        return result.toString();
    }

    private void output(String s) {
        pw.println(s);
        System.out.println(s);
    }

    public void addSafeInput(Integer t) {
        if (topSafeInput.size() > 30 && topSafeInput.peek() < t) {
            topSafeInput.remove();
        }
        topSafeInput.add(t);
    }

    public Integer getSafeInput() {
        if (topSafeInput.isEmpty()) return Main.MinInput;
        return topSafeInput.peek();
    }
}
