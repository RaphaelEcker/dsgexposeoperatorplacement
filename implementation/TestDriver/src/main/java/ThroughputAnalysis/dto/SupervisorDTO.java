package ThroughputAnalysis.dto;

public class SupervisorDTO {
    public String id;
    public String host;
    public String uptime;
    public int uptimeSeconds;
    public int slotsTotal;
    public int slotsUsed;
    public boolean schedulerDisplayResource;
    public double totalMem;
    public double totalCpu;
    public double usedMem;
    public double usedCpu;
}
