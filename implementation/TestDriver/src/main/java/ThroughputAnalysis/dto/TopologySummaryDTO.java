package ThroughputAnalysis.dto;

import java.util.List;

public class TopologySummaryDTO {
    public List<TopologyDTO> topologies;
}
