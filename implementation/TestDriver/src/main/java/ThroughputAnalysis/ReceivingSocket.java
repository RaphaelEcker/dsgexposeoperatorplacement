package ThroughputAnalysis;

import Datagen.DataGenerator;
import Main.Main;
import TimeKeeper.TimeKeeper;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

public class ReceivingSocket implements Runnable {
    private final DataGenerator datagen;
    private final BlockingQueue<Long> queue;
    private boolean running = true;
    private final TimeKeeper timeKeeper;
    private Socket s = null;

    public ReceivingSocket(BlockingQueue<Long> queue, TimeKeeper timeKeeper, DataGenerator datagen) {
        this.queue = queue;
        this.timeKeeper = timeKeeper;
        this.datagen = datagen;
    }

    public void run() {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(Main.ThroughputMeasurementPort);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (serverSocket == null) {
            return;
        }
        while (running) {
            DataInputStream in = null;
            try {
                System.out.println("ThroughputAnalysis.ReceivingSocket: Waiting for connection attempts");
                s = serverSocket.accept();
                System.out.println("ThroughputAnalysis.ReceivingSocket: connected");
                if (datagen.getSendRate() == 0) datagen.setSendRate(Main.MinInput);
                in = new DataInputStream(new BufferedInputStream(s.getInputStream()));
                startReadingThroughput(in);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (in != null) in.close();
                    if (s != null) s.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (Thread.interrupted()) running = false;
        }
        System.out.println(this.getClass().getSimpleName() + " stopping");
    }

    void startReadingThroughput(DataInputStream in) throws IOException {
        while (running) {
            if (!s.isConnected()) return;
            //while(in.available()!=0) {
            //if (in.available()!=0){
            long value = in.readLong();
            //long value = timeKeeper.getTimestamp()-value;
            try {
                queue.put(value);
            } catch (InterruptedException e) {
                e.printStackTrace();
                running = false;
            }
            /*}else{
                try {
                    Thread.sleep(1);
                    if (lasttimestamp+100<System.currentTimeMillis()){
                        System.out.println(this.getClass().getSimpleName()+" no new values received@"+System.currentTimeMillis());
                        lasttimestamp=System.currentTimeMillis();
                    }
                } catch (InterruptedException e) {
                    running=false;
                }
            }*/
            if (Thread.interrupted()) running = false;
        }
    }
}
