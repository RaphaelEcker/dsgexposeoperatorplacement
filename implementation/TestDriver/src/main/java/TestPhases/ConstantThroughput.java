package TestPhases;

import Main.Main;
import ThroughputAnalysis.ThroughputAnalysis;

public class ConstantThroughput extends TestPhase {
    private final long runTime;
    private Long startTime;
    private final int constantThroughput;
    private boolean recover=false;
    private int recoverCounter=0;
    private long lastMin=1000000;
    private long lastMax=1000000;
    private boolean neverExceeded=true;

    public ConstantThroughput(long runtime, int constantThroughput) {
        this.runTime = runtime;
        this.constantThroughput = constantThroughput;
    }

    @Override
    public int execute(long time,
                       long delta,
                       long perSecondThroughput,
                       long min,
                       long max,
                       long avg,
                       long ninetyPercentile,
                       long ninetfivePercentile,
                       long ninetyninePercentile,
                       int datagenSendRate, ThroughputAnalysis throughputAnalysis) {
        if (startTime == null) {
            startTime = time;
        }
        if (time >= startTime + runTime) {
            super.complete();
        }
        int newOutput = 0;
        if (perSecondThroughput == 0) {
            newOutput = Main.MinInput;
            recoverCounter=-6;
            neverExceeded=true;
            recover=true;
        } else if (recover){
        	if (perSecondThroughput>0&&max<1000){
        		recoverCounter++;
		    	if (recoverCounter<0){
		    		newOutput = Main.MinInput;
		    	}else{
        			newOutput = throughputAnalysis.getSafeInput();
        			recover=false;
        		}
            }else{
            	newOutput = Main.MinInput;
            }
        } else if (min >= 350||max>=1200||min>lastMin*1.2+15||max>lastMax*1.2+200) {//if constant throughput can't be achieved reduce it
            newOutput = Math.max((int) Math.round(datagenSendRate / 1.2), Main.MinInput);
            neverExceeded=false;
        } else if (min < 300 && datagenSendRate < constantThroughput&&max<1000) {
            //if below throughput attempt to increase it
            if (neverExceeded){
            	newOutput = Math.min(Math.max((int) (datagenSendRate * 1.25), Main.MinInput), constantThroughput);
            }else{
            	newOutput = Math.min(Math.max((int) (datagenSendRate * 1.15), Main.MinInput), constantThroughput);
            }
            throughputAnalysis.addSafeInput(datagenSendRate);
        } else {
            //use constant rate
            throughputAnalysis.addSafeInput(datagenSendRate);
            newOutput = datagenSendRate;
        }
        lastMin=min;
        lastMax=max;
        setMessage(newOutput + "");
        return newOutput;
    }
}
