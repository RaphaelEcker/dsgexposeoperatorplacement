package TestPhases;

import Main.Main;
import ThroughputAnalysis.ThroughputAnalysis;

public class MeasureLatency extends TestPhase {
    private final long runTime;
    private final int constantThroughput;
    private long minLatencyObserved = Long.MAX_VALUE;
    private long timeCount=0;

    public MeasureLatency(long runtime, int constantThroughput) {
        this.runTime = runtime;
        this.constantThroughput = constantThroughput;
    }

    @Override
    public int execute(long time,
                       long delta,
                       long perSecondThroughput,
                       long min,
                       long max,
                       long avg,
                       long ninetyPercentile,
                       long ninetfivePercentile,
                       long ninetyninePercentile,
                       int datagenSendRate, ThroughputAnalysis throughputAnalysis) {

        if (perSecondThroughput>0){
            timeCount+=delta;
        }
        if (timeCount>= runTime) {
            super.complete();
        }


        if (perSecondThroughput>0)minLatencyObserved = Math.min(min, minLatencyObserved);
        int newOutput = 0;
        if (min > 300) {//if constant throughput can't be achieved reduce it
            newOutput = Math.max(Math.min(datagenSendRate / 2, constantThroughput), Main.MinInput);
        } else if (min < 50 && datagenSendRate < constantThroughput) {
            //if below throughput attempt to increase it
            newOutput = Math.min(Math.max((int) (datagenSendRate * 1.25), Main.MinInput), constantThroughput);
            throughputAnalysis.addSafeInput(datagenSendRate);
        } else {
            //use constant rate
            newOutput = datagenSendRate;
            throughputAnalysis.addSafeInput(datagenSendRate);
        }
        setMessage(minLatencyObserved + "");
        return newOutput;
    }
}
