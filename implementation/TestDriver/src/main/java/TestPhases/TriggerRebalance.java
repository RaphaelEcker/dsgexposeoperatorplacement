package TestPhases;

import Main.Main;
import ThroughputAnalysis.ThroughputAnalysis;
import ThroughputAnalysis.dto.TopologyDTO;
import ThroughputAnalysis.dto.TopologySummaryDTO;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class TriggerRebalance extends TestPhase {
    private final int workers;
    private Gson gson = new Gson();

    public TriggerRebalance(int workers) {
        this.workers = workers;
    }

    @Override
    public int execute(long runtime,
                       long delta,
                       long perSecondThroughput,
                       long min,
                       long max,
                       long avg,
                       long ninetyPercentile,
                       long ninetfivePercentile,
                       long ninetyninePercentile,
                       int datagenSendRate,
                       ThroughputAnalysis throughputAnalysis) {
        try {
            String json = getUrl(Main.uiHost + "/api/v1/topology/summary");
            TopologySummaryDTO summary = gson.fromJson(json, TopologySummaryDTO.class);
            for (TopologyDTO dto : summary.topologies) {
                postUrlJsonData(Main.uiHost + "/api/v1/topology/" + dto.id + "/rebalance/0", "{\"rebalanceOptions\": {\"numWorkers\": " + this.workers + "}}");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }

        setMessage("Rebalanced to: " + workers);
        this.complete();
        return 0;
    }

    public static String getUrl(String urlToRead) throws Exception {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(conn.getInputStream()))) {
            for (String line; (line = reader.readLine()) != null; ) {
                result.append(line);
            }
        }
        return result.toString();
    }

    public static String postUrlJsonData(String urlToPost, String data) throws Exception {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToPost);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        byte[] out = data.getBytes(StandardCharsets.UTF_8);
        int length = out.length;

        conn.setFixedLengthStreamingMode(length);
        conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        conn.connect();
        try (OutputStream os = conn.getOutputStream()) {
            os.write(out);
        }


        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(conn.getInputStream()))) {
            for (String line; (line = reader.readLine()) != null; ) {
                result.append(line);
            }
        }
        return result.toString();
    }
}
