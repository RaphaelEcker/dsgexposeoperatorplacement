package TestPhases;

import ThroughputAnalysis.ThroughputAnalysis;

public abstract class TestPhase {
    private String message = "";
    private boolean completed = false;

    public void complete() {
        this.completed = true;
    }

    public boolean isCompleted() {
        return completed;
    }

    public abstract int execute(long runtime,
                                long delta,
                                long perSecondThroughput,
                                long min,
                                long max,
                                long avg,
                                long ninetyPercentile,
                                long ninetfivePercentile,
                                long ninetyninePercentile,
                                int datagenSendRate,
                                ThroughputAnalysis throughputAnalysis);

    public String getMessage() {
        return message;
    }

    protected void setMessage(String message) {
        this.message = message;
    }
}
