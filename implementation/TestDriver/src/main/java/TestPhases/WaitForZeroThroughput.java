package TestPhases;

import ThroughputAnalysis.ThroughputAnalysis;

public class WaitForZeroThroughput extends TestPhase {
    private static final int TIME_TO_WAIT_WITHOUT_THROUGHPUT = 5;
    private boolean started = false;
    private int stoppedSeconds = 0;
    private final boolean changeScheduling;

    public WaitForZeroThroughput(){
        changeScheduling=false;
    }
    public WaitForZeroThroughput(boolean changeScheduling){
        this.changeScheduling=changeScheduling;
    }

    @Override
    public int execute(long runtime,
                       long delta,
                       long perSecondThroughput,
                       long min,
                       long max,
                       long avg,
                       long ninetyPercentile,
                       long ninetfivePercentile,
                       long ninetyninePercentile,
                       int datagenSendRate,
                       ThroughputAnalysis throughputAnalysis) {
        if (perSecondThroughput == 0) {
            stoppedSeconds++;
            if (stoppedSeconds >= TIME_TO_WAIT_WITHOUT_THROUGHPUT) {
                super.complete();
                if (started) {
                    if (changeScheduling) throughputAnalysis.setSchedulingEnabled(true);
                }
                setMessage("Wait finished");
            } else {
                setMessage("Waiting: " + stoppedSeconds + "/" + TIME_TO_WAIT_WITHOUT_THROUGHPUT);
            }
        } else {
            if (!started) {
                if (changeScheduling)throughputAnalysis.setSchedulingEnabled(false);
                setMessage("Wait start");
                started = true;
            } else {
                setMessage("Waiting");
                stoppedSeconds = 0;
            }
        }
        return 0;
    }
}
