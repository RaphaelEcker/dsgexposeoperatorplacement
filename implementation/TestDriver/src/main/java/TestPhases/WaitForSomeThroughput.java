package TestPhases;

import Main.Main;
import ThroughputAnalysis.ThroughputAnalysis;

public class WaitForSomeThroughput extends TestPhase {
    private boolean started = false;


    @Override
    public int execute(long runtime,
                       long delta,
                       long perSecondThroughput,
                       long min,
                       long max,
                       long avg,
                       long ninetyPercentile,
                       long ninetfivePercentile,
                       long ninetyninePercentile,
                       int datagenSendRate,
                       ThroughputAnalysis throughputAnalysis) {
        int newOutput;
        if (perSecondThroughput > 0) {
            newOutput = datagenSendRate;
            super.complete();
            setMessage("Wait finished");
        } else {
            newOutput = Main.MinInput;
            if (!started) {
                setMessage("Wait start");
                started = true;
            } else {
                setMessage("Waiting");
            }
        }
        return newOutput;
    }
}
