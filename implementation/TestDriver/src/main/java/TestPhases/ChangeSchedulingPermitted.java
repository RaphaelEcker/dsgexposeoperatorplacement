package TestPhases;

import ThroughputAnalysis.ThroughputAnalysis;

public class ChangeSchedulingPermitted extends TestPhase {
    private boolean setSchedulingPermittedState;

    public ChangeSchedulingPermitted(boolean setSchedulingPermittedState) {
        this.setSchedulingPermittedState = setSchedulingPermittedState;
    }

    @Override
    public int execute(long runtime,
                       long delta,
                       long perSecondThroughput,
                       long min,
                       long max,
                       long avg,
                       long ninetyPercentile,
                       long ninetfivePercentile,
                       long ninetyninePercentile,
                       int datagenSendRate,
                       ThroughputAnalysis throughputAnalysis) {
        throughputAnalysis.setSchedulingEnabled(setSchedulingPermittedState);
        setMessage("Scheduling is now: " + setSchedulingPermittedState);
        this.complete();
        return 0;
    }
}
