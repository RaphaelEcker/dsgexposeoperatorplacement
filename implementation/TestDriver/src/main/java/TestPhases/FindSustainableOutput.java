package TestPhases;

import Main.Main;
import ThroughputAnalysis.ThroughputAnalysis;

import java.util.Map;
import java.util.TreeMap;

public class FindSustainableOutput extends TestPhase {
    private final long runTime;
    private Long startTime;
    private boolean sustainable = false;
    private final double PERCENT = 0.10;
    private TreeMap<Integer, Long> treeMap = new TreeMap<>();
    private int stepSize = 5000;
    boolean failedOnce = false;

    public FindSustainableOutput(long runtime) {
        this.runTime = runtime;
    }

    @Override
    public int execute(long time,
                       long delta,
                       long perSecondThroughput,
                       long min,
                       long max,
                       long avg,
                       long ninetyPercentile,
                       long ninetfivePercentile,
                       long ninetyninePercentile,
                       int datagenSendRate, ThroughputAnalysis throughputAnalysis) {
        if (startTime == null) {
            startTime = time;
        }
        if (time >= startTime + runTime) {
            super.complete();
        }

        int newOutput;
        if (sustainable) treeMap.put(datagenSendRate, perSecondThroughput);
        if (perSecondThroughput == 0) {
            sustainable = false;
            failedOnce = true;
            if (System.currentTimeMillis() - throughputAnalysis.getLastPlacementUpdate() < 25000) {
                newOutput = Main.MinInput;
                System.out.println("recent restart: running min output");
            } else {
                newOutput = Main.MinInput;
                //newOutput=Math.max((int)Math.round(200/Main.getApplicationSelectivity()),Main.MinInput);
                //newOutput = Main.MinInput;//Math.min(datagenSendRate * 2,Math.max(Main.MinInput, getAchievedSustainableInput()));
                System.out.println("0 throughput, likely low output");
            }
        } else {
            int achievedOutput = getAchievedSustainableInput();
            if (min <= 200 && checkSustainable(datagenSendRate, perSecondThroughput)) {
                if (!sustainable) {
                    sustainable = true;
                    newOutput = Math.max(10000, achievedOutput + Math.min(stepSize, achievedOutput / 20));
                } else {
                    newOutput = Math.max(10000, achievedOutput + Math.min(stepSize, achievedOutput / 20));
                }
                failedOnce = false;
                System.out.println("sustainable throughput, increasing output by:" + Math.min(stepSize, achievedOutput / 20));
            } else {
                if (sustainable || !failedOnce) {
                    if (!failedOnce) {
                        failedOnce = true;
                        newOutput = datagenSendRate;
                        System.out.println("failed sustainability, ignoring once");
                    } else {
                        newOutput = Math.max(achievedOutput - 3 * Math.min(stepSize, achievedOutput / 20), Main.MinInput);
                        if (stepSize <= 100) {
                            super.complete();
                        } else {
                            System.out.println("newly unsustainable throughput: reducing output by:" + (-3 * Math.min(stepSize, achievedOutput / 20)));
                        }

                        stepSize = Math.max(100, stepSize / 10);
                        System.out.println("newly reduced stepsize: " + stepSize);
                        sustainable = false;
                    }
                } else {
                    sustainable = false;
                    failedOnce = true;
                    newOutput = Math.max(achievedOutput / 4, Main.MinInput);
                    System.out.println("unsustainable throughput: running reduced sustainable output");
                }
            }
        }

        setMessage(getAchievedSustainableThroughput() + "");
        return newOutput;
    }

    private boolean checkSustainable(int datagenSendRate, long perSecondThroughput) {
        double expectedOutput = datagenSendRate * Main.getApplicationSelectivity();
        return (perSecondThroughput >= expectedOutput * (1 - PERCENT)) || perSecondThroughput > expectedOutput - 400;//&&perSecondThroughput<=expectedOutput*(1+PERCENT));
    }

    private int getAchievedSustainableInput() {
        int last = 0;
        for (Map.Entry<Integer, Long> entry : treeMap.entrySet()) {
            if (!checkSustainable(entry.getKey(), entry.getValue())) return last;
            last = entry.getKey();
        }
        return last;
    }

    private long getAchievedSustainableThroughput() {
        long last = 0;
        for (Map.Entry<Integer, Long> entry : treeMap.entrySet()) {
            if (!checkSustainable(entry.getKey(), entry.getValue())) return last;
            last = entry.getValue();
        }
        return last;
    }
}
