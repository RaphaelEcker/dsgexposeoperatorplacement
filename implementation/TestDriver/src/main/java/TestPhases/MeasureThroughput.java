package TestPhases;

import Main.Main;
import ThroughputAnalysis.ThroughputAnalysis;

public class MeasureThroughput extends TestPhase {
    private int count=0;
    long maxThroughput = 0;

    @Override
    public int execute(long runtime, long delta, long perSecondThroughput, long min, long max, long avg, long ninetyPercentile, long ninetfivePercentile, long ninetyninePercentile, int datagenSendRate, ThroughputAnalysis throughputAnalysis) {
        int newOutput;
        if (min < 300 || min == Long.MAX_VALUE) {
            newOutput = datagenSendRate + 1000;
        } else if (min > 1000) {
            newOutput = Math.max(Main.MinInput, datagenSendRate - 500);
            count++;
        } else {
            newOutput = datagenSendRate;
            count++;
        }
        if (count >= 20) super.complete();
        maxThroughput = Math.max(maxThroughput, perSecondThroughput);
        setMessage(maxThroughput + "");
        return newOutput;
    }
}
