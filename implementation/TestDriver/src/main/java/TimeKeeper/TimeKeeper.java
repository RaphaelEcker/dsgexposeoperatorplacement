package TimeKeeper;

public class TimeKeeper implements Runnable {
    private boolean running = true;
    private volatile long timestamp = System.currentTimeMillis();

    @Override
    public void run() {
        while (running) {
            if (Thread.interrupted()) running = false;
            timestamp = System.currentTimeMillis();
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
                running = false;
            }
        }
        System.out.println(this.getClass().getSimpleName() + " stopping");
    }

    public long getTimestamp() {
        return timestamp;
    }
}
