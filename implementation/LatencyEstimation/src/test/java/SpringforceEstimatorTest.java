import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class SpringforceEstimatorTest {

    @Test
    void convergeSinglePeer() {
        Position host=new Position();
        Set<Peer> peerSet=new HashSet<>();
        Peer p=new Peer("ip");
        p.setPing(100);
        p.setPosition(new Position(1,0,0));
        peerSet.add(p);
        Position result=SpringforceEstimator.converge(host,peerSet);
        assertEquals(host.getY(),result.getY(),"Y position shouldn't change");
        assertEquals(host.getZ(),result.getZ(),"Z position shouldn't change");
        Position distanceVector=new Position(p.getPosition());
        distanceVector.subtractPosition(result);
        assertEquals(p.getPing(),distanceVector.getLength(),1d,"estimated distance should match defined ping");
    }
    @Test
    void convergeTwoPeers() {
        Position host=new Position();
        Set<Peer> peerSet=new HashSet<>();
        Peer p=new Peer("ip");
        p.setPing(100);
        p.setPosition(new Position(1,2,3));
        peerSet.add(p);
        Peer p2=new Peer("ip2");
        p2.setPing(50);
        p2.setPosition(new Position(-30,1,30));
        peerSet.add(p2);


        Position result=SpringforceEstimator.converge(host,peerSet);
        result=SpringforceEstimator.converge(result,peerSet);
        //assertNotEquals(host.getY(),result.getY(),"Y position should change");
        //assertNotEquals(host.getZ(),result.getZ(),"Z position should change");
        Position distanceVector=new Position(p.getPosition());
        distanceVector.subtractPosition(result);
        assertEquals(p.getPing(),distanceVector.getLength(),1d,"estimated distance should match defined ping for peer 1");
        distanceVector=new Position(p2.getPosition());
        distanceVector.subtractPosition(result);
        assertEquals(p2.getPing(),distanceVector.getLength(),1d,"estimated distance should match defined ping for peer 2");
    }
    @Test
    void convergeToCenterOfTwoPeers() {
        Position host=new Position(10,10,10);
        Set<Peer> peerSet=new HashSet<>();
        Peer p=new Peer("ip");
        p.setPing(100);
        p.setPosition(new Position(100,0,0));
        peerSet.add(p);
        Peer p2=new Peer("ip2");
        p2.setPing(100);
        p2.setPosition(new Position(-100,0,0));
        peerSet.add(p2);
        Position result=SpringforceEstimator.converge(host,peerSet);
        assertNotEquals(host.getY(),result.getY(),"Y position should change");
        assertNotEquals(host.getZ(),result.getZ(),"Z position should change");
        Position distanceVector=new Position(p.getPosition());
        distanceVector.subtractPosition(result);
        assertEquals(p.getPing(),distanceVector.getLength(),1d,"estimated distance should match defined ping for peer 1");
        distanceVector=new Position(p2.getPosition());
        distanceVector.subtractPosition(result);
        assertEquals(p2.getPing(),distanceVector.getLength(),1d,"estimated distance should match defined ping for peer 2");
    }

    @Test
    void totalLatencyReduction() {
        Position host=new Position(232,132,544);
        Set<Peer> peerSet=new HashSet<>();
        Peer p=new Peer("10.0.0.12");
        p.setPing(454);
        p.setPosition(new Position(-302,881,400));
        peerSet.add(p);
        Peer p2=new Peer("10.0.0.11");
        p2.setPing(245);
        p2.setPosition(new Position(484,-854,410));
        peerSet.add(p2);

        Position distanceVector=new Position(p.getPosition());
        distanceVector.subtractPosition(host);
        double latency=distanceVector.getLength();
        distanceVector=new Position(p2.getPosition());
        distanceVector.subtractPosition(host);
        latency+=distanceVector.getLength();

        Position result=SpringforceEstimator.converge(host,peerSet);

        distanceVector=new Position(p.getPosition());
        distanceVector.subtractPosition(result);
        double latency2=distanceVector.getLength();
        distanceVector=new Position(p2.getPosition());
        distanceVector.subtractPosition(result);
        latency2+=distanceVector.getLength();

        System.out.println("initial latency:"+latency);
        System.out.println("new latency:"+latency2);
        System.out.println("latency improvement:"+(latency-latency2));
        assertTrue(latency>latency2,"new position should approach reduced latency");
    }
}