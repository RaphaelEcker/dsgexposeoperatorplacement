public class Peer {
    private String address;
    private long ping = -1;
    private Long[] lastPings=new Long[0];
    private Position position;


    public Peer(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getPing() {
        return ping;
    }

    public void setPing(long ping) {
        this.ping = ping;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Long[] getLastPings() {
        return lastPings;
    }

    public void setLastPings(Long[] lastPings) {
        this.lastPings = lastPings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Peer)) return false;

        final Peer peer = (Peer) o;

        return getAddress() != null ? getAddress().equals(peer.getAddress()) : peer.getAddress() == null;
    }

    @Override
    public int hashCode() {
        return getAddress() != null ? getAddress().hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Peer{" +
                "address='" + address + '\'' +
                ", ping=" + ping +
                ", position=" + position +
                '}';
    }
}
