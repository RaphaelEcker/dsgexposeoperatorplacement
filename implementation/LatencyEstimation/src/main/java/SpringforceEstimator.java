import java.util.Set;

public class SpringforceEstimator {
    private static final double MINEPSILON = 1;
    private static final ForceCalculation.ErrorCalculation errorType= ForceCalculation.ErrorCalculation.linearAbsolute;
    public static Position converge(Position host, Set<Peer> peerSet) {
        double delta=0.25d;
        double maxMovement = Double.MAX_VALUE;
        Position movement = new Position();
        Position estimatedPosition = new Position(host);
        int iteration = 0;
        while (maxMovement>MINEPSILON&&iteration<100) {

            movement.setX(0);
            movement.setY(0);
            movement.setY(0);
            for (Peer p : peerSet) {
                if (p.getPing() <= 0) continue;

                Position distanceVector=ForceCalculation.calculate(p.getPosition(),estimatedPosition,errorType,p.getPing(),delta);
                //System.out.println("movement vector"+distanceVector);
                movement.addPosition(distanceVector);
            }
            maxMovement = movement.getLength();
            estimatedPosition.addPosition(movement);

            iteration++;
        }
        System.out.println("current " + estimatedPosition+", estimated in "+iteration+" iterations");
        return estimatedPosition;
    }
}
