import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NativeIpPing {
    private static String runSystemCommand(String command) throws IOException {
        StringBuilder builder = new StringBuilder();
        Process p = Runtime.getRuntime().exec(command);
        BufferedReader inputStream = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String s;
        // reading output stream of the command
        while ((s = inputStream.readLine()) != null) {
            builder.append(s);
        }
        return builder.toString();
    }

    public static Long[] ping(String host, int pingCount) throws IOException {
        boolean isWindows = System.getProperty("os.name").toLowerCase().contains("win");
        String output = runSystemCommand("ping " + (isWindows ? "-n" : "-c") + " " + pingCount +(isWindows?"":" -i 0")+ " " + host);
        output=output.replace(',','.');
        Pattern pattern = Pattern.compile("=(\\d+\\.??\\d*?)\\s?ms");
        Matcher m = pattern.matcher(output);
        List<Long> ret = new ArrayList<>();
        while (m.find()) {
            double d=Double.parseDouble(m.group(1));
            ret.add((long)(d*1000));
            //ret.add(Long.parseLong((m.group(1))));
        }
        return ret.toArray(new Long[0]);
    }
    //ping individually to avoid 1 second wait time between pings
    public static Long[] iping(String host, int pingCount) throws IOException{
        Long[] pings=new Long[pingCount];
        for (int i=0;i<pingCount;i++){
            Long[] ret=ping(host,1);
            if (ret.length == 0) throw new IOException("error getting pings");
            pings[i]=ret[0];
        }
        return pings;
    }
}
