import redis.clients.jedis.exceptions.JedisConnectionException;

import java.net.UnknownHostException;

public class Main {
    public static void main(String[] args) throws UnknownHostException {
        System.out.println("starting");
        while (!Thread.interrupted()) {
            try {
                LatencyEstimator estimator = new LatencyEstimator();
                estimator.run();
            } catch (JedisConnectionException e) {
                //
            }
        }
    }
}
