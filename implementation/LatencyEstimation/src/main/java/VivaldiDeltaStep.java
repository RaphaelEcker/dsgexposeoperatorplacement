import java.util.Set;

public class VivaldiDeltaStep {
    private static final ForceCalculation.ErrorCalculation errorType= ForceCalculation.ErrorCalculation.linearAbsolute;
    public static Position converge(Position host, Set<Peer> peerSet) {
        double delta=0.25d;
        Position estimatedPosition = new Position(host);
        for (Peer p : peerSet) {
            for (long ping:p.getLastPings()) {
                if (ping <= 0) continue;
                Position distanceVector=ForceCalculation.calculate(p.getPosition(),estimatedPosition,errorType,ping,delta);
                estimatedPosition.addPosition(distanceVector);
            }
        }
        System.out.println("current " + estimatedPosition);
        return estimatedPosition;
    }
}
