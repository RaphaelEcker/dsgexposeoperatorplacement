public class ForceCalculation {
    public static Position calculate(Position peer, Position estimated, ErrorCalculation errorType, long ping,double delta){
        Position distanceVector = new Position(peer);
        distanceVector.subtractPosition(estimated);

        double vectorLength = distanceVector.getLength();
        if (vectorLength<0.00000001){
            distanceVector.setX(Math.random() - 0.5);
            distanceVector.setY(Math.random() - 0.5);
            if (!LatencyEstimator.USE_2D_POSTION_ESTIMATION)distanceVector.setZ(Math.random() - 0.5);
            vectorLength = distanceVector.getLength();
        }
        double error;

        if (errorType== ErrorCalculation.logRelative) {
            error = Math.log(vectorLength / ping);
        }else{
            error =vectorLength-ping;
        }
        //System.out.println("peer:"+p.getAddress());
        //System.out.println("movementFactor:"+movementFactor+" "+p.getAddress());
        //System.out.println("current "+estimatedPosition);
        //System.out.println("peer "+p.getPosition());
        //System.out.println("distance"+distanceVector+", length:"+vectorLength);
        distanceVector.multiply(1 / distanceVector.getLength());//unit distance vector
        distanceVector.multiply(delta*error);
        return distanceVector;
    }
    public enum ErrorCalculation{
        linearAbsolute,logRelative
    }
}
