import com.google.gson.Gson;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.stream.Collectors;

public class LatencyEstimator {
    private static final StatisticalAggregate statisticalAggregate = StatisticalAggregate.MEDIAN;
    private static boolean CONTAINERNET = true;
    private static boolean USE_VIVALDI=true;
    public static boolean USE_2D_POSTION_ESTIMATION=false;
    private static final boolean USE_NATIVE_PING = true;
    private static final int ROUND_WAIT_TIME = 60000;
    private static final int STARTUP_ROUND_WAIT_TIME = 4000;
    private static final int PINGS_PER_PEER = 5;
    private static final int PEER_ROUND_COUNT = 6;//peers to ping and use as landmark in a round
    private static final int PEER_KEEP_AFTER_ROUND_COUNT = 0;//lowest latency peers to keep for next round
    private Position position;
    private final String hostname;
    private Jedis jedis;
    private final Gson gson = new Gson();
    private int iterations = 0;

    public LatencyEstimator() throws UnknownHostException {
        hostname = InetAddress.getLocalHost().getCanonicalHostName();
        System.out.println("hostname:" + hostname);
        try {
            HostAndPort redis = new HostAndPort(CONTAINERNET ? "10.0.0.5" : "redis", 6379);
            System.out.println("attempt to connect to redis host:" + redis);
            jedis = new Jedis(redis);
            jedis.connect();
        } catch (Exception e) {
            CONTAINERNET = !CONTAINERNET;
            HostAndPort redis = new HostAndPort(CONTAINERNET ? "10.0.0.5" : "redis", 6379);
            System.out.println("attempt to connect to redis host:" + redis);
            jedis = new Jedis(redis);
            jedis.connect();
        }
    }

    public void run() {
        this.position = new Position();
        this.position.setX((Math.random() - 0.5) * 20000);
        this.position.setY((Math.random() - 0.5) * 20000);
        if (!USE_2D_POSTION_ESTIMATION){
            this.position.setZ((Math.random() - 0.5) * 20000);
        }
        HashSet<Peer> peers = new LinkedHashSet<>();//selectRandomPeers(PEER_JOIN_COUNT);
        while (!Thread.interrupted()) {
            getUpdatedPeerPositions(peers);
            peers.addAll(selectRandomPeers(PEER_ROUND_COUNT - peers.size(), new LinkedHashSet<>(peers)));
            pingPeers(peers);
            if (USE_VIVALDI){
                this.position = VivaldiDeltaStep.converge(position, peers);
            }else{
                this.position = SpringforceEstimator.converge(position, peers);
            }
            jedis.set("operator-" + hostname, gson.toJson(this.position));
            peers = sortPeersByLatency(peers);
            System.out.println("sorted Peers:" + peers);
            calculateError(peers);
            calculateRelativeError(peers);
            keepFirstNPeers(PEER_KEEP_AFTER_ROUND_COUNT, peers);
            try {
                if (iterations < 10) {
                    iterations++;
                    Thread.sleep((long) (STARTUP_ROUND_WAIT_TIME*(1+((Math.random() - 0.5)*3)/5)));
                } else {
                    Thread.sleep((long)(ROUND_WAIT_TIME*(1+((Math.random() - 0.5)*3)/5)));
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        exit();
    }

    private void calculateError(HashSet<Peer> peers) {
        if (peers.isEmpty()) return;
        double error = 0;
        double squaredError = 0;
        for (Peer p : peers) {
            Position pos = new Position(p.getPosition());
            pos.subtractPosition(this.position);
            double e = Math.abs(pos.getLength() - p.getPing());
            error += e;
            squaredError += e * e;
        }
        error /= peers.size();
        squaredError /= peers.size();
        System.out.println("average estimation error:" + error + " mean squared error:" + squaredError);
    }

    private void calculateRelativeError(HashSet<Peer> peers) {
        if (peers.isEmpty()) return;
        double error = 0;
        for (Peer p : peers) {
            Position pos = new Position(p.getPosition());
            pos.subtractPosition(this.position);
            double e = Math.abs((pos.getLength() - p.getPing()) / Math.min(pos.getLength(), p.getPing()));
            error += e;
        }
        error /= peers.size();
        System.out.println("relative estimation error:" + error);
    }

    private void getUpdatedPeerPositions(HashSet<Peer> peers) {
        for (Peer p : peers) {
            String json = jedis.get("operator-" + p.getAddress());
            p.setPosition(gson.fromJson(json, Position.class));
        }
    }

    private void exit() {
        jedis.del("operator-" + hostname);
        jedis.close();
        System.out.println("exiting");
    }

    private void pingPeers(HashSet<Peer> peers) {
        Peer peer;
        Set<Peer> replacements=new HashSet<>();
        for (Iterator<Peer> iterator = peers.iterator(); iterator.hasNext(); ) {
            peer = iterator.next();
            try {
                peer.setPing(ping(peer));
            } catch (IOException e) {
                HashSet<Peer> temp=new HashSet<>(peers);
                temp.addAll(replacements);
                Peer replacement = selectRandomPeer(temp);
                iterator.remove();
                if (replacement != null) {
                    try {
                        replacement.setPing(ping(replacement));
                        replacements.add(replacement);
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            }
        }
        peers.addAll(replacements);
    }

    private HashSet<Peer> sortPeersByLatency(HashSet<Peer> peers) {
        List<Peer> tempPeers = new ArrayList<>(peers);
        tempPeers.sort((o1, o2) -> Math.toIntExact(o1.getPing() - o2.getPing()));
        return new LinkedHashSet<>(tempPeers);
    }

    private void keepFirstNPeers(int nPeers, HashSet<Peer> peers) {
        Iterator<Peer> iterator = peers.iterator();
        while (iterator.hasNext() && nPeers > 0) {
            iterator.next();
            nPeers--;
        }
        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
        }
    }

    private Peer selectRandomPeer(HashSet<Peer> blacklist) {
        LinkedList<Peer> shuffledPeers = jedis.keys("operator-*").stream().map(s -> s.substring("operator-".length())).map(Peer::new).filter(p -> !blacklist.contains(p) && !p.getAddress().equals(hostname)).collect(Collectors.toCollection(LinkedList::new));
        if (shuffledPeers.isEmpty()) return null;
        Collections.shuffle(shuffledPeers);
        Peer retPeer = shuffledPeers.get(0);
        String json = jedis.get("operator-" + retPeer.getAddress());
        retPeer.setPosition(gson.fromJson(json, Position.class));
        return retPeer;
    }

    private HashSet<Peer> selectRandomPeers(int nPeers) {
        return selectRandomPeers(nPeers, new LinkedHashSet<Peer>());
    }

    private HashSet<Peer> selectRandomPeers(int nPeers, HashSet<Peer> blacklist) {
        HashSet<Peer> retPeers = new LinkedHashSet<>();
        if (nPeers <= 0) return retPeers;
        LinkedList<Peer> shuffledPeers = jedis.keys("operator-*").stream().map(s -> s.substring("operator-".length())).map(Peer::new).filter(p -> !blacklist.contains(p) && !p.getAddress().equals(hostname)).collect(Collectors.toCollection(LinkedList::new));
        Collections.shuffle(shuffledPeers);
        int i = 0;
        for (Iterator<Peer> it = shuffledPeers.iterator(); it.hasNext() && i < nPeers; i++) {
            Peer peer = it.next();
            String json = jedis.get("operator-" + peer.getAddress());
            peer.setPosition(gson.fromJson(json, Position.class));
            retPeers.add(peer);
        }
        return retPeers;
    }

    private long ping(Peer peer) throws IOException {
        if (USE_NATIVE_PING) {
            //use ping command
            //increased accuracy, java uses TCP rather than ICMP if it is executed unprivileged
            Long[] pings = NativeIpPing.ping(peer.getAddress(), PINGS_PER_PEER);
            if (pings.length == 0) throw new IOException("error getting pings");
            Long[] mergedPings;
            if (USE_VIVALDI){
                mergedPings=pings;
            }else{
                mergedPings = concat(pings, peer.getLastPings());
            }
            peer.setLastPings(pings);
            if (statisticalAggregate==StatisticalAggregate.MEDIAN) return median(mergedPings)/2;
            if (statisticalAggregate==StatisticalAggregate.MIN) return min(mergedPings)/2;
            long sum = 0;
            for (Long i : mergedPings) {
                sum += i / 2;//use "oneway" rather than round-trip time
            }
            return sum / mergedPings.length;
        } else {
            int timeout = 3000;
            long beforeTime = System.nanoTime();//System.currentTimeMillis();
            if (statisticalAggregate==StatisticalAggregate.MEDIAN||statisticalAggregate==StatisticalAggregate.MIN) {
                long afterTime;
                Long[] pings = new Long[PINGS_PER_PEER];
                for (int i = 0; i < PINGS_PER_PEER; i++) {
                    boolean reachable = InetAddress.getByName(peer.getAddress()).isReachable(timeout);
                    if (!reachable) {
                        throw new IOException(peer.getAddress() + " not reachable");
                    }
                    afterTime = System.nanoTime();//System.currentTimeMillis();
                    pings[i] = (afterTime - beforeTime);
                    beforeTime = afterTime;
                }
                Long[] mergedPings;
                if (USE_VIVALDI){
                    mergedPings=pings;
                }else{
                    mergedPings = concat(pings, peer.getLastPings());
                }
                peer.setLastPings(pings);
                if (statisticalAggregate==StatisticalAggregate.MEDIAN) {
                    return median(mergedPings) / 4;
                }else{
                    return min(mergedPings) / 4;
                }
            } else {
                for (int i = 0; i < PINGS_PER_PEER; i++) {
                    boolean reachable = InetAddress.getByName(peer.getAddress()).isReachable(timeout);
                    if (!reachable) {
                        throw new IOException(peer.getAddress() + " not reachable");
                    }
                }
                long afterTime = System.nanoTime();//System.currentTimeMillis();
                if (peer.getPing() != -1) {
                    return ((afterTime - beforeTime + peer.getPing())) / (PINGS_PER_PEER * 2 * 2 * 2);
                } else {
                    return ((afterTime - beforeTime)) / (PINGS_PER_PEER * 2 * 2);
                }
            }
        }
    }

    private long median(Long[] pings) {
        Arrays.sort(pings);
        if (pings.length % 2 == 0) {
            return pings[pings.length / 2] + pings[pings.length / 2 - 1] / 2;
        } else {
            return pings[pings.length / 2];
        }
    }
    private long min(Long[] pings) {
        Arrays.sort(pings);
        return pings[0];
    }

    public static Long[] concat(Long[] first, Long[] second) {
        Long[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }
    private enum StatisticalAggregate
    {
        AVG,MEDIAN,MIN
    }
}
