#!/usr/bin/python
from mininet.net import Containernet
from mininet.node import Controller
from mininet.cli import CLI
from mininet.link import TCLink
from mininet.log import info, setLogLevel
import time
import os
import sys

def fixSecondWorkerInterface(w,workername):
	#fix second interface in parallel no bandwidth limit/latency free testdriver network
	w.cmd("ip addr add 10.1.0."+ips[workername]+"/24 dev 10.0.0."+ips[workername]+"-eth1")

cwd=os.getcwd()
called=0;
nimbus_network="stormevaluation_default"#"container:nimbus"
ips= {}
workerCPU=100
qSizeConstant=None

#java classes for different schedulers
resourceAwareScheduler='org.apache.storm.scheduler.resource.ResourceAwareScheduler'
myScheduler='org.apache.storm.scheduler.MyScheduler'
defaultScheduler='org.apache.storm.scheduler.DefaultScheduler'

topologySeed=123
staticScheduler="true";
#setLogLevel('warning')
setLogLevel('info')
nimbusSchedulerConfig="";
#configure scheduler used
if len(sys.argv)>1:
	if sys.argv[1]=="DefaultScheduler":
		scheduler=defaultScheduler
	elif sys.argv[1]=="ResourceAwareScheduler":
		scheduler=resourceAwareScheduler
	elif sys.argv[1]=="LocalSearch":
		scheduler=myScheduler;
		staticScheduler="false";
		nimbusSchedulerConfig=" -c myScheduler.LocalSearch=true -c myScheduler.HybridSearch=false";
	elif sys.argv[1]=="AntSystem":
		scheduler=myScheduler;
		staticScheduler="false";
		nimbusSchedulerConfig=" -c myScheduler.LocalSearch=false -c myScheduler.HybridSearch=false";
	elif sys.argv[1]=="HybridSearch":
		scheduler=myScheduler;
		staticScheduler="false";
		nimbusSchedulerConfig=" -c myScheduler.LocalSearch=false -c myScheduler.HybridSearch=true";
	else:
		info('Error: unknown scheduler\n')
		sys.exit('Error: unknown scheduler\n')
else:
	scheduler=myScheduler
	staticScheduler="false";
	nimbusSchedulerConfig=" -c myScheduler.LocalSearch=true";
	
if staticScheduler=="true":
	supervisorDockerImage="containernetstorm"#default docker image with containernet requirements, don't run latency estimation
else:
	supervisorDockerImage="containernetsupervisor";
	
if len(sys.argv)>2:
	topologySeed=sys.argv[2]
	
workerLimit="";	
if len(sys.argv)>3:
	workerLimit=" "+sys.argv[3]
info('set scheduler:'+scheduler+', config: '+nimbusSchedulerConfig+'\n')
def createStormWorker(name, cpu, memory):
	global called
	global cwd
	global nimbus_network
	called+=1
	ips[name]=str(9+called)
	return net.addDocker("10.0.0."+str(9+called), ip="10.0.0."+str(9+called),dimage=supervisorDockerImage, cpu_period=100000,cpu_quota=cpu*1000, memswap_limit=str(memory+1024)+"m", mem_limit=str(memory)+"m",dcmd='/bin/bash -x /docker-entrypoint.sh /bin/bash -c \'while ! ping -c 1 -W 1 10.0.0.3 > /dev/null; do echo "Waiting for 10.0.0.3 - network interface might be down..."; sleep 1; done;echo "interface online"; sleep 5; storm supervisor -c supervisor.memory.capacity.mb='+str(memory-256)+' -c supervisor.cpu.capacity='+str(cpu)+' -c worker.childopts="\"-Xmx%HEAP-MEM%m -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=artifacts/heapdump\""\'',volumes=[cwd+"/output/worker_logs/"+name+":/logs", cwd+"/storm_containernet.yaml:/conf/storm.yaml", cwd+"/StormMetricsConsumer/target/StormMetricsConsumer-1.0-SNAPSHOT-jar-with-dependencies.jar:/apache-storm-2.4.0/extlib/StormMetricsConsumer.jar","/etc/timezone:/etc/timezone:ro","/etc/localtime:/etc/localtime:ro"])



try: 
	net = Containernet(controller=Controller)
	info('*** Adding controller\n')
	net.addController('c0')

	info('*** Adding docker containers\n')
	#2=zookeeper
	#3=nimbus
	#4=ui
	#5=redis
	#6=testdriver
	#10+=supervisors
	zookeeper=net.addDocker('zookeeper',ip="10.0.0.2", dimage="containernetzookeeper",dcmd="/bin/bash -x /docker-entrypoint.sh zkServer.sh start-foreground",volumes=["/etc/timezone:/etc/timezone:ro","/etc/localtime:/etc/localtime:ro"])
	redis=net.addDocker('redis', ip="10.0.0.5",dimage="containernetredis",dcmd="/bin/bash -x /usr/local/bin/docker-entrypoint.sh redis-server /usr/local/etc/redis/redis.conf",volumes=["/etc/timezone:/etc/timezone:ro","/etc/localtime:/etc/localtime:ro"])
	testdriver=net.addDocker('testdriver', ip="10.1.0.6",dimage="containernettestdriver",dcmd="/bin/bash -c \'java -jar /usr/local/lib/demo.jar defaultExperimentName "+staticScheduler+" "+topologySeed+" &>/usr/local/lib/output/testdriver.txt\'",volumes=[cwd+"/output/testdriver:/usr/local/lib/output","/etc/timezone:/etc/timezone:ro","/etc/localtime:/etc/localtime:ro"])



	nimbus=net.addDocker('10.0.0.3', ip="10.0.0.3", dimage="containernetstorm", ports=[6627,8000],port_bindings={6627:6627,8000:8000} ,dcmd='/bin/bash -x /docker-entrypoint.sh /bin/bash -c \'while ! ping -c 1 -W 1 10.0.0.2 > /dev/null; do echo "Waiting for 10.0.0.2 - network interface might be down..."; sleep 1; done;echo "interface online"; storm nimbus -c storm.scheduler="'+scheduler+'"'+nimbusSchedulerConfig+'\'',volumes=[cwd+"/output/nimbus_logs:/logs", cwd+"/storm_containernet.yaml:/conf/storm.yaml", cwd+"/StormPlacementScheduler/target/StormPlacementScheduler-1.0-SNAPSHOT-jar-with-dependencies.jar:/apache-storm-2.4.0/extlib-daemon/StormPlacementScheduler.jar","/etc/timezone:/etc/timezone:ro","/etc/localtime:/etc/localtime:ro"])
	
	stormUI=net.addDocker('ui', ip="10.0.0.4", dimage="containernetstorm",dcmd='/bin/bash -x /docker-entrypoint.sh /bin/bash -c \'while ! ping -c 1 -W 1 10.0.0.3 > /dev/null; do echo "Waiting for 10.0.0.3 - network interface might be down...";echo "interface online"; sleep 1; done; storm ui\'',ports=[8080],port_bindings={8080:8080}, volumes=[cwd+"/storm_containernet.yaml:/conf/storm.yaml","/etc/timezone:/etc/timezone:ro","/etc/localtime:/etc/localtime:ro"])


	lowCPU=30
	midCPU=100
	highCPU=150
	lowMemory=768
	midMemory=1024
	highMemory=2048
		
	w1 = createStormWorker('w1',highCPU,highMemory)
	w2 = createStormWorker('w2',midCPU,midMemory)
	w3 = createStormWorker('w3',lowCPU,lowMemory)
	w4 = createStormWorker('w4',lowCPU,lowMemory)
	w5 = createStormWorker('w5',lowCPU,lowMemory)
	w6 = createStormWorker('w6',midCPU,midMemory)
	w7 = createStormWorker('w7',midCPU,midMemory)
	w8 = createStormWorker('w8',lowCPU,lowMemory)
	w9 = createStormWorker('w9',lowCPU,lowMemory)
	w10 = createStormWorker('w10',lowCPU,lowMemory)
	w11 = createStormWorker('w11',lowCPU,lowMemory)
	#sleep for 50 seconds, because supervisors are very slow in joining the cluster
	top=0
	stormTopology=net.addDocker("10.0.0."+str(254-top), ip="10.0.0."+str(254-top), dimage="containernetstorm",dcmd='/bin/bash -x /docker-entrypoint.sh /bin/bash -c \'while ! ping -c 1 -W 1 10.0.0.3 > /dev/null; do echo "Waiting for 10.0.0.3 - network interface might be down..."; sleep 1; done;echo "interface online";sleep 50;storm jar /topology.jar topologies.BenchmarkTopology benchmark_topology '+topologySeed+workerLimit+'\'', volumes=[cwd+"/output/topologySubmitter_logs:/logs",cwd+"/storm_containernet.yaml:/conf/storm.yaml", cwd+"/StormBenchmarkTopology/target/StormBenchmarkTopology-1.0-SNAPSHOT-jar-with-dependencies.jar:/topology.jar","/etc/timezone:/etc/timezone:ro","/etc/localtime:/etc/localtime:ro"])
			

	info('*** Adding switches\n')
	s1 = net.addSwitch('s1')
	s2 = net.addSwitch('s2')
	s3 = net.addSwitch('s3')
	s4 = net.addSwitch('s4')
	tds = net.addSwitch('s0')

	info('*** Creating links\n')
	net.addLink(s1, s3, cls=TCLink, delay='3ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s1, s2, cls=TCLink, delay='2ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s1, s4, cls=TCLink, delay='4ms', max_queue_size=qSizeConstant)#, bw=100)
	
	net.addLink(nimbus, s1)
	net.addLink(redis, s1)
	net.addLink(zookeeper, s1)
	net.addLink(stormUI, s1)
	net.addLink(s1, stormTopology)
	
	net.addLink(s1, w1, cls=TCLink, delay='7ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s3, w2, cls=TCLink, delay='0ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s3, w3, cls=TCLink, delay='2ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s3, w4, cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s4, w5, cls=TCLink, delay='0.5ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s1, w6, cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s4, w7, cls=TCLink, delay='3ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s4, w8, cls=TCLink, delay='0ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s2, w9, cls=TCLink, delay='2ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s2, w10, cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s2, w11, cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
	
	net.addLink(redis, tds)
	
	net.addLink(tds,w1)
	net.addLink(tds,w2)
	net.addLink(tds,w3)
	net.addLink(tds,w4)
	net.addLink(tds,w5)
	net.addLink(tds,w6)
	net.addLink(tds,w7)
	net.addLink(tds,w8)
	net.addLink(tds,w9)
	net.addLink(tds,w10)
	net.addLink(tds,w11)
	
	net.addLink(tds, testdriver)
	net.addLink(tds, redis)
	net.addLink(tds, stormUI)


	info('*** Starting network\n')
	net.start()
	time.sleep(1)
	workers=[w1,w2,w3,w4,w5,w6,w7,w8,w9,w10,w11]
	#fix second interface in parallel no bandwidth limit/latency free testdriver network
	for index, worker in enumerate(workers, start=1):
		fixSecondWorkerInterface(worker,"w"+str(index))
	
	#fixSecondWorkerInterface(w1,"w1")
	#fixSecondWorkerInterface(w2,"w2")
	#fixSecondWorkerInterface(w3,"w3")
	#fixSecondWorkerInterface(w4,"w4")
	#fixSecondWorkerInterface(w5,"w5")
	#fixSecondWorkerInterface(w6,"w6")
	#fixSecondWorkerInterface(w7,"w7")
	#fixSecondWorkerInterface(w8,"w8")
	#fixSecondWorkerInterface(w9,"w9")
	#fixSecondWorkerInterface(w10,"w10")
	#fixSecondWorkerInterface(w11,"w11")
	redis.cmd("ip addr add 10.1.0.5/24 dev redis-eth1")
	stormUI.cmd("ip addr add 10.1.0.4/24 dev ui-eth1")
	#redis.start()
	#zookeeper.start()
	#nimbus.start()
	#d1.start()
	#d2.start()
	#d3.start()
	#stormUI.start()
	info('*** Testing connectivity\n')
	#for index, worker in enumerate(workers, start=1):
	#	for x in range(1,12):
	#		if x!=index:
	#			worker.sendCmd("ping 10.0.0."+str(ips["w"+str(x)])+" -i 1")
	#net.ping([p1, p2])
	#info('zookeeper:\n')
	#info(zookeeper.cmd("ip -c a") + "\n")
	#info('redis:\n')
	#info(redis.cmd("ip -c a") + "\n")
	#info('nimbus:\n')
	#info(nimbus.cmd("ip -c a") + "\n")
	#info(nimbus.cmd("ping -c 3 10.0.0.2") + "\n")#check zookeeper
	#info(nimbus.cmd("nc -zvw10 10.0.0.2 2181") + "\n")#check zookeeper
	#info(nimbus.cmd("ping -c 3 10.0.0.5") + "\n")#check redis
	#info(nimbus.cmd("nc -zvw10 10.0.0.5 6379") + "\n")#check redis
	#info('stormUI:\n')
	#info(stormUI.cmd("ip -c a") + "\n")
	#info(stormUI.cmd("ping -c 3 10.0.0.3") + "\n")#check nimbus
	#info(stormUI.cmd("nc -zvw10 10.0.0.3 6627") + "\n")#check nimbus
	#info('d1:\n')
	#info(d1.cmd("ip -c a") + "\n")
	#info('d2:\n')
	#info(d2.cmd("ip -c a") + "\n")
	#info('d3:\n')
	#info(d3.cmd("ip -c a") + "\n")
	#info(d3.cmd("ping -c 3 10.0.0.3") + "\n")#check nimbus
	#info(d3.cmd("nc -zvw10 10.0.0.3 6627") + "\n")#check nimbus
	#info(d3.cmd("ping -c 3 10.0.0.2") + "\n")#check zookeeper
	#info(d3.cmd("nc -zvw10 10.0.0.2 2181") + "\n")#check zookeeper
	#info(d3.cmd("ping -c 3 10.0.0.5") + "\n")#check redis
	#info(d3.cmd("nc -zvw10 10.0.0.5 6379") + "\n")#check redis

	#info('client:\n')
	#info(client.cmd("ip -c a") + "\n")
	#info(client.cmd("time curl 10.0.0.251/9999") + "\n")
	#info('server:\n')
	#info(server.cmd("ip -c a") + "\n")

	info('*** waiting for testdriver to exit\n')
	while testdriver._is_container_running():
		time.sleep(1)
		
	info('*** testdriver stopped\n')
finally:
	info('*** Stopping network\n')
	net.stop()
	
	zookeeper.stop()
	redis.stop()
	testdriver.stop()
	nimbus.stop()
	stormUI.stop()
	w1.stop()
	w2.stop()
	w3.stop()
	w4.stop()
	w5.stop()
	w6.stop()
	w7.stop()
	w8.stop()
	w9.stop()
	w10.stop()
	w11.stop()
	stormTopology.stop()
	


