import dto.Latency;
import dto.Position;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class GroundTruth {
    private final Map<String,Map<String, Latency>> latencies=new HashMap<>();
    public void read(File file){


        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Set<Map.Entry<Object, Object>> entries=prop.entrySet();
        for (Map.Entry<Object, Object> e:entries){
            String direction=(String)e.getKey();
            String[] hosts=direction.split("->");
            String source=hosts[0];
            String target=hosts[1];
            latencies.computeIfAbsent(source,k->new HashMap<>());
            Map<String, Latency> sourceLatencyMap=latencies.get(source);
            Latency latency=new Latency((String) e.getValue());
            sourceLatencyMap.put(target,latency);
        }
    }
    public Latency getLatency(String host1, String host2){
        Map<String, Latency> host1latencies=latencies.get(host1);
        if (host1latencies==null) throw new RuntimeException("host1 not found in "+latencies.keySet());
        return host1latencies.get(host2);
    }
}
