import com.google.gson.Gson;
import dto.Position;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class Estimations {
    private static Gson gson=new Gson();
    private Map<String, Position> positionMap=new HashMap<>();
    public void read(File file){
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Set<Map.Entry<Object, Object>> entries=prop.entrySet();
        for (Map.Entry<Object, Object> e:entries){
            String host=(String)e.getKey();
            String json=(String) e.getValue();
            Position position=gson.fromJson(json, Position.class);
            positionMap.put(host,position);
        }
    }

    public double getLatency(String host1, String host2){
        Position host1pos=positionMap.get(host1);
        Position host2pos=positionMap.get(host2);
        Position distanceVector=new Position(host1pos);
        distanceVector.subtractPosition(host2pos);
        return distanceVector.getLength();
    }
    public Set<String> getHosts(){
        return positionMap.keySet();
    }
}
