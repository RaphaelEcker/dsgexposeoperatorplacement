import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Parser {

    public Parser() {
    }

    public void run() {
        process("..\\..\\latencyEstimationEvaluation\\Vivaldi\\extractor_logs");

    }
    public void process(String path){
        Path outputDir=Paths.get(path);

        GroundTruth groundTruth=new GroundTruth();
        groundTruth.read(outputDir.resolve("pingTruth.prop").toFile());
        Path historyDir=outputDir.resolve("history");
        String[] list=historyDir.toFile().list();
        List<String> historyFiles=Arrays.asList(list);
        List<Integer> seconds= historyFiles.stream().map(s -> s.split("\\.")[0]).map(Integer::parseInt).sorted().collect(Collectors.toList());
        StatsAggregator aggregator= null;
        try {
            aggregator = new StatsAggregator(outputDir);
            for (int second:seconds){
                System.out.println(second);
                File historyFile=historyDir.resolve(second+".prop").toFile();
                Estimations estimations=new Estimations();
                estimations.read(historyFile);
                aggregator.process(second,estimations,groundTruth, outputDir);
            }
            aggregator.close();
        } catch (IOException e) {
            if (aggregator!=null) {
                try {
                    aggregator.close();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
            throw new RuntimeException(e);
        }
    }
}
