import dto.Latency;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StatsAggregator {
    private final BufferedWriter meanLatencyErrorWriter;
    private final BufferedWriter meanSquaredLatencyErrorWriter;
    private final BufferedWriter meanMinLatencyErrorWriter;
    private final BufferedWriter meanSquaredMinLatencyErrorWriter;
    private final BufferedWriter hostCountWriter;
    private final BufferedWriter estimationsWithinMeanAndStdWriter;
    private final BufferedWriter estimationsWithinMeanAnd1MSWriter;
    private final BufferedWriter estimationsWithinMeanAnd3MSWriter;
    private final BufferedWriter estimationsWithinMeanAnd5MSWriter;
    private final BufferedWriter estimationsWithinMeanAnd10MSWriter;
    private final BufferedWriter estimationsWithinMeanAnd10RelWriter;
    private final BufferedWriter estimationsWithinMeanAnd30RelWriter;
    private final BufferedWriter estimationsWithinMeanAnd50RelWriter;
    private final BufferedWriter estimationsWithinMeanAnd20RelWriter;
    public StatsAggregator(Path outputDir) throws IOException {
        meanMinLatencyErrorWriter = new BufferedWriter(new FileWriter(outputDir.resolve("meanMinLatencyError.dat").toFile(), false));
        meanSquaredMinLatencyErrorWriter = new BufferedWriter(new FileWriter(outputDir.resolve("meanSquaredMinLatencyError.dat").toFile(), false));
        meanLatencyErrorWriter = new BufferedWriter(new FileWriter(outputDir.resolve("meanLatencyError.dat").toFile(), false));
        meanSquaredLatencyErrorWriter = new BufferedWriter(new FileWriter(outputDir.resolve("meanSquaredLatencyError.dat").toFile(), false));
        estimationsWithinMeanAndStdWriter = new BufferedWriter(new FileWriter(outputDir.resolve("estimationsWithinMeanAndStd.dat").toFile(), false));
        estimationsWithinMeanAnd1MSWriter = new BufferedWriter(new FileWriter(outputDir.resolve("estimationsWithinMeanAnd1MS.dat").toFile(), false));
        estimationsWithinMeanAnd3MSWriter = new BufferedWriter(new FileWriter(outputDir.resolve("estimationsWithinMeanAnd3MS.dat").toFile(), false));
        estimationsWithinMeanAnd5MSWriter = new BufferedWriter(new FileWriter(outputDir.resolve("estimationsWithinMeanAnd5MS.dat").toFile(), false));
        estimationsWithinMeanAnd10MSWriter = new BufferedWriter(new FileWriter(outputDir.resolve("estimationsWithinMeanAnd10MS.dat").toFile(), false));
        estimationsWithinMeanAnd10RelWriter = new BufferedWriter(new FileWriter(outputDir.resolve("estimationsWithinMeanAnd10Rel.dat").toFile(), false));
        estimationsWithinMeanAnd30RelWriter = new BufferedWriter(new FileWriter(outputDir.resolve("estimationsWithinMeanAnd30Rel.dat").toFile(), false));
        estimationsWithinMeanAnd50RelWriter = new BufferedWriter(new FileWriter(outputDir.resolve("estimationsWithinMeanAnd50Rel.dat").toFile(), false));
        estimationsWithinMeanAnd20RelWriter = new BufferedWriter(new FileWriter(outputDir.resolve("estimationsWithinMeanAnd20Rel.dat").toFile(), false));
        hostCountWriter = new BufferedWriter(new FileWriter(outputDir.resolve("hostCount.dat").toFile(), false));
        meanMinLatencyErrorWriter.write("x y\n");
        meanSquaredMinLatencyErrorWriter.write("x y\n");
        meanLatencyErrorWriter.write("x y\n");
        meanSquaredLatencyErrorWriter.write("x y\n");
        hostCountWriter.write("x y\n");
        estimationsWithinMeanAndStdWriter.write("x y\n");
        estimationsWithinMeanAnd1MSWriter.write("x y\n");
        estimationsWithinMeanAnd3MSWriter.write("x y\n");
        estimationsWithinMeanAnd5MSWriter.write("x y\n");
        estimationsWithinMeanAnd10MSWriter.write("x y\n");
        estimationsWithinMeanAnd10RelWriter.write("x y\n");
        estimationsWithinMeanAnd30RelWriter.write("x y\n");
        estimationsWithinMeanAnd50RelWriter.write("x y\n");
        estimationsWithinMeanAnd20RelWriter.write("x y\n");

    }

    public void process(int second,Estimations estimations, GroundTruth groundTruth, Path outputDir){
        DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        df.setMaximumFractionDigits(8);
        BigDecimal minErrorSum=BigDecimal.ZERO;
        BigDecimal minSquaredErrorSum=BigDecimal.ZERO;
        BigDecimal errorSum=BigDecimal.ZERO;
        BigDecimal squaredErrorSum=BigDecimal.ZERO;
        BigDecimal count=BigDecimal.ZERO;
        double[] meanErrorAtRank=null;
        double[] meanSquaredErrorAtRank=null;
        double[] meanRelErrorAtRank=null;
        long estimationWithinMeanAndSTDCount=0;
        long estimationWithinMeanAnd1MSCount=0;
        long estimationWithinMeanAnd3MSCount=0;
        long estimationWithinMeanAnd5MSCount=0;
        long estimationWithinMeanAnd10MSCount=0;
        long estimationWithinMeanAnd10RelCount=0;
        long estimationWithinMeanAnd20RelCount=0;
        long estimationWithinMeanAnd30RelCount=0;
        long estimationWithinMeanAnd50RelCount=0;
        long pairingCount=0;
        BigDecimal avgTruthLatency=BigDecimal.ZERO;
        BigDecimal avgLatency=BigDecimal.ZERO;

        if (estimations.getHosts().size()-1>=0) {
            meanErrorAtRank = new double[estimations.getHosts().size() - 1];
            meanSquaredErrorAtRank = new double[estimations.getHosts().size() - 1];
            meanRelErrorAtRank = new double[estimations.getHosts().size() - 1];
            for (int i=0;i<meanErrorAtRank.length;i++){
                meanErrorAtRank[i]=0;
                meanSquaredErrorAtRank[i]=0;
                meanRelErrorAtRank[i]=0;
            }
        }
        for (String host1:estimations.getHosts()){
            Map<String,Double> rankedMap=new HashMap<>();
            for (String host2:estimations.getHosts()){
                if (host1.equals(host2)) continue;
                pairingCount++;

                double ms=2*estimations.getLatency(host1,host2)/1000d;//oneway to rtt latency, ns -> ms
                Latency truth=groundTruth.getLatency(host1, host2);
                rankedMap.put(host2,truth.avgLatency);

                if (truth.avgLatency-truth.std<=ms&&truth.avgLatency+truth.std>=ms) estimationWithinMeanAndSTDCount++;
                if (truth.avgLatency-1<=ms&&truth.avgLatency+1>=ms) estimationWithinMeanAnd1MSCount++;
                if (truth.avgLatency-3<=ms&&truth.avgLatency+3>=ms) estimationWithinMeanAnd3MSCount++;
                if (truth.avgLatency-5<=ms&&truth.avgLatency+5>=ms) estimationWithinMeanAnd5MSCount++;
                if (truth.avgLatency-10<=ms&&truth.avgLatency+10>=ms) estimationWithinMeanAnd10MSCount++;

                if (truth.avgLatency*0.9d<=ms&&truth.avgLatency*1.1d>=ms) estimationWithinMeanAnd10RelCount++;
                if (truth.avgLatency*0.8d<=ms&&truth.avgLatency*1.2d>=ms) estimationWithinMeanAnd20RelCount++;
                if (truth.avgLatency*0.7d<=ms&&truth.avgLatency*1.3d>=ms) estimationWithinMeanAnd30RelCount++;
                if (truth.avgLatency*0.5d<=ms&&truth.avgLatency*1.5d>=ms) estimationWithinMeanAnd50RelCount++;

                avgTruthLatency=avgTruthLatency.add(BigDecimal.valueOf(truth.avgLatency));
                avgLatency=avgLatency.add(BigDecimal.valueOf(ms));
                minErrorSum=minErrorSum.add(BigDecimal.valueOf(Math.abs(ms-truth.minLatency)));
                errorSum=errorSum.add(BigDecimal.valueOf(Math.abs(ms-truth.avgLatency)));
                minSquaredErrorSum=minSquaredErrorSum.add(BigDecimal.valueOf(Math.abs(ms-truth.minLatency)).pow(2));
                squaredErrorSum=squaredErrorSum.add(BigDecimal.valueOf(Math.abs(ms-truth.avgLatency)).pow(2));
                count=count.add(BigDecimal.ONE);
            }
            if (meanErrorAtRank!=null&&meanSquaredErrorAtRank!=null&&meanRelErrorAtRank!=null) {
                List<Map.Entry<String, Double>> sorted =
                        rankedMap.entrySet().stream()
                                .sorted(Map.Entry.comparingByValue()).collect(Collectors.toList());
                for (int i = 0; i < meanErrorAtRank.length; i++) {
                    Map.Entry<String, Double> host2Entry = sorted.get(i);
                    double absoluteError = Math.abs(host2Entry.getValue() - 2 * estimations.getLatency(host1, host2Entry.getKey()) / 1000d);
                    meanErrorAtRank[i] += absoluteError;
                    meanSquaredErrorAtRank[i] += Math.pow(absoluteError, 2);
                    meanRelErrorAtRank[i]+=Math.abs(host2Entry.getValue()/(2 * estimations.getLatency(host1, host2Entry.getKey()) / 1000d)-1);
                }
            }

        }
        if (meanErrorAtRank!=null&&meanSquaredErrorAtRank!=null&&meanRelErrorAtRank!=null) {
            try {
                File plotDir=outputDir.resolve("snapshots").toFile();
                if (!plotDir.exists()) plotDir.mkdir();
                BufferedWriter meanErrorAtRankWriter = new BufferedWriter(new FileWriter(outputDir.resolve("snapshots/meanErrorAtRankAt"+second+".dat").toFile(), false));
                BufferedWriter meanSquaredErrorAtRankWriter = new BufferedWriter(new FileWriter(outputDir.resolve("snapshots/meanSquaredErrorAtRankAt"+second+".dat").toFile(), false));
                BufferedWriter meanRelErrorAtRankWriter = new BufferedWriter(new FileWriter(outputDir.resolve("snapshots/meanRelErrorAtRankAt"+second+".dat").toFile(), false));
                meanErrorAtRankWriter.write("x y\n");
                meanSquaredErrorAtRankWriter.write("x y\n");
                meanRelErrorAtRankWriter.write("x y\n");
                for (int i = 0; i < meanErrorAtRank.length; i++) {
                    meanErrorAtRankWriter.write((i+1)+" "+df.format(meanErrorAtRank[i]/estimations.getHosts().size())+"\n");
                    meanSquaredErrorAtRankWriter.write((i+1)+" "+df.format(meanSquaredErrorAtRank[i]/estimations.getHosts().size())+"\n");
                    meanRelErrorAtRankWriter.write((i+1)+" "+df.format(meanRelErrorAtRank[i]/estimations.getHosts().size())+"\n");
                }
                meanErrorAtRankWriter.close();
                meanSquaredErrorAtRankWriter.close();
                meanRelErrorAtRankWriter.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        if (pairingCount>0){
            try {
                estimationsWithinMeanAndStdWriter.write(second+" "+df.format((100d*estimationWithinMeanAndSTDCount)/pairingCount)+"\n");
                estimationsWithinMeanAnd1MSWriter.write(second+" "+df.format((100d*estimationWithinMeanAnd1MSCount)/pairingCount)+"\n");
                estimationsWithinMeanAnd3MSWriter.write(second+" "+df.format((100d*estimationWithinMeanAnd3MSCount)/pairingCount)+"\n");
                estimationsWithinMeanAnd5MSWriter.write(second+" "+df.format((100d*estimationWithinMeanAnd5MSCount)/pairingCount)+"\n");
                estimationsWithinMeanAnd10MSWriter.write(second+" "+df.format((100d*estimationWithinMeanAnd10MSCount)/pairingCount)+"\n");

                estimationsWithinMeanAnd10RelWriter.write(second+" "+df.format((100d*estimationWithinMeanAnd10RelCount)/pairingCount)+"\n");
                estimationsWithinMeanAnd20RelWriter.write(second+" "+df.format((100d*estimationWithinMeanAnd20RelCount)/pairingCount)+"\n");
                estimationsWithinMeanAnd30RelWriter.write(second+" "+df.format((100d*estimationWithinMeanAnd30RelCount)/pairingCount)+"\n");
                estimationsWithinMeanAnd50RelWriter.write(second+" "+df.format((100d*estimationWithinMeanAnd50RelCount)/pairingCount)+"\n");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        if (count.longValue()>0) {
            avgTruthLatency = avgTruthLatency.divide(count, RoundingMode.HALF_UP);
            avgLatency = avgLatency.divide(count, RoundingMode.HALF_UP);
            System.out.println("truth latency avg:"+avgTruthLatency+" estimated:"+avgLatency);
            minErrorSum = minErrorSum.divide(count, RoundingMode.HALF_UP);
            minSquaredErrorSum = minSquaredErrorSum.divide(count, RoundingMode.HALF_UP);
            errorSum = errorSum.divide(count, RoundingMode.HALF_UP);
            squaredErrorSum = squaredErrorSum.divide(count, RoundingMode.HALF_UP);
            try {

                hostCountWriter.write(second + " " + estimations.getHosts().size() + "\n");
                meanLatencyErrorWriter.write(second + " " + df.format(errorSum.doubleValue()) + "\n");
                meanMinLatencyErrorWriter.write(second + " " + df.format(minErrorSum.doubleValue()) + "\n");
                meanSquaredLatencyErrorWriter.write(second + " " + df.format(squaredErrorSum.doubleValue()) + "\n");
                meanSquaredMinLatencyErrorWriter.write(second + " " + df.format(minSquaredErrorSum.doubleValue()) + "\n");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        //distribution of nearest neighbour latencies (for each second)
    }
    public void close() throws IOException {
        meanLatencyErrorWriter.close();
        meanSquaredLatencyErrorWriter.close();
        meanMinLatencyErrorWriter.close();
        meanSquaredMinLatencyErrorWriter.close();
        hostCountWriter.close();
        estimationsWithinMeanAndStdWriter.close();
        estimationsWithinMeanAnd1MSWriter.close();
        estimationsWithinMeanAnd3MSWriter.close();
        estimationsWithinMeanAnd5MSWriter.close();
        estimationsWithinMeanAnd10MSWriter.close();
        estimationsWithinMeanAnd10RelWriter.close();
        estimationsWithinMeanAnd20RelWriter.close();
        estimationsWithinMeanAnd30RelWriter.close();
        estimationsWithinMeanAnd50RelWriter.close();


    }
}
