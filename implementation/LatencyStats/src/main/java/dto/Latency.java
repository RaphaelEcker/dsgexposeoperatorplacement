package dto;

public class Latency {
    public double minLatency;
    public double maxLatency;
    public double avgLatency;
    public double std;

    public Latency(String latencyStats) {
        String[] split = latencyStats.split("/");
        minLatency = Double.parseDouble(split[0]);
        avgLatency = Double.parseDouble(split[1]);
        maxLatency = Double.parseDouble(split[2]);
        std = Double.parseDouble(split[3]);
    }
}
