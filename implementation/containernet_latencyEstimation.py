#!/usr/bin/python
from mininet.net import Containernet
from mininet.node import Controller
from mininet.cli import CLI
from mininet.link import TCLink
from mininet.log import info, setLogLevel
import time
import os
import sys
import re


cwd=os.getcwd()
called=0;
ips= {}
qSizeConstant=None
smallNet=False;

#setLogLevel('warning')
setLogLevel('info')

def createHost(name):
	global called
	global cwd
	called+=1
	ips[name]=str(9+called)
	return net.addDocker("10.0.0."+str(9+called), ip="10.0.0."+str(9+called),dimage="containernetlatencyestimator",dcmd="/bin/bash -c \'java -jar /usr/local/lib/demo.jar &>/usr/local/lib/output/latencyEstimator.txt\'",volumes=[cwd+"/output/estimator_logs/"+name+":/usr/local/lib/output","/etc/timezone:/etc/timezone:ro","/etc/localtime:/etc/localtime:ro"])


wmap={}
try: 
	net = Containernet(controller=Controller)
	info('*** Adding controller\n')
	net.addController('c0')

	info('*** Adding docker containers\n')
	#5=redis
	#6=extractor
	#10+=supervisors
	redis=net.addDocker('redis', ip="10.0.0.5",dimage="containernetredis",dcmd="/bin/bash -x /usr/local/bin/docker-entrypoint.sh redis-server /usr/local/etc/redis/redis.conf",volumes=["/etc/timezone:/etc/timezone:ro","/etc/localtime:/etc/localtime:ro"])
	extractor=net.addDocker('extractor', ip="10.0.0.6",dimage="containernetredispositionextractor",dcmd="/bin/bash -c \'java -jar /usr/local/lib/demo.jar &>/usr/local/lib/output/extractor.txt\'",volumes=[cwd+"/output/extractor_logs:/usr/local/lib/output","/etc/timezone:/etc/timezone:ro","/etc/localtime:/etc/localtime:ro"])

	if smallNet:
		for x in range(1, 12):
			wmap['w'+str(x)]=createHost('w'+str(x))
	else:
		for x in range(1, 41):
			wmap['w'+str(x)]=createHost('w'+str(x))
	
	
	info('*** Adding switches\n')
	s1 = net.addSwitch('s1')
	s2 = net.addSwitch('s2')
	s3 = net.addSwitch('s3')
	s4 = net.addSwitch('s4')
	if not smallNet:
		s5 = net.addSwitch('s5')
		s6 = net.addSwitch('s6')
		s7 = net.addSwitch('s7')
		s8 = net.addSwitch('s8')
		s9 = net.addSwitch('s9')
		s10 = net.addSwitch('s10')

	info('*** Creating links\n')
	net.addLink(s1, s3, cls=TCLink, delay='3ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s1, s2, cls=TCLink, delay='2ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s1, s4, cls=TCLink, delay='4ms', max_queue_size=qSizeConstant)#, bw=100)
	if not smallNet:
		net.addLink(s5, s3, cls=TCLink, delay='3ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s6, s3, cls=TCLink, delay='2ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s7, s4, cls=TCLink, delay='4ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s8, s4, cls=TCLink, delay='3ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s9, s2, cls=TCLink, delay='2ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s10, s2, cls=TCLink, delay='4ms', max_queue_size=qSizeConstant)#, bw=100)
	
	net.addLink(redis, s1)
	net.addLink(extractor, s1)
	
	net.addLink(s1, wmap.get('w1'), cls=TCLink, delay='7ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s3, wmap.get('w2'), cls=TCLink, delay='0ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s3, wmap.get('w3'), cls=TCLink, delay='2ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s3, wmap.get('w4'), cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s4, wmap.get('w5'), cls=TCLink, delay='0.5ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s1, wmap.get('w6'), cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s4, wmap.get('w7'), cls=TCLink, delay='3ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s4, wmap.get('w8'), cls=TCLink, delay='0ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s2, wmap.get('w9'), cls=TCLink, delay='2ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s2, wmap.get('w10'), cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
	net.addLink(s2, wmap.get('w11'), cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
	if not smallNet:
		net.addLink(s3, wmap.get('w12'), cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s4, wmap.get('w13'), cls=TCLink, delay='2ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s2, wmap.get('w14'), cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s1, wmap.get('w15'), cls=TCLink, delay='3ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s5, wmap.get('w16'), cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s5, wmap.get('w17'), cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s5, wmap.get('w18'), cls=TCLink, delay='0ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s5, wmap.get('w19'), cls=TCLink, delay='2ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s6, wmap.get('w20'), cls=TCLink, delay='4ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s6, wmap.get('w21'), cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s6, wmap.get('w22'), cls=TCLink, delay='0ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s6, wmap.get('w23'), cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s7, wmap.get('w24'), cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s7, wmap.get('w25'), cls=TCLink, delay='2ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s7, wmap.get('w26'), cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s7, wmap.get('w27'), cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s8, wmap.get('w28'), cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s8, wmap.get('w29'), cls=TCLink, delay='2ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s8, wmap.get('w30'), cls=TCLink, delay='0ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s8, wmap.get('w31'), cls=TCLink, delay='0ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s9, wmap.get('w32'), cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s9, wmap.get('w33'), cls=TCLink, delay='3ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s9, wmap.get('w34'), cls=TCLink, delay='2ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s9, wmap.get('w35'), cls=TCLink, delay='2ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s10, wmap.get('w36'), cls=TCLink, delay='0ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s10, wmap.get('w37'), cls=TCLink, delay='0.5ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s10, wmap.get('w38'), cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s10, wmap.get('w39'), cls=TCLink, delay='1ms', max_queue_size=qSizeConstant)#, bw=100)
		net.addLink(s1, wmap.get('w40'), cls=TCLink, delay='3ms', max_queue_size=qSizeConstant)#, bw=100)
	
	
	info('*** Starting network\n')
	net.start()
	workers=list(wmap.values())
	info('*** Testing connectivity\n')
	if smallNet:
		time.sleep(30)
	else:
		time.sleep(360)
	with open("output/extractor_logs/pingTruth.prop", "a") as myfile:
		for index, worker in enumerate(workers, start=1):
			for index2, worker2 in enumerate(workers, start=1):
				if index!=index2:
					op=worker.cmd("ping -i 0 -c 100 10.0.0."+str(ips["w"+str(index2)]))
					for m in re.finditer('rtt min/avg/max/mdev \=\s(\d+\.*\d*)/(\d+\.*\d*)/(\d+\.*\d*)/(\d+\.*\d*) ms',op):
						string="10.0.0."+str(ips["w"+str(index)])+"->10.0.0."+str(ips["w"+str(index2)])+"="+m.group(1)+"/"+m.group(2)+"/"+m.group(3)+"/"+m.group(4)+"\n"
						info(string)
						myfile.write(string)
					time.sleep(0.05)
	info('*** Running Containernet CLI\n')
	CLI(net)
finally:
	info('*** Stopping network\n')
	net.stop()
	
	redis.stop()
	extractor.stop()
	workers=list(wmap.values())
	for w in workers:
		w.stop()