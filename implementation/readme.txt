Latency-aware Online Operator Scheduler for Apache Storm.
Developed for Apache Storm 2.4.0 and, based on a brief evaluation, still compatible with 2.5.0.

The execution environment requires the installation of:
    Python
    Docker
    Containernet
	
Additionally, to build the scheduler and related components the following is required:
	Maven
	Java SDK (compatible with Java 11 language version)

Build each Java Maven project (mvn package):
    StormBenchmarkTopology
    StormMetricsConsumer
    StormPlacementScheduler

Then build all the Docker images: ./dockerBuild.sh

Then everything should be ready to start.
Warning: During the evaluation ./fix_containernet_messes.sh will be executed which stops and removes all Docker containers, adjust the script as necessary.
The Apache Storm online scheduler can then be tested by executing: ./bulktest.sh, depending on the local setup this could require sudo permissions.
Inside ./bulktest there are various variables hardcoded, such as the topologies to test, the algorithms tested, iterations and so on. This will execute containernet_storm.py with various parameter combinations for each individual experiment. During this processes StormUI will also be launched on port 8080 for debugging purposes.

For the evaluation of the latency estimation execute containernet_latencyEstimation.py with python.

Both scripts write to the output folder and in the case of the scheduler evaluation copy the contents to the ExperimentalResults folder once one experiment has been completed. All logs from Nimbus and the workers are accessible. Log output of the scheduler, such as current variables relevant for the operator placement can be found in the Nimbus logs. The TestDriver logs contain the benchmarking results. Additionally, there are other folders like the topologySubmitter logs which are mostly for debugging, but can also contain useful information like the parameters of the submitted topology.

To help aggregate and analyse the created logs there are the LogMerger (scheduler evaluation) and LatencyStats (latency evaluation) projects. In both projects the path to the log files must be manually set in their Main/Parser classes.

Update Storm:

To adjust the Apache Storm version of the environment update the Docker image tags for Storm in the Containernet_supervisor and Containernet_storm Docker files.
Additionally, update the Docker mounting path in containernet_storm.py for the nimbus and supervisor instances to refer to the respective version, such that they can then find the placement heuristic and metric consumer.
For the implementations, the pom.xml files of the Storm-projects should be updated to depend on the specific version and then rebuilt, although Storm does allow for some variances.


Troubleshooting:

Nearly everything relies on Nimbus, check its logs for exceptions or configuration issues first.

Then verify if some services can form connections like intended. Check StormUI (or the Nimbus logs) for launched Storm supervisors or the topology. If nothing shows up with a few minutes of wait time, then it is likely to be a problem with the Containernet setup. Near the bottom of containernet_storm.py are various commented commands that can be used to help debugging potential connection issues.

If the Storm cluster can launch, but a topology is never submitted, then it is likely that the topologySubmitter logs contain a connection refused exception: this can occur when it is attempted to launch a topology before Nimbus is ready. In this case a simple sleep time in containernet_storm.py for the stormTopology can be increased. If instead it only reports to be waiting and the interface never came online, then this could again be a problem with the Containernet setup.

ClassNotFoundExceptions are likely to come from an incorrect mounting path of the scheduler or metrics consumer in the Storm Docker containers. These currently contain the current Storm version number and must therefore be updated if the Storm version is changed.

The Docker networks use the 10.0.0.x and 10.1.0.x ip spaces. While dDcker should keep everything segregated there could be odd side effects if ips overlap, especially considering that some services like StormUI are exposed to the host network.