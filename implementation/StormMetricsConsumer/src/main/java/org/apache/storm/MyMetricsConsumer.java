package org.apache.storm;

import com.google.gson.Gson;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.storm.metric.api.IMetricsConsumer;
import org.apache.storm.task.IErrorReporter;
import org.apache.storm.task.TopologyContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MyMetricsConsumer implements IMetricsConsumer {
    private final Pattern EXECUTE_COUNTS_EXTRACTION_PATTERN = Pattern.compile("__execute-count-(.+?:.+)");
    private final Pattern LATENCY_EXTRACTION_PATTERN = Pattern.compile("__execute-latency-(.+?:.+)");
    private static final int CAPACITY_TIMESPAN = 60000;
    private static final Logger LOG = LoggerFactory.getLogger(MyMetricsConsumer.class);
    private String topologyId;
    private CuratorFramework curatorFramework;
    private final Gson gson = new Gson();
    private Map<String, List<CapacityTuple>> operatorToListOfCapacityTuples = new HashMap<>();

    @Override
    public void prepare(Map<String, Object> topoConf,
                        Object registrationArgument,
                        TopologyContext context,
                        IErrorReporter errorReporter) {
        //LOG.info("loaded MyMetricsConsumer");
        topologyId = context.getStormId();
        List<String> servers = (List<String>) topoConf.get(Config.TRANSACTIONAL_ZOOKEEPER_SERVERS);
        Long port = (Long) topoConf.get(Config.TRANSACTIONAL_ZOOKEEPER_PORT);
        if (servers == null || port == null) {
            servers = (List<String>) topoConf.get(Config.STORM_ZOOKEEPER_SERVERS);
            port = (Long) topoConf.get(Config.STORM_ZOOKEEPER_PORT);
        }
        String connectionString = servers.get(0) + ":" + port.toString();
        curatorFramework = CuratorFrameworkFactory.builder()
                .connectString(connectionString)
                .namespace("MyMetricsConsumer")
                .retryPolicy(new ExponentialBackoffRetry(1000, 3))
                .build();
        curatorFramework.start();
    }

    @Override
    public void handleDataPoints(TaskInfo taskInfo, Collection<DataPoint> dataPoints) {
        //LOG.info(Arrays.asList(taskInfo, dataPoints, topologyId).toString());
        try {
            String path = "/" + topologyId + "/" + taskInfo.srcComponentId + "/" + taskInfo.srcTaskId;
            //computeCustomCapacity(path, taskInfo,dataPoints);
            dataPoints.add(new DataPoint("timestamp",taskInfo.timestamp));
            String json = gson.toJson(dataPoints);
            byte[] bytes = json.getBytes(StandardCharsets.UTF_8);
            if (curatorFramework.checkExists().forPath(path) == null) {
                String test = curatorFramework.create().creatingParentsIfNeeded().forPath(path, bytes);
                //LOG.info("MyMetricsConsumer: " + test);
            } else {
                curatorFramework.setData().forPath(path, bytes);
            }

        } catch (Exception e) {
            LOG.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private void computeCustomCapacity(String path, TaskInfo taskInfo,Collection<DataPoint> dataPoints) {

        /*
        String inputs=dataPoints.stream().map(m->m.name).filter(n ->n.startsWith("__execute-count")&&n.endsWith(":default")).collect(Collectors.joining(", "));
        String latencies=dataPoints.stream().map(m->m.name).filter(n ->n.startsWith("__execute-latency")&&n.endsWith(":default")).collect(Collectors.joining(", "));
        LOG.info(taskInfo.srcTaskId+" has input counts: "+inputs);
        LOG.info(taskInfo.srcTaskId+" has execution latencies: "+latencies);
        */
        List<DataPoint> executeCountDatapointList = dataPoints.stream().filter(m ->m.name.startsWith("__execute-count")&&m.name.endsWith(":default")).collect(Collectors.toList());//__execute-count as map in v1 API
        List<DataPoint> executeLatencyDatapointList = dataPoints.stream().filter(m -> m.name.startsWith("__execute-latency")&&m.name.endsWith(":default")).collect(Collectors.toList());//__execute-latency as map in v1 API
        if (!executeCountDatapointList.isEmpty() && !executeLatencyDatapointList.isEmpty()) {
            Map<String, Long> executeCounts = new HashMap<>();
            Map<String, Double> executeLatencies = new HashMap<>();
            //reconstruct v1-API-like maps
            convertExecutionCountListIntoMap(executeCountDatapointList, executeCounts);
            convertExecutionLatencyListIntoMap(executeLatencyDatapointList, executeLatencies);

            if (!operatorToListOfCapacityTuples.containsKey(path)){
                operatorToListOfCapacityTuples.put(path,new ArrayList<>());
            }

            List<CapacityTuple> capacityTuples = operatorToListOfCapacityTuples.get(path);


            capacityTuples.add(new CapacityTuple(taskInfo.timestamp,path, executeLatencies, executeCounts));
            //remove old entries
            long timestamp = taskInfo.timestamp;//System.currentTimeMillis();
            while (capacityTuples.size() > 2 && capacityTuples.get(0).getTimestamp() + CAPACITY_TIMESPAN < timestamp) {//remove old entries
                capacityTuples.remove(0);
            }
            long summedLatency = getSummedLatency(executeCounts, capacityTuples);

            if (capacityTuples.size() > 1) {
                double capacity = Math.max(summedLatency / (double) (taskInfo.timestamp - capacityTuples.get(0).getTimestamp()),0);
                dataPoints.add(new DataPoint("customCapacity", capacity));
                LOG.info("MyMetricsConsumer: adding capacity:"+capacity);
            }else{
                LOG.info("MyMetricsConsumer: small capacity: " +capacityTuples.size());
            }
        }
    }

    private void convertExecutionLatencyListIntoMap(List<DataPoint> executeLatencyDatapointList, Map<String, Double> executeLatencies) {
        for (DataPoint d: executeLatencyDatapointList){
            Matcher matcher = LATENCY_EXTRACTION_PATTERN.matcher(d.name);
            if (matcher.matches() && matcher.groupCount() >= 1) {
                String name = matcher.group(1);
                executeLatencies.put(name, (Double)d.value);
            }
        }
    }

    private void convertExecutionCountListIntoMap(List<DataPoint> executeCountDatapointList, Map<String, Long> executeCounts) {
        for (DataPoint d: executeCountDatapointList){
            Matcher matcher = EXECUTE_COUNTS_EXTRACTION_PATTERN.matcher(d.name);
            if (matcher.matches() && matcher.groupCount() >= 1) {
                String name = matcher.group(1);
                executeCounts.put(name, (Long)d.value);
            }
        }
    }

    private long getSummedLatency(Map<String, Long> executeCounts, List<CapacityTuple> capacityTuples) {
        long summedLatency = 0;
        Iterator<CapacityTuple> it = capacityTuples.iterator();
        it.next();
        for (CapacityTuple t; it.hasNext(); ) {
            t = it.next();
            Map<String, Double> latency = t.getLatencies();
            for (String s : latency.keySet()) {
                if (executeCounts.containsKey(s)) {
                    summedLatency += executeCounts.get(s)*latency.get(s);
                }
            }
        }
        return summedLatency;
    }

    @Override
    public void cleanup() {
        /*try {
            curatorFramework.delete().guaranteed().deletingChildrenIfNeeded().forPath("/" + topologyId);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error(e.toString());
        }*/
        curatorFramework.close();
    }
}
