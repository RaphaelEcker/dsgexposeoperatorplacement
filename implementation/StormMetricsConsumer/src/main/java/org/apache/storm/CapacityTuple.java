package org.apache.storm;

import java.util.Map;

public class CapacityTuple {
    private long timestamp;
    private String taskId;
    private Map<String, Double> latencies;
    private Map<String, Long> count;

    public CapacityTuple(long timestamp, String taskId,
                         Map<String, Double> latencies,
                         Map<String, Long> count) {
        this.timestamp=timestamp;
        this.taskId = taskId;
        this.latencies = latencies;
        this.count = count;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getTaskId() {
        return taskId;
    }

    public Map<String, Double> getLatencies() {
        return latencies;
    }

    public Map<String, Long> getCount() {
        return count;
    }
}