#
# Build stage
#
FROM maven:3.6.0-jdk-11-slim AS build

COPY LatencyEstimation/pom.xml /home/app/pom.xml
#RUN mvn -f /home/app/pom.xml dependency:go-offline
RUN mvn -f /home/app/pom.xml verify --fail-never
COPY LatencyEstimation/src /home/app/src
RUN mvn -f /home/app/pom.xml clean package -DskipTests

FROM storm:2.4.0
RUN apt-get update && apt-get install -y \
        bash \
        iproute2 \
		iputils-ping \
		net-tools \
		netcat \
		dos2unix && \
		apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/*
COPY docker-entrypoints/storm-docker/docker-entrypoint_Containernet_supervisor.sh /docker-entrypoint.sh
COPY --from=build /home/app/target/LatencyEstimation-1.0-SNAPSHOT-jar-with-dependencies.jar /latencyEstimation.jar
RUN dos2unix /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh
RUN chmod +x /latencyEstimation.jar
RUN set -eux; \
	chown -R storm:storm /docker-entrypoint.sh; \
	chown -R storm:storm /latencyEstimation.jar; 