package org.apache.storm.scheduler.scoring;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.Operator;
import org.apache.storm.scheduler.datastructures.placementProblem.PlacementProblem;
import org.apache.storm.scheduler.datastructures.placementProblem.Topology;

public class UninitializedOperatorsWithAllNotCohostedSuccessors extends IScoring {
    @Override
    public double score(PlacementProblem problem, Assignment assignment, String topId, boolean currentlyExecuted) {
        Topology topology = problem.getTopologies().get(topId);
        double count = 0;
        for (Operator o : topology.getOperatorMap().values()) {
            if (o.getAvgTupleSize() == null) {//not initialized
                boolean allSuccessorsNotCohosted = true;
                for (String successorId : o.getSuccessors()) {
                    Operator successor = topology.getOperatorMap().get(successorId);
                    if (assignment.areColocated(o, successor)) {
                        allSuccessorsNotCohosted = false;
                        //break;
                    } else {
                        count += 1d / o.getSuccessors().size();
                    }
                }
                if (allSuccessorsNotCohosted) count += o.getSuccessors().size();
            }
        }
        return count;
    }
}
