package org.apache.storm.scheduler.constraints;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.PlacementProblem;
import org.apache.storm.scheduler.scoring.EvenAckerDistribution;

public class AllConstraints implements IConstraint {
    MemoryConstraint memoryConstraint=new MemoryConstraint();
    ProcessCPUConstraint processCPUConstraint=new ProcessCPUConstraint();
    OneTopIDPerSlot oneTopIDPerSlot=new OneTopIDPerSlot();
    @Override
    public double score(PlacementProblem problem, Assignment assignment, String topId, boolean currentlyExecuted) {
        double memoryViolations = memoryConstraint.score(problem, assignment, topId,currentlyExecuted);
        double cpuViolations = processCPUConstraint.score(problem, assignment, topId,currentlyExecuted);
        double multipleTopologiesPerSlotViolations = oneTopIDPerSlot.score(problem, assignment, topId,currentlyExecuted);
        return memoryViolations+multipleTopologiesPerSlotViolations+cpuViolations;
    }
}
