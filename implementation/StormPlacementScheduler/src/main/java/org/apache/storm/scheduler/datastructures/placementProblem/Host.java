package org.apache.storm.scheduler.datastructures.placementProblem;

import java.util.HashMap;
import java.util.Map;

public class Host {
    private String id;
    private Map<String, SupervisorProcess> supervisorProcessMap = new HashMap<>();

    public Host(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, SupervisorProcess> getSupervisorProcessMap() {
        return supervisorProcessMap;
    }

    public void setSupervisorProcessMap(Map<String, SupervisorProcess> supervisorProcessMap) {
        this.supervisorProcessMap = supervisorProcessMap;
    }

    @Override
    public String toString() {
        return "Host{" +
                "id='" + id + '\'' +
                '}';
    }
}
