package org.apache.storm.scheduler.localSearch.placementModification;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.Operator;
import org.apache.storm.scheduler.datastructures.placementProblem.SupervisorProcess;

import java.util.Set;
import java.util.stream.Collectors;

public class MoveOperators implements AssignmentModification {

    private final Set<Operator> operators;
    private final SupervisorProcess target;
    private SupervisorProcess source;

    public MoveOperators(Set<Operator> operators, SupervisorProcess target) {
        this.operators = operators;
        this.target = target;
    }

    @Override
    public void execute(Assignment assignment) {
        if (operators.isEmpty()) return;
        source = assignment.getSupervisorProcess(operators.stream().findFirst().get());
        for (Operator o : operators) {
            assignment.assign(o, target);
        }
    }

    @Override
    public void revert(Assignment assignment) {
        for (Operator o : operators) {
            assignment.assign(o, source);
        }
    }


    @Override
    public String toString() {
        return "MoveOperator{" +
                "operator=" + operators.stream().map(w -> w.getId()).collect(Collectors.joining(", ")) +
                ", target=" + target.getId() +
                ", source=" + (source != null ? source.getId() : source) +
                '}';
    }
}
