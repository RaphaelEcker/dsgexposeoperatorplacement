package org.apache.storm.scheduler.localSearch.placementModification;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.Operator;
import org.apache.storm.scheduler.datastructures.placementProblem.SupervisorProcess;

public class MoveOperator implements AssignmentModification {

    private final Operator operator;
    private final SupervisorProcess target;
    private SupervisorProcess source;

    public MoveOperator(Operator operator, SupervisorProcess target) {
        this.operator = operator;
        this.target = target;
    }

    @Override
    public void execute(Assignment assignment) {
        source = assignment.getSupervisorProcess(operator);
        assignment.assign(operator, target);
    }

    @Override
    public void revert(Assignment assignment) {
        assignment.assign(operator, source);
    }


    @Override
    public String toString() {
        return "MoveOperator{" +
                "operator=" + operator.getId() +
                ", target=" + target.getId() +
                ", source=" + (source != null ? source.getId() : source) +
                '}';
    }
}
