package org.apache.storm.scheduler.datastructures.placementProblem;

public class ExecutorProcess {
    private SupervisorProcess supervisorProcess;
    private String id;
    private String runningTopologyId;

    public ExecutorProcess(String id, SupervisorProcess supervisorProcess) {
        this.supervisorProcess = supervisorProcess;
        this.id = id;
    }

    public SupervisorProcess getHost() {
        return supervisorProcess;
    }

    public void setHost(SupervisorProcess supervisorProcess) {
        this.supervisorProcess = supervisorProcess;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRunningTopologyId() {
        return runningTopologyId;
    }

    public void setRunningTopologyId(String runningTopologyId) {
        this.runningTopologyId = runningTopologyId;
    }

    @Override
    public String toString() {
        return "ExecutorProcess{" +
                "id='" + id + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExecutorProcess)) return false;

        final ExecutorProcess that = (ExecutorProcess) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
