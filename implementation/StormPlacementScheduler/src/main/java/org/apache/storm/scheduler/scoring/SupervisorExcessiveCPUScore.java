package org.apache.storm.scheduler.scoring;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.PlacementProblem;
import org.apache.storm.scheduler.datastructures.placementProblem.SupervisorProcess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SupervisorExcessiveCPUScore extends IScoring {
    private static final double USAGE_FACTOR=0.5;

    @Override
    public double score(PlacementProblem problem, Assignment assignment, String topId, boolean currentlyExecuted) {
        if (currentlyExecuted) return 1;
        double largestFactor = 1;
        for (SupervisorProcess s : assignment.getSupervisorToOperatorMap().keySet()) {
            double supervisorUsedCPU = s.getUsedCpu() + assignment.getCPUUsage(s);
            largestFactor = Math.max(largestFactor, supervisorUsedCPU / (s.getTotalCpu() * USAGE_FACTOR));
        }
        largestFactor = Math.pow(largestFactor, 2);
        return largestFactor;
    }
}
