package org.apache.storm.scheduler.scoring;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.PlacementProblem;

public abstract class IScoring {
    protected boolean output = false;

    public abstract double score(PlacementProblem problem, Assignment assignment, String topId, boolean currentlyExecuted);

    public double score(PlacementProblem problem, Assignment assignment, String topId) {
        return score(problem, assignment, topId,false);
    }


    public void setOutput(boolean state) {
        output = state;
    }
}
