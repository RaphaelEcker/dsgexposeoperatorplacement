package org.apache.storm.scheduler.topologicalOrdering;

import org.apache.storm.scheduler.datastructures.placementProblem.Topology;

import java.util.*;

public class TarjansAlgorithm {
    private int index = 0;
    private final Deque<String> S = new ArrayDeque<>();
    private final Map<String, Integer> vertexIndex = new HashMap<>();
    private final Map<String, Integer> lowLink = new HashMap<>();
    private final Map<String, Boolean> onStack = new HashMap<>();
    private Topology topology;
    private Set<String> containedVertices;
    private final List<Set<String>> components = new ArrayList<>();

    public List<Set<String>> calculateComponentsInTopologicalOrder(Topology topology,
                                                                   Set<String> containedVertices) {
        this.topology = topology;
        this.containedVertices = containedVertices;
        for (String vertex : containedVertices) {
            if (!vertexIndex.containsKey(vertex)) {
                strongConnect(vertex);
            }
        }
        return components;
    }

    private void strongConnect(String vertex) {
        vertexIndex.put(vertex, index);
        lowLink.put(vertex, index);
        index++;
        S.push(vertex);
        onStack.put(vertex, true);
        for (String successor : topology.getComponentMap().get(vertex).getSuccessors()) {
            if (!containedVertices.contains(successor)) continue;
            if (!vertexIndex.containsKey(successor)) {
                strongConnect(successor);
                lowLink.put(vertex, Math.min(lowLink.get(vertex), lowLink.get(successor)));
            } else if (onStack.getOrDefault(successor, false).equals(true)) {
                lowLink.put(vertex, Math.min(lowLink.get(vertex), vertexIndex.get(successor)));
            }
        }
        if (lowLink.get(vertex).equals(vertexIndex.get(vertex))) {
            Set<String> component = new HashSet<>();
            String w;
            do {
                w = S.pop();
                onStack.put(w, false);
                component.add(w);
            } while (!vertex.equals(w));
            components.add(component);
        }
    }
}
