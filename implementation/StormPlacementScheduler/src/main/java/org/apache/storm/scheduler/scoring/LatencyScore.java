package org.apache.storm.scheduler.scoring;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class LatencyScore extends IScoring {
    @Override
    public double score(PlacementProblem problem, Assignment assignment, String topId, boolean currentlyExecuted) {
        Map<String, Double> latencyMap = new HashMap<>();
        Topology topology = problem.getTopologies().get(topId);
        for (Set<String> operatorSet : topology.getTopologicalExecutorOrdering()) {
            for (String opId : operatorSet) {
                double l = 0;
                Operator op = topology.getOperatorMap().get(opId);
                for (String preId : op.getPredecessors()) {
                    SupervisorProcess s1 = assignment.getSupervisorProcess(op);
                    SupervisorProcess s2 = assignment.getSupervisorProcess(topology.getOperatorMap().get(preId));
                    if (s1 == null || s2 == null) continue;
                    Host h1 = s1.getHost();
                    Host h2 = s2.getHost();
                    double linkLatency = 0;
                    if (!h1.getId().equals(h2.getId())) {
                        linkLatency = problem.getLatencyMap().getOrDefault(h1.getId(), new HashMap<>()).getOrDefault(h2.getId(), 1000000d);
                    }
                    l = Math.max(l, latencyMap.getOrDefault(preId, 0d) + linkLatency);
                }
                latencyMap.put(opId, l);
            }
        }
        double latency = 0;
        for (Operator o : topology.getDataSinks()) {
            latency = Math.max(latency, latencyMap.get(o.getId()));
        }
        return latency;
    }
}
