package org.apache.storm.scheduler.cache;

import java.util.*;

public class TopologyCache {
    private List<Set<String>> topologicalExecutorOrdering = new ArrayList<>();
    private Map<String, Double> executorTupleSizes = new HashMap<>();

    private Boolean fullyInitialized;

    public Boolean getFullyInitialized() {
        return fullyInitialized;
    }

    public void setFullyInitialized(Boolean fullyInitialized) {
        this.fullyInitialized = fullyInitialized;
    }

    public List<Set<String>> getTopologicalExecutorOrdering() {
        return topologicalExecutorOrdering;
    }

    public void setTopologicalExecutorOrdering(List<Set<String>> topologicalExecutorOrdering) {
        this.topologicalExecutorOrdering = topologicalExecutorOrdering;
    }

    public Map<String, Double> getExecutorTupleSizes() {
        return executorTupleSizes;
    }

    public void setExecutorTupleSizes(Map<String, Double> executorTupleSizes) {
        this.executorTupleSizes = executorTupleSizes;
    }
}
