package org.apache.storm.scheduler.scoring;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.PlacementProblem;
import org.apache.storm.scheduler.datastructures.placementProblem.SupervisorProcess;

public class UsedSupervisorScore extends IScoring {
    @Override
    public double score(PlacementProblem problem, Assignment assignment, String topId, boolean currentlyExecuted) {
        double count = 0;
        for (SupervisorProcess s : problem.getSupervisorProcessMap().values()) {
            if (!assignment.getOperators(s).isEmpty()) count++;
        }
        return count / problem.getSupervisorProcessMap().size();
    }
}
