package org.apache.storm.scheduler.localSearch.placementModification;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.Operator;
import org.apache.storm.scheduler.datastructures.placementProblem.SupervisorProcess;

public class SwapPlacements implements AssignmentModification {
    private final Operator op1;
    private final Operator op2;

    public SwapPlacements(Operator op1, Operator op2) {
        this.op1 = op1;
        this.op2 = op2;
    }

    @Override
    public void execute(Assignment assignment) {
        SupervisorProcess sup1 = assignment.getSupervisorProcess(op1);
        SupervisorProcess sup2 = assignment.getSupervisorProcess(op2);
        assignment.assign(op2, sup1);
        assignment.assign(op1, sup2);
    }

    @Override
    public void revert(Assignment assignment) {
        execute(assignment);
    }

    @Override
    public String toString() {
        return "SwapPlacements{" +
                "op1=" + op1.getId() +
                ", op2=" + op2.getId() +
                '}';
    }
}
