package org.apache.storm.scheduler.datastructures.placementProblem;


import java.util.HashSet;
import java.util.Set;

public class OperatorComponentGroup {
    private String id;
    private Set<String> predecessors = new HashSet<>();
    private Set<String> successors = new HashSet<>();
    private Set<String> operators = new HashSet<>();
    private String topologyId;

    public OperatorComponentGroup(String id) {
        this.id = id;
    }

    public OperatorComponentGroup(OperatorComponentGroup other) {
        this.id = other.id;
        this.operators = new HashSet<>(other.operators);
        this.predecessors = new HashSet<>(other.predecessors);
        this.successors = new HashSet<>(other.successors);
        this.topologyId = other.getTopologyId();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Set<String> getPredecessors() {
        return predecessors;
    }

    public void setPredecessors(Set<String> predecessors) {
        this.predecessors = predecessors;
    }

    public Set<String> getSuccessors() {
        return successors;
    }

    public void setSuccessors(Set<String> successors) {
        this.successors = successors;
    }

    public String getTopologyId() {
        return topologyId;
    }

    public void setTopologyId(String topologyId) {
        this.topologyId = topologyId;
    }

    public Set<String> getOperators() {
        return operators;
    }

    public void setOperators(Set<String> operators) {
        this.operators = operators;
    }

    @Override
    public String toString() {
        return "OperatorComponentGroup{" +
                "id='" + id + '\'' +
                ", predecessors=" + predecessors +
                ", successors=" + successors +
                ", operators=" + operators +
                ", topologyId='" + topologyId + '\'' +
                '}';
    }
}
