package org.apache.storm.scheduler.constraints;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.PlacementProblem;
import org.apache.storm.scheduler.datastructures.placementProblem.SupervisorProcess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProcessCPUConstraint implements IConstraint {
    private static final Logger LOG = LoggerFactory.getLogger(ProcessCPUConstraint.class);

    @Override
    public double score(PlacementProblem problem, Assignment assignment, String topId, boolean currentlyExecuted) {
        double totalCpuFactor;
        if (currentlyExecuted){
            totalCpuFactor=0.95;
        }else{
            totalCpuFactor=0.5;
        }
        double violations = 0;
        for (SupervisorProcess s : assignment.getSupervisorToOperatorMap().keySet()) {
            if (s == null) LOG.info("ERROR e is null");
            if (s.getHost() == null) LOG.info("ERROR e.getHost is null");
            if (!assignment.getOperators(s).isEmpty()&&assignment.getCPUUsage(s) +s.getUsedCpu()> s.getTotalCpu()*totalCpuFactor) {
                violations += ((assignment.getCPUUsage(s) +s.getUsedCpu())/ (s.getTotalCpu()*totalCpuFactor))-1;
                //LOG.info("executor "+e.getId()+" has violated cpu constraint:"+assignment.getCPUUsage(e)+">"+e.getHost().getTotalCpu()/e.getHost().getExecutorProcessMap().size());
            }
        }
        return violations;
    }
}
