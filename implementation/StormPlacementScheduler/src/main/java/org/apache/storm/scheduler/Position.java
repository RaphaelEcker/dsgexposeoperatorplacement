package org.apache.storm.scheduler;

public class Position {
    private double x, y, z;


    public Position() {
        this(0, 0, 0);
    }

    public Position(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Position(Position other) {
        this.x = other.getX();
        this.y = other.getY();
        this.z = other.getZ();
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public double getLength() {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
    }

    public void addPosition(Position other) {
        this.x += other.getX();
        this.y += other.getY();
        this.z += other.getZ();
    }

    public void subtractPosition(Position other) {
        this.x -= other.getX();
        this.y -= other.getY();
        this.z -= other.getZ();
    }

    public void multiply(double factor) {
        this.x *= factor;
        this.y *= factor;
        this.z *= factor;
    }

    @Override
    public String toString() {
        return "Position{" + x + ", " + y + ", " + z + "}";
    }
}
