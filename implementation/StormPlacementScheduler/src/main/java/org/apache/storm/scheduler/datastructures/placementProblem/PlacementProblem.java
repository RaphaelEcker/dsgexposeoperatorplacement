package org.apache.storm.scheduler.datastructures.placementProblem;

import org.apache.storm.metric.api.DataPoint;
import org.apache.storm.scheduler.*;
import org.apache.storm.scheduler.cache.CacheManager;
import org.apache.storm.scheduler.topologicalOrdering.ModifiedEadesLynSmithFeedbackArcSetProblemHeuristic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class PlacementProblem {
    private static final Logger LOG = LoggerFactory.getLogger(PlacementProblem.class);
    public static final boolean OPTIMISE_AUXILIARY_EXECUTOR_LATENCIES = false;
    private Map<String, Host> hostMap = new HashMap<>();
    private Map<String, ExecutorProcess> executorProcessMap = new HashMap<>();
    private Map<String, Topology> topologies = new HashMap<>();
    private Map<String, SupervisorProcess> supervisorProcessMap = new HashMap<>();
    private Map<String, Map<String, Double>> latencyMap = new HashMap<>();


    public PlacementProblem() {
    }

    public PlacementProblem(Cluster cluster,
                            CacheManager cacheManager,
                            Map<String, Map<String, Collection<DataPoint>>> executorMetrics,
                            Map<String, Map<String, Double>> latencyMap) {
        this.latencyMap = latencyMap;
        mapSupervisors(cluster);
        mapWorkers(cluster);
        mapTopologies(cluster, executorMetrics);
        mapComponents(cluster);
        addOperatorPredecessorSuccessorFromComponents(cluster);
        for (Topology t : topologies.values()) {
            if (cacheManager.contains(t.getId())) {
                LOG.info("retrieving cached topological ordering for " + t.getId());
                t.setTopologicalExecutorOrdering(cacheManager.getTopologicalExecutorOrdering(t.getId()));
            } else {
                LOG.info("calculating topological ordering for " + t.getId());
                ModifiedEadesLynSmithFeedbackArcSetProblemHeuristic.calculateOrdering(t);
                cacheManager.setTopologicalExecutorOrdering(t.getId(), t.getTopologicalExecutorOrdering());
            }
        }
        completeInitialization();
    }

    public void completeInitialization(){
        findTopologyDataSinks();
        setMissingOperatorStatsToZeroIfNoSuccessors();
        setMissingOperatorStatsForDataSources();
        for (SupervisorProcess s:supervisorProcessMap.values()){
            s.updateFreeExecutors();
        }
    }

    private void setMissingOperatorStatsForDataSources() {
        for (Topology t : topologies.values()) {
            for (Operator operator : t.getDataSources()) {
                Optional<String> optSuccessorId = operator.getSuccessors().stream().findFirst();
                if (operator.getSelectivity() == null) operator.setSelectivity(1d);
                if (optSuccessorId.isPresent()) {//bandwidth is not reported for sources-> copy tuple size from a successor
                    String successorID = optSuccessorId.get();
                    Operator successor = t.getOperatorMap().get(successorID);
                    if (operator.getAvgTupleSize() == null) operator.setAvgTupleSize(successor.getAvgTupleSize());
                }

            }
        }
    }

    private void setMissingOperatorStatsToZeroIfNoSuccessors() {
        for (Topology t : topologies.values()) {
            for (Operator operator : t.getOperatorMap().values()) {
                if (operator.getSuccessors().isEmpty()) {
                    if (operator.getAvgTupleSize() == null) operator.setAvgTupleSize(0d);
                    if (operator.getSelectivity() == null) operator.setSelectivity(0d);
                }
            }
        }
    }

    private void findTopologyDataSinks() {
        for (Topology t : topologies.values()) {
            Set<String> previousOperators = new HashSet<>();
            for (Set<String> operatorSet : t.getTopologicalExecutorOrdering()) {
                for (String opId : operatorSet) {
                    Operator operator = t.getOperatorMap().get(opId);
                    if (operator.getSuccessors().size() == 0) {//base case: no successors->sink
                        t.getDataSinks().add(operator);
                    } else {//may be involved in a broken cycle
                        boolean cyclicSink = true;
                        for (String o : operator.getSuccessors()) {//check if successors are on an edge that is ignored because of cycles
                            if (!previousOperators.contains(o)) {//operator has successor that hasn't been processed yet
                                cyclicSink = false;//can't be a sink
                                break;
                            }
                        }
                        if (cyclicSink) {
                            t.getDataSinks().add(operator);
                        }
                    }
                    previousOperators.add(opId);
                }
            }
        }
    }

    private void addOperatorPredecessorSuccessorFromComponents(Cluster cluster) {
        for (TopologyDetails t : cluster.getTopologies()) {
            Topology topology = topologies.get(t.getId());
            for (ExecutorDetails e : t.getExecutors()) {
                Operator operator = topology.getOperatorMap().get(e.startTask + "");
                Component c = t.getUserTopolgyComponents().get(t.getComponentFromExecutor(e));
                Set<String> componentParents;
                Set<String> componentChildren;
                if (c == null) {
                    //executor is part of executed topology, but the executed operator is not, this happens for acker instances, which acknowledge tuples for the message guarantees, and metrics consumers
                    //these executors have all usual topology executors as inputs.
                    if (OPTIMISE_AUXILIARY_EXECUTOR_LATENCIES) {
                        componentParents = t.getUserTopolgyComponents().values().stream().map(Component::getId).collect(Collectors.toSet());
                    } else {
                        componentParents = new HashSet<>();
                    }
                    componentChildren = new HashSet<>();
                } else {
                    componentParents = c.getParents();
                    componentChildren = c.getChildren();
                }

                for (String s : componentParents) {
                    for (ExecutorDetails exec : t.getUserTopolgyComponents().get(s).getExecs()) {
                        operator.getPredecessors().add(exec.startTask + "");
                    }
                }
                if (operator.getPredecessors().isEmpty()) {
                    topology.getDataSources().add(operator);
                }
                for (String s : componentChildren) {
                    for (ExecutorDetails exec : t.getUserTopolgyComponents().get(s).getExecs()) {
                        operator.getSuccessors().add(exec.startTask + "");
                    }
                }

            }
        }
    }

    private void mapComponents(Cluster cluster) {
        for (TopologyDetails t : cluster.getTopologies()) {
            Map<String, OperatorComponentGroup> componentMap = this.topologies.get(t.getId()).getComponentMap();
            for (Map.Entry<String, Component> e : t.getUserTopolgyComponents().entrySet()) {
                Component c = e.getValue();
                OperatorComponentGroup component = new OperatorComponentGroup(c.getId());
                component.setTopologyId(t.getId());
                for (ExecutorDetails exec : c.getExecs()) {
                    component.getOperators().add(exec.startTask + "");
                }
                component.setPredecessors(new HashSet<>(c.getParents()));
                component.setSuccessors(new HashSet<>(c.getChildren()));
                componentMap.put(c.getId(), component);
            }
        }
        for (TopologyDetails t : cluster.getTopologies()) {
            Topology topology = topologies.get(t.getId());
            Map<String, OperatorComponentGroup> systemComponentMap = topology.getSystemComponentMap();
            for (ExecutorDetails e : t.getExecutors()) {
                Operator operator = topology.getOperatorMap().get(e.startTask + "");
                String componentId=t.getComponentFromExecutor(e);
                Component c = t.getUserTopolgyComponents().get(componentId);
                if (c == null) {
                    OperatorComponentGroup group=systemComponentMap.get(componentId);
                    if (group==null){
                        group=new OperatorComponentGroup(componentId);
                        group.setTopologyId(t.getId());
                        systemComponentMap.put(componentId,group);
                    }
                    group.getOperators().add(operator.getId());
                }
            }
        }
    }

    private void mapTopologies(Cluster cluster, Map<String, Map<String, Collection<DataPoint>>> executorMetrics) {
        Map<String,Double> workerCapacityMap=new HashMap<>();
        Map<String,Double> workerReservedCPUMap=new HashMap<>();
        for (TopologyDetails t : cluster.getTopologies()) {
            Topology topology = new Topology(t.getId());
            topologies.put(topology.getId(), topology);
            Map<String, Collection<DataPoint>> metrics = executorMetrics.get(t.getId());
            for (ExecutorDetails executorDetail : t.getExecutors()) {
                Collection<DataPoint> dataPoints = metrics.get(executorDetail.startTask + "");
                Double selectivity = null;
                Double avgTupleSize = null;
                Double capacity = null;
                Double emitted = null;
                if (dataPoints != null) {
                    for (DataPoint d : dataPoints) {
                        if (d.getName().equals("calculated_selectivity")) {
                            selectivity = (Double) d.getValue();
                        } else if (d.getName().equals("avgTupleSize")) {
                            avgTupleSize = (Double) d.getValue();
                        } else if (d.getName().equals("__capacity")) {
                            capacity = (Double) d.getValue();
                        } else if (d.getName().equals("__emit-count-default")) {
                            emitted = (Double) d.getValue();
                        }
                    }
                }
                Operator o = new Operator(executorDetail.startTask + "", selectivity, avgTupleSize, capacity);
                o.setTopologyId(topology.getId());
                if (emitted != null) o.setEmittedTuples(Math.round(emitted));
                topology.getOperatorMap().put(o.getId(), o);
                o.setRequestedMemory(t.getTotalMemReqTask(executorDetail));
                o.setRequestedCpu(t.getTotalCpuReqTask(executorDetail));
                if (capacity != null) {
                    WorkerSlot w = cluster.getAssignmentById(t.getId()).getExecutorToSlot().get(executorDetail);
                    if (w != null) {//after operator move capacity may be known, but new executor hasn't started yet-> isn't in assignment-> capacity would be from old/wrong executor anyway
                        workerCapacityMap.put(w.getId(),capacity+workerCapacityMap.getOrDefault(w.getId(),0d));
                        SupervisorDetails supervisorDetails = cluster.getSupervisorById(w.getNodeId());
                        o.setUsedCpu(supervisorDetails.getTotalCpu() * (capacity));
                    } else {
                        o.setUsedCpu(t.getTotalCpuReqTask(executorDetail));
                    }
                } else {
                    o.setUsedCpu(t.getTotalCpuReqTask(executorDetail));
                    SchedulerAssignment assignment=cluster.getAssignmentById(t.getId());
                    if (assignment!=null&&assignment.getExecutorToSlot()!=null) {
                        WorkerSlot w = assignment.getExecutorToSlot().get(executorDetail);
                        if (w != null) {
                            workerReservedCPUMap.put(w.getId(), o.getUsedCpu() + workerReservedCPUMap.getOrDefault(w.getId(), 0d));
                        }
                    }
                }
            }
        }
        for (TopologyDetails t : cluster.getTopologies()) {//scale larger 1 total capacities down. remove static CPU reservation from capacity calculation
            for (ExecutorDetails executorDetail : t.getExecutors()) {
                Topology topology=topologies.get(t.getId());
                Operator o=topology.getOperatorMap().get(executorDetail.startTask+"");
                if (o.getCapacity()!=null){
                    WorkerSlot w = cluster.getAssignmentById(t.getId()).getExecutorToSlot().get(executorDetail);
                    if (w != null) {
                        double usedCapacity=workerCapacityMap.get(w.getId());
                        SupervisorDetails supervisorDetails = cluster.getSupervisorById(w.getNodeId());
                        if (usedCapacity>1) {
                            o.setCapacity(o.getCapacity()/usedCapacity);
                        }
                        o.setUsedCpu((supervisorDetails.getTotalCpu()-workerReservedCPUMap.getOrDefault(w.getId(), 0d)) * o.getCapacity());
                    }
                }
            }
        }
        for (Topology topology:topologies.values()){
            for (Operator o:topology.getOperatorMap().values()){
                LOG.info(o.getId() + " has capacity:" + o.getCapacity() + " and requests cpu:" + o.getUsedCpu()+" larger than original request by factor: "+o.getUsedCpu()/(o.getRequestedCpu()==0?0.1d:o.getRequestedCpu())+" and memory:"+o.getRequestedMemory()+" and emitted:"+o.getEmittedTuples());
            }
        }
    }

    private void mapWorkers(Cluster cluster) {
        for (WorkerSlot w : cluster.getAssignableSlots()) {
            SupervisorProcess s = supervisorProcessMap.get(w.getNodeId());
            ExecutorProcess process = new ExecutorProcess(w.getId(), s);
            s.getExecutorProcessMap().put(process.getId(), process);
            executorProcessMap.put(process.getId(), process);
        }
        for (TopologyDetails t : cluster.getTopologies()) {
            for (WorkerSlot w : cluster.getUsedSlotsByTopologyId(t.getId())) {
                ExecutorProcess e = executorProcessMap.get(w.getId());
                if (e != null) e.setRunningTopologyId(t.getId());//process could have failed/shutdown
            }
        }

    }

    private void mapSupervisors(Cluster cluster) {
        for (Map.Entry<String, SupervisorDetails> e : cluster.getSupervisors().entrySet()) {
            SupervisorDetails supervisorDetails = e.getValue();
            Host h;
            if (hostMap.containsKey(supervisorDetails.getHost())) {
                h = hostMap.get(supervisorDetails.getHost());
            } else {
                h = new Host(supervisorDetails.getHost());
            }
            SupervisorProcess s = new SupervisorProcess(supervisorDetails.getId(), h);
            h.getSupervisorProcessMap().put(s.getId(), s);
            supervisorProcessMap.put(s.getId(), s);
            s.setTotalMemory(supervisorDetails.getTotalMemory());
            s.setTotalCpu(supervisorDetails.getTotalCpu());
        }
    }

    public PlacementProblem(PlacementProblem other) {
        this.hostMap = other.getHostMap();
        this.executorProcessMap = other.getExecutorProcessMap();
        this.supervisorProcessMap = other.getSupervisorProcessMap();
        this.topologies = new HashMap<>();
        for (Map.Entry<String, Topology> e : other.getTopologies().entrySet()) {
            Topology t = e.getValue();
            this.topologies.put(t.getId(), new Topology(t));
        }
        this.latencyMap = other.getLatencyMap();
    }


    public Map<String, Host> getHostMap() {
        return hostMap;
    }

    public void setHostMap(Map<String, Host> hostMap) {
        this.hostMap = hostMap;
    }

    public Map<String, ExecutorProcess> getExecutorProcessMap() {
        return executorProcessMap;
    }

    public void setExecutorProcessMap(Map<String, ExecutorProcess> executorProcessMap) {
        this.executorProcessMap = executorProcessMap;
    }

    public Map<String, Topology> getTopologies() {
        return topologies;
    }

    public void setTopologies(Map<String, Topology> topologies) {
        this.topologies = topologies;
    }

    public Map<String, SupervisorProcess> getSupervisorProcessMap() {
        return supervisorProcessMap;
    }

    public void setSupervisorProcessMap(Map<String, SupervisorProcess> supervisorProcessMap) {
        this.supervisorProcessMap = supervisorProcessMap;
    }

    public Map<String, Map<String, Double>> getLatencyMap() {
        return latencyMap;
    }

    public void setLatencyMap(Map<String, Map<String, Double>> latencyMap) {
        this.latencyMap = latencyMap;
    }

}
