package org.apache.storm.scheduler.datastructures;

import org.apache.storm.scheduler.ExecutorDetails;
import org.apache.storm.scheduler.SchedulerAssignment;
import org.apache.storm.scheduler.WorkerSlot;
import org.apache.storm.scheduler.datastructures.placementProblem.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Assignment {
    private Map<SupervisorProcess, Set<Operator>> supervisorToOperatorMap;
    private Map<Operator, SupervisorProcess> operatorToSupervisorProcess;

    public Assignment() {
        operatorToSupervisorProcess = new HashMap<>();
        supervisorToOperatorMap = new HashMap<>();
    }

    public Assignment(SchedulerAssignment schedulerAssignment, PlacementProblem problem) {
        operatorToSupervisorProcess = new HashMap<>();
        supervisorToOperatorMap = new HashMap<>();
        if (schedulerAssignment != null) {
            Topology t = problem.getTopologies().get(schedulerAssignment.getTopologyId());
            Map<ExecutorDetails, WorkerSlot> assigmentMap = schedulerAssignment.getExecutorToSlot();
            for (Map.Entry<ExecutorDetails, WorkerSlot> entry : assigmentMap.entrySet()) {
                Operator o = t.getOperatorMap().get(entry.getKey().startTask + "");
                SupervisorProcess s = problem.getSupervisorProcessMap().get(entry.getValue().getNodeId());
                assign(o, s);
            }
        }
    }

    public Assignment(Assignment assignment) {
        this.assign(assignment);
    }
    public void assign(Assignment assignment){
        if (assignment==this) return;
        operatorToSupervisorProcess = new HashMap<>(assignment.getOperatorToSupervisorProcess());
        supervisorToOperatorMap = new HashMap<>();
        for (Map.Entry<SupervisorProcess, Set<Operator>> e:assignment.getSupervisorToOperatorMap().entrySet()){
            supervisorToOperatorMap.put(e.getKey(), new HashSet<>(e.getValue()));
        }
    }
    public void assign(Operator o, SupervisorProcess e) {
        Set<Operator> temp;
        if (operatorToSupervisorProcess.containsKey(o)) {
            temp = supervisorToOperatorMap.getOrDefault(operatorToSupervisorProcess.get(o), new HashSet<>());
            temp.remove(o);
        }
        if (supervisorToOperatorMap.containsKey(e)) {
            temp = supervisorToOperatorMap.get(e);
            temp.add(o);
        } else {
            temp = new HashSet<>();
            temp.add(o);
            supervisorToOperatorMap.put(e, temp);
        }
        operatorToSupervisorProcess.put(o, e);
    }

    public Set<Operator> getOperators(SupervisorProcess e) {
        return new HashSet<>(supervisorToOperatorMap.getOrDefault(e, new HashSet<>()));
    }

    public SupervisorProcess getSupervisorProcess(Operator o) {
        return operatorToSupervisorProcess.get(o);
    }

    public Map<SupervisorProcess, Set<Operator>> getSupervisorToOperatorMap() {
        return supervisorToOperatorMap;
    }

    public Map<Operator, SupervisorProcess> getOperatorToSupervisorProcess() {
        return operatorToSupervisorProcess;
    }

    public Double getMemoryUsage(SupervisorProcess e) {
        Double usage = 0d;
        Set<Operator> temp = getOperators(e);
        for (Operator o : temp) {
            usage += o.getRequestedMemory();
        }
        return usage;
    }

    public Double getCPUUsage(SupervisorProcess e) {
        Double usage = 0d;
        Set<Operator> temp = getOperators(e);
        for (Operator o : temp) {
            usage += o.getUsedCpu();
        }
        return usage;
    }

    public boolean areColocated(Operator o1, Operator o2) {
        SupervisorProcess s1 = this.getSupervisorProcess(o1);
        SupervisorProcess s2 = this.getSupervisorProcess(o2);
        if (s1==null||s2==null) return false;
        if  (s1.getId().equals(s2.getId())) return true;
        Host h1 = s1.getHost();
        Host h2 = s2.getHost();
        return h1.getId().equals(h2.getId());
    }

    public Double getAvgCapacity(SupervisorProcess e) {
        Double avgCapacity = 0d;
        Set<Operator> temp = getOperators(e);
        for (Operator o : temp) {
            avgCapacity += o.getCapacity();
        }
        if (temp.size() > 1) {
            avgCapacity = avgCapacity / temp.size();
        }
        return avgCapacity;
    }
}
