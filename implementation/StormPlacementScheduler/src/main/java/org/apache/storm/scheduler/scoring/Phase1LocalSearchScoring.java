package org.apache.storm.scheduler.scoring;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.PlacementProblem;

public class Phase1LocalSearchScoring extends IScoring {
    @Override
    public double score(PlacementProblem problem, Assignment assignment, String topId, boolean currentlyExecuted) {
        double latencyScore = new LatencyScore().score(problem, assignment, topId, currentlyExecuted) / (1000 * 1000);
        double cohostedScore = new UninitializedOperatorsWithAllNotCohostedSuccessors().score(problem, assignment, topId, currentlyExecuted);
        double usedSupervisorScore = new UsedSupervisorScore().score(problem, assignment, topId, currentlyExecuted);
        double cpuFactor = new SupervisorExcessiveCPUScore().score(problem, assignment, topId, currentlyExecuted);
        return cpuFactor * (latencyScore + usedSupervisorScore) - (cohostedScore * 100000);
    }
}
