package org.apache.storm.scheduler.topologicalOrdering;

import org.apache.storm.scheduler.datastructures.placementProblem.OperatorComponentGroup;
import org.apache.storm.scheduler.datastructures.placementProblem.Topology;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

//based on "A FAST EFFECTIVE HEURISTIC FOR THE FEEDBACK ARC SET PROBLEM", cycle removal has been heavily modified
//implemented instead of typical topological sorting, because while stream processing is based on DAGs, storm actually allows for cycles to exist. This is not well-supported as it introduces some limitations such as the acknowledgement of tuples potentially never happening, because they don't reach a sink. but are instead continuously processed. This affects the messaging guarantee mechanisms and any optimisation for latency. Clearly cycles are a rare instance and not fully supported.
//Therefore, cycles are removed to establish a topological ordering so in the rare case of a cyclic graph it can be simplified to a DAG with minimal impact, so these computations can still be performed for the heuristic.

public class ModifiedEadesLynSmithFeedbackArcSetProblemHeuristic {
    private static final Logger LOG = LoggerFactory.getLogger(ModifiedEadesLynSmithFeedbackArcSetProblemHeuristic.class);

    public static void calculateOrdering(Topology topology) {
        LOG.debug("calculate top order for " + topology.getId());
        Deque<Set<String>> sources = new ArrayDeque<>();
        Deque<Set<String>> sinks = new ArrayDeque<>();
        Set<String> containedVertices = new HashSet<>();
        Map<String, Integer> inDegree = new HashMap<>();
        Map<String, Integer> outDegree = new HashMap<>();
        Map<String, Boolean> neighbourSourceWasRemoved = new HashMap<>();
        List<String> sourcesToProcess = new LinkedList<>();
        List<String> sinksToProcess = new LinkedList<>();
        List<String> newSourcesToProcess = new LinkedList<>();
        List<String> newSinksToProcess = new LinkedList<>();
        Set<String> vertexGroup = new HashSet<>();
        for (OperatorComponentGroup c : topology.getComponentMap().values()) {
            containedVertices.add(c.getId());
            inDegree.put(c.getId(), c.getPredecessors().size());
            outDegree.put(c.getId(), c.getSuccessors().size());
            neighbourSourceWasRemoved.put(c.getId(), false);
            if (c.getPredecessors().size() == 0) sourcesToProcess.add(c.getId());
            if (c.getSuccessors().size() == 0) sinksToProcess.add(c.getId());
        }
        while (!containedVertices.isEmpty()) {


            while (!sourcesToProcess.isEmpty()) {//remove sources
                String vertex = sourcesToProcess.get(0);
                sourcesToProcess.remove(0);
                if (!containedVertices.contains(vertex)) continue;
                vertexGroup.add(vertex);
                containedVertices.remove(vertex);
                for (String s : topology.getComponentMap().get(vertex).getSuccessors()) {
                    if (containedVertices.contains(s)) {
                        neighbourSourceWasRemoved.put(s, true);
                        int newDegree = inDegree.get(s) - 1;
                        inDegree.put(s, newDegree);
                        if (newDegree == 0) newSourcesToProcess.add(s);
                    }
                }
                if (sourcesToProcess.isEmpty()) {
                    List<String> temp = sourcesToProcess;
                    sourcesToProcess = newSourcesToProcess;
                    newSourcesToProcess = temp;
                    if (!vertexGroup.isEmpty()) {
                        LOG.debug("removed sources:" + vertexGroup);
                        sources.add(vertexGroup);
                        vertexGroup = new HashSet<>();
                    }
                }
            }


            while (!sinksToProcess.isEmpty()) {//remove sinks
                String vertex = sinksToProcess.get(0);
                sinksToProcess.remove(0);
                if (!containedVertices.contains(vertex)) continue;
                vertexGroup.add(vertex);
                containedVertices.remove(vertex);
                for (String s : topology.getComponentMap().get(vertex).getPredecessors()) {
                    if (containedVertices.contains(s)) {
                        int newDegree = outDegree.get(s) - 1;
                        outDegree.put(s, newDegree);
                        if (newDegree == 0) newSinksToProcess.add(s);
                    }
                }
                if (sinksToProcess.isEmpty()) {
                    List<String> temp = sinksToProcess;
                    sinksToProcess = newSinksToProcess;
                    newSinksToProcess = temp;
                    if (!vertexGroup.isEmpty()) {
                        sinks.add(vertexGroup);
                        LOG.debug("removed sinks:" + vertexGroup);
                        vertexGroup = new HashSet<>();
                    }
                }
            }

            if (containedVertices.isEmpty()) continue;//graph empty-> done
            //find cycle
            List<Set<String>> components = new TarjansAlgorithm().calculateComponentsInTopologicalOrder(topology, containedVertices);
            LOG.debug("found graph components:" + components + " with vertices " + containedVertices);
            String maxVertice = "";
            int max = Integer.MIN_VALUE;
            for (String vertex : components.get(components.size() - 1)) {//last component, because tarjan returns inverse topological order
                if (neighbourSourceWasRemoved.get(vertex)) {
                    int current = outDegree.get(vertex) - inDegree.get(vertex);
                    if (current > max) {
                        maxVertice = vertex;
                        max = current;
                    }
                }
            }
            //remove cycle;
            Set<String> temp = new HashSet<>();
            temp.add(maxVertice);
            sources.add(temp);
            containedVertices.remove(maxVertice);
            LOG.debug("removed maxVertice is :" + maxVertice);
            for (String s : topology.getComponentMap().get(maxVertice).getSuccessors()) {
                if (containedVertices.contains(s)) {
                    neighbourSourceWasRemoved.put(s, true);
                    int newDegree = inDegree.get(s) - 1;
                    inDegree.put(s, newDegree);
                    if (newDegree == 0) sourcesToProcess.add(s);
                }
            }
            for (String s : topology.getComponentMap().get(maxVertice).getPredecessors()) {
                if (containedVertices.contains(s)) {
                    int newDegree = outDegree.get(s) - 1;
                    outDegree.put(s, newDegree);
                    if (newDegree == 0) sinksToProcess.add(s);
                }
            }
        }

        while (!sinks.isEmpty()) {
            sources.add(sinks.pop());
        }
        LOG.debug("component order:" + sources);
        //translate top order of Storm components to operators
        ArrayList<Set<String>> operatorOrder = new ArrayList<>();
        Set<String> includedOperators = new HashSet<>();
        for (Set<String> componentSet : sources) {
            Set<String> set = new HashSet<>();
            operatorOrder.add(set);
            for (String componentId : componentSet) {
                set.addAll(topology.getComponentMap().get(componentId).getOperators());
                includedOperators.addAll(topology.getComponentMap().get(componentId).getOperators());
            }
        }
        Set<String> systemOperators = new HashSet<>();
        for (String k : topology.getOperatorMap().keySet()) {
            if (!includedOperators.contains(k)) {
                systemOperators.add(k);
            }
        }
        if (!systemOperators.isEmpty()) operatorOrder.add(systemOperators);
        LOG.debug("operator order:" + operatorOrder);
        topology.setTopologicalExecutorOrdering(operatorOrder);

    }
}
