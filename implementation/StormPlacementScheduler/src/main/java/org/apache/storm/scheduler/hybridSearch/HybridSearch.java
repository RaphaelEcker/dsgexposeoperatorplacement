package org.apache.storm.scheduler.hybridSearch;

import org.apache.storm.scheduler.Solver;
import org.apache.storm.scheduler.antSystem.AntSystem;
import org.apache.storm.scheduler.constraints.IConstraint;
import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.PlacementProblem;
import org.apache.storm.scheduler.localSearch.LocalSearch;
import org.apache.storm.scheduler.scoring.IScoring;

public class HybridSearch implements Solver {
    private final IScoring scoringFunction;
    private final IConstraint constraints;
    private long lastRuntimeMS=-1;
    private int lastIterations=-1;

    public HybridSearch(IScoring scoringFunction, IConstraint constraints) {
        this.scoringFunction = scoringFunction;
        this.constraints = constraints;
    }
    @Override
    public void search(PlacementProblem problem, String topId, Assignment assignment) {
        AntSystem.MAX_MS_RUNTIME=500;
        LocalSearch.MAX_MS_RUNTIME=500;
        AntSystem antSystem=new AntSystem(scoringFunction,constraints);
        LocalSearch localSearch=new LocalSearch(scoringFunction,constraints);
        antSystem.search(problem,topId,assignment);
        localSearch.search(problem,topId,assignment);
        this.lastIterations= antSystem.getLastIterations()+ localSearch.getLastIterations();
        this.lastRuntimeMS=antSystem.getLastRuntimeMS()+ localSearch.getLastRuntimeMS();
    }

    @Override
    public long getLastRuntimeMS() {
        return lastRuntimeMS;
    }

    @Override
    public int getLastIterations() {
        return lastIterations;
    }
}
