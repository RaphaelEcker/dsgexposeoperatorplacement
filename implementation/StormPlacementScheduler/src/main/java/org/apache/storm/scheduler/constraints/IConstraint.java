package org.apache.storm.scheduler.constraints;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.PlacementProblem;

public interface IConstraint {
    double score(PlacementProblem problem, Assignment assignment, String topId, boolean currentlyExecuted);
    default double score(PlacementProblem problem, Assignment assignment, String topId){
        return score(problem, assignment, topId, false);
    }
}
