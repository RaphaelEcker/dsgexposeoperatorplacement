package org.apache.storm.scheduler.scoring;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.Operator;
import org.apache.storm.scheduler.datastructures.placementProblem.PlacementProblem;
import org.apache.storm.scheduler.datastructures.placementProblem.Topology;

public class TupleEmittedScoring extends IScoring {
    @Override
    public double score(PlacementProblem problem, Assignment assignment, String topId, boolean currentlyExecuted) {
        Topology topology = problem.getTopologies().get(topId);
        double count = 0;
        double divisor = 0;
        for (Operator o : topology.getOperatorMap().values()) {
            if (o.getSuccessors().isEmpty()||o.getEmittedTuples()==0) continue;
            long stepIncrease=o.getEmittedTuples()/o.getSuccessors().size();
            divisor+=o.getSuccessors().size()*stepIncrease;
            for (String successorId : o.getSuccessors()) {
                Operator successor = topology.getOperatorMap().get(successorId);
                if (!assignment.areColocated(o, successor)) {
                    count += stepIncrease;
                }
            }
        }
        if (divisor != 0) {
            return count / divisor;
        } else {
            return 1;
        }
    }
}
