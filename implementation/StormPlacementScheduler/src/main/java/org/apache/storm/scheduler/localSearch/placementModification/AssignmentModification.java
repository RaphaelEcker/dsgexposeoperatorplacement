package org.apache.storm.scheduler.localSearch.placementModification;

import org.apache.storm.scheduler.datastructures.Assignment;

public interface AssignmentModification {
    void execute(Assignment assignment);

    void revert(Assignment assignment);
}
