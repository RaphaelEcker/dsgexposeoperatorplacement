package org.apache.storm.scheduler.localSearch;

import org.apache.storm.scheduler.constraints.IConstraint;
import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.PlacementProblem;
import org.apache.storm.scheduler.localSearch.placementModification.AssignmentModification;
import org.apache.storm.scheduler.scoring.IScoring;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

public class LocalSearch implements org.apache.storm.scheduler.Solver {
    private static final Logger LOG = LoggerFactory.getLogger(LocalSearch.class);
    public static final int MAX_ITERATIONS = 1000;
    public static final int MAX_ITERATIONS_WITHOUT_IMPROVEMENT = 1;
    public static long MAX_MS_RUNTIME = 1000;
    private final NeighbourhoodGenerator NBgenerator = new NeighbourhoodGenerator();
    private final IScoring scoringFunction;
    private final IConstraint constraints;
    private long lastRuntimeMS=-1;
    private int lastIterations=-1;

    public LocalSearch(IScoring scoringFunction, IConstraint constraints) {
        this.scoringFunction = scoringFunction;
        this.constraints = constraints;
    }

    public void search(PlacementProblem problem, String topId, Assignment assignment) {
        double currentScore = scoringFunction.score(problem, assignment, topId,false);
        double currentViolations = constraints.score(problem, assignment, topId);
        double bestMoveScore = currentScore;
        double bestMoveViolations = currentViolations;
        int iterationsWithoutImprovement = 0;
        long endTime = System.currentTimeMillis() + MAX_MS_RUNTIME;
        int iterations=0;
        for (; iterations < MAX_ITERATIONS && iterationsWithoutImprovement < MAX_ITERATIONS_WITHOUT_IMPROVEMENT && System.currentTimeMillis() < endTime; iterations++) {
            Set<AssignmentModification> moves = NBgenerator.generate(problem, topId, assignment);

            AssignmentModification bestMove = null;
            for (AssignmentModification m : moves) {
                m.execute(assignment);
                double score = scoringFunction.score(problem, assignment, topId,false);
                double violations = constraints.score(problem, assignment, topId);
                if ((violations <= bestMoveViolations && score < bestMoveScore) || violations < bestMoveViolations) {
                    bestMoveScore = score;
                    bestMove = m;
                    bestMoveViolations = violations;
                }
                m.revert(assignment);
            }
            if (bestMove != null && ((currentViolations <= bestMoveViolations && bestMoveScore < currentScore) || bestMoveViolations < currentViolations)) {
                LOG.info("LocalSearch: " + currentScore + "(" + currentViolations + ")" + "->" + bestMoveScore + "(" + bestMoveViolations + ")" + "\t" + bestMove);
                currentScore = bestMoveScore;
                currentViolations = bestMoveViolations;
                bestMove.execute(assignment);
                iterationsWithoutImprovement = 0;
            } else {
                iterationsWithoutImprovement++;
            }
        }
        this.lastIterations=iterations;
        this.lastRuntimeMS=(System.currentTimeMillis()-(endTime-MAX_MS_RUNTIME));
        if (iterationsWithoutImprovement >= MAX_ITERATIONS_WITHOUT_IMPROVEMENT) {
            LOG.info("LocalSearch abort: no more improvement in " + MAX_ITERATIONS_WITHOUT_IMPROVEMENT + " iterations, reached in "+(System.currentTimeMillis()-(endTime-MAX_MS_RUNTIME))+"ms, "+iterations+" total iterations");
        } else if (System.currentTimeMillis() >= endTime) {
            LOG.info("LocalSearch abort: timeout, reached in "+iterations+" iterations");
        } else {
            LOG.info("LocalSearch abort: iteration limit, reached in "+(System.currentTimeMillis()-(endTime-MAX_MS_RUNTIME))+"ms");
        }
    }

    public long getLastRuntimeMS() {
        return lastRuntimeMS;
    }

    public int getLastIterations() {
        return lastIterations;
    }
}
