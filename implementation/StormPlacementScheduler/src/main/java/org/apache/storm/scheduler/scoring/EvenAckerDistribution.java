package org.apache.storm.scheduler.scoring;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.*;
import org.apache.storm.scheduler.scoring.IScoring;
import org.apache.storm.scheduler.scoring.Phase2LocalSearchScoring;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

public class EvenAckerDistribution extends IScoring {
    private static final Logger LOG = LoggerFactory.getLogger(Phase2LocalSearchScoring.class);
    @Override
    public double score(PlacementProblem problem, Assignment assignment, String topId, boolean currentlyExecuted) {
        Topology topology=problem.getTopologies().get(topId);
        OperatorComponentGroup ackers=topology.getSystemComponentMap().get("__acker");
        if (ackers==null) return 0;//disabled
        Set<String> ackerIds=ackers.getOperators();
        int usedSupervisors=0;
        for (SupervisorProcess s : problem.getSupervisorProcessMap().values()) {
            if (!assignment.getOperators(s).isEmpty()) usedSupervisors++;
        }
        if (usedSupervisors==0) return 0;
        double evenAckerPerSupervisorCount=(double)ackerIds.size()/usedSupervisors;
        int upperLimit= (int) Math.ceil(evenAckerPerSupervisorCount);
        int lowerLimit= (int) Math.floor(evenAckerPerSupervisorCount);
        int goodSupervisors=0;
        for (SupervisorProcess s : problem.getSupervisorProcessMap().values()) {
            if (!assignment.getOperators(s).isEmpty()) {
                boolean notOnlyAckers = false;
                int ackerCount = 0;
                for (Operator o : assignment.getOperators(s)) {
                    if (ackerIds.contains(o.getId())) {
                        ackerCount++;
                    } else {
                        notOnlyAckers = true;
                    }
                }
                if (notOnlyAckers&&ackerCount <= upperLimit&&ackerCount >= lowerLimit) goodSupervisors++;
            }
        }
        return 1-((double)goodSupervisors/usedSupervisors);
    }
}
