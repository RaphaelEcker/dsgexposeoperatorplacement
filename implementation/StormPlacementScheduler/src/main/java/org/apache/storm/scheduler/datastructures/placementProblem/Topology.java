package org.apache.storm.scheduler.datastructures.placementProblem;

import java.util.*;

public class Topology {
    private String id;
    private Map<String, Operator> operatorMap = new TreeMap<>();
    private Map<String, OperatorComponentGroup> componentMap = new TreeMap<>();
    private Map<String, OperatorComponentGroup> systemComponentMap = new TreeMap<>();
    private Set<Operator> dataSources = new HashSet<>();
    private Set<Operator> dataSinks = new HashSet<>();
    private List<Set<String>> topologicalExecutorOrdering;


    public Topology(String id) {
        this.id = id;
    }

    public Topology(Topology other) {
        this.id = other.getId();
        this.operatorMap = new TreeMap<>();
        for (Map.Entry<String, Operator> e : other.getOperatorMap().entrySet()) {
            operatorMap.put(e.getKey(), new Operator(e.getValue()));
        }
        this.componentMap = new TreeMap<>();
        for (Map.Entry<String, OperatorComponentGroup> e : other.getComponentMap().entrySet()) {
            componentMap.put(e.getKey(), new OperatorComponentGroup(e.getValue()));
        }
        this.systemComponentMap = new TreeMap<>();
        for (Map.Entry<String, OperatorComponentGroup> e : other.getSystemComponentMap().entrySet()) {
            systemComponentMap.put(e.getKey(), new OperatorComponentGroup(e.getValue()));
        }
        this.topologicalExecutorOrdering = new ArrayList<>(other.getTopologicalExecutorOrdering());
        this.dataSources = new HashSet<>(other.getDataSources());
        this.dataSinks = new HashSet<>(other.getDataSinks());
    }

    public java.lang.String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, Operator> getOperatorMap() {
        return operatorMap;
    }

    public void setOperatorMap(Map<String, Operator> operatorMap) {
        this.operatorMap = operatorMap;
    }


    public Set<Operator> getDataSources() {
        return dataSources;
    }

    public void setDataSources(Set<Operator> dataSources) {
        this.dataSources = dataSources;
    }

    public Set<Operator> getDataSinks() {
        return dataSinks;
    }

    public void setDataSinks(Set<Operator> dataSinks) {
        this.dataSinks = dataSinks;
    }

    public List<Set<String>> getTopologicalExecutorOrdering() {
        return topologicalExecutorOrdering;
    }

    public void setTopologicalExecutorOrdering(List<Set<String>> topologicalExecutorOrdering) {
        this.topologicalExecutorOrdering = topologicalExecutorOrdering;
    }

    public Map<String, OperatorComponentGroup> getComponentMap() {
        return componentMap;
    }

    public void setComponentMap(Map<String, OperatorComponentGroup> componentMap) {
        this.componentMap = componentMap;
    }

    public Map<String, OperatorComponentGroup> getSystemComponentMap() {
        return systemComponentMap;
    }

    public void setSystemComponentMap(Map<String, OperatorComponentGroup> systemComponentMap) {
        this.systemComponentMap = systemComponentMap;
    }
    @Override
    public String toString() {
        return "Topology{" +
                "id='" + id + '\'' +
                ", operatorMap=" + operatorMap +
                '}';
    }
}
