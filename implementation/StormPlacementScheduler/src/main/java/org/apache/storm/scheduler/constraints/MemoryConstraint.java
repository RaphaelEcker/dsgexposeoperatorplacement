package org.apache.storm.scheduler.constraints;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.PlacementProblem;
import org.apache.storm.scheduler.datastructures.placementProblem.SupervisorProcess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MemoryConstraint implements IConstraint {
    private static final Logger LOG = LoggerFactory.getLogger(MemoryConstraint.class);

    @Override
    public double score(PlacementProblem problem, Assignment assignment, String topId, boolean currentlyExecuted) {
        double violations = 0;
        for (SupervisorProcess s : problem.getSupervisorProcessMap().values()) {
            if (s.getUsedMemory() + assignment.getMemoryUsage(s) > s.getTotalMemory()) {
                violations += (s.getUsedMemory() + assignment.getMemoryUsage(s)) / s.getTotalMemory()-1;
                //LOG.info("supervisor "+s.getId()+" has violated memory constraints:"+s.getUsedMemory()+assignment.getMemoryUsage(s)+">"+s.getTotalMemory());
            }
        }
        return violations;
    }
}
