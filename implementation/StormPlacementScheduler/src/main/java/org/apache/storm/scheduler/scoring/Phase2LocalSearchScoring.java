package org.apache.storm.scheduler.scoring;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.PlacementProblem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Phase2LocalSearchScoring extends IScoring {
    private final boolean isInitialized;
    private static final Logger LOG = LoggerFactory.getLogger(Phase2LocalSearchScoring.class);

    public Phase2LocalSearchScoring(boolean isInitialized) {
        this.isInitialized = isInitialized;
    }

    public Phase2LocalSearchScoring(boolean isInitialized, boolean logging) {
        this.isInitialized = isInitialized;
        super.setOutput(logging);
    }

    @Override
    public double score(PlacementProblem problem, Assignment assignment, String topId, boolean currentlyExecuted) {

        double latencyScore = new LatencyScore().score(problem, assignment, topId, currentlyExecuted) / (1000 * 1000);
        double colocatedScore = new ColocatedScoring().score(problem, assignment, topId, currentlyExecuted);
        double colocatedTupleEmittedScore = new TupleEmittedScoring().score(problem, assignment, topId, currentlyExecuted);
        double usedSupervisorScore = new UsedSupervisorScore().score(problem, assignment, topId, currentlyExecuted);
        if (isInitialized) {
            double bandwidthScore = new BandwidthScoring().score(problem, assignment, topId, currentlyExecuted);
            double score = latencyScore + colocatedScore + usedSupervisorScore + colocatedTupleEmittedScore + bandwidthScore;
            if (this.output)
                LOG.info("score: " + score + " lat:" + latencyScore + " co:" + colocatedScore + " usedSupv:" + usedSupervisorScore  + " coTupleEmitted:" + colocatedTupleEmittedScore +" bandwidth:" + bandwidthScore);

            return score;

        } else {
            double score = latencyScore + colocatedScore + usedSupervisorScore + colocatedTupleEmittedScore ;
            if (this.output)
                LOG.info("score: " + score + " lat:" + latencyScore + " co:" + colocatedScore + " usedSupv:" + usedSupervisorScore  + " coTupleEmitted:" + colocatedTupleEmittedScore );
            return score;
        }
    }
}
