package org.apache.storm.scheduler.datastructures.placementProblem;


import java.util.HashSet;
import java.util.Set;

public class Operator {
    private Double selectivity;
    private String id;
    private Set<String> predecessors = new HashSet<>();
    private Set<String> successors = new HashSet<>();
    private String topologyId;
    private Double requestedMemory;
    private Double usedCpu;
    private Double requestedCpu;
    private Double avgTupleSize;
    private Double capacity;
    private long emittedTuples = 0;

    public Operator(String id, Double selectivity, Double avgTupleSize, Double capacity) {
        this.selectivity = selectivity;
        this.id = id;
        this.avgTupleSize = avgTupleSize;
        this.capacity = capacity;
    }

    public Operator(Operator other) {
        this.selectivity = other.selectivity;
        this.id = other.id;
        this.requestedCpu= other.getRequestedCpu();
        this.predecessors = new HashSet<>(other.predecessors);
        this.successors = new HashSet<>(other.successors);
        this.topologyId = other.getTopologyId();
        this.usedCpu = other.getUsedCpu();
        this.requestedMemory = other.getRequestedMemory();
        this.avgTupleSize = other.getAvgTupleSize();
        this.capacity = other.getCapacity();
        this.emittedTuples = other.getEmittedTuples();
    }

    public Double getCapacity() {
        return capacity;
    }

    public void setCapacity(Double capacity) {
        this.capacity = capacity;
    }

    public Double getSelectivity() {
        return selectivity;
    }

    public void setSelectivity(Double selectivity) {
        this.selectivity = selectivity;
    }

    public java.lang.String getId() {
        return id;
    }

    public void setId(java.lang.String id) {
        this.id = id;
    }

    public Set<String> getPredecessors() {
        return predecessors;
    }

    public void setPredecessors(Set<String> predecessors) {
        this.predecessors = predecessors;
    }

    public Set<String> getSuccessors() {
        return successors;
    }

    public void setSuccessors(Set<String> successors) {
        this.successors = successors;
    }

    public long getEmittedTuples() {
        return emittedTuples;
    }

    public void setEmittedTuples(long emittedTuples) {
        this.emittedTuples = emittedTuples;
    }

    public Double getRequestedCpu() {
        return requestedCpu;
    }

    public void setRequestedCpu(Double requestedCpu) {
        this.requestedCpu = requestedCpu;
    }

    public String getTopologyId() {
        return topologyId;
    }

    public void setTopologyId(String topologyId) {
        this.topologyId = topologyId;
    }

    public Double getRequestedMemory() {
        return requestedMemory;
    }

    public void setRequestedMemory(Double requestedMemory) {
        this.requestedMemory = requestedMemory;
    }

    public Double getUsedCpu() {
        return usedCpu;
    }

    public void setUsedCpu(Double usedCpu) {
        this.usedCpu = usedCpu;
    }

    public Double getAvgTupleSize() {
        return avgTupleSize;
    }

    public void setAvgTupleSize(Double avgTupleSize) {
        this.avgTupleSize = avgTupleSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Operator)) return false;

        final Operator operator = (Operator) o;

        return getId() != null ? getId().equals(operator.getId()) : operator.getId() == null;
    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Op"+id;
    }
}
