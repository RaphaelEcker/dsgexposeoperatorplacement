package org.apache.storm.scheduler.datastructures.placementProblem;

import java.util.HashMap;
import java.util.Map;

public class SupervisorProcess {
    private Host host;
    private String id;
    private Map<String, ExecutorProcess> executorProcessMap = new HashMap<>();
    private double totalMemory;
    private double totalCpu;
    private double usedMemory;
    private double usedCpu;
    private int freeExecutors;

    public SupervisorProcess(String id, Host host) {
        this.host = host;
        this.id = id;
    }

    public void updateFreeExecutors(){
        freeExecutors=0;
        for (ExecutorProcess e: executorProcessMap.values()){
            if (e.getRunningTopologyId()==null||e.getRunningTopologyId().equals("")){
                freeExecutors++;
            }
        }
    }

    public Host getHost() {
        return host;
    }

    public void setHost(Host host) {
        this.host = host;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, ExecutorProcess> getExecutorProcessMap() {
        return executorProcessMap;
    }

    public void setExecutorProcessMap(Map<String, ExecutorProcess> executorProcessMap) {
        this.executorProcessMap = executorProcessMap;
    }

    public double getTotalMemory() {
        return totalMemory;
    }

    public void setTotalMemory(double totalMemory) {
        this.totalMemory = totalMemory;
    }

    public double getTotalCpu() {
        return totalCpu;
    }

    public void setTotalCpu(double totalCpu) {
        this.totalCpu = totalCpu;
    }

    public double getUsedMemory() {
        return usedMemory;
    }

    public void setUsedMemory(double usedMemory) {
        this.usedMemory = usedMemory;
    }

    public double getUsedCpu() {
        return usedCpu;
    }

    public void setUsedCpu(double usedCpu) {
        this.usedCpu = usedCpu;
    }

    public int getFreeExecutors() {
        return freeExecutors;
    }

    public void setFreeExecutors(int freeExecutors) {
        this.freeExecutors = freeExecutors;
    }

    @Override
    public String toString() {
        return "SupervisorProcess{" +
                "host=" + host +
                ", id='" + id + '\'' +
                ", executorProcessMap=" + executorProcessMap +
                ", totalMemory=" + totalMemory +
                ", totalCpu=" + totalCpu +
                ", usedMemory=" + usedMemory +
                ", usedCpu=" + usedCpu +
                '}';
    }
}
