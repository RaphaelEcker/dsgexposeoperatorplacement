package org.apache.storm.scheduler.constraints;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.PlacementProblem;
import org.apache.storm.scheduler.datastructures.placementProblem.SupervisorProcess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OneTopIDPerSlot implements IConstraint {
    private static final Logger LOG = LoggerFactory.getLogger(OneTopIDPerSlot.class);

    @Override
    public double score(PlacementProblem problem, Assignment assignment, String topId, boolean currentlyExecuted) {
        double violations = 0;
        for (SupervisorProcess s : assignment.getSupervisorToOperatorMap().keySet()) {
            if (s.getFreeExecutors()==0 && !assignment.getOperators(s).isEmpty()) {
                violations++;
            }
        }
        return violations;
    }
}
