package org.apache.storm.scheduler.antSystem;

import org.apache.storm.scheduler.constraints.IConstraint;
import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.Operator;
import org.apache.storm.scheduler.datastructures.placementProblem.PlacementProblem;
import org.apache.storm.scheduler.datastructures.placementProblem.SupervisorProcess;
import org.apache.storm.scheduler.scoring.IScoring;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class AntSystem implements org.apache.storm.scheduler.Solver{
    private static final Logger LOG = LoggerFactory.getLogger(AntSystem.class);
    public static final int MAX_ITERATIONS = 1000;
    public static int MAX_ITERATIONS_WITHOUT_IMPROVEMENT = 15;
    public static long MAX_MS_RUNTIME = 1000;
    public static double INITIAL_PHEROMONE=5;
    public static double MIN_PHEROMONE=0.6d;
    public static double PHEROMONE_DECAY=1-0.05;
    public static int ANT_COUNT=30;
    public static int PLACE_PHEROMONE_CONSTANT=5;
    private final IScoring scoringFunction;
    private final IConstraint constraints;
    private long lastRuntimeMS=-1;
    private int lastIterations=-1;
    private Map<Operator, Map<SupervisorProcess,Double>> pheromones;
    private final Random random=new Random();

    public AntSystem(IScoring scoringFunction, IConstraint constraints) {
        this.scoringFunction = scoringFunction;
        this.constraints = constraints;
    }


    @Override
    public void search(PlacementProblem problem, String topId, Assignment assignment) {
        long startTime=System.currentTimeMillis();
        int iterations=0;
        long endTime=startTime;
        long runTime=0;
        int iterationsWithoutImprovement=0;
        List<SupervisorProcess> potentialSupervisors= selectPotentialSupervisorsForPlacement(problem, topId, assignment);
        Ant bestAnt=new Ant(assignment);
        initPheromone(problem,topId,bestAnt,potentialSupervisors);
        while(runTime<MAX_MS_RUNTIME&&iterations<MAX_ITERATIONS&&iterationsWithoutImprovement<MAX_ITERATIONS_WITHOUT_IMPROVEMENT){
            Ant[] ants=constructAnts(problem,topId,potentialSupervisors);
            decayPheromone();
            placePheromone(problem,topId,ants);
            Ant newBestAnt=selectBestAnt(bestAnt,ants);
            if (newBestAnt==bestAnt){
                iterationsWithoutImprovement++;
            }else{
                iterationsWithoutImprovement=0;
            }
            bestAnt=newBestAnt;
            endTime=System.currentTimeMillis();
            runTime=endTime-startTime;
            iterations++;

        }
        assign(bestAnt,assignment);
        this.lastIterations=iterations;
        this.lastRuntimeMS=runTime;
        if (iterationsWithoutImprovement >= MAX_ITERATIONS_WITHOUT_IMPROVEMENT) {
            LOG.info("AntSystem abort: no more improvement in " + MAX_ITERATIONS_WITHOUT_IMPROVEMENT + " iterations, reached in "+runTime+"ms, "+iterations+" total iterations");
        } else if (runTime>MAX_MS_RUNTIME) {
            LOG.info("AntSystem abort: timeout, reached in "+iterations+" iterations");
        } else {
            LOG.info("AntSystem abort: iteration limit, reached in "+(System.currentTimeMillis()-(endTime-MAX_MS_RUNTIME))+"ms");
        }
    }

    private Ant selectBestAnt(Ant bestAnt, Ant[] ants) {
        for (Ant a:ants){
            if ((a.violations <= bestAnt.violations && a.score < bestAnt.score) || a.violations < bestAnt.violations) {
                bestAnt = a;
            }
        }
        return bestAnt;
    }

    private void assign(Ant bestAnt, Assignment assignment) {
        if (assignment!=bestAnt.assignment) {
            assignment.assign(bestAnt.assignment);
        }
    }

    private Ant[] constructAnts(PlacementProblem problem, String topId,List<SupervisorProcess> potentialSupervisors) {
        Ant[] ants=new Ant[ANT_COUNT];
        for (int i=0;i<ants.length;i++){
            Ant ant=new Ant();
            ants[i]=ant;
            List<Operator> operators=new ArrayList<>(problem.getTopologies().get(topId).getOperatorMap().values());
            Collections.shuffle(operators);
            for (Operator o:operators){
                SupervisorProcess e=probabilisticExecutorSelection(ant, o,problem,topId,potentialSupervisors);
                ant.assignment.assign(o,e);
            }
        }
        return ants;
    }

    private SupervisorProcess probabilisticExecutorSelection(Ant a, Operator o, PlacementProblem problem, String topId, List<SupervisorProcess> potentialSupervisors) {
        double[] probabilities=new double[potentialSupervisors.size()];
        double[] cumulativeProbabilities=new double[potentialSupervisors.size()];
        double probabilitySum=0;
        for (int i=0;i< potentialSupervisors.size();i++){
            SupervisorProcess potential=potentialSupervisors.get(i);

            if (a.assignment.getMemoryUsage(potential)+potential.getUsedMemory()+o.getRequestedMemory()>potential.getTotalMemory()){
                probabilities[i]=0;
            }else{
                Set<Operator> operators=a.assignment.getOperators(potential);
                if (operators.isEmpty()){
                    probabilities[i]=pheromones.get(o).get(potential);
                }else{
                    int hostsNeighbours=0;
                    Set<String> neighbourIds=new HashSet<>(o.getSuccessors());
                    neighbourIds.addAll(o.getPredecessors());
                    for (String id:neighbourIds){
                        if (operators.contains(problem.getTopologies().get(topId).getOperatorMap().get(id))){
                            hostsNeighbours++;
                            break;
                        }
                    }
                    if (hostsNeighbours>0) {
                        probabilities[i] = pheromones.get(o).get(potential) * 2*2;
                    }else{
                        probabilities[i] = pheromones.get(o).get(potential) * 2;
                    }
                }
                if (o.getUsedCpu()+a.assignment.getCPUUsage(potential)+potential.getUsedCpu()>potential.getTotalCpu()){
                    probabilities[i]/=2;
                }
                if (o.getRequestedMemory()+a.assignment.getMemoryUsage(potential)+potential.getUsedMemory()>potential.getTotalMemory()){
                    probabilities[i]/=2;
                }
            }
            probabilitySum+=probabilities[i];
            cumulativeProbabilities[i]=probabilitySum;
        }
        double selected=random.nextDouble()*probabilitySum;
        int index = Arrays.binarySearch(cumulativeProbabilities, selected);
        if (index < 0)
        {
            // not found: convert insertion point to array index.
            index = Math.abs(index + 1);
        }
        return potentialSupervisors.get(index);
    }

    private void decayPheromone() {
        for (Map.Entry<Operator, Map<SupervisorProcess,Double>> e:pheromones.entrySet()){
            Map<SupervisorProcess,Double>opPheromones=e.getValue();
            opPheromones.replaceAll((k, v) -> Math.max(v * PHEROMONE_DECAY,MIN_PHEROMONE));
        }
    }

    private void placePheromone(PlacementProblem problem, String topId, Ant[] ants) {
        for (Ant a:ants){
            placePheromone(problem,topId,a);
        }
    }
    private void placePheromone(PlacementProblem problem, String topId, Ant ant){
        double combinedScore=combinedScore(problem,topId,ant);
        for (Map.Entry<Operator,SupervisorProcess> e:ant.assignment.getOperatorToSupervisorProcess().entrySet()){
            Operator o=e.getKey();
            SupervisorProcess supervisor=e.getValue();
            Map<SupervisorProcess, Double> opPheromones = pheromones.computeIfAbsent(o, k -> new HashMap<>());
            double currentPheromone=opPheromones.getOrDefault(supervisor,0d);
            opPheromones.put(supervisor,currentPheromone+PLACE_PHEROMONE_CONSTANT/combinedScore);
        }
    }
    private void initPheromone(PlacementProblem problem, String topId, Ant ant,List<SupervisorProcess> potentialSupervisors) {
        pheromones=new HashMap<>();
        for (Operator o:problem.getTopologies().get(topId).getOperatorMap().values()){
            HashMap<SupervisorProcess,Double>opPheromones=new HashMap<>();
            pheromones.put(o,opPheromones);
            for (SupervisorProcess supervisorProcess:potentialSupervisors){
                opPheromones.put(supervisorProcess,INITIAL_PHEROMONE);
            }
        }
        placePheromone(problem,topId,ant);
        placePheromone(problem,topId,ant);
        placePheromone(problem,topId,ant);
        placePheromone(problem,topId,ant);
        placePheromone(problem,topId,ant);
    }
    private double combinedScore(PlacementProblem problem, String topId, Ant ant){
        if(ant.score==null) ant.score = scoringFunction.score(problem, ant.assignment, topId,false);
        if (ant.violations==null) ant.violations = constraints.score(problem, ant.assignment, topId);
        return ant.score+ant.violations*1000;
    }

    private List<SupervisorProcess> selectPotentialSupervisorsForPlacement(PlacementProblem problem, String topId, Assignment assignment){
        return problem.getSupervisorProcessMap().values().stream().filter(s->s.getFreeExecutors()>0).collect(Collectors.toList());
    }

    public long getLastRuntimeMS() {
        return lastRuntimeMS;
    }

    public int getLastIterations() {
        return lastIterations;
    }

}
