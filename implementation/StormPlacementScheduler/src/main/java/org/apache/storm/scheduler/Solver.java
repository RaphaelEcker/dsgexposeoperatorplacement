package org.apache.storm.scheduler;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.PlacementProblem;

public interface Solver {
    void search(PlacementProblem problem, String topId, Assignment assignment);
    long getLastRuntimeMS();
    int getLastIterations();
}
