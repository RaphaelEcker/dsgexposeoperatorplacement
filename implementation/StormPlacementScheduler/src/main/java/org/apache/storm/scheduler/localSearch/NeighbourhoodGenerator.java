package org.apache.storm.scheduler.localSearch;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.Operator;
import org.apache.storm.scheduler.datastructures.placementProblem.PlacementProblem;
import org.apache.storm.scheduler.datastructures.placementProblem.SupervisorProcess;
import org.apache.storm.scheduler.datastructures.placementProblem.Topology;
import org.apache.storm.scheduler.localSearch.placementModification.AssignmentModification;
import org.apache.storm.scheduler.localSearch.placementModification.MoveOperator;
import org.apache.storm.scheduler.localSearch.placementModification.MoveOperators;
import org.apache.storm.scheduler.localSearch.placementModification.SwapPlacements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class NeighbourhoodGenerator {
    private static final Logger LOG = LoggerFactory.getLogger(NeighbourhoodGenerator.class);
    private final Random random = new Random(System.currentTimeMillis());

    public Set<AssignmentModification> generate(PlacementProblem problem, String topId, Assignment assignment) {
        return this.generateFullNeighbourhood(problem, topId, assignment);
    }

    public Set<AssignmentModification> generateFullNeighbourhood(PlacementProblem problem,
                                                                 String topId,
                                                                 Assignment assignment) {
        Set<AssignmentModification> set = new HashSet<>();
        Topology topology = problem.getTopologies().get(topId);
        Collection<Operator> operatorCollection = topology.getOperatorMap().values();
        List<Operator> operatorList = new ArrayList<>(operatorCollection);
        for (int i = 0; i < operatorList.size(); i++) {//swap two operators
            Operator o = operatorList.get(i);
            SupervisorProcess process1 = assignment.getSupervisorProcess(o);
            for (int targetIndex = i + 1; targetIndex < operatorList.size(); targetIndex++) {
                if (i < targetIndex) {
                    Operator o2 = operatorList.get(targetIndex);
                    SupervisorProcess process2 = assignment.getSupervisorProcess(o2);
                    if (!process1.equals(process2)) {
                        //if (process2.getUsedMemory() + +assignment.getMemoryUsage(process2) + o.getRequestedMemory() - o2.getRequestedMemory() <= process2.getTotalMemory() && process2.getUsedCpu() + assignment.getCpuUsage(process2) + o.getRequestedCpu() - o2.getRequestedCpu() <= process2.getTotalCpu()
                        //        && process1.getUsedMemory() + +assignment.getMemoryUsage(process1) + o2.getRequestedMemory() - o.getRequestedMemory() <= process1.getTotalMemory() && process1.getUsedCpu() + assignment.getCpuUsage(process1) + o2.getRequestedCpu() - o.getRequestedCpu() <= process1.getTotalCpu()) {
                        set.add(new SwapPlacements(o, o2));
                        //}
                    }
                }
            }
        }
        List<SupervisorProcess> supervisorProcessList = problem.getSupervisorProcessMap().values().stream().filter(s -> s.getFreeExecutors() > 0).collect(Collectors.toList());

        for (Operator o : operatorList) {//move operator to target supervisor
            SupervisorProcess process1 = assignment.getSupervisorProcess(o);
            for (SupervisorProcess process2 : supervisorProcessList) {
                if (!process1.equals(process2)) {
                    set.add(new MoveOperator(o, process2));
                }
            }
        }
        Map<SupervisorProcess, Set<Operator>> executorMap = assignment.getSupervisorToOperatorMap();//move all operators of a process to target process
        for (Map.Entry<SupervisorProcess, Set<Operator>> entry : executorMap.entrySet()) {
            Set<Operator> operatorSet = entry.getValue();
            if (operatorSet.size() > 1) {
                SupervisorProcess sourceProcess = entry.getKey();
                Set<Operator> operators = new HashSet<>(operatorSet);
                for (SupervisorProcess targetProcess : supervisorProcessList) {
                    if (!targetProcess.equals(sourceProcess)) {
                        set.add(new MoveOperators(operators, targetProcess));
                    }
                }
            }

        }
        return set;
    }

    public Set<AssignmentModification> generateOneOpEach(PlacementProblem problem,
                                                         String topId,
                                                         Assignment assignment) {
        Set<AssignmentModification> set = new HashSet<>();
        Topology topology = problem.getTopologies().get(topId);
        Collection<Operator> operatorCollection = topology.getOperatorMap().values();
        List<Operator> operatorList = new ArrayList<>(operatorCollection);
        for (int i = 0; i < operatorList.size(); i++) {//swap two operators
            int targetIndex = random.nextInt(operatorList.size());
            if (i != targetIndex) {
                Operator o = operatorList.get(i);
                Operator o2 = operatorList.get(targetIndex);
                SupervisorProcess process1 = assignment.getSupervisorProcess(o);
                SupervisorProcess process2 = assignment.getSupervisorProcess(o2);
                if (!process1.equals(process2)) {
                    set.add(new SwapPlacements(o, o2));
                }
            }
        }
        List<SupervisorProcess> supervisorProcessList = problem.getSupervisorProcessMap().values().stream().filter(s -> s.getFreeExecutors() > 0).collect(Collectors.toList());
        for (Operator o : operatorList) {//move operator to target process
            SupervisorProcess process1 = assignment.getSupervisorProcess(o);
            int targetIndex = random.nextInt(supervisorProcessList.size());
            SupervisorProcess process2 = supervisorProcessList.get(targetIndex);
            if (!process1.equals(process2) ) {
                if (!process1.equals(process2)) {
                    set.add(new MoveOperator(o, process2));
                }
            }
        }

        Map<SupervisorProcess, Set<Operator>> executorMap = assignment.getSupervisorToOperatorMap();
        for (Map.Entry<SupervisorProcess, Set<Operator>> entry : executorMap.entrySet()) {//move all operators of a process to target process
            Set<Operator> operatorSet = entry.getValue();
            if (operatorSet.size() > 1) {
                SupervisorProcess sourceProcess = entry.getKey();
                Set<Operator> operators = new HashSet<>(operatorSet);
                int targetIndex = random.nextInt(supervisorProcessList.size());
                SupervisorProcess targetProcess = supervisorProcessList.get(targetIndex);
                if (!targetProcess.equals(sourceProcess)) {
                     set.add(new MoveOperators(operators, targetProcess));
                }
            }

        }
        return set;
    }
}
