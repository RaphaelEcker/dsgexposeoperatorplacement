package org.apache.storm.scheduler.scoring;

import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.Operator;
import org.apache.storm.scheduler.datastructures.placementProblem.PlacementProblem;
import org.apache.storm.scheduler.datastructures.placementProblem.Topology;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BandwidthScoring extends IScoring {
    private static final Logger LOG = LoggerFactory.getLogger(BandwidthScoring.class);

    @Override
    public double score(PlacementProblem problem, Assignment assignment, String topId, boolean currentlyExecuted) {
        Topology topology = problem.getTopologies().get(topId);
        double count = 0;
        double divisor = 0;
        for (Operator o : topology.getOperatorMap().values()) {
            if (o.getAvgTupleSize() == null) {
                LOG.error(o.getId() + " has Avg Tuples=null");
                continue;
            }
            double operatorUsedBandwidth = o.getAvgTupleSize() * o.getEmittedTuples();
            divisor += operatorUsedBandwidth;
            for (String successorId : o.getSuccessors()) {
                Operator successor = topology.getOperatorMap().get(successorId);
                if (!assignment.areColocated(o, successor)) {
                    count += operatorUsedBandwidth / o.getSuccessors().size();
                }
            }
        }
        if (divisor != 0) {
            return count / divisor;
        } else {
            return 1;
        }
    }
}
