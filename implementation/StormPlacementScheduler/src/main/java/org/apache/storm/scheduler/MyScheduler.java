package org.apache.storm.scheduler;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.storm.Config;
import org.apache.storm.metric.StormMetricsRegistry;
import org.apache.storm.metric.api.DataPoint;
import org.apache.storm.scheduler.antSystem.AntSystem;
import org.apache.storm.scheduler.cache.CacheManager;
import org.apache.storm.scheduler.constraints.AllConstraints;
import org.apache.storm.scheduler.constraints.IConstraint;
import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.*;
import org.apache.storm.scheduler.hybridSearch.HybridSearch;
import org.apache.storm.scheduler.localSearch.LocalSearch;
import org.apache.storm.scheduler.scoring.IScoring;
import org.apache.storm.scheduler.scoring.LatencyScore;
import org.apache.storm.scheduler.scoring.Phase2LocalSearchScoring;
import org.apache.storm.scheduler.scoring.UninitializedOperatorsWithAllNotCohostedSuccessors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MyScheduler implements IScheduler {
    private static final boolean DONT_COLOCATE_IN_GREEDY_PLACEMENT=false;
    private static final boolean SHUFFLE_SLOT_PLACEMENT=false;//temporary fix for storm error when restarting workers
    private static final long MIN_PLACE_TOPOLOGY_MS_INTERVAL=90000;
    private static final long EXECUTOR_STATS_MS_OLD_TO_BE_ACTIVE=25000;
    private final Random random = new Random(System.currentTimeMillis());
    private final Pattern TRANSFER_BYTES_EXTRACTION_PATTERN = Pattern.compile("(-*\\d)-(-*\\d)");
    private final Pattern BROKEN_METRICS_ID_EXTRACTION_PATTERN = Pattern.compile("\\[(-*\\d)--*\\d\\]");
    private static boolean CONTAINERNET = true;
    private static boolean LOCALSEARCH = false;
    private static boolean HYBRIDSEARCH = false;

    private static final Logger LOG = LoggerFactory.getLogger(MyScheduler.class);
    private CuratorFramework curatorFramework;
    private final Gson gson = new Gson();
    private Jedis jedis;
    private final CacheManager cacheManager = new CacheManager();
    private Map<String,Long> updatedTopologyLastTimestamp =new HashMap<>();

    @Override
    public void prepare(Map<String, Object> conf, StormMetricsRegistry metricsRegistry) {
        List<String> servers = (List<String>) conf.get(Config.TRANSACTIONAL_ZOOKEEPER_SERVERS);
        Integer port = (Integer) conf.get(Config.TRANSACTIONAL_ZOOKEEPER_PORT);
        if (servers == null || port == null) {
            servers = (List<String>) conf.get(Config.STORM_ZOOKEEPER_SERVERS);
            port = (Integer) conf.get(Config.STORM_ZOOKEEPER_PORT);
        }
        String connectionString = servers.get(0) + ":" + port.toString();
        curatorFramework = CuratorFrameworkFactory.builder()
                .connectString(connectionString)
                .namespace("MyMetricsConsumer")
                .retryPolicy(new ExponentialBackoffRetry(1000, 3))
                .build();
        curatorFramework.start();
        LOCALSEARCH=(boolean) conf.getOrDefault("myScheduler.LocalSearch",false);
        if (LOCALSEARCH){
            LOG.info("setup LocalSearch");
        }else{
            HYBRIDSEARCH=(boolean) conf.getOrDefault("myScheduler.HybridSearch",false);
            if (HYBRIDSEARCH){
                LOG.info("setup HybridSearch");
            }else{
                LOG.info("setup AntSystem");
            }
        }

        try {
            jedis = new Jedis(new HostAndPort(CONTAINERNET ? "10.0.0.5" : "redis", 6379));
            jedis.connect();
        } catch (Exception e) {
            CONTAINERNET = !CONTAINERNET;
            jedis = new Jedis(new HostAndPort(CONTAINERNET ? "10.0.0.5" : "redis", 6379));
            jedis.connect();
        }
    }

    @Override
    public void schedule(Topologies topologies, Cluster cluster) {
        removeStoppedTopologiesFromZookeeperIfMetricsConsumerFailed(cluster);

        cacheManager.removeOldCacheEntries(cluster.getTopologies().getAllIds());
        String workers = topologies.getTopologies().stream().map(t -> t.getId() + ":" + t.getNumWorkers() + "").collect(Collectors.joining(", "));
        LOG.info("desired number of slots used:" + workers);
        String assignedWorkers = topologies.getTopologies().stream().map(t -> t.getId() + ":" + cluster.getAssignedNumWorkers(t) + "").collect(Collectors.joining(", "));
        LOG.info("assigned number of slots used:" + assignedWorkers);

        LOG.info("cluster assignments:" + formatMap(cluster.getAssignments()));
       // LOG.info("used slots:" + cluster.getUsedSlots());
        LOG.info("assignable(all) WorkerSlots:" + formatList(cluster.getAssignableSlots()));//worker processes

        String schedulingEnabled = jedis.get("schedulingEnabled");
        if (schedulingEnabled != null && schedulingEnabled.equals("false")) {
            updatedTopologyLastTimestamp =new HashMap<>();
            LOG.info("scheduling is disabled, skipping...");
            jedis.set("!Nimbus-enactAssignment", "0");
            return;
        }
        doSchedule(topologies, cluster);
    }


    public void doSchedule(Topologies topologies, Cluster cluster) {
        //LOG.info("passed topologies:" + topologies.getTopologies());
        //LOG.info("topologies needing scheduling:" + cluster.needsSchedulingTopologies());
        Map<String, Map<String, Collection<DataPoint>>> executorMetrics = new HashMap<>();

        retrieveExecutorMetricsAndBandwidth(cluster, executorMetrics);
        calculateSelectivityAndAvgTupleSizeMetrics(executorMetrics);
        LOG.info("Operator metrics for all topologies:");
        for (Map.Entry<String,Map<String,Collection<DataPoint>>> e:executorMetrics.entrySet()){
            LOG.info("\t"+e.getKey()+":\n"+formatMap(e.getValue()));
        }


        Map<String, Position> supervisorPositions = new HashMap<>();
        retrieveSupervisorPositions(supervisorPositions);

        Map<String, Map<String, Double>> latencyMap = new HashMap<>();
        computeLatencyMap(latencyMap, supervisorPositions);


        PlacementProblem problem = new PlacementProblem(cluster, this.cacheManager, executorMetrics, latencyMap);
        for (Topology t : problem.getTopologies().values()) {
            LOG.info(t.getId() + " has operator top ordering: " + t.getTopologicalExecutorOrdering());
        }
        checkIfTopologiesAreFullyInitialized(problem);


        LinkedList<SupervisorDetails> availableSupervisors = new LinkedList<>();
        for (SupervisorDetails supervisor : cluster.getSupervisors().values()) {
            if (!cluster.isBlackListed(supervisor.getId()) && (cluster.getAssignableSlots(supervisor).size() > 0)) {
                if (cluster.getUsedPorts(supervisor).size() > 0) {
                    availableSupervisors.addLast(supervisor); // has used ports
                } else {
                    availableSupervisors.addFirst(supervisor); // totally unoccupied supervisor
                }
            }
        }
        //LOG.info("supervisor positions:" + supervisorPositions);

        LOG.info("latency map:" + formatMap(latencyMap));

        //LOG.info("supervisors with available workers:" + formatList(availableSupervisors));//worker processes


        nextTopology:for (TopologyDetails t : cluster.getTopologies()) {
            SchedulerAssignment schedulerAssignment = cluster.getAssignments().get(t.getId());
            Assignment assignment = new Assignment(schedulerAssignment, problem);
            Assignment currentPlacement = new Assignment(assignment);
            updateResourceUsage(cluster, problem,t.getId());
            IScoring scoringFunction;
            IConstraint constraints = new AllConstraints();
            boolean previouslyUnassignedOperators;
            Boolean initialized = cacheManager.getFullyInitialized(t.getId());
            if (initialized==null) initialized=false;
            if (initialized) {
                LOG.info("using phase 2 scoring");
                scoringFunction = new Phase2LocalSearchScoring(false);
            } else {
                LOG.info("using phase 2 uninitialized scoring");
                scoringFunction = new Phase2LocalSearchScoring(false);
            }
            previouslyUnassignedOperators = assignUnassignedOperators(problem, t.getId(), assignment);
            for (Operator o : problem.getTopologies().get(t.getId()).getOperatorMap().values()) {
                if (!assignment.getOperatorToSupervisorProcess().containsKey(o)) {
                    LOG.error("couldn't assign "+o.getId()+" anywhere, because cluster is out of resources -> skipping topology "+t.getId());
                    continue nextTopology;
                }
            }
            double oldViolations, oldScore,oldCohostedScore;
            if (previouslyUnassignedOperators) {
                oldViolations = Double.MAX_VALUE;
                oldScore = Double.MAX_VALUE;
                oldCohostedScore=Double.MIN_VALUE;
            } else {
                oldViolations = constraints.score(problem, currentPlacement, t.getId(),true);
                LOG.info("old scoring:");
                if (initialized) {
                    oldScore = new Phase2LocalSearchScoring(false,true).score(problem, currentPlacement, t.getId(),true);
                } else {
                    oldScore = new Phase2LocalSearchScoring(false,true).score(problem, currentPlacement, t.getId(),true);
                }
                oldCohostedScore = new UninitializedOperatorsWithAllNotCohostedSuccessors().score(problem, currentPlacement, t.getId(),true);
                if (System.currentTimeMillis()-updatedTopologyLastTimestamp.getOrDefault(t.getId(), 0L)<MIN_PLACE_TOPOLOGY_MS_INTERVAL) {
                    LOG.info("skipping search for topology placement of " + t.getId() + ", because topology was recently placed and might not be fully activated yet");
                    jedis.set("!Nimbus-enactAssignment", "0");
                    continue nextTopology;
                }else if(!checkTopologyIsActive(t.getId(), problem,executorMetrics)){
                    LOG.info("skipping search for topology placement of " + t.getId() + ", because topology might not be fully activated since last placement");
                    jedis.set("!Nimbus-enactAssignment", "0");
                    continue nextTopology;
                }else{
                    LOG.info(t.getId() + ": topology is active, searching for better placement");
                }
            }
            int lastIterations=-1;
            long lastRuntimeTimeMS=-1;
            Solver solver;
            if (LOCALSEARCH) {
                LOG.info("using LocalSearch");
                solver = new LocalSearch(scoringFunction, constraints);
            }else if (HYBRIDSEARCH) {
                LOG.info("using HybridSearch");
                solver = new HybridSearch(scoringFunction, constraints);
            }else{
                LOG.info("using AntSystem");
                solver = new AntSystem(scoringFunction, constraints);
            }
            solver.search(problem, t.getId(), assignment);
            lastRuntimeTimeMS=solver.getLastRuntimeMS();
            lastIterations= solver.getLastIterations();
            jedis.set("!Nimbus-iterations", lastIterations+"");
            jedis.set("!Nimbus-timeMS", lastRuntimeTimeMS+"");
            String oldCPUUsage =formatSupervisorResourceUsage(currentPlacement);
            String newCPUUsage= formatSupervisorResourceUsage(assignment);

            LOG.info("old supervisor CPU usage:\n" + oldCPUUsage);
            LOG.info("new supervisor CPU usage:\n" + newCPUUsage);
            if (previouslyUnassignedOperators) {
                LOG.info("enacting topology " + t.getId() + " assignment, because unplaced operators exist");
                enactAssignment(cluster, problem, t, assignment);
            } else {
                scoringFunction = new Phase2LocalSearchScoring(false,true);
                LOG.info("oldViolations: " + oldViolations);
                if (oldViolations > Double.MIN_VALUE) {
                    double newViolations = constraints.score(problem, assignment, t.getId());
                    if (newViolations < oldViolations) {
                        double newScore = scoringFunction.score(problem, assignment, t.getId(),false);
                        LOG.info("enacting topology " + t.getId() + " assignment, because violations have been reduced by " + (oldViolations - newViolations) + ", new violations:" + newViolations + " improved score by" + (oldScore - newScore) + ", new score:" + newScore);
                        enactAssignment(cluster, problem, t, assignment);
                        continue nextTopology;
                    }
                }
                double newScore = scoringFunction.score(problem, assignment, t.getId(),false);

                if (true) {
                    if ((newScore+0.3d<oldScore)) {
                        LOG.info("enacting topology " + t.getId() + " assignment, because score has improved by " + (oldScore - newScore) + ", new score:" + newScore + " violations:" + oldViolations);
                        enactAssignment(cluster, problem, t, assignment);
                    } else {
                        LOG.info("not enacting topology " + t.getId() + " assignment, because score would have only improved by " + (oldScore - newScore) + ", current score:" + oldScore + " violations:" + oldViolations);
                        jedis.set("!Nimbus-enactAssignment", "0");
                    }
                }else{
                    double newCohostedScore = new UninitializedOperatorsWithAllNotCohostedSuccessors().score(problem, assignment, t.getId(),false);
                    if (newCohostedScore>oldCohostedScore){
                        LOG.info("enacting topology " + t.getId() + " assignment, because of phase1 reduced cohosting, score has improved by " + (oldScore - newScore) + ", new score:" + newScore + " violations:" + oldViolations);
                        enactAssignment(cluster, problem, t, assignment);
                    }else {
                        LOG.info("not enacting topology " + t.getId() + " assignment, because topology is not yet initialized, score would have improved by " + (oldScore - newScore) + ", current score:" + oldScore + " violations:" + oldViolations);
                        jedis.set("!Nimbus-enactAssignment", "0");
                    }
                }
            }
        }

    }

    private String formatSupervisorResourceUsage(Assignment assignmentState) {
        return assignmentState.getSupervisorToOperatorMap().keySet()
                .stream()
                .filter(e->!assignmentState.getOperators(e).isEmpty())
                .map(e -> e.getId() + ":" + assignmentState.getCPUUsage(e)+"\t cpu factor:"+(assignmentState.getCPUUsage(e)+e.getUsedCpu())/e.getTotalCpu()+"\t operators:"+assignmentState.getOperators(e)+ " memory consumption:"+assignmentState.getMemoryUsage(e)+"+"+e.getUsedMemory()+"/"+e.getTotalMemory())
                .collect(Collectors.joining("\n"));
    }

    private void updateRedisLatencyEstimation(PlacementProblem problem, Assignment assignment, String topId){

        jedis.set("!latencyEstimation", new LatencyScore().score(problem,assignment,topId,false)+"");
    }

    private boolean assignUnassignedOperators(PlacementProblem problem, String topId, Assignment assignment) {
        boolean ret = false;
        Map<String, Operator> operatorMap = problem.getTopologies().get(topId).getOperatorMap();
        Map<Operator, SupervisorProcess> placementMap = assignment.getOperatorToSupervisorProcess();
        List<SupervisorProcess> supervisorProcessList = new ArrayList<>(problem.getSupervisorProcessMap().values());
        nextOperator: for (Operator o : operatorMap.values()) {
            if (!placementMap.containsKey(o)) {
                ret = true;
                if (DONT_COLOCATE_IN_GREEDY_PLACEMENT) {
                    Set<SupervisorProcess> placedNeighbours = new HashSet<>();//find where predecessors&successors are placed
                    for (String preId : o.getPredecessors()) {
                        Operator pre = problem.getTopologies().get(topId).getOperatorMap().get(preId);
                        if (placementMap.containsKey(pre)) {
                            placedNeighbours.add(placementMap.get(pre));
                        }
                    }
                    for (String sucId : o.getSuccessors()) {
                        Operator pre = problem.getTopologies().get(topId).getOperatorMap().get(sucId);
                        if (placementMap.containsKey(pre)) {
                            placedNeighbours.add(placementMap.get(pre));
                        }
                    }
                    //place away from predecessors&successor if memory allows
                    for (SupervisorProcess process : problem.getSupervisorProcessMap().values()) {
                        if (placedNeighbours.contains(process)) continue;
                        if (process.getFreeExecutors() > 0 && process.getUsedMemory() + assignment.getMemoryUsage(process) + o.getRequestedMemory() <= process.getTotalMemory()) {//process.getUsedCpu() + assignment.getCpuUsage(process) + o.getRequestedCpu() <= process.getTotalCpu()
                            assignment.assign(o, process);
                            LOG.info("placed unassigned " + o.getId() + " on " + process.getId());
                            continue nextOperator;
                        }
                    }
                }
                //otherwise, place somewhere with memory

                Set<Integer> blacklist = new HashSet<>();
                while (blacklist.size() < supervisorProcessList.size()) {
                    int targetIndex = random.nextInt(supervisorProcessList.size());
                    if (blacklist.contains(targetIndex)) continue;
                    SupervisorProcess process = supervisorProcessList.get(targetIndex);
                    if (process.getFreeExecutors()>0 && process.getUsedMemory() + assignment.getMemoryUsage(process) + o.getRequestedMemory() <= process.getTotalMemory()) {//process.getUsedCpu() + assignment.getCpuUsage(process) + o.getRequestedCpu() <= process.getTotalCpu()
                        assignment.assign(o, process);
                        LOG.info("placed unassigned " + o.getId() + " on " + process.getId());
                        break;
                    } else {
                        blacklist.add(targetIndex);
                        LOG.info("can't place unassigned " + o.getId() + " on " + process.getId());
                    }
                }
            }
        }
        return ret;
    }

    /**
     * set supervisor cpu and memory consumption, excluding the current topology.
     * This is mainly so the supervisor usage doesn't have to be updated with each assignment change and logically separates the usage of the current topology and all other already placed topologies.
     */
    private void updateResourceUsage(Cluster cluster, PlacementProblem problem, String topId) {
        Map<String,Assignment> assignmentMap=new HashMap<>();
        for (String id:problem.getTopologies().keySet()) {
            assignmentMap.put(id,new Assignment(cluster.getAssignmentById(id), problem));
        }
        Assignment assignment=assignmentMap.get(topId);
        Map<String, SupervisorResources> resourcesMap = cluster.getSupervisorsResourcesMap();
        for (Map.Entry<String, SupervisorProcess> entry : problem.getSupervisorProcessMap().entrySet()) {
            SupervisorProcess supervisor = entry.getValue();
            SupervisorResources resources = resourcesMap.get(supervisor.getId());

            supervisor.setUsedMemory(resources.getUsedMem());
            //calculate cpu usage, because storm assumes supervisor cpu usage based on the originally requested cpu usage, not on the actual usage based on capacity.
            double usedCPU=0;
            for (Map.Entry<String,Assignment> e:assignmentMap.entrySet()){
                if (!e.getKey().equals(topId)){
                    usedCPU+=e.getValue().getCPUUsage(supervisor);
                }
            }
            supervisor.setUsedCpu(usedCPU);
            //remove usage of topology currently being rescheduled
            supervisor.setUsedMemory(supervisor.getUsedMemory() - assignment.getMemoryUsage(supervisor));
        }
    }


    private void enactAssignment(Cluster cluster, PlacementProblem problem, TopologyDetails t, Assignment assignment) {
        Set<String> modifiedSupervisors = new HashSet<>();
        SchedulerAssignment currentAssignment = cluster.getAssignmentById(t.getId());
        if (currentAssignment != null) {
            Map<ExecutorDetails, WorkerSlot> currentExecutorMap = currentAssignment.getExecutorToSlot();
            Topology topology = problem.getTopologies().get(t.getId());
            for (Map.Entry<ExecutorDetails, WorkerSlot> currentAssignmentEntry : currentExecutorMap.entrySet()) {
                Operator o = topology.getOperatorMap().get(currentAssignmentEntry.getKey().startTask + "");
                String currentSupervisorId = currentAssignmentEntry.getValue().getNodeId();
                String newSupervisorId = assignment.getSupervisorProcess(o).getId();
                if (!newSupervisorId.equals(currentSupervisorId)) {
                    modifiedSupervisors.add(currentSupervisorId);
                    modifiedSupervisors.add(newSupervisorId);
                }
            }
            for (ExecutorDetails executorDetails : t.getExecutors()) {
                if (!currentAssignment.isExecutorAssigned(executorDetails)) {
                    Operator o = topology.getOperatorMap().get(executorDetails.startTask + "");
                    String newSupervisorId = assignment.getSupervisorProcess(o).getId();
                    modifiedSupervisors.add(newSupervisorId);
                }
            }
            for (String id : modifiedSupervisors) {
                SupervisorDetails supervisorDetails=cluster.getSupervisorById(id);
                for (WorkerSlot w:cluster.getAssignableSlots(supervisorDetails)) {
                    if (currentAssignment.isSlotOccupied(w)) {
                        LOG.info("freed "+currentAssignment.getSlotToExecutors().get(w)+" from WorkerSlot "+w.getId());
                        cluster.freeSlot(w);
                    }
                    //don't set running topology to null for slot so it can't get reassigned in the same scheduling to ensure it just shuts down
                }
            }
        } else {
            for (SupervisorProcess s:assignment.getSupervisorToOperatorMap().entrySet().stream().filter(e -> !e.getValue().isEmpty()).map(e->e.getKey()).collect(Collectors.toList())){
                modifiedSupervisors.add(s.getId());
            }
        }
        Map<String, ExecutorDetails> executorDetailsMap = new HashMap<>();
        for (ExecutorDetails e : t.getExecutors()) {
            executorDetailsMap.put(e.startTask + "", e);
        }
        nextSupervisor: for (String s: modifiedSupervisors) {
            SupervisorProcess supervisorProcess=problem.getSupervisorProcessMap().get(s);
            Set<Operator> operators = assignment.getOperators(supervisorProcess);
            if (!operators.isEmpty()) {
                if (SHUFFLE_SLOT_PLACEMENT) {
                    for (WorkerSlot w : cluster.getAssignableSlots(cluster.getSupervisorById(supervisorProcess.getId()))) {
                        ExecutorProcess e = supervisorProcess.getExecutorProcessMap().get(w.getId());
                        if (e.getRunningTopologyId() == null || e.getRunningTopologyId().equals("")) {
                            List<ExecutorDetails> executorDetails = operators.stream().map(o -> executorDetailsMap.get(o.getId())).collect(Collectors.toList());//map back to execs
                            cluster.assign(w, t.getId(), executorDetails);
                            LOG.info("assigned " + executorDetails + " to WorkerSlot " + w.getId());
                            e.setRunningTopologyId(t.getId());
                            supervisorProcess.updateFreeExecutors();
                            break;
                        }
                    }
                }else{
                    for (WorkerSlot w : cluster.getAssignableSlots(cluster.getSupervisorById(supervisorProcess.getId()))) {
                        ExecutorProcess e = supervisorProcess.getExecutorProcessMap().get(w.getId());
                        if (e.getRunningTopologyId() != null && e.getRunningTopologyId().equals(t.getId())) {
                            List<ExecutorDetails> executorDetails = operators.stream().map(o -> executorDetailsMap.get(o.getId())).collect(Collectors.toList());//map back to execs
                            cluster.assign(w, t.getId(), executorDetails);
                            LOG.info("reassigned " + executorDetails + " to WorkerSlot " + w.getId());
                            e.setRunningTopologyId(t.getId());
                            supervisorProcess.updateFreeExecutors();
                            continue nextSupervisor;
                        }
                    }
                    for (WorkerSlot w : cluster.getAssignableSlots(cluster.getSupervisorById(supervisorProcess.getId()))) {
                        ExecutorProcess e = supervisorProcess.getExecutorProcessMap().get(w.getId());
                        if (e.getRunningTopologyId() == null || e.getRunningTopologyId().equals("")) {
                            List<ExecutorDetails> executorDetails = operators.stream().map(o -> executorDetailsMap.get(o.getId())).collect(Collectors.toList());//map back to execs
                            cluster.assign(w, t.getId(), executorDetails);
                            LOG.info("assigned " + executorDetails + " to WorkerSlot " + w.getId());
                            e.setRunningTopologyId(t.getId());
                            supervisorProcess.updateFreeExecutors();
                            break;
                        }
                    }
                }
            }
        }
        if (modifiedSupervisors.isEmpty()) {
            LOG.info("keeping current operator placement for " + t.getId());
        } else {
            updatedTopologyLastTimestamp.put(t.getId(),System.currentTimeMillis());
            LOG.info("changed operator placement for " + t.getId() + ": (" + modifiedSupervisors.size() + ")modified supervisors:\n" + formatSet(modifiedSupervisors));
            updateRedisLatencyEstimation(problem,assignment,t.getId());
            jedis.set("!Nimbus-enactAssignment", "1");
        }

    }

    private void removeStoppedTopologiesFromZookeeperIfMetricsConsumerFailed(Cluster cluster) {
        try {//cleanup stored metrics of stopped topologies if metricsConsumer failed to do it
            List<String> topologyIds = curatorFramework.getChildren().forPath("/");
            for (String id : topologyIds) {
                if (cluster.getTopologies().getById(id) == null) {
                    LOG.error("deleting /" + id);
                    try {
                        curatorFramework.delete().guaranteed().deletingChildrenIfNeeded().forPath("/" + id);
                    } catch (Exception e) {
                        e.printStackTrace();
                        LOG.error("failed to delete Zookeeper Topology Stats children",e);
                    }
                    topologyIds.remove(id);
                }
            }
        } catch (Exception e) {
            LOG.error("failed to get Zookeeper Topology Ids for deletion",e);
        }
    }

    private void checkIfTopologiesAreFullyInitialized(PlacementProblem problem) {
        for (Topology t : problem.getTopologies().values()) {
            if (cacheManager.getFullyInitialized(t.getId()) == null) {
                boolean initialized = true;
                for (Operator o : t.getOperatorMap().values()) {
                    if (o.getAvgTupleSize() == null && !t.getDataSinks().contains(o)) {
                        initialized = false;
                        LOG.info("operator " + t.getId() + ":" + o.getId() + " is NOT initialized (unknown avg tuple size)");
                        //break;
                    } else {
                        LOG.info("operator:" + t.getId() + ":" + o.getId() + " is initialized");
                    }
                }
                if (initialized) cacheManager.setFullyInitialized(t.getId(), true);
            }
        }
    }

    private void computeLatencyMap(Map<String, Map<String, Double>> latencyMap,
                                   Map<String, Position> supervisorPositions) {
        for (Map.Entry<String, Position> outer : supervisorPositions.entrySet()) {
            Map<String, Double> outerLatencyMap = new HashMap<>();
            latencyMap.put(outer.getKey(), outerLatencyMap);
            for (Map.Entry<String, Position> inner : supervisorPositions.entrySet()) {
                if (!inner.equals(outer)) {
                    Position latencyVector = new Position(outer.getValue());
                    latencyVector.subtractPosition(inner.getValue());
                    outerLatencyMap.put(inner.getKey(), latencyVector.getLength());
                }
            }
        }
    }

    private void retrieveSupervisorPositions(Map<String, Position> supervisorPositions) {
        for (String s : jedis.keys("operator-*")) {
            Position position = gson.fromJson(jedis.get(s), Position.class);
            s = s.substring("operator-".length());
            supervisorPositions.put(s, position);
        }
    }

    private void calculateSelectivityAndAvgTupleSizeMetrics(Map<String, Map<String, Collection<DataPoint>>> executorMetrics) {
        for (Map.Entry<String, Map<String, Collection<DataPoint>>> topology : executorMetrics.entrySet()) {
            for (Map.Entry<String, Collection<DataPoint>> executor : topology.getValue().entrySet()) {
                try {
                    Collection<DataPoint> metrics = executor.getValue();
                    Optional<DataPoint> execOptDatapoint = metrics.stream().filter(m -> m.getName().startsWith("__execute-count") && m.getName().endsWith(":default")).findFirst();//__execute-count as map in v1 API
                    Optional<DataPoint> transferOptDatapoint = metrics.stream().filter(m -> m.getName().equals("__transfer-count-default")).findFirst();//__transfer-count as map in v1 API
                    if (execOptDatapoint.isPresent() && transferOptDatapoint.isPresent()) {
                        DataPoint executeDatapoint = execOptDatapoint.get();
                        DataPoint transferDatapoint = transferOptDatapoint.get();

                        Double executeCount = (Double) executeDatapoint.getValue();
                        Double transferCount = (Double) transferDatapoint.getValue();

                        if (transferCount != 0 && executeCount > 1) {
                            DataPoint d = new DataPoint("calculated_selectivity", transferCount / executeCount);
                            metrics.add(d);
                            //LOG.info(d.toString());
                        }
                    }
                } catch (NoSuchElementException e) {
                    LOG.error("error calculating selectivity", e);
                }

                Collection<DataPoint> metrics = executor.getValue();
                Optional<DataPoint> optTransfer = metrics.stream().filter(m -> m.getName().equals("__transfer-count-default")).findFirst();//__transfer-count as map in v1 API
                Optional<DataPoint> optBandwidthUsage = metrics.stream().filter(m -> m.getName().equals("bandwidthUsage")).findFirst();
                if (optTransfer.isPresent() && optBandwidthUsage.isPresent()) {
                    DataPoint transferCount = optTransfer.get();
                    DataPoint dpBandwidthUsage = optBandwidthUsage.get();
                    Double bandwidthUsage = (Double) dpBandwidthUsage.getValue();
                    Double transfer = (Double) transferCount.getValue();

                    if (transfer != null && transfer != 0 && bandwidthUsage != null) {
                        Double avgTupleSize = bandwidthUsage / transfer;
                        DataPoint d = new DataPoint("avgTupleSize", avgTupleSize);
                        cacheManager.setTopologyOperatorTupleSize(topology.getKey(), executor.getKey(), avgTupleSize);
                        metrics.add(d);
                        //LOG.info("new received (" + executor.getKey() + "):" + d);
                    } else {
                        Double avgTupleSize = cacheManager.getTopologyOperatorTupleSize(topology.getKey(), executor.getKey());
                        if (avgTupleSize != null) {
                            DataPoint d = new DataPoint("avgTupleSize", avgTupleSize);
                            metrics.add(d);
                            //LOG.info("retrieved from cache(" + executor.getKey() + "):" + d);
                        }
                    }
                } else {
                    Double avgTupleSize = cacheManager.getTopologyOperatorTupleSize(topology.getKey(), executor.getKey());
                    if (avgTupleSize != null) {
                        DataPoint d = new DataPoint("avgTupleSize", avgTupleSize);
                        metrics.add(d);
                        //LOG.info("retrieved from cache(" + executor.getKey() + "):" + d);
                    }
                }

            }
        }
    }

    private void retrieveExecutorMetricsAndBandwidth(Cluster cluster,
                                                     Map<String, Map<String, Collection<DataPoint>>> executorMetrics) {
        for (TopologyDetails t : cluster.getTopologies()) {//collect statistics for all executors, set up data structures
            Map<String, Collection<DataPoint>> topologyExecutorMetrics = new HashMap<>();
            executorMetrics.put(t.getId(), topologyExecutorMetrics);

            //https://storm.apache.org/releases/2.2.0/Understanding-the-parallelism-of-a-Storm-topology.html
            //In the above code we configured Storm to run the bolt GreenBolt with an initial number of two executors and four associated tasks. Storm will run two tasks per executor (process). If you do not explicitly configure the number of tasks, Storm will run by default one task per executor.
            LOG.info("keys to components:" + t.getUserTopolgyComponents().entrySet().stream().map(e -> e.getKey() + "=" + e.getValue().getId() + " with execs " + e.getValue().getExecs() + "\n").collect(Collectors.joining()));
            try {
                if (curatorFramework.checkExists().forPath("/" + t.getId()) != null) {
                    List<String> componentNodeNames = curatorFramework.getChildren().forPath("/" + t.getId());
                    for (String componentId : componentNodeNames) {
                        if (curatorFramework.checkExists().forPath("/" + t.getId() + "/" + componentId) != null) {
                            List<String> childNodeNames = curatorFramework.getChildren().forPath("/" + t.getId() + "/" + componentId);
                            //LOG.info("curator framework found children nodes for /" + t.getId() + "/" + componentId + " : " + childNodeNames);
                            for (String executorId : childNodeNames) {
                                if (curatorFramework.checkExists().forPath("/" + t.getId() + "/" + componentId + "/" + executorId) != null) {
                                    String json = new String(curatorFramework.getData().forPath("/" + t.getId() + "/" + componentId + "/" + executorId), StandardCharsets.UTF_8);
                                    Collection<DataPoint> metricsData = gson.fromJson(json, new TypeToken<Collection<DataPoint>>() {
                                    }.getType());
                                    //LOG.info("read stats data for /" + t.getId() + "/" + componentId + "/" + executorId + ", is:" + metricsData);
                                    topologyExecutorMetrics.put(executorId, metricsData);
                                } else {
                                    LOG.info(t.getId() + "/" + componentId + "/" + executorId + " has no operator Stats");
                                }
                            }
                            //fixBrokenExecuteLatencyStatsByRetrievingThemFromStormUiRestApi(topologyExecutorMetrics, componentId, t.getId());
                        } else {
                            LOG.info(t.getId() + "/" + componentId + " has no operator Stats");
                        }
                    }
                } else {
                    LOG.info(t.getId() + " has no operator Stats");
                }
            } catch (Exception e) {
                LOG.error("error reading zookeeper data", e);
            }


            try {
                if (topologyExecutorMetrics.containsKey(-1 + "")) {
                    Collection<DataPoint> metricsData = topologyExecutorMetrics.get(-1 + "");
                    try {
                        Optional<DataPoint> optionalDataPoint = metricsData.stream().filter(m -> m.getName().equals("__recv-iconnection")).findFirst();
                        if (optionalDataPoint.isPresent()) {
                            DataPoint recvConnection = optionalDataPoint.get();
                            //LOG.info("recvConnection:" + recvConnection);
                            Map<String, Object> recvConnectionMap = (Map<String, Object>) recvConnection.getValue();
                            Map<String, Double> messageBytes = (Map<String, Double>) recvConnectionMap.get("messageBytes");//https://stackoverflow.com/a/66485178
                            //LOG.info("\n".repeat(5) + "messageBytes:" + messageBytes);

                            Map<String, Double> bandwidthUsage = extractBandwidthUsageFromMessageBytes(messageBytes);
                            for (Map.Entry<String, Double> entry : bandwidthUsage.entrySet()) {
                                Collection<DataPoint> storedMetrics = topologyExecutorMetrics.get(entry.getKey());
                                if (storedMetrics != null) {
                                    storedMetrics.add(new DataPoint("bandwidthUsage", entry.getValue()));
                                }
                            }
                        }
                    } catch (NoSuchElementException e) {
                        LOG.error("error finding bandwidth information", e);
                    }
                } else {
                    LOG.info("system stats for topology:" + t.getId() + " are not available");
                }
            } catch (Exception e) {
                LOG.error("error when finding bandwidth information", e);
            }
        }

    }

    public class ExecutorRestApiStats {
        public List<ExecutorStats> executorStats;

        private class ExecutorStats {
            public String executeLatency;
            public Long uptimeSeconds;
            public String id;
        }
    }

    private void fixBrokenExecuteLatencyStatsByRetrievingThemFromStormUiRestApi(Map<String, Collection<DataPoint>> topologyExecutorMetrics,
                                                                                String componentId,
                                                                                String topologyId) {
        try {
            String json = getUrl("http://10.0.0.4:8080/api/v1/topology/" + topologyId + "/component/" + componentId);
            ExecutorRestApiStats summary = gson.fromJson(json, ExecutorRestApiStats.class);
            for (ExecutorRestApiStats.ExecutorStats e : summary.executorStats) {
                if (e.executeLatency == null || e.executeLatency.equals("")) continue;
                Double execLatency = Double.parseDouble(e.executeLatency);
                Matcher matcher = BROKEN_METRICS_ID_EXTRACTION_PATTERN.matcher(e.id);
                if (matcher.matches() && matcher.groupCount() >= 1) {
                    String id = matcher.group(1);
                    Collection<DataPoint> oldDataPoints = topologyExecutorMetrics.get(id);
                    ArrayList<DataPoint> newDataPoints = new ArrayList<>(oldDataPoints);
                    topologyExecutorMetrics.put(id, newDataPoints);
                    for (DataPoint d : oldDataPoints) {
                        if (d.getName().startsWith("__execute-latency-") && d.getName().endsWith(":default")) {
                            newDataPoints.remove(d);
                            newDataPoints.add(new DataPoint(d.getName(), execLatency));
                            LOG.info(id + ":replaced execute latency datapoint:" + d.getValue() + " with:" + execLatency + "|" + e.executeLatency);
                        }
                    }
                    if (e.uptimeSeconds != null) {
                        newDataPoints.add(new DataPoint("uptime", e.uptimeSeconds));
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("error during metric fixing", e);
        }

    }

    public static String getUrl(String urlToRead) throws Exception {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(conn.getInputStream()))) {
            for (String line; (line = reader.readLine()) != null; ) {
                result.append(line);
            }
        }
        return result.toString();
    }

    private Map<String, Double> extractBandwidthUsageFromMessageBytes(Map<String, Double> messageBytes) {
        Map<String, Double> bandwidthUsage = new HashMap<>();
        for (Map.Entry<String, Double> entry : messageBytes.entrySet()) {
            String transfer = entry.getKey();

            Matcher matcher = TRANSFER_BYTES_EXTRACTION_PATTERN.matcher(transfer);
            if (matcher.matches() && matcher.groupCount() >= 2) {
                String source = matcher.group(1);
                String target = matcher.group(2);
                if (Integer.parseInt(source) >= 0 && Integer.parseInt(target) >= 0) {
                    double usage = bandwidthUsage.getOrDefault(source, 0d);
                    usage += entry.getValue();
                    bandwidthUsage.put(source, usage);
                }
            }
        }
        return bandwidthUsage;
    }

    private boolean checkTopologyIsActive(String topId, PlacementProblem problem,Map<String, Map<String, Collection<DataPoint>>> executorMetrics) {
            Topology topology=problem.getTopologies().get(topId);
            Map<String, Collection<DataPoint>> metrics = executorMetrics.getOrDefault(topId,new HashMap<>());
            int count=0;
            nextOperator: for (Map.Entry<String,Collection<DataPoint>> entry: metrics.entrySet()) {
                if (entry.getKey().equals("-1")) continue;
                Collection<DataPoint> dataPoints=entry.getValue();
                if (dataPoints != null) {
                    for (DataPoint d : dataPoints) {
                        if (d.getName().equals("timestamp")) {
                            long timestamp;
                            timestamp = Math.round((Double)d.getValue());
                            if (System.currentTimeMillis()<=timestamp*1000+EXECUTOR_STATS_MS_OLD_TO_BE_ACTIVE){
                                Operator o=topology.getOperatorMap().get(entry.getKey());
                                if (o!=null){
                                    //if (o.getEmittedTuples()>0) {
                                        count++;
                                        continue nextOperator;
                                    //}
                                }else{
                                    LOG.error("checkTopologyIsActive can't find: "+entry.getKey()+" in operatorMap:"+topology.getOperatorMap().keySet()+" operatorMap contains key:"+topology.getOperatorMap().containsKey(entry.getKey()));
                                }
                            }
                        }
                    }
                }
            }
            int oCount=0;
            for (Operator o:topology.getOperatorMap().values()){
                if (!o.getSuccessors().isEmpty()&&!o.getPredecessors().isEmpty()) oCount++;
            }
            if (count >= oCount - 1){
                return true;
            }else{
                LOG.info("topology not active because only "+count+"/"+oCount+" operators are active");
                return false;
            }
    }

    @Override
    public Map config() {
        return null;
    }

    @Override
    public void cleanup() {
        curatorFramework.close();
        jedis.close();
    }
    private<K,V> String formatMap(Map<K,V> map){
        Set<Map.Entry<K,V>> entrySet=map.entrySet();
        StringBuilder s= new StringBuilder();
        for (Map.Entry<K,V> e:entrySet){
            s.append(e.getKey().toString()).append(":").append(e.getValue().toString()).append("\n");
        }
        return s.toString();
    }
    private<V> String formatList(List<V> list){
        StringBuilder s= new StringBuilder();
        for (V e:list){
            s.append(e.toString()).append("\n");
        }
        return s.toString();
    }
    private<V> String formatSet(Set<V> set){
        return formatList(Arrays.asList(set.toArray()));
    }
}
