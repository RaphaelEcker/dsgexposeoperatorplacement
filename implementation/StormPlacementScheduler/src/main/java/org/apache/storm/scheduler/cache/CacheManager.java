package org.apache.storm.scheduler.cache;


import java.util.*;

public class CacheManager {
    private final Map<String, TopologyCache> topologyCache = new HashMap<>();

    public boolean contains(String topId) {
        return topologyCache.containsKey(topId);
    }

    public List<Set<String>> getTopologicalExecutorOrdering(String topId) {
        TopologyCache cache = topologyCache.get(topId);
        if (cache == null) {
            return null;
        }
        return cache.getTopologicalExecutorOrdering();
    }

    public void setTopologicalExecutorOrdering(String topId, List<Set<String>> topologicalExecutorOrdering) {
        TopologyCache cache = topologyCache.get(topId);
        if (cache == null) {
            cache = new TopologyCache();
            topologyCache.put(topId, cache);
        }
        cache.setTopologicalExecutorOrdering(topologicalExecutorOrdering);
    }

    public void setTopologyOperatorTupleSize(String topId, String opId, Double tupleSize) {
        TopologyCache cache = topologyCache.get(topId);
        if (cache == null) {
            cache = new TopologyCache();
            topologyCache.put(topId, cache);
        }
        cache.getExecutorTupleSizes().put(opId, tupleSize);
    }

    public Double getTopologyOperatorTupleSize(String topId, String opId) {
        TopologyCache cache = topologyCache.get(topId);
        if (cache == null) {
            return null;
        }
        return cache.getExecutorTupleSizes().get(opId);
    }

    public Boolean getFullyInitialized(String topId) {
        return topologyCache.get(topId).getFullyInitialized();
    }

    public void setFullyInitialized(String topId, Boolean fullyInitialized) {
        TopologyCache cache = topologyCache.get(topId);
        if (cache == null) {
            cache = new TopologyCache();
            topologyCache.put(topId, cache);
        }
        cache.setFullyInitialized(fullyInitialized);
    }

    public void removeOldCacheEntries(Collection<String> topologyIds) {
        for (Map.Entry<String, TopologyCache> e : topologyCache.entrySet()) {
            if (!topologyIds.contains(e.getKey())) {
                topologyCache.remove(e.getKey());
            }
        }
    }

}
