package org.apache.storm.scheduler.datastructures;

import org.apache.storm.scheduler.datastructures.placementProblem.ExecutorProcess;
import org.apache.storm.scheduler.datastructures.placementProblem.Host;
import org.apache.storm.scheduler.datastructures.placementProblem.Operator;
import org.apache.storm.scheduler.datastructures.placementProblem.SupervisorProcess;
import org.junit.Test;

import static org.junit.Assert.*;


public class AssignmentTest {
    @Test
    public void deepCopyPreAssignment() {
        Assignment assignment1 = new Assignment();
        Operator o = new Operator("op", 0d, 0d, 0d);
        SupervisorProcess s1 = new SupervisorProcess("supervisor1", new Host("host1"));
        ExecutorProcess e1 = new ExecutorProcess("exec1", s1);
        SupervisorProcess s2 = new SupervisorProcess("supervisor2", new Host("host2"));
        ExecutorProcess e2 = new ExecutorProcess("exec2", s2);
        Assignment assignment2 = new Assignment(assignment1);

        assignment1.assign(o, s1);
        assertEquals(assignment2.getSupervisorProcess(o), null);
        assignment2.assign(o, s2);
        assertEquals(assignment1.getSupervisorProcess(o), s1);
        assertTrue(assignment1.getOperators(s1).contains(o));
        assertFalse(assignment1.getOperators(s2).contains(o));
        assertTrue(assignment2.getOperators(s2).contains(o));
        assertFalse(assignment2.getOperators(s1).contains(o));



    }
    @Test
    public void deepCopyPostAssignment() {
        Assignment assignment1 = new Assignment();
        Operator o = new Operator("op", 0d, 0d, 0d);
        SupervisorProcess s1 = new SupervisorProcess("supervisor1", new Host("host1"));
        ExecutorProcess e1 = new ExecutorProcess("exec1", s1);
        SupervisorProcess s2 = new SupervisorProcess("supervisor2", new Host("host2"));
        ExecutorProcess e2 = new ExecutorProcess("exec2", s2);


        assignment1.assign(o, s1);
        Assignment assignment2 = new Assignment(assignment1);
        assertEquals(assignment1.getSupervisorProcess(o), s1);
        assertEquals(assignment2.getSupervisorProcess(o), s1);
        assignment2.assign(o, s2);
        assertEquals(assignment1.getSupervisorProcess(o), s1);
        assertTrue(assignment1.getOperators(s1).contains(o));
        assertFalse(assignment1.getOperators(s2).contains(o));
        assertTrue(assignment2.getOperators(s2).contains(o));
        assertFalse(assignment2.getOperators(s1).contains(o));


    }
}