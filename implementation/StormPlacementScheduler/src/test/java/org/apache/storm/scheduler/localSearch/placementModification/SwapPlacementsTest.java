package org.apache.storm.scheduler.localSearch.placementModification;

import org.apache.storm.scheduler.UtilProblemGeneration;
import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SwapPlacementsTest {
    @Test
    public void executeModifiesAssignment(){
        PlacementProblem problem = UtilProblemGeneration.generatePlacementProblem();
        Topology t = problem.getTopologies().values().stream().findFirst().get();
        Assignment assignment = new Assignment();
        int i = 10;
        for (Operator o : t.getOperatorMap().values()) {
            o.setRequestedCpu(40d);
            o.setUsedCpu(40d);
            o.setRequestedMemory(10d);
            assignment.assign(o, problem.getSupervisorProcessMap().get("s" + i++));
        }
        List<Operator> operatorList=new ArrayList<>(t.getOperatorMap().values());
        Operator op=operatorList.get(0);
        Operator op2=operatorList.get(operatorList.size()-1);
        SupervisorProcess target=assignment.getSupervisorProcess(op2);
        SupervisorProcess source=assignment.getSupervisorProcess(op);
        assertNotEquals(source, target);
        SwapPlacements sw=new SwapPlacements(op,op2);
        sw.execute(assignment);

        assertEquals(assignment.getSupervisorProcess(op), target);
        assertEquals(assignment.getSupervisorProcess(op2), source);
        assertTrue(assignment.getOperators(target).contains(op));
        assertFalse(assignment.getOperators(source).contains(op));
        assertTrue(assignment.getOperators(source).contains(op2));
        assertFalse(assignment.getOperators(target).contains(op2));
        assertEquals(assignment.getOperatorToSupervisorProcess().size(), t.getOperatorMap().size());
    }

    @Test
    public void revertSucceeds(){
        PlacementProblem problem = UtilProblemGeneration.generatePlacementProblem();
        Topology t = problem.getTopologies().values().stream().findFirst().get();
        Assignment assignment = new Assignment();
        int i = 10;
        for (Operator o : t.getOperatorMap().values()) {
            o.setRequestedCpu(40d);
            o.setUsedCpu(40d);
            o.setRequestedMemory(10d);
            assignment.assign(o, problem.getSupervisorProcessMap().get("s" + i++));
        }

        List<Operator> operatorList=new ArrayList<>(t.getOperatorMap().values());
        Operator op=operatorList.get(0);
        Operator op2=operatorList.get(operatorList.size()-1);
        SupervisorProcess target=assignment.getSupervisorProcess(op2);
        SupervisorProcess source=assignment.getSupervisorProcess(op);
        assertNotEquals(source, target);
        SwapPlacements sw=new SwapPlacements(op,op2);
        sw.execute(assignment);

        assertEquals(assignment.getSupervisorProcess(op), target);
        assertEquals(assignment.getSupervisorProcess(op2), source);
        assertTrue(assignment.getOperators(target).contains(op));
        assertFalse(assignment.getOperators(source).contains(op));
        assertTrue(assignment.getOperators(source).contains(op2));
        assertFalse(assignment.getOperators(target).contains(op2));
        assertEquals(assignment.getOperatorToSupervisorProcess().size(), t.getOperatorMap().size());

        sw.revert(assignment);
        assertEquals(assignment.getSupervisorProcess(op2), target);
        assertEquals(assignment.getSupervisorProcess(op), source);
        assertTrue(assignment.getOperators(target).contains(op2));
        assertFalse(assignment.getOperators(source).contains(op2));
        assertTrue(assignment.getOperators(source).contains(op));
        assertFalse(assignment.getOperators(target).contains(op));
    }

}