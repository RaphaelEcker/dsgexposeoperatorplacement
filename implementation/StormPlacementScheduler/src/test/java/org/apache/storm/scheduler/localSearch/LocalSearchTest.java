package org.apache.storm.scheduler.localSearch;

import org.apache.storm.scheduler.constraints.AllConstraints;
import org.apache.storm.scheduler.constraints.IConstraint;
import org.apache.storm.scheduler.scoring.IScoring;
import org.apache.storm.scheduler.scoring.Phase2LocalSearchScoring;
import org.apache.storm.scheduler.Solver;
import org.apache.storm.scheduler.UtilProblemGeneration;
import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.*;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

public class LocalSearchTest {
    private static final double USEDCPU=30d;

    @Test
    public void singleOverloadedNodeTest() {
        PlacementProblem problem = UtilProblemGeneration.generatePlacementProblem();
        Topology t = problem.getTopologies().values().stream().findFirst().get();
        Assignment assignment = new Assignment();
        for (Operator o : t.getOperatorMap().values()) {
            o.setRequestedCpu(40d);
            o.setUsedCpu(USEDCPU);
            o.setRequestedMemory(10d);
            assignment.assign(o, problem.getSupervisorProcessMap().get("s10"));
        }
        IScoring scoringFunction = new Phase2LocalSearchScoring(false);
        IConstraint constraints = new AllConstraints();
        Solver solver = new LocalSearch(scoringFunction, constraints);
        solver.search(problem, t.getId(), assignment);
        double lastRuntimeTimeMS = solver.getLastRuntimeMS();
        int lastIterations = solver.getLastIterations();
        System.out.println("iterations: " + lastIterations + " runtime: " + lastRuntimeTimeMS);
        List<Map.Entry<SupervisorProcess, Set<Operator>>> usedSupervisors = assignment.getSupervisorToOperatorMap().entrySet().stream().filter(e -> e.getValue() != null && !e.getValue().isEmpty()).collect(Collectors.toList());
        List<Map.Entry<SupervisorProcess, Set<Operator>>> overloadedSupervisors = assignment.getSupervisorToOperatorMap().entrySet().stream().filter(e -> e.getValue() != null && !e.getValue().isEmpty()).filter(e -> assignment.getCPUUsage(e.getKey()) > e.getKey().getTotalCpu()).collect(Collectors.toList());
        scoringFunction.setOutput(true);
        System.out.println("final score:" + scoringFunction.score(problem, assignment, t.getId(),false) + " violations:"+ constraints.score(problem,assignment,t.getId())+ " usedSupervisors:" + usedSupervisors.size());
        assertTrue("single node is overloaded, should expand to other resources", usedSupervisors.size() > 1);
        //assertTrue("no node should be overloaded: "+overloadedSupervisors.stream().map(e->e.getKey().getId()+": "+assignment.getCPUUsage(e.getKey())+">"+e.getKey().getHost().getTotalCpu()).collect(Collectors.joining(", ")), overloadedSupervisors.isEmpty());
    }

    @Test
    public void TooSpreadPlacementTest() {
        PlacementProblem problem = UtilProblemGeneration.generatePlacementProblem();
        Topology t = problem.getTopologies().values().stream().findFirst().get();
        Assignment assignment = new Assignment();
        int i = 10;
        for (Operator o : t.getOperatorMap().values()) {
            o.setRequestedCpu(40d);
            o.setUsedCpu(USEDCPU);
            o.setRequestedMemory(10d);
            assignment.assign(o, problem.getSupervisorProcessMap().get("s" + i++));
        }
        IScoring scoringFunction = new Phase2LocalSearchScoring(false);
        IConstraint constraints = new AllConstraints();
        Solver solver = new LocalSearch(scoringFunction, constraints);
        solver.search(problem, t.getId(), assignment);
        double lastRuntimeTimeMS = solver.getLastRuntimeMS();
        int lastIterations = solver.getLastIterations();
        System.out.println("iterations: " + lastIterations + " runtime: " + lastRuntimeTimeMS);
        List<Map.Entry<SupervisorProcess, Set<Operator>>> usedSupervisors = assignment.getSupervisorToOperatorMap().entrySet().stream().filter(e -> e.getValue() != null && !e.getValue().isEmpty()).collect(Collectors.toList());
        List<Map.Entry<SupervisorProcess, Set<Operator>>> overloadedSupervisors = assignment.getSupervisorToOperatorMap().entrySet().stream().filter(e -> e.getValue() != null && !e.getValue().isEmpty()).filter(e -> assignment.getCPUUsage(e.getKey()) > e.getKey().getTotalCpu()).collect(Collectors.toList());
        scoringFunction.setOutput(true);
        System.out.println("final score:" + scoringFunction.score(problem, assignment, t.getId()) + " violations:"+ constraints.score(problem,assignment,t.getId())+ " usedSupervisors:" + usedSupervisors.size());
        assertTrue("some executors should be colocated", usedSupervisors.size() < t.getOperatorMap().size());
        //assertTrue("no node should be overloaded: "+overloadedSupervisors.stream().map(e->e.getKey().getId()+": "+assignment.getCPUUsage(e.getKey())+">"+e.getKey().getHost().getTotalCpu()).collect(Collectors.joining(", ")), overloadedSupervisors.isEmpty());
    }

}