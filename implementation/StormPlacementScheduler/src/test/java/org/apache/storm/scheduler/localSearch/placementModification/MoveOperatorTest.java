package org.apache.storm.scheduler.localSearch.placementModification;

import org.apache.storm.scheduler.UtilProblemGeneration;
import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class MoveOperatorTest {
    @Test
    public void executeModifiesAssignment(){
        PlacementProblem problem = UtilProblemGeneration.generatePlacementProblem();
        Topology t = problem.getTopologies().values().stream().findFirst().get();
        Assignment assignment = new Assignment();
        int i = 10;
        for (Operator o : t.getOperatorMap().values()) {
            o.setRequestedCpu(40d);
            o.setUsedCpu(40d);
            o.setRequestedMemory(10d);
            assignment.assign(o, problem.getSupervisorProcessMap().get("s" + i++));
        }

        Operator op=t.getOperatorMap().values().stream().findFirst().get();
        SupervisorProcess target=problem.getSupervisorProcessMap().get("s14");
        SupervisorProcess source=assignment.getSupervisorProcess(op);
        assertNotEquals(source, target);
        MoveOperator mv=new MoveOperator(op,target);
        mv.execute(assignment);

        assertEquals(assignment.getSupervisorProcess(op), target);
        assertTrue(assignment.getOperators(target).contains(op));
        assertFalse(assignment.getOperators(source).contains(op));
        assertEquals(assignment.getOperatorToSupervisorProcess().size(), t.getOperatorMap().size());
    }

    @Test
    public void revertSucceeds(){
        PlacementProblem problem = UtilProblemGeneration.generatePlacementProblem();
        Topology t = problem.getTopologies().values().stream().findFirst().get();
        Assignment assignment = new Assignment();
        int i = 10;
        for (Operator o : t.getOperatorMap().values()) {
            o.setRequestedCpu(40d);
            o.setUsedCpu(40d);
            o.setRequestedMemory(10d);
            assignment.assign(o, problem.getSupervisorProcessMap().get("s" + i++));
        }

        Operator op=t.getOperatorMap().values().stream().findFirst().get();
        SupervisorProcess target=problem.getSupervisorProcessMap().get("s14");
        SupervisorProcess source=assignment.getSupervisorProcess(op);
        assertNotEquals(source, target);
        MoveOperator mv=new MoveOperator(op,target);
        mv.execute(assignment);

        assertEquals(assignment.getSupervisorProcess(op), target);
        assertTrue(assignment.getOperators(target).contains(op));
        assertFalse(assignment.getOperators(source).contains(op));
        assertEquals(assignment.getOperatorToSupervisorProcess().size(), t.getOperatorMap().size());

        mv.revert(assignment);
        assertEquals(assignment.getSupervisorProcess(op), source);
        assertTrue(assignment.getOperators(source).contains(op));
        assertFalse(assignment.getOperators(target).contains(op));
        assertEquals(assignment.getOperatorToSupervisorProcess().size(), t.getOperatorMap().size());
    }

}