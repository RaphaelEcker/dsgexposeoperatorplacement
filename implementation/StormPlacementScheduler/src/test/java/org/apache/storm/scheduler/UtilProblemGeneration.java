package org.apache.storm.scheduler;

import org.apache.storm.scheduler.datastructures.placementProblem.*;
import org.apache.storm.scheduler.topologicalOrdering.ModifiedEadesLynSmithFeedbackArcSetProblemHeuristic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class UtilProblemGeneration {
    static public PlacementProblem generatePlacementProblem() {
        PlacementProblem problem = new PlacementProblem();
        generateNetworkTopology(problem);
        problem.getTopologies().put("test", generateTopology("test"));
        problem.completeInitialization();

        return problem;
    }

    static private void generateNetworkTopology(PlacementProblem problem) {
        Map<String, Host> hostMap = problem.getHostMap();
        int executorCount = 0;
        for (int i = 10; i <= 20; i++) {
            Host h = new Host("10.0.0." + i);
            hostMap.put(h.getId(), h);
            SupervisorProcess supervisorProcess = new SupervisorProcess("s" + i, h);
            h.getSupervisorProcessMap().put("s" + i, supervisorProcess);
            problem.getSupervisorProcessMap().put("s" + i, supervisorProcess);
            ExecutorProcess exec1 = new ExecutorProcess("e" + executorCount++, supervisorProcess);
            ExecutorProcess exec2 = new ExecutorProcess("e" + executorCount++, supervisorProcess);
            supervisorProcess.getExecutorProcessMap().put(exec1.getId(), exec1);
            supervisorProcess.getExecutorProcessMap().put(exec2.getId(), exec2);
            problem.getExecutorProcessMap().put(exec1.getId(), exec1);
            problem.getExecutorProcessMap().put(exec2.getId(), exec2);
            if (i == 10) {
                supervisorProcess.setTotalCpu(150);
                supervisorProcess.setTotalMemory(2048);
            } else if (i == 11 || i == 15 || i == 16) {
                supervisorProcess.setTotalCpu(100);
                supervisorProcess.setTotalMemory(1024);
            } else {
                supervisorProcess.setTotalCpu(30);
                supervisorProcess.setTotalMemory(768);
            }
        }


        HashMap<String, Double> map = new HashMap<>();
        problem.getLatencyMap().put("10.0.0.19", map);
        map.put("10.0.0.18", 101964.59102379363);
        map.put("10.0.0.17", 90036.09133344657);
        map.put("10.0.0.16", 160233.17518791402);
        map.put("10.0.0.15", 252767.6538783896);
        map.put("10.0.0.14", 113500.28942012928);
        map.put("10.0.0.13", 56175.651272559066);
        map.put("10.0.0.12", 6255.853196538846);
        map.put("10.0.0.11", 266053.26728216594);
        map.put("10.0.0.10", 27614.432775503356);
        map.put("10.0.0.20", 91147.23620799331);
        map = new HashMap<>();
        problem.getLatencyMap().put("10.0.0.18", map);
        map.put("10.0.0.19", 101964.59102379363);
        map.put("10.0.0.17", 12299.73052278518);
        map.put("10.0.0.16", 58449.28102852496);
        map.put("10.0.0.15", 150819.78396682898);
        map.put("10.0.0.14", 11818.264124412595);
        map.put("10.0.0.13", 46166.204156750515);
        map.put("10.0.0.12", 97225.4294375521);
        map.put("10.0.0.11", 368017.0223723927);
        map.put("10.0.0.10", 74746.6637952875);
        map.put("10.0.0.20", 193097.97236916085);
        map = new HashMap<>();
        problem.getLatencyMap().put("10.0.0.17", map);
        map.put("10.0.0.19", 90036.09133344657);
        map.put("10.0.0.18", 12299.73052278518);
        map.put("10.0.0.16", 70241.14178832067);
        map.put("10.0.0.15", 162754.92262660354);
        map.put("10.0.0.14", 23476.439686915255);
        map.put("10.0.0.13", 34049.44140038272);
        map.put("10.0.0.12", 85182.52576629176);
        map.put("10.0.0.11", 356052.93843948306);
        map.put("10.0.0.10", 62849.92057120689);
        map.put("10.0.0.20", 181176.0064046894);
        map = new HashMap<>();
        problem.getLatencyMap().put("10.0.0.16", map);
        map.put("10.0.0.19", 160233.17518791402);
        map.put("10.0.0.18", 58449.28102852496);
        map.put("10.0.0.17", 70241.14178832067);
        map.put("10.0.0.15", 92651.65825783928);
        map.put("10.0.0.14", 46814.274236579666);
        map.put("10.0.0.13", 104245.41290832471);
        map.put("10.0.0.12", 155368.1129607996);
        map.put("10.0.0.11", 426226.44023853255);
        map.put("10.0.0.10", 133082.7720572299);
        map.put("10.0.0.20", 251343.38622633793);
        map = new HashMap<>();
        problem.getLatencyMap().put("10.0.0.15", map);
        map.put("10.0.0.19", 252767.6538783896);
        map.put("10.0.0.18", 150819.78396682898);
        map.put("10.0.0.17", 162754.92262660354);
        map.put("10.0.0.16", 92651.65825783928);
        map.put("10.0.0.14", 139279.44475556104);
        map.put("10.0.0.13", 196756.34279846054);
        map.put("10.0.0.12", 247929.64534494106);
        map.put("10.0.0.11", 518806.8393935023);
        map.put("10.0.0.10", 225496.2019952473);
        map.put("10.0.0.20", 343903.2041530482);
        map = new HashMap<>();
        problem.getLatencyMap().put("10.0.0.14", map);
        map.put("10.0.0.19", 113500.28942012928);
        map.put("10.0.0.18", 11818.264124412595);
        map.put("10.0.0.17", 23476.439686915255);
        map.put("10.0.0.16", 46814.274236579666);
        map.put("10.0.0.15", 139279.44475556104);
        map.put("10.0.0.13", 57505.28657337338);
        map.put("10.0.0.12", 108658.05745493546);
        map.put("10.0.0.11", 379527.87503087363);
        map.put("10.0.0.10", 86277.89206428974);
        map.put("10.0.0.20", 204641.65475133702);
        map = new HashMap<>();
        problem.getLatencyMap().put("10.0.0.13", map);
        map.put("10.0.0.19", 56175.651272559066);
        map.put("10.0.0.18", 46166.204156750515);
        map.put("10.0.0.17", 34049.44140038272);
        map.put("10.0.0.16", 104245.41290832471);
        map.put("10.0.0.15", 196756.34279846054);
        map.put("10.0.0.14", 57505.28657337338);
        map.put("10.0.0.12", 51190.59544972831);
        map.put("10.0.0.11", 322077.2821973595);
        map.put("10.0.0.10", 29032.323818893565);
        map.put("10.0.0.20", 147271.00310765352);
        map = new HashMap<>();
        problem.getLatencyMap().put("10.0.0.12", map);
        map.put("10.0.0.19", 6255.853196538846);
        map.put("10.0.0.18", 97225.4294375521);
        map.put("10.0.0.17", 85182.52576629176);
        map.put("10.0.0.16", 155368.1129607996);
        map.put("10.0.0.15", 247929.64534494106);
        map.put("10.0.0.14", 108658.05745493546);
        map.put("10.0.0.13", 51190.59544972831);
        map.put("10.0.0.11", 270894.5090536126);
        map.put("10.0.0.10", 23014.749744272587);
        map.put("10.0.0.20", 96133.90497947218);
        map = new HashMap<>();
        problem.getLatencyMap().put("10.0.0.11", map);
        map.put("10.0.0.19", 266053.26728216594);
        map.put("10.0.0.18", 368017.0223723927);
        map.put("10.0.0.17", 356052.93843948306);
        map.put("10.0.0.16", 426226.44023853255);
        map.put("10.0.0.15", 518806.8393935023);
        map.put("10.0.0.14", 379527.87503087363);
        map.put("10.0.0.13", 322077.2821973595);
        map.put("10.0.0.12", 270894.5090536126);
        map.put("10.0.0.10", 293387.8874756569);
        map.put("10.0.0.20", 174959.73573113282);
        map = new HashMap<>();
        problem.getLatencyMap().put("10.0.0.10", map);
        map.put("10.0.0.19", 27614.432775503356);
        map.put("10.0.0.18", 74746.6637952875);
        map.put("10.0.0.17", 62849.92057120689);
        map.put("10.0.0.16", 133082.7720572299);
        map.put("10.0.0.15", 225496.2019952473);
        map.put("10.0.0.14", 86277.89206428974);
        map.put("10.0.0.13", 29032.323818893565);
        map.put("10.0.0.12", 23014.749744272587);
        map.put("10.0.0.11", 293387.8874756569);
        map.put("10.0.0.20", 118577.57469711162);
        map = new HashMap<>();
        problem.getLatencyMap().put("10.0.0.20", map);
        map.put("10.0.0.19", 91147.23620799331);
        map.put("10.0.0.18", 193097.97236916085);
        map.put("10.0.0.17", 181176.0064046894);
        map.put("10.0.0.16", 251343.38622633793);
        map.put("10.0.0.15", 343903.2041530482);
        map.put("10.0.0.14", 204641.65475133702);
        map.put("10.0.0.13", 147271.00310765352);
        map.put("10.0.0.12", 96133.90497947218);
        map.put("10.0.0.11", 174959.73573113282);
        map.put("10.0.0.10", 118577.57469711162);

    }

    static public Topology generateTopology(String name) {
        Topology topology = new Topology(name);
        Set<Operator> dataSources = topology.getDataSources();
        Map<String, OperatorComponentGroup> componentGroupMap = topology.getComponentMap();
        componentGroupMap.put("c1", new OperatorComponentGroup("c1"));
        componentGroupMap.put("c2", new OperatorComponentGroup("c2"));
        componentGroupMap.put("c3", new OperatorComponentGroup("c3"));
        componentGroupMap.put("c4", new OperatorComponentGroup("c4"));
        componentGroupMap.put("c5", new OperatorComponentGroup("c5"));
        int operatorId = 0;
        for (int i = 1; i <= 5; i++) {
            OperatorComponentGroup one = componentGroupMap.get("c" + i);
            for (int j = 0; j < 2; j++) {
                Operator n = new Operator("o" + operatorId++, 1d, null, null);
                one.getOperators().add(n.getId());
                topology.getOperatorMap().put(n.getId(), n);
            }
        }
        for (int i = 1; i < 5; i++) {
            OperatorComponentGroup predecessor = componentGroupMap.get("c" + i);
            OperatorComponentGroup successor = componentGroupMap.get("c" + (i + 1));
            predecessor.getSuccessors().add(successor.getId());
            successor.getPredecessors().add(predecessor.getId());
        }
        for (String id:componentGroupMap.get("c1").getOperators()){
            dataSources.add(topology.getOperatorMap().get(id));
        }
        for (OperatorComponentGroup group : componentGroupMap.values()) {
            for (String succId : group.getSuccessors()) {
                OperatorComponentGroup successor = componentGroupMap.get(succId);
                for (String o1 : group.getOperators()) {
                    Operator op1 = topology.getOperatorMap().get(o1);
                    for (String o2 : successor.getOperators()) {
                        Operator op2 = topology.getOperatorMap().get(o2);
                        op1.getSuccessors().add(o2);
                        op2.getPredecessors().add(o1);
                    }
                }
            }
        }


        ModifiedEadesLynSmithFeedbackArcSetProblemHeuristic.calculateOrdering(topology);
        return topology;
    }
}
