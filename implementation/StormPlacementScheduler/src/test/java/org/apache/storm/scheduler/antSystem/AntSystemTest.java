package org.apache.storm.scheduler.antSystem;

import org.apache.storm.scheduler.constraints.AllConstraints;
import org.apache.storm.scheduler.constraints.IConstraint;
import org.apache.storm.scheduler.scoring.IScoring;
import org.apache.storm.scheduler.scoring.Phase2LocalSearchScoring;
import org.apache.storm.scheduler.Solver;
import org.apache.storm.scheduler.UtilProblemGeneration;
import org.apache.storm.scheduler.datastructures.Assignment;
import org.apache.storm.scheduler.datastructures.placementProblem.*;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

public class AntSystemTest {
    private static final double USEDCPU=30d;
    @Test
    public void singleOverloadedNodeTest() {
        PlacementProblem problem = UtilProblemGeneration.generatePlacementProblem();
        Topology t = problem.getTopologies().values().stream().findFirst().get();
        Assignment assignment = new Assignment();
        for (Operator o : t.getOperatorMap().values()) {
            o.setRequestedCpu(40d);
            o.setUsedCpu(USEDCPU);
            o.setRequestedMemory(10d);
            assignment.assign(o, problem.getSupervisorProcessMap().get("s10"));
        }
        IScoring scoringFunction = new Phase2LocalSearchScoring(false);
        IConstraint constraints = new AllConstraints();
        Solver solver = new AntSystem(scoringFunction, constraints);
        solver.search(problem, t.getId(), assignment);
        double lastRuntimeTimeMS = solver.getLastRuntimeMS();
        int lastIterations = solver.getLastIterations();
        System.out.println("iterations: " + lastIterations + " runtime: " + lastRuntimeTimeMS);
        List<Map.Entry<SupervisorProcess, Set<Operator>>> usedSupervisors = assignment.getSupervisorToOperatorMap().entrySet().stream().filter(e -> e.getValue() != null && !e.getValue().isEmpty()).collect(Collectors.toList());
        List<Map.Entry<SupervisorProcess, Set<Operator>>> overloadedSupervisors = assignment.getSupervisorToOperatorMap().entrySet().stream().filter(e -> e.getValue() != null && !e.getValue().isEmpty()).filter(e -> assignment.getCPUUsage(e.getKey()) > e.getKey().getTotalCpu()).collect(Collectors.toList());
        scoringFunction.setOutput(true);
        System.out.println("final score:" + scoringFunction.score(problem, assignment, t.getId()) + " violations:"+ constraints.score(problem,assignment,t.getId())+" usedSupervisors:" + usedSupervisors.size());
        assertTrue("single node is overloaded, should expand to other resources", usedSupervisors.size() > 1);
        //assertTrue("no node should be overloaded: "+overloadedSupervisors.stream().map(e->e.getKey().getId()+": "+assignment.getCPUUsage(e.getKey())+">"+e.getKey().getHost().getTotalCpu()).collect(Collectors.joining(", ")), overloadedSupervisors.isEmpty());
    }

    @Test
    public void TooSpreadPlacementTest() {
        PlacementProblem problem = UtilProblemGeneration.generatePlacementProblem();
        Topology t = problem.getTopologies().values().stream().findFirst().get();
        Assignment assignment = new Assignment();
        int i = 10;
        for (Operator o : t.getOperatorMap().values()) {
            o.setRequestedCpu(40d);
            o.setUsedCpu(USEDCPU);
            o.setRequestedMemory(10d);
            assignment.assign(o, problem.getSupervisorProcessMap().get("s" + i++));
        }
        IScoring scoringFunction = new Phase2LocalSearchScoring(false);
        IConstraint constraints = new AllConstraints();
        Solver solver = new AntSystem(scoringFunction, constraints);
        solver.search(problem, t.getId(), assignment);
        double lastRuntimeTimeMS = solver.getLastRuntimeMS();
        int lastIterations = solver.getLastIterations();
        System.out.println("iterations: " + lastIterations + " runtime: " + lastRuntimeTimeMS);
        List<Map.Entry<SupervisorProcess, Set<Operator>>> usedSupervisors = assignment.getSupervisorToOperatorMap().entrySet().stream().filter(e -> e.getValue() != null && !e.getValue().isEmpty()).collect(Collectors.toList());
        List<Map.Entry<SupervisorProcess, Set<Operator>>> overloadedSupervisors = assignment.getSupervisorToOperatorMap().entrySet().stream().filter(e -> e.getValue() != null && !e.getValue().isEmpty()).filter(e -> assignment.getCPUUsage(e.getKey()) > e.getKey().getTotalCpu()).collect(Collectors.toList());
        scoringFunction.setOutput(true);
        System.out.println("final score:" + scoringFunction.score(problem, assignment, t.getId()) + " violations:"+ constraints.score(problem,assignment,t.getId())+ " usedSupervisors:" + usedSupervisors.size());
        assertTrue("some executors should be colocated", usedSupervisors.size() < t.getOperatorMap().size());
        //assertTrue("no node should be overloaded: "+overloadedSupervisors.stream().map(e->e.getKey().getId()+": "+assignment.getCPUUsage(e.getKey())+">"+e.getKey().getHost().getTotalCpu()).collect(Collectors.joining(", ")), overloadedSupervisors.isEmpty());
    }


    public void parameterTuning() {
        Map<Double,Integer> distribution_bestInitialPheromone=new TreeMap<>();
        Map<Double,Integer> distribution_bestMinPheromone=new TreeMap<>();
        Map<Double,Integer> distribution_bestPheromoneDecay=new TreeMap<>();
        Map<Integer,Integer> distribution_bestAntCount=new TreeMap<>();
        Map<Integer,Integer> distribution_bestMaxIterationsWithoutImprovement=new TreeMap<>();
        Map<Integer,Integer> distribution_bestPlacePheromoneConstant=new TreeMap<>();
        double bestFirstScore = Double.MAX_VALUE;
        double bestSecondScore = Double.MAX_VALUE;
        double firstScore;
        double secondScore;
        PlacementProblem problem = UtilProblemGeneration.generatePlacementProblem();
        Topology t = problem.getTopologies().values().stream().findFirst().get();
        IScoring scoringFunction = new Phase2LocalSearchScoring(false);
        IConstraint constraints = new AllConstraints();
        int placePheromoneConstantLoopCount=(int)Math.ceil((11-1)/3d);
        int initialPheromoneLoopCount=(int)Math.ceil((20-1)/3d);
        int minPheromoneLoopCount=(int)Math.ceil((0.2-0.01)/0.04);
        int maxIterationsWithoutImprovementLoopCount=(int)Math.ceil((30-10)/5d);
        int loopCount=0;

        for (AntSystem.PLACE_PHEROMONE_CONSTANT = 1; AntSystem.PLACE_PHEROMONE_CONSTANT < 11; AntSystem.PLACE_PHEROMONE_CONSTANT += 3) {
            for (AntSystem.INITIAL_PHEROMONE = 1; AntSystem.INITIAL_PHEROMONE < 20; AntSystem.INITIAL_PHEROMONE += 3) {

                for (AntSystem.MIN_PHEROMONE = 0.01; AntSystem.MIN_PHEROMONE < 0.2; AntSystem.MIN_PHEROMONE += 0.04) {
                    for (AntSystem.MAX_ITERATIONS_WITHOUT_IMPROVEMENT = 10; AntSystem.MAX_ITERATIONS_WITHOUT_IMPROVEMENT < 30; AntSystem.MAX_ITERATIONS_WITHOUT_IMPROVEMENT += 5) {
                        System.out.println("progress:" + loopCount++/(double)(placePheromoneConstantLoopCount*initialPheromoneLoopCount*minPheromoneLoopCount*maxIterationsWithoutImprovementLoopCount));
                        for (AntSystem.ANT_COUNT = 15; AntSystem.ANT_COUNT < 40; AntSystem.ANT_COUNT+=5) {
                        for (AntSystem.PHEROMONE_DECAY = 0.98; AntSystem.PHEROMONE_DECAY > 0.9; AntSystem.PHEROMONE_DECAY -= 0.02) {
                            firstScore = Double.MAX_VALUE;
                            secondScore = Double.MAX_VALUE;
                            for (int iterations = 0; iterations < 3; iterations++) {
                                {
                                    Assignment assignment = new Assignment();
                                    for (Operator o : t.getOperatorMap().values()) {
                                        o.setRequestedCpu(40d);
                                        o.setUsedCpu(USEDCPU);
                                        o.setRequestedMemory(10d);
                                        assignment.assign(o, problem.getSupervisorProcessMap().get("s10"));
                                    }
                                    Solver solver = new AntSystem(scoringFunction, constraints);
                                    solver.search(problem, t.getId(), assignment);
                                    firstScore = Math.min(firstScore, scoringFunction.score(problem, assignment, t.getId()));
                                    }


                                {
                                    Assignment assignment = new Assignment();
                                    int i = 10;
                                    for (Operator o : t.getOperatorMap().values()) {
                                        o.setRequestedCpu(40d);
                                        o.setUsedCpu(USEDCPU);
                                        o.setRequestedMemory(10d);
                                        assignment.assign(o, problem.getSupervisorProcessMap().get("s" + i++));
                                    }

                                    Solver solver = new AntSystem(scoringFunction, constraints);
                                    solver.search(problem, t.getId(), assignment);
                                    secondScore = Math.min(secondScore, scoringFunction.score(problem, assignment, t.getId()));
                                }
                            }

                            if (firstScore <= bestFirstScore && secondScore <= bestSecondScore) {
                                if ((firstScore < bestFirstScore || secondScore < bestSecondScore)) {
                                    bestFirstScore = firstScore;
                                    bestSecondScore = secondScore;
                                    distribution_bestInitialPheromone = new TreeMap<>();
                                    distribution_bestMinPheromone = new TreeMap<>();
                                    distribution_bestPheromoneDecay = new TreeMap<>();
                                    distribution_bestAntCount = new TreeMap<>();
                                    distribution_bestMaxIterationsWithoutImprovement = new TreeMap<>();
                                    distribution_bestPlacePheromoneConstant = new TreeMap<>();
                                }
                                distribution_bestInitialPheromone.put(AntSystem.INITIAL_PHEROMONE, distribution_bestInitialPheromone.getOrDefault(AntSystem.INITIAL_PHEROMONE, 0) + 1);
                                distribution_bestMinPheromone.put(AntSystem.MIN_PHEROMONE, distribution_bestMinPheromone.getOrDefault(AntSystem.MIN_PHEROMONE, 0) + 1);
                                distribution_bestPheromoneDecay.put(AntSystem.PHEROMONE_DECAY, distribution_bestPheromoneDecay.getOrDefault(AntSystem.PHEROMONE_DECAY, 0) + 1);
                                distribution_bestAntCount.put(AntSystem.ANT_COUNT, distribution_bestAntCount.getOrDefault(AntSystem.ANT_COUNT, 0) + 1);
                                distribution_bestMaxIterationsWithoutImprovement.put(AntSystem.MAX_ITERATIONS_WITHOUT_IMPROVEMENT, distribution_bestMaxIterationsWithoutImprovement.getOrDefault(AntSystem.MAX_ITERATIONS_WITHOUT_IMPROVEMENT, 0) + 1);
                                distribution_bestPlacePheromoneConstant.put(AntSystem.PLACE_PHEROMONE_CONSTANT, distribution_bestPlacePheromoneConstant.getOrDefault(AntSystem.PLACE_PHEROMONE_CONSTANT, 0) + 1);
                            }
                        }
                    }

                    }
                }
            }
        }


        System.out.println("best first Score" + bestFirstScore + " best second score:" + bestSecondScore);
        System.out.println("Place Pheromone distribution:");
        distribution_bestPlacePheromoneConstant.forEach((key, value) -> System.out.println(key + ":" + value));
        System.out.println("Ant Count distribution:");
        distribution_bestAntCount.forEach((key, value) -> System.out.println(key + ":" + value));
        System.out.println("Initial Pheromone distribution:");
        distribution_bestInitialPheromone.forEach((key, value) -> System.out.println(key + ":" + value));
        System.out.println("Pheromone Decay distribution:");
        distribution_bestPheromoneDecay.forEach((key, value) -> System.out.println(key + ":" + value));
        System.out.println("Min Pheromone distribution:");
        distribution_bestMinPheromone.forEach((key, value) -> System.out.println(key + ":" + value));
        System.out.println("Max Iterations Without Improvement distribution:");
        distribution_bestMaxIterationsWithoutImprovement.forEach((key, value) -> System.out.println(key + ":" + value));

    }

}