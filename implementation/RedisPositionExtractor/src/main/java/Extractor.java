import com.google.gson.Gson;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.stream.Collectors;

public class Extractor {
    private static boolean CONTAINERNET = true;
    private Jedis jedis;

    public Extractor() throws UnknownHostException {
        try {
            HostAndPort redis = new HostAndPort(CONTAINERNET ? "10.0.0.5" : "redis", 6379);
            System.out.println("attempt to connect to redis host:" + redis);
            jedis = new Jedis(redis);
            jedis.connect();
        } catch (Exception e) {
            CONTAINERNET = !CONTAINERNET;
            HostAndPort redis = new HostAndPort(CONTAINERNET ? "10.0.0.5" : "redis", 6379);
            System.out.println("attempt to connect to redis host:" + redis);
            jedis = new Jedis(redis);
            jedis.connect();
        }
    }

    public void run() {
        File outputDir=new File("/usr/local/lib/output");
        if (!outputDir.exists()) outputDir.mkdir();
        outputDir=new File("/usr/local/lib/output/history");
        if (!outputDir.exists()) outputDir.mkdir();

        long startTimestamp=System.currentTimeMillis();
        while (!Thread.interrupted()) {
            long timestamp=System.currentTimeMillis();
            long currentSecond=(timestamp-startTimestamp)/1000;
            File propFile=new File("/usr/local/lib/output/history/"+currentSecond+".prop");
            if (propFile.exists()){
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
                continue;
            }

            Set<String> hosts=jedis.keys("operator-*");
            if (hosts.isEmpty()){
                startTimestamp=timestamp;
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
                continue;
            }
            String[] keys= hosts.toArray(new String[0]);
            List<String> values=jedis.mget(keys);

            if (values.isEmpty()){
                startTimestamp=timestamp;
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
                continue;
            }
            Properties prop = new Properties();
            int index=0;
            for (final String json : values) {
                String keyString = keys[index];
                String hostIdentifier = keyString.substring("operator-".length());//probably ip, maybe still includes docker container id
                prop.put(hostIdentifier,json);
                index++;
            }
            try {
                FileOutputStream f = new FileOutputStream(propFile,false);
                prop.store(f, "Properties");
                f.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        exit();
    }




    private void exit() {
        jedis.close();
        System.out.println("exiting");
    }
}
