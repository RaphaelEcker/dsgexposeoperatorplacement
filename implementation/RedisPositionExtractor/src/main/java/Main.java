import redis.clients.jedis.exceptions.JedisConnectionException;

import java.net.UnknownHostException;

public class Main {
    public static void main(String[] args) throws UnknownHostException {
        System.out.println("starting");
        while (!Thread.interrupted()) {
            try {
                Extractor estimator = new Extractor();
                estimator.run();
            } catch (JedisConnectionException e) {
                //
            }
        }
        System.out.println("stopping");
    }
}
