#
# Build stage
#
FROM maven:3.6.0-jdk-11-slim AS build

COPY LatencyEstimation/pom.xml /home/app/pom.xml
#RUN mvn -f /home/app/pom.xml dependency:go-offline
RUN mvn -f /home/app/pom.xml verify --fail-never
COPY LatencyEstimation/src /home/app/src
RUN mvn -f /home/app/pom.xml clean package -DskipTests

#
# Package stage
#
FROM openjdk:11-jre-slim
RUN apt-get update && apt-get install -y \
        bash \
        iproute2 \
		iputils-ping \
		net-tools \
		netcat \
		dos2unix && \
		apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/*
COPY --from=build /home/app/target/LatencyEstimation-1.0-SNAPSHOT-jar-with-dependencies.jar /usr/local/lib/demo.jar
WORKDIR /usr/local/lib/
ENTRYPOINT ["java","-jar","/usr/local/lib/demo.jar"]