#!/bin/bash
#docker build -t customsupervisor .
docker build -t containernetsupervisor -f Containernet_supervisor.Dockerfile .
docker build -t containernetstorm -f Containernet_storm.Dockerfile .
docker build -t containernetzookeeper -f Containernet_zookeeper.Dockerfile .
docker build -t containernetredis -f Containernet_redis.Dockerfile .
docker build -t containernettestdriver -f Containernet_TestDriver.Dockerfile .
docker build -t containernetlatencyestimator -f Containernet_OnlyLatencyEstimator.Dockerfile .
docker build -t containernetredispositionextractor -f Containernet_RedisPositionExtractor.Dockerfile .