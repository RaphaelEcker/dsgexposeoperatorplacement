FROM zookeeper:3.5.9
RUN apt-get update && apt-get install -y \
        bash \
        iproute2 \
		iputils-ping \
		net-tools \
		netcat \
		dos2unix && \
		apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/*
COPY docker-entrypoints/zookeeper-docker/docker-entrypoint_Containernet_zookeeper.sh /docker-entrypoint.sh	
RUN dos2unix /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh