FROM storm:2.4.0
RUN apt-get update && apt-get install -y \
        bash \
        iproute2 \
		iputils-ping \
		net-tools \
		netcat \
		dos2unix && \
		apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/*
COPY docker-entrypoints/storm-docker/docker-entrypoint_Containernet_storm.sh /docker-entrypoint.sh
RUN dos2unix /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh
RUN set -eux; \
	chown -R storm:storm /docker-entrypoint.sh;
